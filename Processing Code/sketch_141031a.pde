import processing.serial.*;

Serial myPort;  // Create object from Serial class
String val;     // Data received from the serial port

int img_rows_alloc = 123;
int img_cols_alloc = 128;
//int[][] pval = new int[img_rows_alloc][img_cols_alloc]; // [rows][cols]
int[][] pval = new int[128][128]; // [rows][cols]


void setup()
{
  size(800, 400);
  background(204);
  
  //String portName = Serial.list()[3]; // 0 = COM1 (generally)
  //myPort = new Serial(this, portName, 9600);
  
  // List all the available serial ports
  println(Serial.list());
  
  myPort = new Serial(this, Serial.list()[0], 115200);

  for(int x = 0; x < 128; x++)
    for(int y = 0; y < img_rows_alloc; y++)
      pval[y][x] = ((5*x)+2*y)%250;


}


int num_buttons = 26;
int button1_x = 20; // top left corner
int button1_y = 40; // top left corner of button
int button_width = 20;
int button_height = 20;
int button_interval = 25;

String input;

int num_rows, num_cols;

int pixel;

void draw()
{
  if ( myPort.available() > 0) // If data is available,
  {  
    val = myPort.readStringUntil('\n');         // read it and store it in val
    if(val != null)
    {
      print(val); //print it out in the console

      input = trim(val);
    
      if(input.equals("pic") == true)
      {
        println("picture!!");
        
        // read num rows:
        //do
          input = myPort.readStringUntil(']');
        //while(input != null);
        //print("input(");
        //print(input.length());
        //print("):");
        //println(input);
                
        input = input.substring(1, input.length()-1);
        
        num_rows = int(input); 
        print("image num rows: ");
        println(num_rows);
        
        // read num columns:
        input = myPort.readStringUntil(']');
        
        //print("input(");
        //print(input.length());
        //print("):");
        //println(input);
        
        input = input.substring(1, input.length()-1);
        num_cols = int(input);  
        print("image num columns: ");
        println(num_cols);
        
        myPort.readStringUntil('\n');
        
        // begin reading pixel values
        // pixels come in one row at a time
        for(int j = 0; j < num_cols; j++)
        {
          for(int i = 0; i < num_rows; i++)
          {
            input = myPort.readStringUntil(',');
            if(input == null)
              i--; // bad read, don't count it
            else
            {
              input = input.substring(0, input.length()-1); // trim the comma
              pval[i][j] = int(input);
//              pval[4*j  ][i] = 4*int(input);
//              pval[4*j+1][i] = 4*int(input);
//              pval[4*j+2][i] = 4*int(input);
//              pval[4*j+3][i] = 4*int(input);
              
              //pval[j][i] = 10;
            }
          }
          myPort.readStringUntil('\n');

          print("row:");
          println(j);
          
          for(int i = 0; i < 128; i++)
          {
            print(pval[i][j]);
            print(",");
          }
          println();
          
        }
        
      }
  
    }
    // how to convert a collected string into an int value:
    //myString = trim(myString); //Trim any spaces from the sides of the string
    //myInt = int(myString); //Actually converting the string value into an integer
  }  


  fill(0xFF,0xFF,0xFF);
    
  for(int i = 0; i < num_buttons; i++)
  { 
    rect(button1_x + button_interval*i, button1_y, button_width, button_height);
  }

  //text("word", 10, 60);
  
  fill(0,0,0);
  for(int i = 0; i < num_buttons; i++)
  { 
    text(str(char(int('a')+i)), button1_x + 7 + button_interval*i, button1_y + 15);
  }
  
  fill(0xFF,0xFF,0xFF);
  rect(100,100,img_cols_alloc+1,img_rows_alloc+1); // we fill values (101 - 128) x (101 - 123)
  
  /*
  for(int i = 1; i <=100; i+=2)
    for(int j = 1; j <= 100; j+=2)
    {
      //fill(100+i,100+j,0xFF);
      point(100+i,100+j);  
      
    }
  */

  loadPixels();  
  // Loop through every pixel column
  for (int x = 0; x < 128; x++)
  {
    // Loop through every pixel row
    for (int y = 0; y < img_rows_alloc; y++)
    {
      // Use the formula to find the 1D location
      int loc = 101+x + ((101+y) * width);  // width = window width
      
      pixels[loc] = color(pval[y][x]); // color() with one paramater makes a grayscale RGB value
    }
  }
  updatePixels(); 


}


void mouseClicked()
{
  //delay(10);
  int x = mouseX;
  int y = mouseY;
  

  for(int i = 0; i < num_buttons; i++)
  {
    if((x >= button1_x + i*button_interval)&&(x < button1_x + i*button_interval + button_width))
      if((y >= button1_y)&&(y < button1_y + button_height))
      {
        myPort.write(char(int('a') + i));
        break;  
      } 
  }
}
