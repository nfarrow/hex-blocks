DO NOT MAKE CHANGES TO THESE FILES, THEY ARE FOR DOCUMENTATION OF THE FIRST (ONLY) BATCH OF BOARDS.

The final version sent to goldphoenixPCB was ver11.

However, after the quote came back, a small fix was made (ver12), and so ver12 is the actual version that was made in Nov 2012 (11/2/12).