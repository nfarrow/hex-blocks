<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.2">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="11" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Droplet">
<packages>
<package name="0402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<text x="1.0668" y="-0.2794" size="0.635" layer="21" ratio="15">&gt;NAME</text>
<wire x1="-0.9144" y1="0.5334" x2="0.9144" y2="0.5334" width="0.127" layer="21"/>
<wire x1="-0.9144" y1="-0.5334" x2="0.9144" y2="-0.5334" width="0.127" layer="21"/>
</package>
<package name="0603">
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<text x="1.4732" y="-0.3302" size="0.635" layer="21" ratio="15">&gt;NAME</text>
<wire x1="-1.2446" y1="0.635" x2="1.2446" y2="0.635" width="0.127" layer="21"/>
<wire x1="-1.2446" y1="-0.635" x2="1.2446" y2="-0.635" width="0.127" layer="21"/>
</package>
<package name="SOT-25">
<wire x1="-0.8" y1="1.45" x2="0.8" y2="1.45" width="0.127" layer="21"/>
<wire x1="0.8" y1="1.45" x2="0.8" y2="-1.45" width="0.127" layer="51"/>
<wire x1="0.8" y1="-1.45" x2="-0.8" y2="-1.45" width="0.127" layer="21"/>
<wire x1="-0.8" y1="-1.45" x2="-0.8" y2="1.45" width="0.127" layer="51"/>
<smd name="VIN" x="-1.22" y="0.97" dx="0.8" dy="0.55" layer="1"/>
<smd name="VSS" x="-1.22" y="0" dx="0.8" dy="0.55" layer="1"/>
<smd name="CE" x="-1.22" y="-0.97" dx="0.8" dy="0.55" layer="1"/>
<smd name="VOUT" x="1.22" y="0.97" dx="0.8" dy="0.55" layer="1"/>
<smd name="NC" x="1.22" y="-0.97" dx="0.8" dy="0.55" layer="1"/>
<wire x1="0.8" y1="0.5" x2="0.8" y2="-0.5" width="0.127" layer="21"/>
<text x="-1.27" y="1.524" size="0.635" layer="21" ratio="15">&gt;NAME</text>
</package>
<package name="SOT-23-6">
<wire x1="0" y1="-1.29" x2="0" y2="-1.3" width="0.01" layer="21"/>
<wire x1="1.42" y1="0.8" x2="1.42" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.42" y1="-0.8" x2="-1.42" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-1.42" y1="-0.8" x2="-1.42" y2="0.8" width="0.127" layer="21"/>
<wire x1="-1.42" y1="0.8" x2="1.42" y2="0.8" width="0.127" layer="51"/>
<smd name="1" x="-0.95" y="-1.29" dx="0.69" dy="0.99" layer="1"/>
<smd name="2" x="0" y="-1.29" dx="0.69" dy="0.99" layer="1"/>
<smd name="3" x="0.95" y="-1.29" dx="0.69" dy="0.99" layer="1"/>
<smd name="4" x="0.95" y="1.3" dx="0.69" dy="0.99" layer="1"/>
<smd name="6" x="-0.95" y="1.3" dx="0.69" dy="0.99" layer="1"/>
<smd name="5" x="0" y="1.3" dx="0.69" dy="0.99" layer="1"/>
<rectangle x1="-1.11" y1="0.8" x2="-0.78" y2="1.43" layer="51"/>
<rectangle x1="0.79" y1="0.8" x2="1.12" y2="1.42" layer="51"/>
<rectangle x1="-1.11" y1="-1.42" x2="-0.78" y2="-0.8" layer="51"/>
<rectangle x1="-0.16" y1="-1.42" x2="0.17" y2="-0.8" layer="51"/>
<rectangle x1="0.79" y1="-1.42" x2="1.12" y2="-0.8" layer="51"/>
<rectangle x1="-0.16" y1="0.8" x2="0.17" y2="1.42" layer="51"/>
<rectangle x1="-1.375" y1="-0.75" x2="-0.625" y2="0" layer="21"/>
<text x="-1.524" y="-1.016" size="0.635" layer="21" ratio="15" rot="R90">&gt;NAME</text>
</package>
<package name="RGB_LED">
<wire x1="1.6" y1="1.4" x2="1.6" y2="-1.4" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.4" x2="-1.6" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-1.4" x2="-1.6" y2="1.4" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.4" x2="1.6" y2="1.4" width="0.127" layer="21"/>
<circle x="-0.63" y="0.938" radius="0.1414" width="0.254" layer="21"/>
<smd name="4" x="1.55" y="0.75" dx="1.1" dy="0.9" layer="1"/>
<smd name="3" x="1.55" y="-0.75" dx="1.1" dy="0.9" layer="1"/>
<smd name="1" x="-1.55" y="0.75" dx="1.1" dy="0.9" layer="1"/>
<smd name="2" x="-1.55" y="-0.75" dx="1.1" dy="0.9" layer="1"/>
<text x="-1.524" y="1.524" size="0.635" layer="21" ratio="15">&gt;NAME</text>
</package>
<package name="MLF-64-L">
<wire x1="-4.05" y1="4.4" x2="4.05" y2="4.4" width="0.254" layer="51"/>
<wire x1="4.05" y1="4.4" x2="4.4" y2="4.05" width="0.254" layer="21"/>
<wire x1="4.4" y1="4.05" x2="4.4" y2="-4.05" width="0.254" layer="51"/>
<wire x1="4.4" y1="-4.05" x2="4.05" y2="-4.4" width="0.254" layer="21"/>
<wire x1="4.05" y1="-4.4" x2="-4.05" y2="-4.4" width="0.254" layer="51"/>
<wire x1="-4.05" y1="-4.4" x2="-4.4" y2="-4.05" width="0.254" layer="21"/>
<wire x1="-4.4" y1="-4.05" x2="-4.4" y2="4.05" width="0.254" layer="51"/>
<wire x1="-4.4" y1="4.05" x2="-4.05" y2="4.4" width="0.254" layer="21"/>
<circle x="-3.4" y="3.4" radius="0.2" width="0.254" layer="21"/>
<smd name="1" x="-4.425" y="3.75" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="2" x="-4.425" y="3.25" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="3" x="-4.425" y="2.75" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="4" x="-4.425" y="2.25" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="5" x="-4.425" y="1.75" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="6" x="-4.425" y="1.25" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="7" x="-4.425" y="0.75" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="8" x="-4.425" y="0.25" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="9" x="-4.425" y="-0.25" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="10" x="-4.425" y="-0.75" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="11" x="-4.425" y="-1.25" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="12" x="-4.425" y="-1.75" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="13" x="-4.425" y="-2.25" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="14" x="-4.425" y="-2.75" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="15" x="-4.425" y="-3.25" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="16" x="-4.425" y="-3.75" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="17" x="-3.75" y="-4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="18" x="-3.25" y="-4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="19" x="-2.75" y="-4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="20" x="-2.25" y="-4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="21" x="-1.75" y="-4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="22" x="-1.25" y="-4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="23" x="-0.75" y="-4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="24" x="-0.25" y="-4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="25" x="0.25" y="-4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="26" x="0.75" y="-4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="27" x="1.25" y="-4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="28" x="1.75" y="-4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="29" x="2.25" y="-4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="30" x="2.75" y="-4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="31" x="3.25" y="-4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="32" x="3.75" y="-4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="33" x="4.425" y="-3.75" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="34" x="4.425" y="-3.25" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="35" x="4.425" y="-2.75" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="36" x="4.425" y="-2.25" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="37" x="4.425" y="-1.75" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="38" x="4.425" y="-1.25" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="39" x="4.425" y="-0.75" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="40" x="4.425" y="-0.25" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="41" x="4.425" y="0.25" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="42" x="4.425" y="0.75" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="43" x="4.425" y="1.25" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="44" x="4.425" y="1.75" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="45" x="4.425" y="2.25" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="46" x="4.425" y="2.75" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="47" x="4.425" y="3.25" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="48" x="4.425" y="3.75" dx="0.9" dy="0.3" layer="1" roundness="50"/>
<smd name="49" x="3.75" y="4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="50" x="3.25" y="4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="51" x="2.75" y="4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="52" x="2.25" y="4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="53" x="1.75" y="4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="54" x="1.25" y="4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="55" x="0.75" y="4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="56" x="0.25" y="4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="57" x="-0.25" y="4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="58" x="-0.75" y="4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="59" x="-1.25" y="4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="60" x="-1.75" y="4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="61" x="-2.25" y="4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="62" x="-2.75" y="4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="63" x="-3.25" y="4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<smd name="64" x="-3.75" y="4.425" dx="0.3" dy="0.9" layer="1" roundness="50"/>
<rectangle x1="-4.5" y1="3.625" x2="-4" y2="3.875" layer="51"/>
<rectangle x1="-4.5" y1="3.125" x2="-4" y2="3.375" layer="51"/>
<rectangle x1="-4.5" y1="2.625" x2="-4" y2="2.875" layer="51"/>
<rectangle x1="-4.5" y1="2.125" x2="-4" y2="2.375" layer="51"/>
<rectangle x1="-4.5" y1="1.625" x2="-4" y2="1.875" layer="51"/>
<rectangle x1="-4.5" y1="1.125" x2="-4" y2="1.375" layer="51"/>
<rectangle x1="-4.5" y1="0.625" x2="-4" y2="0.875" layer="51"/>
<rectangle x1="-4.5" y1="0.125" x2="-4" y2="0.375" layer="51"/>
<rectangle x1="-4.5" y1="-0.375" x2="-4" y2="-0.125" layer="51"/>
<rectangle x1="-4.5" y1="-0.875" x2="-4" y2="-0.625" layer="51"/>
<rectangle x1="-4.5" y1="-1.375" x2="-4" y2="-1.125" layer="51"/>
<rectangle x1="-4.5" y1="-1.875" x2="-4" y2="-1.625" layer="51"/>
<rectangle x1="-4.5" y1="-2.375" x2="-4" y2="-2.125" layer="51"/>
<rectangle x1="-4.5" y1="-2.875" x2="-4" y2="-2.625" layer="51"/>
<rectangle x1="-4.5" y1="-3.375" x2="-4" y2="-3.125" layer="51"/>
<rectangle x1="-4.5" y1="-3.875" x2="-4" y2="-3.625" layer="51"/>
<rectangle x1="-3.875" y1="-4.5" x2="-3.625" y2="-4" layer="51"/>
<rectangle x1="-3.375" y1="-4.5" x2="-3.125" y2="-4" layer="51"/>
<rectangle x1="-2.875" y1="-4.5" x2="-2.625" y2="-4" layer="51"/>
<rectangle x1="-2.375" y1="-4.5" x2="-2.125" y2="-4" layer="51"/>
<rectangle x1="-1.875" y1="-4.5" x2="-1.625" y2="-4" layer="51"/>
<rectangle x1="-1.375" y1="-4.5" x2="-1.125" y2="-4" layer="51"/>
<rectangle x1="-0.875" y1="-4.5" x2="-0.625" y2="-4" layer="51"/>
<rectangle x1="-0.375" y1="-4.5" x2="-0.125" y2="-4" layer="51"/>
<rectangle x1="0.125" y1="-4.5" x2="0.375" y2="-4" layer="51"/>
<rectangle x1="0.625" y1="-4.5" x2="0.875" y2="-4" layer="51"/>
<rectangle x1="1.125" y1="-4.5" x2="1.375" y2="-4" layer="51"/>
<rectangle x1="1.625" y1="-4.5" x2="1.875" y2="-4" layer="51"/>
<rectangle x1="2.125" y1="-4.5" x2="2.375" y2="-4" layer="51"/>
<rectangle x1="2.625" y1="-4.5" x2="2.875" y2="-4" layer="51"/>
<rectangle x1="3.125" y1="-4.5" x2="3.375" y2="-4" layer="51"/>
<rectangle x1="3.625" y1="-4.5" x2="3.875" y2="-4" layer="51"/>
<rectangle x1="4" y1="-3.875" x2="4.5" y2="-3.625" layer="51"/>
<rectangle x1="4" y1="-3.375" x2="4.5" y2="-3.125" layer="51"/>
<rectangle x1="4" y1="-2.875" x2="4.5" y2="-2.625" layer="51"/>
<rectangle x1="4" y1="-2.375" x2="4.5" y2="-2.125" layer="51"/>
<rectangle x1="4" y1="-1.875" x2="4.5" y2="-1.625" layer="51"/>
<rectangle x1="4" y1="-1.375" x2="4.5" y2="-1.125" layer="51"/>
<rectangle x1="4" y1="-0.875" x2="4.5" y2="-0.625" layer="51"/>
<rectangle x1="4" y1="-0.375" x2="4.5" y2="-0.125" layer="51"/>
<rectangle x1="4" y1="0.125" x2="4.5" y2="0.375" layer="51"/>
<rectangle x1="4" y1="0.625" x2="4.5" y2="0.875" layer="51"/>
<rectangle x1="4" y1="1.125" x2="4.5" y2="1.375" layer="51"/>
<rectangle x1="4" y1="1.625" x2="4.5" y2="1.875" layer="51"/>
<rectangle x1="4" y1="2.125" x2="4.5" y2="2.375" layer="51"/>
<rectangle x1="4" y1="2.625" x2="4.5" y2="2.875" layer="51"/>
<rectangle x1="4" y1="3.125" x2="4.5" y2="3.375" layer="51"/>
<rectangle x1="4" y1="3.625" x2="4.5" y2="3.875" layer="51"/>
<rectangle x1="3.625" y1="4" x2="3.875" y2="4.5" layer="51"/>
<rectangle x1="3.125" y1="4" x2="3.375" y2="4.5" layer="51"/>
<rectangle x1="2.625" y1="4" x2="2.875" y2="4.5" layer="51"/>
<rectangle x1="2.125" y1="4" x2="2.375" y2="4.5" layer="51"/>
<rectangle x1="1.625" y1="4" x2="1.875" y2="4.5" layer="51"/>
<rectangle x1="1.125" y1="4" x2="1.375" y2="4.5" layer="51"/>
<rectangle x1="0.625" y1="4" x2="0.875" y2="4.5" layer="51"/>
<rectangle x1="0.125" y1="4" x2="0.375" y2="4.5" layer="51"/>
<rectangle x1="-0.375" y1="4" x2="-0.125" y2="4.5" layer="51"/>
<rectangle x1="-0.875" y1="4" x2="-0.625" y2="4.5" layer="51"/>
<rectangle x1="-1.375" y1="4" x2="-1.125" y2="4.5" layer="51"/>
<rectangle x1="-1.875" y1="4" x2="-1.625" y2="4.5" layer="51"/>
<rectangle x1="-2.375" y1="4" x2="-2.125" y2="4.5" layer="51"/>
<rectangle x1="-2.875" y1="4" x2="-2.625" y2="4.5" layer="51"/>
<rectangle x1="-3.375" y1="4" x2="-3.125" y2="4.5" layer="51"/>
<rectangle x1="-3.875" y1="4" x2="-3.625" y2="4.5" layer="51"/>
<text x="-3.81" y="5.08" size="0.635" layer="21" ratio="15">&gt;NAME</text>
</package>
<package name="0805">
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<text x="1.6256" y="-0.3556" size="0.635" layer="21" ratio="15">&gt;NAME</text>
<wire x1="-1.4224" y1="0.8382" x2="1.4224" y2="0.8382" width="0.127" layer="21"/>
<wire x1="-1.4224" y1="-0.8382" x2="1.4224" y2="-0.8382" width="0.127" layer="21"/>
</package>
<package name="1210">
<text x="2.3288" y="-0.254" size="0.635" layer="21" ratio="15">&gt;NAME</text>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-2.1" y1="-1.5" x2="2.1" y2="-1.5" width="0.127" layer="21"/>
<wire x1="2.1" y1="1.5" x2="-2.1" y2="1.5" width="0.127" layer="21"/>
</package>
<package name="SOT-363">
<wire x1="-1" y1="0.55" x2="1" y2="0.55" width="0.127" layer="51"/>
<wire x1="1" y1="0.55" x2="1" y2="-0.55" width="0.127" layer="21"/>
<wire x1="1" y1="-0.55" x2="-1" y2="-0.55" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.55" x2="-1" y2="0.55" width="0.127" layer="21"/>
<circle x="-0.7" y="-0.25" radius="0.15" width="0" layer="21"/>
<smd name="1" x="-0.65" y="-0.8" dx="0.4" dy="0.8" layer="1"/>
<smd name="2" x="0" y="-0.8" dx="0.4" dy="0.8" layer="1"/>
<smd name="3" x="0.65" y="-0.8" dx="0.4" dy="0.8" layer="1"/>
<smd name="4" x="0.65" y="0.8" dx="0.4" dy="0.8" layer="1"/>
<smd name="5" x="0" y="0.8" dx="0.4" dy="0.8" layer="1"/>
<smd name="6" x="-0.65" y="0.8" dx="0.4" dy="0.8" layer="1"/>
<rectangle x1="-0.8" y1="-1.1" x2="-0.5" y2="-0.6" layer="51"/>
<rectangle x1="-0.15" y1="-1.1" x2="0.15" y2="-0.6" layer="51"/>
<rectangle x1="0.5" y1="-1.1" x2="0.8" y2="-0.6" layer="51"/>
<rectangle x1="0.5" y1="0.6" x2="0.8" y2="1.1" layer="51"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="1.1" layer="51"/>
<rectangle x1="-0.8" y1="0.6" x2="-0.5" y2="1.1" layer="51"/>
<text x="-1.27" y="-1.27" size="0.635" layer="21" ratio="15" rot="R90">&gt;NAME</text>
</package>
<package name="MLF-64">
<wire x1="-4.05" y1="4.4" x2="4.05" y2="4.4" width="0.254" layer="51"/>
<wire x1="4.05" y1="4.4" x2="4.4" y2="4.05" width="0.254" layer="21"/>
<wire x1="4.4" y1="4.05" x2="4.4" y2="-4.05" width="0.254" layer="51"/>
<wire x1="4.4" y1="-4.05" x2="4.05" y2="-4.4" width="0.254" layer="21"/>
<wire x1="4.05" y1="-4.4" x2="-4.05" y2="-4.4" width="0.254" layer="51"/>
<wire x1="-4.05" y1="-4.4" x2="-4.4" y2="-4.05" width="0.254" layer="21"/>
<wire x1="-4.4" y1="-4.05" x2="-4.4" y2="4.05" width="0.254" layer="51"/>
<wire x1="-4.4" y1="4.05" x2="-4.05" y2="4.4" width="0.254" layer="21"/>
<circle x="-3.4" y="3.4" radius="0.2" width="0.254" layer="21"/>
<smd name="1" x="-4.325" y="3.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="2" x="-4.325" y="3.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="3" x="-4.325" y="2.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="4" x="-4.325" y="2.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="5" x="-4.325" y="1.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="6" x="-4.325" y="1.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="7" x="-4.325" y="0.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="8" x="-4.325" y="0.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="9" x="-4.325" y="-0.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="10" x="-4.325" y="-0.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="11" x="-4.325" y="-1.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="12" x="-4.325" y="-1.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="13" x="-4.325" y="-2.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="14" x="-4.325" y="-2.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="15" x="-4.325" y="-3.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="16" x="-4.325" y="-3.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="17" x="-3.75" y="-4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="18" x="-3.25" y="-4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="19" x="-2.75" y="-4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="20" x="-2.25" y="-4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="21" x="-1.75" y="-4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="22" x="-1.25" y="-4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="23" x="-0.75" y="-4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="24" x="-0.25" y="-4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="25" x="0.25" y="-4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="26" x="0.75" y="-4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="27" x="1.25" y="-4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="28" x="1.75" y="-4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="29" x="2.25" y="-4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="30" x="2.75" y="-4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="31" x="3.25" y="-4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="32" x="3.75" y="-4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="33" x="4.325" y="-3.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="34" x="4.325" y="-3.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="35" x="4.325" y="-2.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="36" x="4.325" y="-2.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="37" x="4.325" y="-1.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="38" x="4.325" y="-1.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="39" x="4.325" y="-0.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="40" x="4.325" y="-0.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="41" x="4.325" y="0.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="42" x="4.325" y="0.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="43" x="4.325" y="1.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="44" x="4.325" y="1.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="45" x="4.325" y="2.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="46" x="4.325" y="2.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="47" x="4.325" y="3.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="48" x="4.325" y="3.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="49" x="3.75" y="4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="50" x="3.25" y="4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="51" x="2.75" y="4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="52" x="2.25" y="4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="53" x="1.75" y="4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="54" x="1.25" y="4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="55" x="0.75" y="4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="56" x="0.25" y="4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="57" x="-0.25" y="4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="58" x="-0.75" y="4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="59" x="-1.25" y="4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="60" x="-1.75" y="4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="61" x="-2.25" y="4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="62" x="-2.75" y="4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="63" x="-3.25" y="4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="64" x="-3.75" y="4.325" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="-4.5" y1="3.625" x2="-4" y2="3.875" layer="51"/>
<rectangle x1="-4.5" y1="3.125" x2="-4" y2="3.375" layer="51"/>
<rectangle x1="-4.5" y1="2.625" x2="-4" y2="2.875" layer="51"/>
<rectangle x1="-4.5" y1="2.125" x2="-4" y2="2.375" layer="51"/>
<rectangle x1="-4.5" y1="1.625" x2="-4" y2="1.875" layer="51"/>
<rectangle x1="-4.5" y1="1.125" x2="-4" y2="1.375" layer="51"/>
<rectangle x1="-4.5" y1="0.625" x2="-4" y2="0.875" layer="51"/>
<rectangle x1="-4.5" y1="0.125" x2="-4" y2="0.375" layer="51"/>
<rectangle x1="-4.5" y1="-0.375" x2="-4" y2="-0.125" layer="51"/>
<rectangle x1="-4.5" y1="-0.875" x2="-4" y2="-0.625" layer="51"/>
<rectangle x1="-4.5" y1="-1.375" x2="-4" y2="-1.125" layer="51"/>
<rectangle x1="-4.5" y1="-1.875" x2="-4" y2="-1.625" layer="51"/>
<rectangle x1="-4.5" y1="-2.375" x2="-4" y2="-2.125" layer="51"/>
<rectangle x1="-4.5" y1="-2.875" x2="-4" y2="-2.625" layer="51"/>
<rectangle x1="-4.5" y1="-3.375" x2="-4" y2="-3.125" layer="51"/>
<rectangle x1="-4.5" y1="-3.875" x2="-4" y2="-3.625" layer="51"/>
<rectangle x1="-3.875" y1="-4.5" x2="-3.625" y2="-4" layer="51"/>
<rectangle x1="-3.375" y1="-4.5" x2="-3.125" y2="-4" layer="51"/>
<rectangle x1="-2.875" y1="-4.5" x2="-2.625" y2="-4" layer="51"/>
<rectangle x1="-2.375" y1="-4.5" x2="-2.125" y2="-4" layer="51"/>
<rectangle x1="-1.875" y1="-4.5" x2="-1.625" y2="-4" layer="51"/>
<rectangle x1="-1.375" y1="-4.5" x2="-1.125" y2="-4" layer="51"/>
<rectangle x1="-0.875" y1="-4.5" x2="-0.625" y2="-4" layer="51"/>
<rectangle x1="-0.375" y1="-4.5" x2="-0.125" y2="-4" layer="51"/>
<rectangle x1="0.125" y1="-4.5" x2="0.375" y2="-4" layer="51"/>
<rectangle x1="0.625" y1="-4.5" x2="0.875" y2="-4" layer="51"/>
<rectangle x1="1.125" y1="-4.5" x2="1.375" y2="-4" layer="51"/>
<rectangle x1="1.625" y1="-4.5" x2="1.875" y2="-4" layer="51"/>
<rectangle x1="2.125" y1="-4.5" x2="2.375" y2="-4" layer="51"/>
<rectangle x1="2.625" y1="-4.5" x2="2.875" y2="-4" layer="51"/>
<rectangle x1="3.125" y1="-4.5" x2="3.375" y2="-4" layer="51"/>
<rectangle x1="3.625" y1="-4.5" x2="3.875" y2="-4" layer="51"/>
<rectangle x1="4" y1="-3.875" x2="4.5" y2="-3.625" layer="51"/>
<rectangle x1="4" y1="-3.375" x2="4.5" y2="-3.125" layer="51"/>
<rectangle x1="4" y1="-2.875" x2="4.5" y2="-2.625" layer="51"/>
<rectangle x1="4" y1="-2.375" x2="4.5" y2="-2.125" layer="51"/>
<rectangle x1="4" y1="-1.875" x2="4.5" y2="-1.625" layer="51"/>
<rectangle x1="4" y1="-1.375" x2="4.5" y2="-1.125" layer="51"/>
<rectangle x1="4" y1="-0.875" x2="4.5" y2="-0.625" layer="51"/>
<rectangle x1="4" y1="-0.375" x2="4.5" y2="-0.125" layer="51"/>
<rectangle x1="4" y1="0.125" x2="4.5" y2="0.375" layer="51"/>
<rectangle x1="4" y1="0.625" x2="4.5" y2="0.875" layer="51"/>
<rectangle x1="4" y1="1.125" x2="4.5" y2="1.375" layer="51"/>
<rectangle x1="4" y1="1.625" x2="4.5" y2="1.875" layer="51"/>
<rectangle x1="4" y1="2.125" x2="4.5" y2="2.375" layer="51"/>
<rectangle x1="4" y1="2.625" x2="4.5" y2="2.875" layer="51"/>
<rectangle x1="4" y1="3.125" x2="4.5" y2="3.375" layer="51"/>
<rectangle x1="4" y1="3.625" x2="4.5" y2="3.875" layer="51"/>
<rectangle x1="3.625" y1="4" x2="3.875" y2="4.5" layer="51"/>
<rectangle x1="3.125" y1="4" x2="3.375" y2="4.5" layer="51"/>
<rectangle x1="2.625" y1="4" x2="2.875" y2="4.5" layer="51"/>
<rectangle x1="2.125" y1="4" x2="2.375" y2="4.5" layer="51"/>
<rectangle x1="1.625" y1="4" x2="1.875" y2="4.5" layer="51"/>
<rectangle x1="1.125" y1="4" x2="1.375" y2="4.5" layer="51"/>
<rectangle x1="0.625" y1="4" x2="0.875" y2="4.5" layer="51"/>
<rectangle x1="0.125" y1="4" x2="0.375" y2="4.5" layer="51"/>
<rectangle x1="-0.375" y1="4" x2="-0.125" y2="4.5" layer="51"/>
<rectangle x1="-0.875" y1="4" x2="-0.625" y2="4.5" layer="51"/>
<rectangle x1="-1.375" y1="4" x2="-1.125" y2="4.5" layer="51"/>
<rectangle x1="-1.875" y1="4" x2="-1.625" y2="4.5" layer="51"/>
<rectangle x1="-2.375" y1="4" x2="-2.125" y2="4.5" layer="51"/>
<rectangle x1="-2.875" y1="4" x2="-2.625" y2="4.5" layer="51"/>
<rectangle x1="-3.375" y1="4" x2="-3.125" y2="4.5" layer="51"/>
<rectangle x1="-3.875" y1="4" x2="-3.625" y2="4.5" layer="51"/>
<text x="-3.81" y="4.826" size="0.635" layer="21" ratio="15">&gt;NAME</text>
</package>
<package name="1206">
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="2.33" y="-0.3522" size="0.635" layer="21" ratio="15">&gt;NAME</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<wire x1="-2.1" y1="1.1" x2="2.1" y2="1.1" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.1" x2="2.1" y2="-1.1" width="0.127" layer="21"/>
</package>
<package name="DFN-10-EJ">
<wire x1="-1.37" y1="-1.39" x2="-1.37" y2="1.41" width="0.2032" layer="21"/>
<wire x1="-1.37" y1="1.41" x2="1.43" y2="1.41" width="0.2032" layer="51"/>
<wire x1="1.43" y1="1.41" x2="1.43" y2="-1.39" width="0.2032" layer="21"/>
<wire x1="1.43" y1="-1.39" x2="-1.37" y2="-1.39" width="0.2032" layer="51"/>
<circle x="-0.72" y="-0.765" radius="0.275" width="0" layer="21"/>
<smd name="1" x="-0.97" y="-1.54" dx="0.3" dy="0.85" layer="1" roundness="25" stop="no"/>
<smd name="2" x="-0.47" y="-1.54" dx="0.3" dy="0.85" layer="1" roundness="25" stop="no"/>
<smd name="3" x="0.03" y="-1.54" dx="0.3" dy="0.85" layer="1" roundness="25" stop="no"/>
<smd name="4" x="0.53" y="-1.54" dx="0.3" dy="0.85" layer="1" roundness="25" stop="no"/>
<smd name="5" x="1.03" y="-1.54" dx="0.3" dy="0.85" layer="1" roundness="25" stop="no"/>
<smd name="6" x="1.03" y="1.56" dx="0.3" dy="0.85" layer="1" roundness="25" stop="no"/>
<smd name="7" x="0.53" y="1.56" dx="0.3" dy="0.85" layer="1" roundness="25" stop="no"/>
<smd name="8" x="0.03" y="1.56" dx="0.3" dy="0.85" layer="1" roundness="25" stop="no"/>
<smd name="9" x="-0.47" y="1.56" dx="0.3" dy="0.85" layer="1" roundness="25" stop="no"/>
<smd name="10" x="-0.97" y="1.56" dx="0.3" dy="0.85" layer="1" roundness="25" stop="no"/>
<rectangle x1="-1.145" y1="-1.99" x2="-0.795" y2="-1.09" layer="29"/>
<rectangle x1="-0.645" y1="-1.99" x2="-0.295" y2="-1.09" layer="29"/>
<rectangle x1="-0.145" y1="-1.99" x2="0.205" y2="-1.09" layer="29"/>
<rectangle x1="0.355" y1="-1.99" x2="0.705" y2="-1.09" layer="29"/>
<rectangle x1="0.855" y1="-1.99" x2="1.205" y2="-1.09" layer="29"/>
<rectangle x1="0.855" y1="1.11" x2="1.205" y2="2.01" layer="29" rot="R180"/>
<rectangle x1="0.355" y1="1.11" x2="0.705" y2="2.01" layer="29" rot="R180"/>
<rectangle x1="-0.145" y1="1.11" x2="0.205" y2="2.01" layer="29" rot="R180"/>
<rectangle x1="-0.645" y1="1.11" x2="-0.295" y2="2.01" layer="29" rot="R180"/>
<rectangle x1="-1.145" y1="1.11" x2="-0.795" y2="2.01" layer="29" rot="R180"/>
<text x="-1.524" y="-1.27" size="0.635" layer="21" ratio="15" rot="R90">&gt;NAME</text>
</package>
<package name="S08">
<wire x1="-1.924" y1="2.41" x2="1.924" y2="2.41" width="0.127" layer="21"/>
<wire x1="1.924" y1="2.41" x2="1.924" y2="2.26" width="0.127" layer="21"/>
<wire x1="-1.924" y1="2.41" x2="-1.924" y2="2.26" width="0.127" layer="21"/>
<wire x1="1.924" y1="-2.41" x2="-1.924" y2="-2.41" width="0.127" layer="21"/>
<wire x1="-1.924" y1="-2.41" x2="-1.924" y2="-2.26" width="0.127" layer="21"/>
<wire x1="1.924" y1="-2.41" x2="1.924" y2="-2.26" width="0.127" layer="21"/>
<wire x1="-1.924" y1="2.41" x2="-1.924" y2="-2.41" width="0.127" layer="51"/>
<wire x1="1.924" y1="-2.41" x2="1.924" y2="2.41" width="0.127" layer="51"/>
<circle x="-1.258" y="1.78" radius="0.22" width="0.127" layer="21"/>
<smd name="1" x="-2.89" y="1.905" dx="0.5" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-2.89" y="0.635" dx="0.5" dy="1" layer="1" rot="R90"/>
<smd name="3" x="-2.89" y="-0.635" dx="0.5" dy="1" layer="1" rot="R90"/>
<smd name="4" x="-2.89" y="-1.905" dx="0.5" dy="1" layer="1" rot="R90"/>
<smd name="5" x="2.89" y="-1.905" dx="0.5" dy="1" layer="1" rot="R270"/>
<smd name="6" x="2.89" y="-0.635" dx="0.5" dy="1" layer="1" rot="R270"/>
<smd name="7" x="2.89" y="0.635" dx="0.5" dy="1" layer="1" rot="R270"/>
<smd name="8" x="2.89" y="1.905" dx="0.5" dy="1" layer="1" rot="R270"/>
<text x="-1.397" y="2.54" size="0.635" layer="21" ratio="15">&gt;NAME</text>
</package>
<package name="DTSM-6">
<smd name="P$1" x="-3.93" y="2.04" dx="2.1844" dy="1.0668" layer="1"/>
<smd name="P$2" x="-3.93" y="-2" dx="2.1844" dy="1.0668" layer="1"/>
<smd name="P$3" x="3.88" y="-2" dx="2.1844" dy="1.0668" layer="1"/>
<smd name="P$4" x="3.88" y="2.04" dx="2.1844" dy="1.0668" layer="1"/>
<text x="-2.7" y="3.31" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-3" y1="3" x2="-3" y2="-3" width="0.127" layer="21"/>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.127" layer="21"/>
<wire x1="3" y1="-3" x2="3" y2="3" width="0.127" layer="21"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.802775" width="0.127" layer="21"/>
</package>
<package name="16_PIN_LARGE">
<pad name="RESET" x="-7.62" y="8.89" drill="1.016" shape="square" rot="R180"/>
<pad name="AUDIO-L" x="-7.62" y="6.35" drill="1.016"/>
<pad name="NC$2" x="-7.62" y="3.81" drill="1.016"/>
<pad name="SPK+" x="-7.62" y="1.27" drill="1.016"/>
<pad name="SPK-" x="-7.62" y="-1.27" drill="1.016"/>
<pad name="NC$1" x="-7.62" y="-3.81" drill="1.016"/>
<pad name="P04" x="-7.62" y="-6.35" drill="1.016"/>
<pad name="GND" x="-7.62" y="-8.89" drill="1.016"/>
<pad name="P07" x="7.62" y="-8.89" drill="1.016"/>
<pad name="P05" x="7.62" y="-6.35" drill="1.016"/>
<pad name="NC$4" x="7.62" y="-3.81" drill="1.016"/>
<pad name="P03" x="7.62" y="-1.27" drill="1.016"/>
<pad name="P02" x="7.62" y="1.27" drill="1.016"/>
<pad name="NC$3" x="7.62" y="3.81" drill="1.016"/>
<pad name="P06" x="7.62" y="6.35" drill="1.016"/>
<pad name="VDD" x="7.62" y="8.89" drill="1.016"/>
<wire x1="-7.62" y1="10.16" x2="-1.27" y2="10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="10.16" x2="7.62" y2="10.16" width="0.127" layer="21"/>
<wire x1="1.27" y1="10.16" x2="-1.27" y2="10.16" width="0.127" layer="21" curve="-180"/>
<wire x1="-7.62" y1="-10.16" x2="7.62" y2="-10.16" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="XMEGA_A3">
<wire x1="30.48" y1="68.58" x2="-30.48" y2="68.58" width="0.254" layer="94"/>
<wire x1="-30.48" y1="68.58" x2="-30.48" y2="-68.58" width="0.254" layer="94"/>
<wire x1="-30.48" y1="-68.58" x2="30.48" y2="-68.58" width="0.254" layer="94"/>
<wire x1="30.48" y1="-68.58" x2="30.48" y2="68.58" width="0.254" layer="94"/>
<text x="-28.194" y="24.638" size="1.6764" layer="95">AGND</text>
<text x="-28.194" y="62.738" size="1.6764" layer="95">PDI_Data</text>
<text x="-28.194" y="65.278" size="1.6764" layer="95">/Reset_PDI_CLK</text>
<text x="-28.194" y="50.038" size="1.6764" layer="95">GND</text>
<text x="-28.194" y="47.498" size="1.6764" layer="95">GND</text>
<text x="-28.194" y="44.958" size="1.6764" layer="95">GND</text>
<text x="-28.194" y="42.418" size="1.6764" layer="95">GND</text>
<text x="-28.194" y="39.878" size="1.6764" layer="95">GND</text>
<text x="-28.194" y="29.718" size="1.6764" layer="95">AVCC</text>
<text x="-28.194" y="17.018" size="1.6764" layer="95">VCC</text>
<text x="-28.194" y="14.478" size="1.6764" layer="95">VCC</text>
<text x="-28.194" y="11.938" size="1.6764" layer="95">VCC</text>
<text x="-28.194" y="9.398" size="1.6764" layer="95">VCC</text>
<text x="-28.194" y="6.858" size="1.6764" layer="95">VCC</text>
<text x="28.194" y="66.802" size="1.6764" layer="95" rot="R180">ADC0_AC0_AREF_PA0</text>
<text x="28.194" y="64.262" size="1.6764" layer="95" rot="R180">ADC1_AC1_PA1</text>
<text x="28.194" y="61.722" size="1.6764" layer="95" rot="R180">ADC2_AC2_PA2</text>
<text x="28.194" y="59.182" size="1.6764" layer="95" rot="R180">ADC3_AC3_PA3</text>
<text x="28.194" y="56.642" size="1.6764" layer="95" rot="R180">ADC4_AC4_PA4</text>
<text x="28.194" y="54.102" size="1.6764" layer="95" rot="R180">ADC5_AC5_PA5</text>
<text x="28.194" y="51.562" size="1.6764" layer="95" rot="R180">ADC6_AC6_PA6</text>
<text x="28.194" y="49.022" size="1.6764" layer="95" rot="R180">ADC7_AC7_AC0OUT_PA7</text>
<text x="28.194" y="43.942" size="1.6764" layer="95" rot="R180">ADC0_AC0_AREF_PB0</text>
<text x="28.194" y="41.402" size="1.6764" layer="95" rot="R180">ADC1_AC1_PB1</text>
<text x="28.194" y="38.862" size="1.6764" layer="95" rot="R180">ADC2_AC2_DAC0_PB2</text>
<text x="28.194" y="36.322" size="1.6764" layer="95" rot="R180">ADC3_AC3_DAC1_PB3</text>
<text x="28.194" y="33.782" size="1.6764" layer="95" rot="R180">ADC4_AC4_TMS_PB4</text>
<text x="28.194" y="31.242" size="1.6764" layer="95" rot="R180">ADC5_AC5_TDI_PB5</text>
<text x="28.194" y="28.702" size="1.6764" layer="95" rot="R180">ADC6_AC6_TCK_PB6</text>
<text x="28.194" y="26.162" size="1.6764" layer="95" rot="R180">ADC7_AC7_AC0OUT_TDO_PB7</text>
<text x="28.194" y="21.082" size="1.6764" layer="95" rot="R180">OC0A_/OC0A_SDA_PC0</text>
<text x="28.194" y="18.542" size="1.6764" layer="95" rot="R180">OC0B_OC0A_XCK0_SCL_PC1</text>
<text x="28.194" y="16.002" size="1.6764" layer="95" rot="R180">OC0C_/OC0B_RXD0_PC2</text>
<text x="28.194" y="13.462" size="1.6764" layer="95" rot="R180">OC0D_OC0B_TXD0_PC3</text>
<text x="28.194" y="10.922" size="1.6764" layer="95" rot="R180">/OC0C_OC1A_/SS_PC4</text>
<text x="28.194" y="8.382" size="1.6764" layer="95" rot="R180">OC0C_OC1B_XCK1_MOSI_PC5</text>
<text x="28.194" y="5.842" size="1.6764" layer="95" rot="R180">/OC0D_RXD1_MISO_PC6</text>
<text x="28.194" y="3.302" size="1.6764" layer="95" rot="R180">OC0D_TXD1_SCK_CLKO_EVO_PC7</text>
<text x="28.194" y="-1.778" size="1.6764" layer="95" rot="R180">OC0A_PD0</text>
<text x="28.194" y="-4.318" size="1.6764" layer="95" rot="R180">OC0B_XCK0_PD1</text>
<text x="28.194" y="-6.858" size="1.6764" layer="95" rot="R180">OC0C_RXD0_PD2</text>
<text x="28.194" y="-9.398" size="1.6764" layer="95" rot="R180">OC0D_TXD0_PD3</text>
<text x="28.194" y="-11.938" size="1.6764" layer="95" rot="R180">OC1A_/SS_PD4</text>
<text x="28.194" y="-14.478" size="1.6764" layer="95" rot="R180">OC1B_XCK1_MOSI_PD5</text>
<text x="28.194" y="-17.018" size="1.6764" layer="95" rot="R180">RXD1_MISO_PD6</text>
<text x="28.194" y="-19.558" size="1.6764" layer="95" rot="R180">TXD1_SCK_CLKO_EVO_PD7</text>
<text x="28.194" y="-24.638" size="1.6764" layer="95" rot="R180">OC0A_SDA_PE0</text>
<text x="28.194" y="-27.178" size="1.6764" layer="95" rot="R180">OC0B_XCK0_SCL_PE1</text>
<text x="28.194" y="-29.718" size="1.6764" layer="95" rot="R180">OC0C_RXD0_PE2</text>
<text x="28.194" y="-32.258" size="1.6764" layer="95" rot="R180">OC0D_TXD0_PE3</text>
<text x="28.194" y="-34.798" size="1.6764" layer="95" rot="R180">OC1A_/SS_PE4</text>
<text x="28.194" y="-37.338" size="1.6764" layer="95" rot="R180">OC1B_XCK1_MOSI_PE5</text>
<text x="28.194" y="-39.878" size="1.6764" layer="95" rot="R180">RXD1_MISO_TOSC2_PE6</text>
<text x="28.194" y="-42.418" size="1.6764" layer="95" rot="R180">TXD1_SCK_CLKO_EVO_TOSC1_PE7</text>
<text x="28.194" y="-47.498" size="1.6764" layer="95" rot="R180">OC0A_PF0</text>
<text x="28.194" y="-50.038" size="1.6764" layer="95" rot="R180">OC0B_XCK0_PF1</text>
<text x="28.194" y="-52.578" size="1.6764" layer="95" rot="R180">OC0C_RXD0_PF2</text>
<text x="28.194" y="-55.118" size="1.6764" layer="95" rot="R180">OC0D_TXD0_PF3</text>
<text x="28.194" y="-57.658" size="1.6764" layer="95" rot="R180">PF4</text>
<text x="28.194" y="-60.198" size="1.6764" layer="95" rot="R180">PF5</text>
<text x="28.194" y="-62.738" size="1.6764" layer="95" rot="R180">PF6</text>
<text x="28.194" y="-65.278" size="1.6764" layer="95" rot="R180">PF7</text>
<text x="-28.194" y="-5.842" size="1.6764" layer="95">XTAL2_PR0</text>
<text x="-28.194" y="-8.382" size="1.6764" layer="95">XTAL1_PR1</text>
<text x="-30.48" y="69.85" size="1.6764" layer="95">&gt;Name</text>
<text x="-30.48" y="-71.12" size="1.6764" layer="96">&gt;Value</text>
<pin name="PA6" x="33.02" y="50.8" visible="pad" length="short" rot="R180"/>
<pin name="PA7" x="33.02" y="48.26" visible="pad" length="short" rot="R180"/>
<pin name="AGND" x="-33.02" y="25.4" visible="pad" length="short"/>
<pin name="PB0" x="33.02" y="43.18" visible="pad" length="short" rot="R180"/>
<pin name="PB1" x="33.02" y="40.64" visible="pad" length="short" rot="R180"/>
<pin name="PB2" x="33.02" y="38.1" visible="pad" length="short" rot="R180"/>
<pin name="PB3" x="33.02" y="35.56" visible="pad" length="short" rot="R180"/>
<pin name="PB4" x="33.02" y="33.02" visible="pad" length="short" rot="R180"/>
<pin name="PB5" x="33.02" y="30.48" visible="pad" length="short" rot="R180"/>
<pin name="PB6" x="33.02" y="27.94" visible="pad" length="short" rot="R180"/>
<pin name="PB7" x="33.02" y="25.4" visible="pad" length="short" rot="R180"/>
<pin name="GND1" x="-33.02" y="50.8" visible="pad" length="short"/>
<pin name="VCC1" x="-33.02" y="17.78" visible="pad" length="short"/>
<pin name="PC0" x="33.02" y="20.32" visible="pad" length="short" rot="R180"/>
<pin name="PC1" x="33.02" y="17.78" visible="pad" length="short" rot="R180"/>
<pin name="PC2" x="33.02" y="15.24" visible="pad" length="short" rot="R180"/>
<pin name="PC3" x="33.02" y="12.7" visible="pad" length="short" rot="R180"/>
<pin name="PC4" x="33.02" y="10.16" visible="pad" length="short" rot="R180"/>
<pin name="PC5" x="33.02" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="PC6" x="33.02" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="PC7" x="33.02" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="GND2" x="-33.02" y="48.26" visible="pad" length="short"/>
<pin name="VCC2" x="-33.02" y="15.24" visible="pad" length="short"/>
<pin name="PD0" x="33.02" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="PD1" x="33.02" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="PD2" x="33.02" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="PD3" x="33.02" y="-10.16" visible="pad" length="short" rot="R180"/>
<pin name="PD4" x="33.02" y="-12.7" visible="pad" length="short" rot="R180"/>
<pin name="PD5" x="33.02" y="-15.24" visible="pad" length="short" rot="R180"/>
<pin name="PD6" x="33.02" y="-17.78" visible="pad" length="short" rot="R180"/>
<pin name="PD7" x="33.02" y="-20.32" visible="pad" length="short" rot="R180"/>
<pin name="GND3" x="-33.02" y="45.72" visible="pad" length="short"/>
<pin name="VCC3" x="-33.02" y="12.7" visible="pad" length="short"/>
<pin name="PE0" x="33.02" y="-25.4" visible="pad" length="short" rot="R180"/>
<pin name="PE1" x="33.02" y="-27.94" visible="pad" length="short" rot="R180"/>
<pin name="PE2" x="33.02" y="-30.48" visible="pad" length="short" rot="R180"/>
<pin name="PE3" x="33.02" y="-33.02" visible="pad" length="short" rot="R180"/>
<pin name="PE4" x="33.02" y="-35.56" visible="pad" length="short" rot="R180"/>
<pin name="PE5" x="33.02" y="-38.1" visible="pad" length="short" rot="R180"/>
<pin name="PE6" x="33.02" y="-40.64" visible="pad" length="short" rot="R180"/>
<pin name="PE7" x="33.02" y="-43.18" visible="pad" length="short" rot="R180"/>
<pin name="GND4" x="-33.02" y="43.18" visible="pad" length="short"/>
<pin name="VCC4" x="-33.02" y="10.16" visible="pad" length="short"/>
<pin name="PF0" x="33.02" y="-48.26" visible="pad" length="short" rot="R180"/>
<pin name="PF1" x="33.02" y="-50.8" visible="pad" length="short" rot="R180"/>
<pin name="PF2" x="33.02" y="-53.34" visible="pad" length="short" rot="R180"/>
<pin name="PF3" x="33.02" y="-55.88" visible="pad" length="short" rot="R180"/>
<pin name="PF4" x="33.02" y="-58.42" visible="pad" length="short" rot="R180"/>
<pin name="PF5" x="33.02" y="-60.96" visible="pad" length="short" rot="R180"/>
<pin name="PF6" x="33.02" y="-63.5" visible="pad" length="short" rot="R180"/>
<pin name="PF7" x="33.02" y="-66.04" visible="pad" length="short" rot="R180"/>
<pin name="GND5" x="-33.02" y="40.64" visible="pad" length="short"/>
<pin name="VCC5" x="-33.02" y="7.62" visible="pad" length="short"/>
<pin name="PDI_DATA" x="-33.02" y="63.5" visible="pad" length="short"/>
<pin name="RESET" x="-33.02" y="66.04" visible="pad" length="short"/>
<pin name="PR0" x="-33.02" y="-5.08" visible="pad" length="short"/>
<pin name="PR1" x="-33.02" y="-7.62" visible="pad" length="short"/>
<pin name="AVCC" x="-33.02" y="30.48" visible="pad" length="short"/>
<pin name="PA0" x="33.02" y="66.04" visible="pad" length="short" rot="R180"/>
<pin name="PA1" x="33.02" y="63.5" visible="pad" length="short" rot="R180"/>
<pin name="PA2" x="33.02" y="60.96" visible="pad" length="short" rot="R180"/>
<pin name="PA3" x="33.02" y="58.42" visible="pad" length="short" rot="R180"/>
<pin name="PA4" x="33.02" y="55.88" visible="pad" length="short" rot="R180"/>
<pin name="PA5" x="33.02" y="53.34" visible="pad" length="short" rot="R180"/>
</symbol>
<symbol name="CAPACITOR">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="REGULATOR">
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<pin name="VIN" x="-10.16" y="5.08" visible="pin" length="short"/>
<pin name="VSS" x="-10.16" y="-5.08" visible="pin" length="short"/>
<pin name="CE" x="-10.16" y="0" visible="pin" length="short"/>
<pin name="VOUT" x="10.16" y="5.08" visible="pin" length="short" rot="R180"/>
<text x="-7.62" y="8.1026" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-9.9314" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="INDUCTOR">
<wire x1="-5.08" y1="0" x2="-3.81" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-2.54" y1="0" x2="-3.81" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="2.54" y1="0" x2="3.81" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="5.08" y1="0" x2="3.81" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<text x="2.54" y="3.81" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="5.08" y="-1.27" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<pin name="2" x="7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="REGULATOR_5V">
<pin name="EN" x="-10.16" y="2.54" length="short"/>
<pin name="VIN" x="-10.16" y="7.62" length="short"/>
<pin name="GND" x="-10.16" y="-7.62" length="short"/>
<pin name="SW" x="-10.16" y="-2.54" length="short"/>
<pin name="VOUT" x="10.16" y="7.62" length="short" rot="R180"/>
<pin name="VFB" x="10.16" y="-7.62" length="short" rot="R180"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<text x="-7.62" y="11.43" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="RGB_LED">
<wire x1="0" y1="1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="8.89" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="6.35" width="0.254" layer="94"/>
<wire x1="0" y1="6.35" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="0" y2="8.89" width="0.254" layer="94"/>
<wire x1="2.54" y1="8.89" x2="2.54" y2="6.35" width="0.254" layer="94"/>
<wire x1="0" y1="-6.35" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="0" y2="-8.89" width="0.254" layer="94"/>
<wire x1="0" y1="-8.89" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="0" y2="-6.35" width="0.254" layer="94"/>
<wire x1="2.54" y1="-6.35" x2="2.54" y2="-8.89" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="0" y2="-7.62" width="0.254" layer="94"/>
<pin name="ANODE" x="-7.62" y="0" visible="off" length="middle"/>
<pin name="R" x="7.62" y="7.62" visible="off" length="middle" rot="R180"/>
<pin name="G" x="7.62" y="0" visible="off" length="middle" rot="R180"/>
<pin name="B" x="7.62" y="-7.62" visible="off" length="middle" rot="R180"/>
<wire x1="2.794" y1="9.652" x2="4.191" y2="11.049" width="0.1524" layer="94"/>
<wire x1="2.667" y1="10.795" x2="4.064" y2="12.192" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="4.191" y="11.049"/>
<vertex x="3.81" y="10.16"/>
<vertex x="3.302" y="10.668"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="4.064" y="12.192"/>
<vertex x="3.683" y="11.303"/>
<vertex x="3.175" y="11.811"/>
</polygon>
<text x="4.572" y="9.398" size="1.27" layer="94">R</text>
<text x="4.572" y="1.778" size="1.27" layer="94">G</text>
<text x="4.572" y="-5.842" size="1.27" layer="94">B</text>
<wire x1="2.794" y1="2.032" x2="4.191" y2="3.429" width="0.1524" layer="94"/>
<wire x1="2.667" y1="3.175" x2="4.064" y2="4.572" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="4.191" y="3.429"/>
<vertex x="3.81" y="2.54"/>
<vertex x="3.302" y="3.048"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="4.064" y="4.572"/>
<vertex x="3.683" y="3.683"/>
<vertex x="3.175" y="4.191"/>
</polygon>
<wire x1="2.794" y1="-5.588" x2="4.191" y2="-4.191" width="0.1524" layer="94"/>
<wire x1="2.667" y1="-4.445" x2="4.064" y2="-3.048" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="4.191" y="-4.191"/>
<vertex x="3.81" y="-5.08"/>
<vertex x="3.302" y="-4.572"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="4.064" y="-3.048"/>
<vertex x="3.683" y="-3.937"/>
<vertex x="3.175" y="-3.429"/>
</polygon>
</symbol>
<symbol name="N-CHANNEL_MOSFET">
<wire x1="0.762" y1="0.762" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.175" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="1.905" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.905" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="-3.175" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.762" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="2.54" x2="3.81" y2="2.54" width="0.1524" layer="94"/>
<wire x1="4.572" y1="0.762" x2="4.318" y2="0.508" width="0.1524" layer="94"/>
<wire x1="4.318" y1="0.508" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.302" y1="0.508" x2="3.048" y2="0.254" width="0.1524" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.3592" width="0" layer="94"/>
<circle x="2.54" y="2.54" radius="0.3592" width="0" layer="94"/>
<text x="6.35" y="0" size="1.778" layer="95">&gt;NAME</text>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="G" x="-2.54" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="3.81" y="0.508"/>
<vertex x="3.302" y="-0.254"/>
<vertex x="4.318" y="-0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="1.016" y="0"/>
<vertex x="2.032" y="0.762"/>
<vertex x="2.032" y="-0.762"/>
</polygon>
</symbol>
<symbol name="ACCELEROMETER">
<wire x1="-7.62" y1="17.78" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="27.94" y2="-10.16" width="0.254" layer="94"/>
<wire x1="27.94" y1="-10.16" x2="27.94" y2="17.78" width="0.254" layer="94"/>
<wire x1="27.94" y1="17.78" x2="-7.62" y2="17.78" width="0.254" layer="94"/>
<pin name="RESERVED" x="-12.7" y="12.7" length="middle"/>
<pin name="N/C" x="-12.7" y="7.62" length="middle"/>
<pin name="AVDD" x="-12.7" y="2.54" length="middle"/>
<pin name="AVSS" x="-12.7" y="-2.54" length="middle"/>
<pin name="!INT" x="-12.7" y="-7.62" length="middle"/>
<pin name="RESERVADO" x="33.02" y="12.7" length="middle" rot="R180"/>
<pin name="DVDD" x="33.02" y="7.62" length="middle" rot="R180"/>
<pin name="DVSS" x="33.02" y="2.54" length="middle" rot="R180"/>
<pin name="SDA" x="33.02" y="-2.54" length="middle" rot="R180"/>
<pin name="SCL" x="33.02" y="-7.62" length="middle" rot="R180"/>
</symbol>
<symbol name="I2C_MEMORY">
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<pin name="N/C" x="-15.24" y="5.08" length="middle"/>
<pin name="E1" x="-15.24" y="2.54" length="middle"/>
<pin name="E2" x="-15.24" y="-2.54" length="middle"/>
<pin name="VSS" x="-15.24" y="-5.08" length="middle"/>
<pin name="VCC" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="!WC" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="SCL" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="SDA" x="15.24" y="-5.08" length="middle" rot="R180"/>
</symbol>
<symbol name="SMD_PUSHBUTTON">
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<pin name="P1" x="0" y="5.08" visible="off" length="short" rot="R270"/>
<pin name="P2" x="0" y="-5.08" visible="off" length="short" rot="R90"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.032" y1="2.032" x2="-2.032" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="0" x2="-2.032" y2="-2.032" width="0.254" layer="94"/>
<wire x1="-2.032" y1="0" x2="-2.794" y2="0" width="0.254" layer="94"/>
<wire x1="-2.794" y1="0.762" x2="-2.794" y2="-0.762" width="0.254" layer="94"/>
</symbol>
<symbol name="MP3_MODULE">
<wire x1="-7.62" y1="12.7" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="7.62" y2="12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="12.7" x2="-7.62" y2="12.7" width="0.254" layer="94"/>
<pin name="RESET" x="-12.7" y="10.16" length="middle"/>
<pin name="P02" x="-12.7" y="7.62" length="middle"/>
<pin name="P03" x="-12.7" y="5.08" length="middle"/>
<pin name="P04" x="-12.7" y="2.54" length="middle"/>
<pin name="P05" x="-12.7" y="0" length="middle"/>
<pin name="P06" x="-12.7" y="-2.54" length="middle"/>
<pin name="P07" x="-12.7" y="-5.08" length="middle"/>
<pin name="VDD" x="12.7" y="7.62" length="middle" rot="R180"/>
<pin name="AUDIO-L" x="12.7" y="-7.62" length="middle" rot="R180"/>
<pin name="SPK-" x="12.7" y="-5.08" length="middle" rot="R180"/>
<pin name="SPK+" x="12.7" y="-2.54" length="middle" rot="R180"/>
<pin name="GND" x="12.7" y="5.08" length="middle" rot="R180"/>
<text x="7.62" y="15.24" size="1.4224" layer="95" rot="R180">WTV020-SD-16P</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="XMEGA_A3">
<gates>
<gate name="G$1" symbol="XMEGA_A3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MLF-64">
<connects>
<connect gate="G$1" pin="AGND" pad="60"/>
<connect gate="G$1" pin="AVCC" pad="61"/>
<connect gate="G$1" pin="GND1" pad="14"/>
<connect gate="G$1" pin="GND2" pad="24"/>
<connect gate="G$1" pin="GND3" pad="34"/>
<connect gate="G$1" pin="GND4" pad="44"/>
<connect gate="G$1" pin="GND5" pad="52"/>
<connect gate="G$1" pin="PA0" pad="62"/>
<connect gate="G$1" pin="PA1" pad="63"/>
<connect gate="G$1" pin="PA2" pad="64"/>
<connect gate="G$1" pin="PA3" pad="1"/>
<connect gate="G$1" pin="PA4" pad="2"/>
<connect gate="G$1" pin="PA5" pad="3"/>
<connect gate="G$1" pin="PA6" pad="4"/>
<connect gate="G$1" pin="PA7" pad="5"/>
<connect gate="G$1" pin="PB0" pad="6"/>
<connect gate="G$1" pin="PB1" pad="7"/>
<connect gate="G$1" pin="PB2" pad="8"/>
<connect gate="G$1" pin="PB3" pad="9"/>
<connect gate="G$1" pin="PB4" pad="10"/>
<connect gate="G$1" pin="PB5" pad="11"/>
<connect gate="G$1" pin="PB6" pad="12"/>
<connect gate="G$1" pin="PB7" pad="13"/>
<connect gate="G$1" pin="PC0" pad="16"/>
<connect gate="G$1" pin="PC1" pad="17"/>
<connect gate="G$1" pin="PC2" pad="18"/>
<connect gate="G$1" pin="PC3" pad="19"/>
<connect gate="G$1" pin="PC4" pad="20"/>
<connect gate="G$1" pin="PC5" pad="21"/>
<connect gate="G$1" pin="PC6" pad="22"/>
<connect gate="G$1" pin="PC7" pad="23"/>
<connect gate="G$1" pin="PD0" pad="26"/>
<connect gate="G$1" pin="PD1" pad="27"/>
<connect gate="G$1" pin="PD2" pad="28"/>
<connect gate="G$1" pin="PD3" pad="29"/>
<connect gate="G$1" pin="PD4" pad="30"/>
<connect gate="G$1" pin="PD5" pad="31"/>
<connect gate="G$1" pin="PD6" pad="32"/>
<connect gate="G$1" pin="PD7" pad="33"/>
<connect gate="G$1" pin="PDI_DATA" pad="56"/>
<connect gate="G$1" pin="PE0" pad="36"/>
<connect gate="G$1" pin="PE1" pad="37"/>
<connect gate="G$1" pin="PE2" pad="38"/>
<connect gate="G$1" pin="PE3" pad="39"/>
<connect gate="G$1" pin="PE4" pad="40"/>
<connect gate="G$1" pin="PE5" pad="41"/>
<connect gate="G$1" pin="PE6" pad="42"/>
<connect gate="G$1" pin="PE7" pad="43"/>
<connect gate="G$1" pin="PF0" pad="46"/>
<connect gate="G$1" pin="PF1" pad="47"/>
<connect gate="G$1" pin="PF2" pad="48"/>
<connect gate="G$1" pin="PF3" pad="49"/>
<connect gate="G$1" pin="PF4" pad="50"/>
<connect gate="G$1" pin="PF5" pad="51"/>
<connect gate="G$1" pin="PF6" pad="54"/>
<connect gate="G$1" pin="PF7" pad="55"/>
<connect gate="G$1" pin="PR0" pad="58"/>
<connect gate="G$1" pin="PR1" pad="59"/>
<connect gate="G$1" pin="RESET" pad="57"/>
<connect gate="G$1" pin="VCC1" pad="15"/>
<connect gate="G$1" pin="VCC2" pad="25"/>
<connect gate="G$1" pin="VCC3" pad="35"/>
<connect gate="G$1" pin="VCC4" pad="45"/>
<connect gate="G$1" pin="VCC5" pad="53"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L" package="MLF-64-L">
<connects>
<connect gate="G$1" pin="AGND" pad="60"/>
<connect gate="G$1" pin="AVCC" pad="61"/>
<connect gate="G$1" pin="GND1" pad="14"/>
<connect gate="G$1" pin="GND2" pad="24"/>
<connect gate="G$1" pin="GND3" pad="34"/>
<connect gate="G$1" pin="GND4" pad="44"/>
<connect gate="G$1" pin="GND5" pad="52"/>
<connect gate="G$1" pin="PA0" pad="62"/>
<connect gate="G$1" pin="PA1" pad="63"/>
<connect gate="G$1" pin="PA2" pad="64"/>
<connect gate="G$1" pin="PA3" pad="1"/>
<connect gate="G$1" pin="PA4" pad="2"/>
<connect gate="G$1" pin="PA5" pad="3"/>
<connect gate="G$1" pin="PA6" pad="4"/>
<connect gate="G$1" pin="PA7" pad="5"/>
<connect gate="G$1" pin="PB0" pad="6"/>
<connect gate="G$1" pin="PB1" pad="7"/>
<connect gate="G$1" pin="PB2" pad="8"/>
<connect gate="G$1" pin="PB3" pad="9"/>
<connect gate="G$1" pin="PB4" pad="10"/>
<connect gate="G$1" pin="PB5" pad="11"/>
<connect gate="G$1" pin="PB6" pad="12"/>
<connect gate="G$1" pin="PB7" pad="13"/>
<connect gate="G$1" pin="PC0" pad="16"/>
<connect gate="G$1" pin="PC1" pad="17"/>
<connect gate="G$1" pin="PC2" pad="18"/>
<connect gate="G$1" pin="PC3" pad="19"/>
<connect gate="G$1" pin="PC4" pad="20"/>
<connect gate="G$1" pin="PC5" pad="21"/>
<connect gate="G$1" pin="PC6" pad="22"/>
<connect gate="G$1" pin="PC7" pad="23"/>
<connect gate="G$1" pin="PD0" pad="26"/>
<connect gate="G$1" pin="PD1" pad="27"/>
<connect gate="G$1" pin="PD2" pad="28"/>
<connect gate="G$1" pin="PD3" pad="29"/>
<connect gate="G$1" pin="PD4" pad="30"/>
<connect gate="G$1" pin="PD5" pad="31"/>
<connect gate="G$1" pin="PD6" pad="32"/>
<connect gate="G$1" pin="PD7" pad="33"/>
<connect gate="G$1" pin="PDI_DATA" pad="56"/>
<connect gate="G$1" pin="PE0" pad="36"/>
<connect gate="G$1" pin="PE1" pad="37"/>
<connect gate="G$1" pin="PE2" pad="38"/>
<connect gate="G$1" pin="PE3" pad="39"/>
<connect gate="G$1" pin="PE4" pad="40"/>
<connect gate="G$1" pin="PE5" pad="41"/>
<connect gate="G$1" pin="PE6" pad="42"/>
<connect gate="G$1" pin="PE7" pad="43"/>
<connect gate="G$1" pin="PF0" pad="46"/>
<connect gate="G$1" pin="PF1" pad="47"/>
<connect gate="G$1" pin="PF2" pad="48"/>
<connect gate="G$1" pin="PF3" pad="49"/>
<connect gate="G$1" pin="PF4" pad="50"/>
<connect gate="G$1" pin="PF5" pad="51"/>
<connect gate="G$1" pin="PF6" pad="54"/>
<connect gate="G$1" pin="PF7" pad="55"/>
<connect gate="G$1" pin="PR0" pad="58"/>
<connect gate="G$1" pin="PR1" pad="59"/>
<connect gate="G$1" pin="RESET" pad="57"/>
<connect gate="G$1" pin="VCC1" pad="15"/>
<connect gate="G$1" pin="VCC2" pad="25"/>
<connect gate="G$1" pin="VCC3" pad="35"/>
<connect gate="G$1" pin="VCC4" pad="45"/>
<connect gate="G$1" pin="VCC5" pad="53"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR">
<gates>
<gate name="C1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="0402">
<connects>
<connect gate="C1" pin="1" pad="1"/>
<connect gate="C1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="C1" pin="1" pad="1"/>
<connect gate="C1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="C1" pin="1" pad="1"/>
<connect gate="C1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="1206">
<connects>
<connect gate="C1" pin="1" pad="1"/>
<connect gate="C1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="REGULATOR">
<gates>
<gate name="G$1" symbol="REGULATOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-25">
<connects>
<connect gate="G$1" pin="CE" pad="CE"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
<connect gate="G$1" pin="VOUT" pad="VOUT"/>
<connect gate="G$1" pin="VSS" pad="VSS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="INDUCTOR">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="REGULATOR_5V">
<gates>
<gate name="G$1" symbol="REGULATOR_5V" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="SOT-23-6">
<connects>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="SW" pad="1"/>
<connect gate="G$1" pin="VFB" pad="4"/>
<connect gate="G$1" pin="VIN" pad="6"/>
<connect gate="G$1" pin="VOUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RGB_LED">
<gates>
<gate name="G$1" symbol="RGB_LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RGB_LED">
<connects>
<connect gate="G$1" pin="ANODE" pad="2"/>
<connect gate="G$1" pin="B" pad="3"/>
<connect gate="G$1" pin="G" pad="4"/>
<connect gate="G$1" pin="R" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET-N_X2">
<gates>
<gate name="G$1" symbol="N-CHANNEL_MOSFET" x="-2.54" y="10.16"/>
<gate name="G$2" symbol="N-CHANNEL_MOSFET" x="-2.54" y="-10.16"/>
</gates>
<devices>
<device name="" package="SOT-363">
<connects>
<connect gate="G$1" pin="D" pad="6"/>
<connect gate="G$1" pin="G" pad="2"/>
<connect gate="G$1" pin="S" pad="1"/>
<connect gate="G$2" pin="D" pad="3"/>
<connect gate="G$2" pin="G" pad="5"/>
<connect gate="G$2" pin="S" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ACCELEROMETER">
<gates>
<gate name="G$1" symbol="ACCELEROMETER" x="-10.16" y="-2.54"/>
</gates>
<devices>
<device name="" package="DFN-10-EJ">
<connects>
<connect gate="G$1" pin="!INT" pad="5"/>
<connect gate="G$1" pin="AVDD" pad="3"/>
<connect gate="G$1" pin="AVSS" pad="4"/>
<connect gate="G$1" pin="DVDD" pad="9"/>
<connect gate="G$1" pin="DVSS" pad="8"/>
<connect gate="G$1" pin="N/C" pad="2"/>
<connect gate="G$1" pin="RESERVADO" pad="10"/>
<connect gate="G$1" pin="RESERVED" pad="1"/>
<connect gate="G$1" pin="SCL" pad="6"/>
<connect gate="G$1" pin="SDA" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="I2C_MEMORY">
<gates>
<gate name="G$1" symbol="I2C_MEMORY" x="0" y="0"/>
</gates>
<devices>
<device name="" package="S08">
<connects>
<connect gate="G$1" pin="!WC" pad="7"/>
<connect gate="G$1" pin="E1" pad="2"/>
<connect gate="G$1" pin="E2" pad="3"/>
<connect gate="G$1" pin="N/C" pad="1"/>
<connect gate="G$1" pin="SCL" pad="6"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="VSS" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMD_SWITCH">
<gates>
<gate name="G$1" symbol="SMD_PUSHBUTTON" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DTSM-6">
<connects>
<connect gate="G$1" pin="P1" pad="P$1 P$2"/>
<connect gate="G$1" pin="P2" pad="P$3 P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MP3_MODULE_DEVICE">
<gates>
<gate name="G$1" symbol="MP3_MODULE" x="35.56" y="2.54"/>
</gates>
<devices>
<device name="" package="16_PIN_LARGE">
<connects>
<connect gate="G$1" pin="AUDIO-L" pad="AUDIO-L"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="P02" pad="P02"/>
<connect gate="G$1" pin="P03" pad="P03"/>
<connect gate="G$1" pin="P04" pad="P04"/>
<connect gate="G$1" pin="P05" pad="P05"/>
<connect gate="G$1" pin="P06" pad="P06"/>
<connect gate="G$1" pin="P07" pad="P07"/>
<connect gate="G$1" pin="RESET" pad="RESET"/>
<connect gate="G$1" pin="SPK+" pad="SPK+"/>
<connect gate="G$1" pin="SPK-" pad="SPK-"/>
<connect gate="G$1" pin="VDD" pad="VDD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="linear">
<description>&lt;b&gt;Linear Devices&lt;/b&gt;&lt;p&gt;
Operational amplifiers,  comparators, voltage regulators, ADCs, DACs, etc.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL08">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="5.08" y1="2.921" x2="-5.08" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.921" x2="5.08" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="5.08" y1="2.921" x2="5.08" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="2.921" x2="-5.08" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.921" x2="-5.08" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.016" x2="-5.08" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-3.81" y="-3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="2" x="-1.27" y="-3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="7" x="-1.27" y="3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="8" x="-3.81" y="3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="3" x="1.27" y="-3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="4" x="3.81" y="-3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="6" x="1.27" y="3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="5" x="3.81" y="3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<text x="-5.334" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-3.556" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SO08">
<description>&lt;b&gt;Small Outline Package 8&lt;/b&gt;&lt;br&gt;
NS Package M08A</description>
<wire x1="2.4" y1="1.9" x2="2.4" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.4" x2="2.4" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.9" x2="-2.4" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.9" x2="-2.4" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.4" x2="-2.4" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="1.9" x2="2.4" y2="1.9" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.4" x2="-2.4" y2="-1.4" width="0.2032" layer="51"/>
<smd name="2" x="-0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="-0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="-1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="5" x="1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<text x="-2.667" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.937" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.15" y1="-3.1" x2="-1.66" y2="-2" layer="51"/>
<rectangle x1="-0.88" y1="-3.1" x2="-0.39" y2="-2" layer="51"/>
<rectangle x1="0.39" y1="-3.1" x2="0.88" y2="-2" layer="51"/>
<rectangle x1="1.66" y1="-3.1" x2="2.15" y2="-2" layer="51"/>
<rectangle x1="1.66" y1="2" x2="2.15" y2="3.1" layer="51"/>
<rectangle x1="0.39" y1="2" x2="0.88" y2="3.1" layer="51"/>
<rectangle x1="-0.88" y1="2" x2="-0.39" y2="3.1" layer="51"/>
<rectangle x1="-2.15" y1="2" x2="-1.66" y2="3.1" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="OPAMP">
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="0" width="0.4064" layer="94"/>
<wire x1="5.08" y1="0" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="3.175" x2="-3.81" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="94"/>
<text x="2.54" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="-IN" x="-7.62" y="-2.54" visible="pad" length="short" direction="in"/>
<pin name="+IN" x="-7.62" y="2.54" visible="pad" length="short" direction="in"/>
<pin name="OUT" x="7.62" y="0" visible="pad" length="short" direction="out" rot="R180"/>
</symbol>
<symbol name="PWR+-">
<text x="1.27" y="3.175" size="0.8128" layer="93" rot="R90">V+</text>
<text x="1.27" y="-4.445" size="0.8128" layer="93" rot="R90">V-</text>
<pin name="V+" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<pin name="V-" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LM358" prefix="IC">
<description>&lt;b&gt;OP AMP&lt;/b&gt; also LM158; LM258; LM2904&lt;p&gt;
Source: http://cache.national.com/ds/LM/LM158.pdf</description>
<gates>
<gate name="A" symbol="OPAMP" x="15.24" y="10.16" swaplevel="1"/>
<gate name="B" symbol="OPAMP" x="15.24" y="-12.7" swaplevel="1"/>
<gate name="P" symbol="PWR+-" x="15.24" y="10.16" addlevel="request"/>
</gates>
<devices>
<device name="N" package="DIL08">
<connects>
<connect gate="A" pin="+IN" pad="3"/>
<connect gate="A" pin="-IN" pad="2"/>
<connect gate="A" pin="OUT" pad="1"/>
<connect gate="B" pin="+IN" pad="5"/>
<connect gate="B" pin="-IN" pad="6"/>
<connect gate="B" pin="OUT" pad="7"/>
<connect gate="P" pin="V+" pad="8"/>
<connect gate="P" pin="V-" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D" package="SO08">
<connects>
<connect gate="A" pin="+IN" pad="3"/>
<connect gate="A" pin="-IN" pad="2"/>
<connect gate="A" pin="OUT" pad="1"/>
<connect gate="B" pin="+IN" pad="5"/>
<connect gate="B" pin="-IN" pad="6"/>
<connect gate="B" pin="OUT" pad="7"/>
<connect gate="P" pin="V+" pad="8"/>
<connect gate="P" pin="V-" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MX" package="SO08">
<connects>
<connect gate="A" pin="+IN" pad="3"/>
<connect gate="A" pin="-IN" pad="2"/>
<connect gate="A" pin="OUT" pad="1"/>
<connect gate="B" pin="+IN" pad="5"/>
<connect gate="B" pin="-IN" pad="6"/>
<connect gate="B" pin="OUT" pad="7"/>
<connect gate="P" pin="V+" pad="8"/>
<connect gate="P" pin="V-" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="microphon">
<description>&lt;b&gt;Microphon&lt;/b&gt; from&lt;p&gt;
JLI Electronics - www.jlielectronics.com&lt;br&gt;
Panansonic - http://industrial.panasonic.com&lt;br&gt;
&lt;p&gt;
&lt;author&gt;librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="F6035AP">
<description>&lt;b&gt;Omnibidirectional Electret Condenser Microphon&lt;/b&gt;&lt;p&gt;
Source: F6035AP.pdf</description>
<circle x="0" y="0" radius="2.8754" width="0.2032" layer="21"/>
<circle x="1.9" y="0" radius="0.4279" width="0" layer="21"/>
<circle x="0" y="0" radius="2.625" width="1" layer="41"/>
<pad name="2" x="0" y="-0.95" drill="0.7" diameter="1.2"/>
<pad name="1" x="0" y="0.95" drill="0.7" diameter="1.2"/>
<text x="-2.4" y="3.2" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.4" y="-4.8" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="F6050AP">
<description>&lt;b&gt;Omnibidirectional Electret Condenser Microphon&lt;/b&gt;&lt;p&gt;
Source: F6050AP.pdf</description>
<circle x="0" y="0" radius="2.8754" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="2.625" width="1" layer="41"/>
<pad name="2" x="0" y="-1.27" drill="0.7" diameter="1.2"/>
<pad name="1" x="0" y="1.27" drill="0.7" diameter="1.2"/>
<text x="-2.4" y="3.2" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.4" y="-4.8" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="1.5499" y1="-2.0251" x2="1.9499" y2="-1.4751" layer="21" rot="R45"/>
<rectangle x1="-1.95" y1="-2.025" x2="-1.55" y2="-1.475" layer="21" rot="R315"/>
</package>
<package name="WM-61A">
<description>&lt;b&gt;Omnidirectional Back Electret Condenser Microphone Cartridge&lt;/b&gt;&lt;p&gt;
Source: www.panasonic.com/industrial/components/pdf/em06_wm61_a_b_dne.pdf</description>
<wire x1="-2.75" y1="0.5" x2="-2.25" y2="0.5" width="0.2032" layer="21"/>
<wire x1="-2.25" y1="0.5" x2="-2.25" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="-2.25" y1="-0.5" x2="-2.75" y2="-0.5" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="2.9" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="2.85" width="0.6" layer="41"/>
<circle x="1.9" y="0" radius="0.3758" width="0" layer="21"/>
<smd name="1" x="0" y="0.95" dx="2.5" dy="1.3" layer="1" roundness="100"/>
<smd name="2" x="0" y="-0.95" dx="2.5" dy="1.3" layer="1" roundness="100"/>
<text x="-2.794" y="3.302" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.794" y="-4.572" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WM-61B">
<description>&lt;b&gt;Omnidirectional Back Electret Condenser Microphone Cartridge&lt;/b&gt;&lt;p&gt;
Source: www.panasonic.com/industrial/components/pdf/em06_wm61_a_b_dne.pdf</description>
<circle x="0" y="0" radius="2.9" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="2.85" width="0.6" layer="41"/>
<circle x="2" y="0" radius="0.3783" width="0" layer="21"/>
<pad name="1" x="0" y="0.95" drill="0.6" diameter="1"/>
<pad name="2" x="0" y="-0.95" drill="0.6" diameter="1"/>
<text x="-2.794" y="3.302" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.794" y="-4.572" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-0.225" x2="-2.325" y2="0.225" layer="21"/>
</package>
<package name="WM-62PC/62PK">
<description>&lt;b&gt;Omnidirectional Back Electret Condenser Microphone Cartridge&lt;/b&gt;&lt;p&gt;
Source: http://industrial.panasonic.com/www-data/pdf/ABA5000/ABA5000CE10.pdf</description>
<circle x="0" y="0" radius="2.9" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="2.85" width="0.6" layer="41"/>
<pad name="1" x="-1" y="0.95" drill="0.6" diameter="1"/>
<pad name="2" x="-1" y="-0.95" drill="0.6" diameter="1"/>
<text x="-2.794" y="3.302" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.794" y="-4.572" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WM-63PR">
<description>&lt;b&gt;Omnidirectional Back Electret Condenser Microphone Cartridge&lt;/b&gt;&lt;p&gt;
Source: http://industrial.panasonic.com/www-data/pdf/ABA5000/ABA5000CE3.pdf</description>
<circle x="0" y="0" radius="2.9" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="2.85" width="0.6" layer="41"/>
<pad name="1" x="-1" y="0.95" drill="0.6" diameter="1"/>
<pad name="2" x="-1" y="-0.95" drill="0.6" diameter="1"/>
<text x="-2.794" y="3.302" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.794" y="-4.572" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WM-64PC/64PK">
<description>&lt;b&gt;Omnidirectional Back Electret Condenser Microphone Cartridge&lt;/b&gt;&lt;p&gt;
Source: http://industrial.panasonic.com/www-data/pdf/ABA5000/ABA5000CE10.pdf</description>
<circle x="0" y="0" radius="2.9" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="2.85" width="0.6" layer="41"/>
<pad name="1" x="-1" y="0.95" drill="0.6" diameter="1"/>
<pad name="2" x="-1" y="-0.95" drill="0.6" diameter="1"/>
<text x="-2.794" y="3.302" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.794" y="-4.572" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WM-64PN">
<description>&lt;b&gt;Omnidirectional Back Electret Condenser Microphone Cartridge&lt;/b&gt;&lt;p&gt;
Source: http://industrial.panasonic.com/www-data/pdf/ABA5000/ABA5000CE10.pdf</description>
<circle x="0" y="0" radius="2.9" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="2.85" width="0.6" layer="41"/>
<pad name="1" x="-1" y="0.95" drill="0.6" diameter="1"/>
<pad name="2" x="-1" y="-0.95" drill="0.6" diameter="1"/>
<text x="-2.794" y="3.302" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.794" y="-4.572" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="CONDENSER-MICROPHON">
<wire x1="1.27" y1="-1.27" x2="2.032" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="2.032" y1="-0.254" x2="2.032" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="2.032" y1="-1.27" x2="2.032" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.254" x2="2.54" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="3.302" y2="-1.27" width="0.1524" layer="94"/>
<circle x="2.286" y="-1.27" radius="2.54" width="0.254" layer="94"/>
<rectangle x1="4.826" y1="-3.81" x2="5.334" y2="1.27" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="middle" direction="pas"/>
<pin name="2" x="-5.08" y="-2.54" visible="pad" length="middle" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ELECTRET_MICROPHON-" prefix="M">
<description>&lt;b&gt;Electret Condenser Microphone&lt;/b&gt;&lt;p&gt;</description>
<gates>
<gate name="G$1" symbol="CONDENSER-MICROPHON" x="0" y="0"/>
</gates>
<devices>
<device name="F6035AP" package="F6035AP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="F6050AP" package="F6050AP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WM-61A" package="WM-61A">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WM-61B" package="WM-61B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WM-62PC/62PK" package="WM-62PC/62PK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WM-63PR" package="WM-63PR">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WM-64PC/64PK" package="WM-64PC/64PK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WM.64PN" package="WM-64PN">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="2X03">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<text x="-3.81" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
</package>
<package name="2X03/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.81" y1="-1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="6.985" x2="-2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="6.985" x2="0" y2="1.27" width="0.762" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="6.985" x2="2.54" y2="1.27" width="0.762" layer="21"/>
<pad name="2" x="-2.54" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="4" x="0" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="6" x="2.54" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="1" x="-2.54" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="3" x="0" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="5" x="2.54" y="-6.35" drill="1.016" shape="octagon"/>
<text x="-4.445" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="5.715" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.921" y1="0.635" x2="-2.159" y2="1.143" layer="21"/>
<rectangle x1="-0.381" y1="0.635" x2="0.381" y2="1.143" layer="21"/>
<rectangle x1="2.159" y1="0.635" x2="2.921" y2="1.143" layer="21"/>
<rectangle x1="-2.921" y1="-2.921" x2="-2.159" y2="-1.905" layer="21"/>
<rectangle x1="-0.381" y1="-2.921" x2="0.381" y2="-1.905" layer="21"/>
<rectangle x1="-2.921" y1="-5.461" x2="-2.159" y2="-4.699" layer="21"/>
<rectangle x1="-2.921" y1="-4.699" x2="-2.159" y2="-2.921" layer="51"/>
<rectangle x1="-0.381" y1="-4.699" x2="0.381" y2="-2.921" layer="51"/>
<rectangle x1="-0.381" y1="-5.461" x2="0.381" y2="-4.699" layer="21"/>
<rectangle x1="2.159" y1="-2.921" x2="2.921" y2="-1.905" layer="21"/>
<rectangle x1="2.159" y1="-5.461" x2="2.921" y2="-4.699" layer="21"/>
<rectangle x1="2.159" y1="-4.699" x2="2.921" y2="-2.921" layer="51"/>
</package>
<package name="1X04">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-5.1562" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
</package>
<package name="1X04/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-3.81" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-5.715" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="6.985" y="-4.445" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
</package>
<package name="1X02">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
<package name="1X02/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-3.175" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="4.445" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
</package>
<package name="1X18">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="15.875" y1="1.27" x2="17.145" y2="1.27" width="0.1524" layer="21"/>
<wire x1="17.145" y1="1.27" x2="17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="17.78" y1="0.635" x2="17.78" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-0.635" x2="17.145" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="17.78" y1="0.635" x2="18.415" y2="1.27" width="0.1524" layer="21"/>
<wire x1="18.415" y1="1.27" x2="19.685" y2="1.27" width="0.1524" layer="21"/>
<wire x1="19.685" y1="1.27" x2="20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="20.32" y1="0.635" x2="20.32" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-0.635" x2="19.685" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="19.685" y1="-1.27" x2="18.415" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="18.415" y1="-1.27" x2="17.78" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="0.635" x2="13.335" y2="1.27" width="0.1524" layer="21"/>
<wire x1="13.335" y1="1.27" x2="14.605" y2="1.27" width="0.1524" layer="21"/>
<wire x1="14.605" y1="1.27" x2="15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="15.24" y1="0.635" x2="15.24" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-0.635" x2="14.605" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="14.605" y1="-1.27" x2="13.335" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.7" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="15.875" y1="1.27" x2="15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-0.635" x2="15.875" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="17.145" y1="-1.27" x2="15.875" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.27" x2="9.525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-0.635" x2="9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="10.795" y2="1.27" width="0.1524" layer="21"/>
<wire x1="10.795" y1="1.27" x2="12.065" y2="1.27" width="0.1524" layer="21"/>
<wire x1="12.065" y1="1.27" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="0.635" x2="12.7" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-0.635" x2="12.065" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="10.795" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-9.525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="1.27" x2="-8.255" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.27" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-0.635" x2="-8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-1.27" x2="-9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="-1.27" x2="-10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-0.635" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="1.27" x2="-13.335" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="1.27" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="0.635" x2="-12.7" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-0.635" x2="-13.335" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="0.635" x2="-12.065" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="1.27" x2="-10.795" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="1.27" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-0.635" x2="-10.795" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-1.27" x2="-12.065" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-1.27" x2="-12.7" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="0.635" x2="-17.145" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-17.145" y1="1.27" x2="-15.875" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="1.27" x2="-15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="0.635" x2="-15.24" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="-0.635" x2="-15.875" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="-1.27" x2="-17.145" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-17.145" y1="-1.27" x2="-17.78" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="1.27" x2="-15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="-0.635" x2="-14.605" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-1.27" x2="-14.605" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-22.225" y1="1.27" x2="-20.955" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-20.955" y1="1.27" x2="-20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="0.635" x2="-20.32" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="-0.635" x2="-20.955" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="0.635" x2="-19.685" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="1.27" x2="-18.415" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-18.415" y1="1.27" x2="-17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="0.635" x2="-17.78" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="-0.635" x2="-18.415" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-18.415" y1="-1.27" x2="-19.685" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="-1.27" x2="-20.32" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="0.635" x2="-22.86" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-22.225" y1="1.27" x2="-22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="-0.635" x2="-22.225" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-20.955" y1="-1.27" x2="-22.225" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="20.32" y1="0.635" x2="20.955" y2="1.27" width="0.1524" layer="21"/>
<wire x1="20.955" y1="1.27" x2="22.225" y2="1.27" width="0.1524" layer="21"/>
<wire x1="22.225" y1="1.27" x2="22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="22.86" y1="0.635" x2="22.86" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="22.86" y1="-0.635" x2="22.225" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="22.225" y1="-1.27" x2="20.955" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="20.955" y1="-1.27" x2="20.32" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-21.59" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-19.05" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="-16.51" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="-13.97" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="-11.43" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="-8.89" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="-6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="8" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="9" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="10" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="11" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="12" x="6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="13" x="8.89" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="14" x="11.43" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="15" x="13.97" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="16" x="16.51" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="17" x="19.05" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="18" x="21.59" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-22.9362" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-22.86" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="18.796" y1="-0.254" x2="19.304" y2="0.254" layer="51"/>
<rectangle x1="16.256" y1="-0.254" x2="16.764" y2="0.254" layer="51"/>
<rectangle x1="13.716" y1="-0.254" x2="14.224" y2="0.254" layer="51"/>
<rectangle x1="11.176" y1="-0.254" x2="11.684" y2="0.254" layer="51"/>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="-11.684" y1="-0.254" x2="-11.176" y2="0.254" layer="51"/>
<rectangle x1="-14.224" y1="-0.254" x2="-13.716" y2="0.254" layer="51"/>
<rectangle x1="-16.764" y1="-0.254" x2="-16.256" y2="0.254" layer="51"/>
<rectangle x1="-19.304" y1="-0.254" x2="-18.796" y2="0.254" layer="51"/>
<rectangle x1="-21.844" y1="-0.254" x2="-21.336" y2="0.254" layer="51"/>
<rectangle x1="21.336" y1="-0.254" x2="21.844" y2="0.254" layer="51"/>
</package>
<package name="1X18/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-22.86" y1="-1.905" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="-1.905" x2="-20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="0.635" x2="-22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="0.635" x2="-22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-21.59" y1="6.985" x2="-21.59" y2="1.27" width="0.762" layer="21"/>
<wire x1="-20.32" y1="-1.905" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="0.635" x2="-20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-19.05" y1="6.985" x2="-19.05" y2="1.27" width="0.762" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="0.635" x2="-17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-16.51" y1="6.985" x2="-16.51" y2="1.27" width="0.762" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="0.635" x2="-15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-13.97" y1="6.985" x2="-13.97" y2="1.27" width="0.762" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="6.985" x2="-11.43" y2="1.27" width="0.762" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="6.985" x2="-8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="6.985" x2="-6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="6.985" x2="6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="6.985" x2="8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="0.635" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="11.43" y1="6.985" x2="11.43" y2="1.27" width="0.762" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="15.24" y1="0.635" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="13.97" y1="6.985" x2="13.97" y2="1.27" width="0.762" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="17.78" y1="0.635" x2="15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="16.51" y1="6.985" x2="16.51" y2="1.27" width="0.762" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="20.32" y1="0.635" x2="17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="19.05" y1="6.985" x2="19.05" y2="1.27" width="0.762" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="22.86" y1="0.635" x2="20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="21.59" y1="6.985" x2="21.59" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-21.59" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-19.05" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="-16.51" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="-13.97" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="-11.43" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="-8.89" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="-6.35" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="8" x="-3.81" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="9" x="-1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="10" x="1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="11" x="3.81" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="12" x="6.35" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="13" x="8.89" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="14" x="11.43" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="15" x="13.97" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="16" x="16.51" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="17" x="19.05" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="18" x="21.59" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-23.495" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="24.765" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-21.971" y1="0.635" x2="-21.209" y2="1.143" layer="21"/>
<rectangle x1="-19.431" y1="0.635" x2="-18.669" y2="1.143" layer="21"/>
<rectangle x1="-16.891" y1="0.635" x2="-16.129" y2="1.143" layer="21"/>
<rectangle x1="-14.351" y1="0.635" x2="-13.589" y2="1.143" layer="21"/>
<rectangle x1="-11.811" y1="0.635" x2="-11.049" y2="1.143" layer="21"/>
<rectangle x1="-9.271" y1="0.635" x2="-8.509" y2="1.143" layer="21"/>
<rectangle x1="-6.731" y1="0.635" x2="-5.969" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.731" y2="1.143" layer="21"/>
<rectangle x1="8.509" y1="0.635" x2="9.271" y2="1.143" layer="21"/>
<rectangle x1="11.049" y1="0.635" x2="11.811" y2="1.143" layer="21"/>
<rectangle x1="13.589" y1="0.635" x2="14.351" y2="1.143" layer="21"/>
<rectangle x1="16.129" y1="0.635" x2="16.891" y2="1.143" layer="21"/>
<rectangle x1="18.669" y1="0.635" x2="19.431" y2="1.143" layer="21"/>
<rectangle x1="21.209" y1="0.635" x2="21.971" y2="1.143" layer="21"/>
<rectangle x1="-21.971" y1="-2.921" x2="-21.209" y2="-1.905" layer="21"/>
<rectangle x1="-19.431" y1="-2.921" x2="-18.669" y2="-1.905" layer="21"/>
<rectangle x1="-16.891" y1="-2.921" x2="-16.129" y2="-1.905" layer="21"/>
<rectangle x1="-14.351" y1="-2.921" x2="-13.589" y2="-1.905" layer="21"/>
<rectangle x1="-11.811" y1="-2.921" x2="-11.049" y2="-1.905" layer="21"/>
<rectangle x1="-9.271" y1="-2.921" x2="-8.509" y2="-1.905" layer="21"/>
<rectangle x1="-6.731" y1="-2.921" x2="-5.969" y2="-1.905" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
<rectangle x1="5.969" y1="-2.921" x2="6.731" y2="-1.905" layer="21"/>
<rectangle x1="8.509" y1="-2.921" x2="9.271" y2="-1.905" layer="21"/>
<rectangle x1="11.049" y1="-2.921" x2="11.811" y2="-1.905" layer="21"/>
<rectangle x1="13.589" y1="-2.921" x2="14.351" y2="-1.905" layer="21"/>
<rectangle x1="16.129" y1="-2.921" x2="16.891" y2="-1.905" layer="21"/>
<rectangle x1="18.669" y1="-2.921" x2="19.431" y2="-1.905" layer="21"/>
<rectangle x1="21.209" y1="-2.921" x2="21.971" y2="-1.905" layer="21"/>
</package>
<package name="1X03">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-3.8862" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
</package>
<package name="1X03/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.81" y1="-1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="6.985" x2="-2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="6.985" x2="0" y2="1.27" width="0.762" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="6.985" x2="2.54" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="0" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-4.445" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="5.715" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.921" y1="0.635" x2="-2.159" y2="1.143" layer="21"/>
<rectangle x1="-0.381" y1="0.635" x2="0.381" y2="1.143" layer="21"/>
<rectangle x1="2.159" y1="0.635" x2="2.921" y2="1.143" layer="21"/>
<rectangle x1="-2.921" y1="-2.921" x2="-2.159" y2="-1.905" layer="21"/>
<rectangle x1="-0.381" y1="-2.921" x2="0.381" y2="-1.905" layer="21"/>
<rectangle x1="2.159" y1="-2.921" x2="2.921" y2="-1.905" layer="21"/>
</package>
<package name="2X04">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="3.81" y="1.27" drill="1.016" shape="octagon"/>
<text x="-5.08" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
</package>
<package name="2X04/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<pad name="2" x="-3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="4" x="-1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="6" x="1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="8" x="3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="1" x="-3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="3" x="-1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="5" x="1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="7" x="3.81" y="-6.35" drill="1.016" shape="octagon"/>
<text x="-5.715" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="6.985" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="-4.191" y1="-5.461" x2="-3.429" y2="-4.699" layer="21"/>
<rectangle x1="-4.191" y1="-4.699" x2="-3.429" y2="-2.921" layer="51"/>
<rectangle x1="-1.651" y1="-4.699" x2="-0.889" y2="-2.921" layer="51"/>
<rectangle x1="-1.651" y1="-5.461" x2="-0.889" y2="-4.699" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-5.461" x2="1.651" y2="-4.699" layer="21"/>
<rectangle x1="0.889" y1="-4.699" x2="1.651" y2="-2.921" layer="51"/>
<rectangle x1="3.429" y1="-4.699" x2="4.191" y2="-2.921" layer="51"/>
<rectangle x1="3.429" y1="-5.461" x2="4.191" y2="-4.699" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="PINH2X3">
<wire x1="-6.35" y1="-5.08" x2="8.89" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-5.08" x2="8.89" y2="5.08" width="0.4064" layer="94"/>
<wire x1="8.89" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="PINHD4">
<wire x1="-6.35" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="7.62" x2="-6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINHD2">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINHD18">
<wire x1="-6.35" y1="-25.4" x2="1.27" y2="-25.4" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-25.4" x2="1.27" y2="22.86" width="0.4064" layer="94"/>
<wire x1="1.27" y1="22.86" x2="-6.35" y2="22.86" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="22.86" x2="-6.35" y2="-25.4" width="0.4064" layer="94"/>
<text x="-6.35" y="23.495" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-27.94" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="20.32" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="7" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="9" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="11" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="13" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="14" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="15" x="-2.54" y="-15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="16" x="-2.54" y="-17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="17" x="-2.54" y="-20.32" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="18" x="-2.54" y="-22.86" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINHD3">
<wire x1="-6.35" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINH2X4">
<wire x1="-6.35" y1="-5.08" x2="8.89" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-5.08" x2="8.89" y2="7.62" width="0.4064" layer="94"/>
<wire x1="8.89" y1="7.62" x2="-6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-2X3" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X03">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="2X03/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X4" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X04">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X04/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X2" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X02/90">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X18" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD18" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X18">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X18/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X3" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X03">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X03/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-2X4" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X04">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="2X04/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find connectors and sockets- basically anything that can be plugged into or onto.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; CC v3.0 Share-Alike You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1X06">
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X6">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.048" x2="13.97" y2="-2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="13.97" y1="-2.54" x2="12.7" y2="-2.54" width="0.127" layer="21"/>
<wire x1="12.7" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="12.7" y2="-1.27" width="0.127" layer="21"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="MOLEX-1X6-RA">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="-1.27" y2="0.635" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.175" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<wire x1="12.7" y1="3.175" x2="0" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="0" y2="7.62" width="0.127" layer="21"/>
<wire x1="0" y1="7.62" x2="12.7" y2="7.62" width="0.127" layer="21"/>
<wire x1="12.7" y1="7.62" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796"/>
<text x="-0.889" y="-2.794" size="1.27" layer="25">&gt;NAME</text>
<text x="8.001" y="-2.794" size="1.27" layer="25">&gt;VALUE</text>
</package>
<package name="1X06-SMD">
<wire x1="7.62" y1="1.25" x2="-7.62" y2="1.25" width="0.127" layer="51"/>
<wire x1="-7.62" y1="1.25" x2="-7.62" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-7.62" y1="-1.25" x2="-6.35" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-6.35" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="6.35" y2="-1.25" width="0.127" layer="51"/>
<wire x1="6.35" y1="-1.25" x2="7.62" y2="-1.25" width="0.127" layer="51"/>
<wire x1="7.62" y1="-1.25" x2="7.62" y2="1.25" width="0.127" layer="51"/>
<wire x1="6.35" y1="-1.25" x2="6.35" y2="-7.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-6.35" y1="-1.25" x2="-6.35" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="5" x="3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="6" x="6.35" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="-1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-6.35" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<hole x="-5.08" y="0" drill="1.4"/>
<hole x="5.08" y="0" drill="1.4"/>
</package>
<package name="1X06_LOCK">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="-1.27" y1="0.508" x2="-0.635" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.143" x2="0.635" y2="1.143" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.143" x2="1.27" y2="0.508" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.508" x2="1.905" y2="1.143" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.143" x2="3.175" y2="1.143" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.143" x2="3.81" y2="0.508" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.508" x2="4.445" y2="1.143" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.143" x2="5.715" y2="1.143" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.143" x2="6.35" y2="0.508" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.508" x2="6.985" y2="1.143" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.143" x2="8.255" y2="1.143" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.143" x2="8.89" y2="0.508" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.508" x2="9.525" y2="1.143" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.143" x2="10.795" y2="1.143" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.143" x2="11.43" y2="0.508" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.508" x2="12.065" y2="1.143" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.143" x2="13.335" y2="1.143" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.143" x2="13.97" y2="0.508" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.508" x2="13.97" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.762" x2="13.335" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.397" x2="12.065" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.397" x2="11.43" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.762" x2="10.795" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.397" x2="9.525" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.397" x2="8.89" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.762" x2="8.255" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.397" x2="6.985" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.985" y1="-1.397" x2="6.35" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.762" x2="5.715" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.397" x2="4.445" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.397" x2="3.81" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.762" x2="3.175" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.397" x2="1.905" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.397" x2="1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.762" x2="0.635" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.397" x2="-0.635" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.397" x2="-1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="0.508" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.508" x2="6.35" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.508" x2="8.89" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.508" x2="11.43" y2="-0.762" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" diameter="1.8796"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51"/>
</package>
<package name="1X06_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.604" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="9.144" y1="-0.127" x2="8.636" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="11.684" y1="-0.127" x2="11.176" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.127" x2="13.716" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.127" x2="13.97" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-1.1176" x2="13.6906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.127" x2="13.97" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.8636" x2="13.6906" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51" rot="R90"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51"/>
</package>
<package name="MOLEX-1X6_LOCK">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.048" x2="13.97" y2="-2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="13.97" y1="-2.54" x2="12.7" y2="-2.54" width="0.127" layer="21"/>
<wire x1="12.7" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="12.7" y2="-1.27" width="0.127" layer="21"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" diameter="1.8796"/>
<text x="-1.27" y="3.302" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-4.064" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51"/>
</package>
<package name="MOLEX-1X6-RA_LOCK">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="-1.27" y2="0.635" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.175" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<wire x1="12.7" y1="3.175" x2="0" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="0" y2="7.62" width="0.127" layer="21"/>
<wire x1="0" y1="7.62" x2="12.7" y2="7.62" width="0.127" layer="21"/>
<wire x1="12.7" y1="7.62" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" diameter="1.8796"/>
<text x="-0.889" y="-2.794" size="1.27" layer="25">&gt;NAME</text>
<text x="8.001" y="-2.794" size="1.27" layer="25">&gt;VALUE</text>
</package>
<package name="1X06-SIP_LOCK">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" diameter="1.8796"/>
<text x="-1.27" y="1.524" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-2.921" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51"/>
</package>
<package name="1X06_FEMALE_LOCK.010">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="13.97" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="1.27" x2="13.97" y2="-1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3175" y1="-0.1905" x2="0.3175" y2="0.1905" layer="51"/>
<rectangle x1="2.2225" y1="-0.1905" x2="2.8575" y2="0.1905" layer="51"/>
<rectangle x1="4.7625" y1="-0.1905" x2="5.3975" y2="0.1905" layer="51"/>
<rectangle x1="7.3025" y1="-0.1905" x2="7.9375" y2="0.1905" layer="51"/>
<rectangle x1="9.8425" y1="-0.1905" x2="10.4775" y2="0.1905" layer="51"/>
<rectangle x1="12.3825" y1="-0.1905" x2="13.0175" y2="0.1905" layer="51"/>
</package>
<package name="1X06_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X06-SMD_EDGE">
<wire x1="13.97" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="-11.176" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-11.176" x2="13.97" y2="-11.176" width="0.127" layer="51"/>
<wire x1="13.97" y1="-11.176" x2="13.97" y2="-2.54" width="0.127" layer="51"/>
<smd name="4" x="7.62" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="5" x="10.16" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="6" x="12.7" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="5.08" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="2.54" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="0" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<text x="0" y="-6.35" size="0.4064" layer="51">thru-hole vertical Female Header</text>
<text x="0" y="-7.62" size="0.4064" layer="51">used as an SMD part </text>
<text x="0" y="-8.89" size="0.4064" layer="51">(placed horizontally, at board's edge)</text>
<rectangle x1="-0.381" y1="-2.4892" x2="0.381" y2="0.2794" layer="51"/>
<rectangle x1="2.159" y1="-2.4892" x2="2.921" y2="0.2794" layer="51"/>
<rectangle x1="4.699" y1="-2.4892" x2="5.461" y2="0.2794" layer="51"/>
<rectangle x1="7.239" y1="-2.4892" x2="8.001" y2="0.2794" layer="51"/>
<rectangle x1="9.779" y1="-2.4892" x2="10.541" y2="0.2794" layer="51"/>
<rectangle x1="12.319" y1="-2.4892" x2="13.081" y2="0.2794" layer="51"/>
</package>
<package name="SCREWTERMINAL-3.5MM-6">
<wire x1="-2.3" y1="3.4" x2="19.76" y2="3.4" width="0.2032" layer="21"/>
<wire x1="19.76" y1="3.4" x2="19.76" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="19.76" y1="-2.8" x2="19.76" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="19.76" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="19.76" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="19.76" y1="3.15" x2="20.16" y2="3.15" width="0.2032" layer="51"/>
<wire x1="20.16" y1="3.15" x2="20.16" y2="2.15" width="0.2032" layer="51"/>
<wire x1="20.16" y1="2.15" x2="19.76" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="5" x="14" y="0" drill="1.2" diameter="2.032"/>
<pad name="6" x="17.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X06-SMD-FEMALE">
<wire x1="-7.62" y1="4.05" x2="7.62" y2="4.05" width="0.2032" layer="51"/>
<wire x1="7.62" y1="4.05" x2="7.62" y2="-4.05" width="0.2032" layer="51"/>
<wire x1="7.62" y1="-4.05" x2="-7.62" y2="-4.05" width="0.2032" layer="51"/>
<wire x1="-7.62" y1="-4.05" x2="-7.62" y2="4.05" width="0.2032" layer="51"/>
<wire x1="-6.85" y1="-3" x2="-6.85" y2="-2" width="0.2032" layer="21"/>
<wire x1="-6.85" y1="-2" x2="-5.85" y2="-2" width="0.2032" layer="21"/>
<wire x1="-5.85" y1="-2" x2="-5.85" y2="-3" width="0.2032" layer="21"/>
<wire x1="-7.1" y1="4" x2="-7.6" y2="4" width="0.2032" layer="21"/>
<wire x1="-7.6" y1="4" x2="-7.6" y2="-4" width="0.2032" layer="21"/>
<wire x1="-7.6" y1="-4" x2="-7.1" y2="-4" width="0.2032" layer="21"/>
<wire x1="7.1" y1="4" x2="7.6" y2="4" width="0.2032" layer="21"/>
<wire x1="7.6" y1="4" x2="7.6" y2="-4" width="0.2032" layer="21"/>
<wire x1="7.6" y1="-4" x2="7.1" y2="-4" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="4" x2="0.3" y2="4" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="4" x2="-2.2" y2="4" width="0.2032" layer="21"/>
<wire x1="-5.4" y1="4" x2="-4.8" y2="4" width="0.2032" layer="21"/>
<wire x1="2.3" y1="4" x2="2.9" y2="4" width="0.2032" layer="21"/>
<wire x1="4.8" y1="4" x2="5.4" y2="4" width="0.2032" layer="21"/>
<wire x1="4.8" y1="-4" x2="5.4" y2="-4" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-4" x2="2.8" y2="-4" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="-4" x2="0.3" y2="-4" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="-4" x2="-2.2" y2="-4" width="0.2032" layer="21"/>
<wire x1="-5.4" y1="-4" x2="-4.8" y2="-4" width="0.2032" layer="21"/>
<smd name="6" x="6.35" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="5" x="3.81" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="4" x="1.27" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="3" x="-1.27" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="2" x="-3.81" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="1" x="-6.35" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A1" x="-6.35" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A2" x="-3.81" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A3" x="-1.27" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A4" x="1.27" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A5" x="3.81" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A6" x="6.35" y="4.62" dx="1.02" dy="2.16" layer="1"/>
</package>
<package name="1X06-SMD-FEMALE-V2">
<description>Package for 4UCONN part #19686 *UNPROVEN*</description>
<wire x1="-7.5" y1="0.45" x2="-7.5" y2="-8.05" width="0.127" layer="21"/>
<wire x1="7.5" y1="0.45" x2="-7.5" y2="0.45" width="0.127" layer="21"/>
<wire x1="7.5" y1="-8.05" x2="7.5" y2="0.45" width="0.127" layer="21"/>
<wire x1="-7.5" y1="-8.05" x2="7.5" y2="-8.05" width="0.127" layer="21"/>
<smd name="4" x="-1.27" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="5" x="-3.81" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="6" x="-6.35" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="3" x="1.27" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="2" x="3.81" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="1" x="6.35" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<text x="7.6" y="-8.3" size="1" layer="27" rot="R180">&gt;Value</text>
<text x="-7.4" y="-9.3" size="1" layer="25">&gt;Name</text>
</package>
<package name="1X06_HOLES_ONLY">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="7.62" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="10.16" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="12.7" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<hole x="5.08" y="0" drill="1.4732"/>
<hole x="7.62" y="0" drill="1.4732"/>
<hole x="10.16" y="0" drill="1.4732"/>
<hole x="12.7" y="0" drill="1.4732"/>
</package>
<package name="1X06_SMD_STRAIGHT">
<wire x1="1.37" y1="1.25" x2="-14.07" y2="1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="1.25" x2="-14.07" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="1.25" x2="1.37" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-14.07" y1="-1.25" x2="-14.07" y2="1.25" width="0.127" layer="21"/>
<wire x1="0.85" y1="1.25" x2="1.37" y2="1.25" width="0.127" layer="21"/>
<wire x1="-14.07" y1="1.25" x2="-10.883" y2="1.25" width="0.127" layer="21"/>
<wire x1="-13.55" y1="-1.25" x2="-14.07" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="-1.25" x2="-1.817" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-4.377" y1="1.25" x2="-0.703" y2="1.25" width="0.127" layer="21"/>
<wire x1="-9.457" y1="1.25" x2="-5.783" y2="1.25" width="0.127" layer="21"/>
<wire x1="-3.329" y1="-1.25" x2="-6.831" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-8.409" y1="-1.25" x2="-11.911" y2="-1.25" width="0.127" layer="21"/>
<smd name="5" x="-10.16" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3" x="-5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="1" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="6" x="-12.7" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4" x="-7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="-13.97" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-13.97" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X06_SMD_STRAIGHT_ALT">
<wire x1="1.37" y1="1.25" x2="-14.07" y2="1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="1.25" x2="-14.07" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="1.25" x2="-14.07" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="21"/>
<wire x1="-13.55" y1="1.25" x2="-14.07" y2="1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="1.25" x2="-1.817" y2="1.25" width="0.127" layer="21"/>
<wire x1="0.85" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-14.07" y1="-1.25" x2="-10.883" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-8.323" y1="1.25" x2="-11.997" y2="1.25" width="0.127" layer="21"/>
<wire x1="-3.243" y1="1.25" x2="-6.917" y2="1.25" width="0.127" layer="21"/>
<wire x1="-9.371" y1="-1.25" x2="-5.869" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-4.291" y1="-1.25" x2="-0.789" y2="-1.25" width="0.127" layer="21"/>
<smd name="5" x="-10.16" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="3" x="-5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="6" x="-12.7" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="-7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="-2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<text x="-13.97" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-13.97" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X06_SMD_STRAIGHT_COMBO">
<wire x1="12.7" y1="1.27" x2="12.7" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="10.16" y1="1.27" x2="10.16" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="-1.37" y1="-1.25" x2="-1.37" y2="1.25" width="0.1778" layer="21"/>
<wire x1="14.07" y1="1.25" x2="14.07" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-0.73" y1="-1.25" x2="-1.37" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="14.07" y1="-1.25" x2="13.4" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="13.4" y1="1.25" x2="14.07" y2="1.25" width="0.1778" layer="21"/>
<wire x1="-1.37" y1="1.25" x2="-0.73" y2="1.25" width="0.1778" layer="21"/>
<wire x1="10.949" y1="1.25" x2="11.911" y2="1.25" width="0.1778" layer="21"/>
<wire x1="10.949" y1="-1.29" x2="11.911" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="8.409" y1="1.25" x2="9.371" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.409" y1="-1.29" x2="9.371" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.869" y1="-1.29" x2="6.831" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.869" y1="1.25" x2="6.831" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.329" y1="-1.29" x2="4.291" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="3.329" y1="1.25" x2="4.291" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.789" y1="-1.29" x2="1.751" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="0.789" y1="1.25" x2="1.751" y2="1.25" width="0.1778" layer="21"/>
<smd name="5" x="10.16" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="3" x="5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="6" x="12.7" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1-2" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2-2" x="2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3-2" x="5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4-2" x="7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="5-2" x="10.16" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="6-2" x="12.7" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="0" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="EM406">
<wire x1="0" y1="-2.921" x2="5.08" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-1.778" y1="-0.762" x2="-1.778" y2="0.635" width="0.254" layer="21"/>
<wire x1="5.842" y1="0.635" x2="6.858" y2="0.635" width="0.254" layer="21"/>
<wire x1="6.858" y1="0.635" x2="6.858" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-1.778" y1="0.635" x2="-0.762" y2="0.635" width="0.254" layer="21"/>
<circle x="0" y="1.27" radius="0.1047" width="0.4064" layer="21"/>
<smd name="P$1" x="-1.3" y="-2.225" dx="1.2" dy="1.8" layer="1"/>
<smd name="P$2" x="6.3" y="-2.225" dx="1.2" dy="1.8" layer="1"/>
<smd name="1" x="0" y="0" dx="0.6" dy="1.55" layer="1"/>
<smd name="2" x="1" y="0" dx="0.6" dy="1.55" layer="1"/>
<smd name="3" x="2" y="0" dx="0.6" dy="1.55" layer="1"/>
<smd name="4" x="3" y="0" dx="0.6" dy="1.55" layer="1"/>
<smd name="5" x="4" y="0" dx="0.6" dy="1.55" layer="1"/>
<smd name="6" x="5" y="0" dx="0.6" dy="1.55" layer="1"/>
<text x="1.27" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="1.27" y="-3.81" size="0.4064" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="M06">
<wire x1="1.27" y1="-7.62" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="0" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.6096" layer="94"/>
<text x="-5.08" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="10.922" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="5.08" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="M06" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 6&lt;/b&gt;
Standard 6-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115). Molex polarized connector foot print use with SKU : PRT-08094 with associated crimp pins and housings.

NOTES ON THE VARIANTS LOCK and LOCK_LONGPADS...
This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<gates>
<gate name="G$1" symbol="M06" x="-2.54" y="0"/>
</gates>
<devices>
<device name="SIP" package="1X06">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RA" package="MOLEX-1X6-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="1X06-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X06_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X06_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X6_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RA_LOCK" package="MOLEX-1X6-RA_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIP_LOCK" package="1X06-SIP_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FEMALE_LOCK" package="1X06_FEMALE_LOCK.010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X06_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X06-SMD_EDGE_FEMALE" package="1X06-SMD_EDGE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM-6" package="SCREWTERMINAL-3.5MM-6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMDF" package="1X06-SMD-FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-FEMALE-V2" package="1X06-SMD-FEMALE-V2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGOPIN_HOLES_ONLY" package="1X06_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-STRAIGHT" package="1X06_SMD_STRAIGHT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-STRAIGHT-ALT" package="1X06_SMD_STRAIGHT_ALT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-STRAIGHT-COMBO" package="1X06_SMD_STRAIGHT_COMBO">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EM406" package="EM406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Sensors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find sensors- accelerometers, gyros, compasses, magnetometers, light sensors, imagers, temp sensors, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; CC v3.0 Share-Alike You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="QFN-20-0.4MM">
<description>** 20-pin QFN ** 
initially designed for MPR121 | 
0.4mm pitch | 
3x3x0.65mm |
** VERY UNPROVEN **</description>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="1.5" width="0.127" layer="51"/>
<wire x1="1.5" y1="1.5" x2="-1.5" y2="1.5" width="0.127" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="1" x2="-1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="-1" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="1" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="1" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1" x2="1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1" x2="-1.5" y2="-1.5" width="0.127" layer="21"/>
<circle x="-0.66" y="0.66" radius="0.05" width="0.127" layer="21"/>
<smd name="18" x="0" y="1.4" dx="0.2" dy="0.6" layer="1"/>
<smd name="20" x="-0.8" y="1.4" dx="0.2" dy="0.6" layer="1"/>
<smd name="16" x="0.8" y="1.4" dx="0.2" dy="0.6" layer="1"/>
<smd name="19" x="-0.4" y="1.4" dx="0.2" dy="0.6" layer="1"/>
<smd name="17" x="0.4" y="1.4" dx="0.2" dy="0.6" layer="1"/>
<smd name="3" x="-1.4" y="0" dx="0.2" dy="0.6" layer="1" rot="R90"/>
<smd name="5" x="-1.4" y="-0.8" dx="0.2" dy="0.6" layer="1" rot="R90"/>
<smd name="1" x="-1.4" y="0.8" dx="0.2" dy="0.6" layer="1" rot="R90"/>
<smd name="4" x="-1.4" y="-0.4" dx="0.2" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="-1.4" y="0.4" dx="0.2" dy="0.6" layer="1" rot="R90"/>
<smd name="13" x="1.4" y="0" dx="0.2" dy="0.6" layer="1" rot="R270"/>
<smd name="15" x="1.4" y="0.8" dx="0.2" dy="0.6" layer="1" rot="R270"/>
<smd name="11" x="1.4" y="-0.8" dx="0.2" dy="0.6" layer="1" rot="R270"/>
<smd name="14" x="1.4" y="0.4" dx="0.2" dy="0.6" layer="1" rot="R270"/>
<smd name="12" x="1.4" y="-0.4" dx="0.2" dy="0.6" layer="1" rot="R270"/>
<smd name="8" x="0" y="-1.4" dx="0.2" dy="0.6" layer="1" rot="R180"/>
<smd name="10" x="0.8" y="-1.4" dx="0.2" dy="0.6" layer="1" rot="R180"/>
<smd name="6" x="-0.8" y="-1.4" dx="0.2" dy="0.6" layer="1" rot="R180"/>
<smd name="9" x="0.4" y="-1.4" dx="0.2" dy="0.6" layer="1" rot="R180"/>
<smd name="7" x="-0.4" y="-1.4" dx="0.2" dy="0.6" layer="1" rot="R180"/>
<text x="-0.889" y="0.2032" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0668" y="-0.5842" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="LTR-301">
<wire x1="-2.159" y1="0.762" x2="2.159" y2="0.762" width="0.127" layer="21"/>
<wire x1="2.159" y1="0.762" x2="2.159" y2="-0.762" width="0.127" layer="21"/>
<wire x1="2.159" y1="-0.762" x2="0.762" y2="-0.762" width="0.127" layer="21"/>
<wire x1="0.762" y1="-0.762" x2="-0.762" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-0.762" y1="-0.762" x2="-2.159" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-2.159" y1="-0.762" x2="-2.159" y2="0.762" width="0.127" layer="21"/>
<wire x1="0" y1="0.254" x2="-0.381" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="-0.254" x2="-0.381" y2="0" width="0.127" layer="21"/>
<wire x1="-0.381" y1="0" x2="0.381" y2="0" width="0.127" layer="21"/>
<wire x1="0.762" y1="-0.762" x2="-0.762" y2="-0.762" width="0.127" layer="21" curve="-180"/>
<pad name="E" x="-1.27" y="0" drill="0.8"/>
<pad name="C" x="1.27" y="0" drill="0.8"/>
</package>
</packages>
<symbols>
<symbol name="MPR121">
<wire x1="-12.7" y1="-15.24" x2="-12.7" y2="17.78" width="0.254" layer="94"/>
<wire x1="-12.7" y1="17.78" x2="12.7" y2="17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="17.78" x2="12.7" y2="-15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="-15.24" x2="-12.7" y2="-15.24" width="0.254" layer="94"/>
<text x="-12.7" y="18.034" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.954" y="-17.272" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VDD" x="-17.78" y="15.24" length="middle"/>
<pin name="VSS" x="-17.78" y="12.7" length="middle"/>
<pin name="VREG" x="-17.78" y="10.16" length="middle"/>
<pin name="~IRQ" x="-17.78" y="7.62" length="middle"/>
<pin name="SCL" x="-17.78" y="5.08" length="middle"/>
<pin name="SDA" x="-17.78" y="2.54" length="middle"/>
<pin name="ADDR" x="-17.78" y="0" length="middle"/>
<pin name="REXT" x="-17.78" y="-2.54" length="middle"/>
<pin name="ELE11/LED7" x="17.78" y="15.24" length="middle" rot="R180"/>
<pin name="ELE10/LED6" x="17.78" y="12.7" length="middle" rot="R180"/>
<pin name="ELE9/LED5" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="ELE8/LED4" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="ELE7/LED3" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="ELE6/LED2" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="ELE5/LED1" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="ELE4/LED0" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="ELE3" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="ELE2" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="ELE1" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="ELE0" x="17.78" y="-12.7" length="middle" rot="R180"/>
</symbol>
<symbol name="LTR-301">
<wire x1="0" y1="0" x2="-1.27" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="-1.27" y2="-3.302" width="0.254" layer="94"/>
<wire x1="0" y1="-4.318" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="-0.762" y2="-4.826" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-1.016" x2="-1.27" y2="-4.064" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.778" y2="-3.302" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-2.794" x2="-1.778" y2="-3.302" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-3.302" x2="-2.286" y2="-3.048" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="-1.778" y2="-2.032" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-1.524" x2="-1.778" y2="-2.032" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-2.032" x2="-2.286" y2="-1.778" width="0.254" layer="94"/>
<text x="1.27" y="-5.08" size="1.27" layer="94">&gt;VALUE</text>
<text x="1.27" y="-3.302" size="1.27" layer="94">&gt;NAME</text>
<pin name="C" x="0" y="2.54" visible="off" length="short" rot="R270"/>
<pin name="E" x="0" y="-7.62" visible="off" length="short" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MPR121">
<description>MPR121 Capacitive Touch Sensor Controller</description>
<gates>
<gate name="G$1" symbol="MPR121" x="0" y="0"/>
</gates>
<devices>
<device name="QFN20" package="QFN-20-0.4MM">
<connects>
<connect gate="G$1" pin="ADDR" pad="4"/>
<connect gate="G$1" pin="ELE0" pad="8"/>
<connect gate="G$1" pin="ELE1" pad="9"/>
<connect gate="G$1" pin="ELE10/LED6" pad="18"/>
<connect gate="G$1" pin="ELE11/LED7" pad="19"/>
<connect gate="G$1" pin="ELE2" pad="10"/>
<connect gate="G$1" pin="ELE3" pad="11"/>
<connect gate="G$1" pin="ELE4/LED0" pad="12"/>
<connect gate="G$1" pin="ELE5/LED1" pad="13"/>
<connect gate="G$1" pin="ELE6/LED2" pad="14"/>
<connect gate="G$1" pin="ELE7/LED3" pad="15"/>
<connect gate="G$1" pin="ELE8/LED4" pad="16"/>
<connect gate="G$1" pin="ELE9/LED5" pad="17"/>
<connect gate="G$1" pin="REXT" pad="7"/>
<connect gate="G$1" pin="SCL" pad="2"/>
<connect gate="G$1" pin="SDA" pad="3"/>
<connect gate="G$1" pin="VDD" pad="20"/>
<connect gate="G$1" pin="VREG" pad="5"/>
<connect gate="G$1" pin="VSS" pad="6"/>
<connect gate="G$1" pin="~IRQ" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LTR-301" prefix="Q" uservalue="yes">
<description>Through-hole side-receiving IR phototransistor</description>
<gates>
<gate name="G$1" symbol="LTR-301" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LTR-301">
<connects>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="gameboy camera">
<packages>
<package name="1X09">
<wire x1="14.605" y1="1.27" x2="15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="-1.27" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="18.415" y2="1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="1.27" x2="19.05" y2="0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="-1.27" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.685" y1="1.27" x2="20.955" y2="1.27" width="0.2032" layer="21"/>
<wire x1="20.955" y1="1.27" x2="21.59" y2="0.635" width="0.2032" layer="21"/>
<wire x1="21.59" y1="-0.635" x2="20.955" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.685" y1="1.27" x2="19.05" y2="0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="19.685" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="20.955" y1="-1.27" x2="19.685" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="21.59" y1="0.635" x2="21.59" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="9" x="20.32" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
<rectangle x1="20.066" y1="-0.254" x2="20.574" y2="0.254" layer="51"/>
</package>
<package name="1X09_LOCK">
<wire x1="14.605" y1="1.27" x2="15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="-1.27" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="18.415" y2="1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="1.27" x2="19.05" y2="0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="-1.27" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.685" y1="1.27" x2="20.955" y2="1.27" width="0.2032" layer="21"/>
<wire x1="20.955" y1="1.27" x2="21.59" y2="0.635" width="0.2032" layer="21"/>
<wire x1="21.59" y1="-0.635" x2="20.955" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.685" y1="1.27" x2="19.05" y2="0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="19.685" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="20.955" y1="-1.27" x2="19.685" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="21.59" y1="0.635" x2="21.59" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="9" x="20.32" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
<rectangle x1="20.066" y1="-0.254" x2="20.574" y2="0.254" layer="51"/>
</package>
<package name="1X09_LOCK_LONGPADS">
<wire x1="1.524" y1="0" x2="1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="4.064" y1="0" x2="3.556" y2="0" width="0.2032" layer="21"/>
<wire x1="6.604" y1="0" x2="6.096" y2="0" width="0.2032" layer="21"/>
<wire x1="9.144" y1="0" x2="8.636" y2="0" width="0.2032" layer="21"/>
<wire x1="11.684" y1="0" x2="11.176" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.9906" x2="-0.9906" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.9906" x2="-0.9906" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="14.224" y1="0" x2="13.716" y2="0" width="0.2032" layer="21"/>
<wire x1="16.764" y1="0" x2="16.256" y2="0" width="0.2032" layer="21"/>
<wire x1="19.304" y1="0" x2="18.796" y2="0" width="0.2032" layer="21"/>
<wire x1="21.59" y1="0" x2="21.59" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="21.59" y1="-0.9906" x2="21.3106" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="21.59" y1="0" x2="21.59" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="21.59" y1="0.9906" x2="21.3106" y2="1.27" width="0.2032" layer="21"/>
<wire x1="21.59" y1="0" x2="21.336" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="15.24" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="8" x="17.78" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="9" x="20.32" y="0.127" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.905" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<rectangle x1="4.7879" y1="-0.2921" x2="5.3721" y2="0.2921" layer="51"/>
<rectangle x1="7.3279" y1="-0.2921" x2="7.9121" y2="0.2921" layer="51" rot="R90"/>
<rectangle x1="9.8679" y1="-0.2921" x2="10.4521" y2="0.2921" layer="51"/>
<rectangle x1="12.4079" y1="-0.2921" x2="12.9921" y2="0.2921" layer="51"/>
<rectangle x1="14.9479" y1="-0.2921" x2="15.5321" y2="0.2921" layer="51"/>
<rectangle x1="17.4879" y1="-0.2921" x2="18.0721" y2="0.2921" layer="51"/>
<rectangle x1="20.0279" y1="-0.2921" x2="20.6121" y2="0.2921" layer="51"/>
</package>
<package name="1X09_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="21.59" y1="0.635" x2="21.59" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="9" x="20.32" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
<rectangle x1="20.066" y1="-0.254" x2="20.574" y2="0.254" layer="51"/>
</package>
<package name="1.27MM-TH">
<hole x="1.27" y="0" drill="0.5"/>
<hole x="2.54" y="0" drill="0.5"/>
<hole x="3.81" y="0" drill="0.5"/>
<hole x="5.08" y="0" drill="0.5"/>
<hole x="-1.27" y="0" drill="0.5"/>
<hole x="-2.54" y="0" drill="0.5"/>
<hole x="-3.81" y="0" drill="0.5"/>
<hole x="0" y="0" drill="0.5"/>
<pad name="P1" x="-3.81" y="0" drill="0.5" shape="square"/>
<pad name="P2" x="-2.54" y="0" drill="0.5"/>
<pad name="P3" x="-1.27" y="0" drill="0.5"/>
<pad name="P4" x="0" y="0" drill="0.5"/>
<pad name="P5" x="1.27" y="0" drill="0.5"/>
<pad name="P6" x="2.54" y="0" drill="0.5"/>
<pad name="P7" x="3.81" y="0" drill="0.5"/>
<pad name="P8" x="5.08" y="0" drill="0.5"/>
<hole x="6.35" y="0" drill="0.5"/>
<pad name="P9" x="6.35" y="0" drill="0.5"/>
</package>
<package name="1.5MM-SMD">
<smd name="P5" x="0" y="0" dx="0.85" dy="7" layer="1" rot="R90"/>
<smd name="P4" x="0" y="-1.5" dx="0.85" dy="7" layer="1" rot="R90"/>
<smd name="P3" x="0" y="-3" dx="0.85" dy="7" layer="1" rot="R90"/>
<smd name="P2" x="0" y="-4.5" dx="0.85" dy="7" layer="1" rot="R90"/>
<smd name="P6" x="0" y="1.5" dx="0.85" dy="7" layer="1" rot="R90"/>
<smd name="P1" x="0" y="-6" dx="0.85" dy="7" layer="1" rot="R90"/>
<smd name="P7" x="0" y="3" dx="0.85" dy="7" layer="1" rot="R90"/>
<smd name="P8" x="0" y="4.5" dx="0.85" dy="7" layer="1" rot="R90"/>
<smd name="P9" x="0" y="6" dx="0.85" dy="7" layer="1" rot="R90"/>
</package>
<package name="1.5MM-TH">
<hole x="0" y="0" drill="0.6"/>
<hole x="-1.5" y="0" drill="0.6"/>
<hole x="-3" y="0" drill="0.6"/>
<hole x="-4.5" y="0" drill="0.6"/>
<hole x="-6" y="0" drill="0.6"/>
<hole x="1.5" y="0" drill="0.6"/>
<hole x="3" y="0" drill="0.6"/>
<hole x="4.5" y="0" drill="0.6"/>
<hole x="6" y="0" drill="0.6"/>
<pad name="P1" x="-6" y="0" drill="0.6" shape="square"/>
<pad name="P2" x="-4.5" y="0" drill="0.6"/>
<pad name="P3" x="-3" y="0" drill="0.6"/>
<pad name="P4" x="-1.5" y="0" drill="0.6"/>
<pad name="P5" x="0" y="0" drill="0.6"/>
<pad name="P6" x="1.5" y="0" drill="0.6"/>
<pad name="P7" x="3" y="0" drill="0.6"/>
<pad name="P8" x="4.5" y="0" drill="0.6"/>
<pad name="P9" x="6" y="0" drill="0.6"/>
</package>
</packages>
<symbols>
<symbol name="M09">
<wire x1="6.35" y1="-12.7" x2="0" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="5.08" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="5.08" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-10.16" x2="5.08" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="0" y1="12.7" x2="0" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-12.7" x2="6.35" y2="12.7" width="0.4064" layer="94"/>
<wire x1="0" y1="12.7" x2="6.35" y2="12.7" width="0.4064" layer="94"/>
<wire x1="3.81" y1="0" x2="5.08" y2="0" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="5.08" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.6096" layer="94"/>
<wire x1="3.81" y1="5.08" x2="5.08" y2="5.08" width="0.6096" layer="94"/>
<wire x1="3.81" y1="7.62" x2="5.08" y2="7.62" width="0.6096" layer="94"/>
<wire x1="3.81" y1="10.16" x2="5.08" y2="10.16" width="0.6096" layer="94"/>
<text x="0" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
<text x="0" y="16.51" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="10.16" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="10.16" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="10.16" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="10.16" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="10.16" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="10.16" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="10.16" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="10.16" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="10.16" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="M09" prefix="JP">
<description>&lt;b&gt;Header 9&lt;/b&gt;
Standard 9-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115).</description>
<gates>
<gate name="G$1" symbol="M09" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X09">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X09_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK-LONGPADS" package="1X09_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X09_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMALLER" package="1.27MM-TH">
<connects>
<connect gate="G$1" pin="1" pad="P1"/>
<connect gate="G$1" pin="2" pad="P2"/>
<connect gate="G$1" pin="3" pad="P3"/>
<connect gate="G$1" pin="4" pad="P4"/>
<connect gate="G$1" pin="5" pad="P5"/>
<connect gate="G$1" pin="6" pad="P6"/>
<connect gate="G$1" pin="7" pad="P7"/>
<connect gate="G$1" pin="8" pad="P8"/>
<connect gate="G$1" pin="9" pad="P9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1.5MM" package="1.27MM-TH">
<connects>
<connect gate="G$1" pin="1" pad="P1"/>
<connect gate="G$1" pin="2" pad="P2"/>
<connect gate="G$1" pin="3" pad="P3"/>
<connect gate="G$1" pin="4" pad="P4"/>
<connect gate="G$1" pin="5" pad="P5"/>
<connect gate="G$1" pin="6" pad="P6"/>
<connect gate="G$1" pin="7" pad="P7"/>
<connect gate="G$1" pin="8" pad="P8"/>
<connect gate="G$1" pin="9" pad="P9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1.5MM-SMD" package="1.5MM-SMD">
<connects>
<connect gate="G$1" pin="1" pad="P1"/>
<connect gate="G$1" pin="2" pad="P2"/>
<connect gate="G$1" pin="3" pad="P3"/>
<connect gate="G$1" pin="4" pad="P4"/>
<connect gate="G$1" pin="5" pad="P5"/>
<connect gate="G$1" pin="6" pad="P6"/>
<connect gate="G$1" pin="7" pad="P7"/>
<connect gate="G$1" pin="8" pad="P8"/>
<connect gate="G$1" pin="9" pad="P9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1.5MM-TH" package="1.5MM-TH">
<connects>
<connect gate="G$1" pin="1" pad="P1"/>
<connect gate="G$1" pin="2" pad="P2"/>
<connect gate="G$1" pin="3" pad="P3"/>
<connect gate="G$1" pin="4" pad="P4"/>
<connect gate="G$1" pin="5" pad="P5"/>
<connect gate="G$1" pin="6" pad="P6"/>
<connect gate="G$1" pin="7" pad="P7"/>
<connect gate="G$1" pin="8" pad="P8"/>
<connect gate="G$1" pin="9" pad="P9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="relay">
<description>&lt;b&gt;Relays&lt;/b&gt;&lt;p&gt;
&lt;ul&gt;
&lt;li&gt;Eichhoff
&lt;li&gt;Finder
&lt;li&gt;Fujitsu
&lt;li&gt;HAMLIN
&lt;li&gt;OMRON
&lt;li&gt;Matsushita
&lt;li&gt;NAiS
&lt;li&gt;Siemens
&lt;li&gt;Schrack
&lt;/ul&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SOP04">
<description>&lt;b&gt;SOP 04 - 4.3x4.4mm RM 2.54mm&lt;/b&gt; AQY21, AQY22, AQY61 Series NAiS&lt;p&gt;Source: http://www.mew-europe.com/..  pti_en.pdf</description>
<wire x1="-2" y1="2.075" x2="2" y2="2.075" width="0.254" layer="21"/>
<wire x1="2" y1="2.075" x2="2" y2="-2.075" width="0.254" layer="21"/>
<wire x1="2" y1="-2.075" x2="-2" y2="-2.075" width="0.254" layer="21"/>
<wire x1="-2" y1="-2.075" x2="-2" y2="2.075" width="0.254" layer="21"/>
<circle x="-1.27" y="-1.524" radius="0.254" width="0" layer="21"/>
<smd name="1" x="-1.27" y="-3" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="1.27" y="-3" dx="0.8" dy="1.2" layer="1"/>
<smd name="3" x="1.27" y="3" dx="0.8" dy="1.2" layer="1"/>
<smd name="4" x="-1.27" y="3" dx="0.8" dy="1.2" layer="1"/>
<text x="-2.54" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.5" y1="-3.35" x2="-1" y2="-2.1" layer="51"/>
<rectangle x1="1.04" y1="-3.35" x2="1.54" y2="-2.1" layer="51"/>
<rectangle x1="1" y1="2.1" x2="1.5" y2="3.35" layer="51"/>
<rectangle x1="-1.54" y1="2.1" x2="-1.04" y2="3.35" layer="51"/>
</package>
<package name="AQY27-SMD04">
<description>&lt;b&gt;SMD 04 9.4x8.8mm RM 5.08mm&lt;/b&gt; NAiS&lt;p&gt;
Source: http://www.mew-europe.com/..  pti_en.pdf</description>
<wire x1="-4.55" y1="4.3" x2="-3.302" y2="4.3" width="0.2032" layer="21"/>
<wire x1="-1.778" y1="4.3" x2="1.778" y2="4.3" width="0.2032" layer="21"/>
<wire x1="3.302" y1="4.3" x2="4.55" y2="4.3" width="0.2032" layer="21"/>
<wire x1="4.55" y1="4.3" x2="4.55" y2="-4.3" width="0.2032" layer="21"/>
<wire x1="-4.55" y1="-4.3" x2="-4.55" y2="4.3" width="0.2032" layer="21"/>
<wire x1="-3.302" y1="4.3" x2="-1.778" y2="4.3" width="0.2032" layer="51"/>
<wire x1="1.778" y1="4.3" x2="3.302" y2="4.3" width="0.2032" layer="51"/>
<wire x1="4.55" y1="-4.3" x2="3.302" y2="-4.3" width="0.2032" layer="21"/>
<wire x1="1.778" y1="-4.3" x2="-1.778" y2="-4.3" width="0.2032" layer="21"/>
<wire x1="-3.302" y1="-4.3" x2="-4.55" y2="-4.3" width="0.2032" layer="21"/>
<wire x1="3.302" y1="-4.3" x2="1.778" y2="-4.3" width="0.2032" layer="51"/>
<wire x1="-1.778" y1="-4.3" x2="-3.302" y2="-4.3" width="0.2032" layer="51"/>
<circle x="-2.54" y="-2.794" radius="0.508" width="0" layer="21"/>
<smd name="1" x="-2.54" y="-5.5" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="2.54" y="-5.5" dx="1.8" dy="1.8" layer="1"/>
<smd name="3" x="2.54" y="5.5" dx="1.8" dy="1.8" layer="1"/>
<smd name="4" x="-2.54" y="5.5" dx="1.8" dy="1.8" layer="1"/>
<text x="-4.9164" y="-4.445" size="1.397" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="6.2626" y="-4.445" size="1.397" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.921" y1="4.318" x2="-2.159" y2="5.461" layer="51"/>
<rectangle x1="2.159" y1="4.318" x2="2.921" y2="5.461" layer="51"/>
<rectangle x1="2.159" y1="-5.461" x2="2.921" y2="-4.318" layer="51"/>
<rectangle x1="-2.921" y1="-5.461" x2="-2.159" y2="-4.318" layer="51"/>
</package>
<package name="AQY27">
<description>&lt;b&gt;DIL 04&lt;/b&gt; NAiS&lt;p&gt;
Source: http://www.mew-europe.com/..  pti_en.pdf</description>
<wire x1="-4.55" y1="4.3" x2="-3.302" y2="4.3" width="0.2032" layer="21"/>
<wire x1="-1.778" y1="4.3" x2="1.778" y2="4.3" width="0.2032" layer="21"/>
<wire x1="3.302" y1="4.3" x2="4.55" y2="4.3" width="0.2032" layer="21"/>
<wire x1="4.55" y1="4.3" x2="4.55" y2="-4.3" width="0.2032" layer="21"/>
<wire x1="-4.55" y1="-4.3" x2="-4.55" y2="4.3" width="0.2032" layer="21"/>
<wire x1="-3.302" y1="4.3" x2="-1.778" y2="4.3" width="0.2032" layer="51"/>
<wire x1="1.778" y1="4.3" x2="3.302" y2="4.3" width="0.2032" layer="51"/>
<wire x1="4.55" y1="-4.3" x2="3.302" y2="-4.3" width="0.2032" layer="21"/>
<wire x1="1.778" y1="-4.3" x2="-1.778" y2="-4.3" width="0.2032" layer="21"/>
<wire x1="-3.302" y1="-4.3" x2="-4.55" y2="-4.3" width="0.2032" layer="21"/>
<wire x1="3.302" y1="-4.3" x2="1.778" y2="-4.3" width="0.2032" layer="51"/>
<wire x1="-1.778" y1="-4.3" x2="-3.302" y2="-4.3" width="0.2032" layer="51"/>
<circle x="-2.54" y="-2.794" radius="0.508" width="0" layer="21"/>
<pad name="1" x="-2.54" y="-5.08" drill="0.8" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-5.08" drill="0.8" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="5.08" drill="0.8" shape="long" rot="R90"/>
<pad name="4" x="-2.54" y="5.08" drill="0.8" shape="long" rot="R90"/>
<text x="-4.9164" y="-4.445" size="1.397" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="6.2626" y="-4.445" size="1.397" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.921" y1="4.572" x2="-2.159" y2="5.08" layer="51"/>
<rectangle x1="2.159" y1="4.572" x2="2.921" y2="5.08" layer="51"/>
<rectangle x1="2.159" y1="-5.08" x2="2.921" y2="-4.572" layer="51"/>
<rectangle x1="-2.921" y1="-5.08" x2="-2.159" y2="-4.572" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="PHOTOMOS_2D">
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.842" y1="2.794" x2="5.842" y2="2.286" width="0.1524" layer="94"/>
<wire x1="5.842" y1="1.778" x2="5.842" y2="1.524" width="0.1524" layer="94"/>
<wire x1="5.842" y1="1.524" x2="5.842" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.842" y1="0.762" x2="5.842" y2="0.508" width="0.1524" layer="94"/>
<wire x1="5.842" y1="0.508" x2="5.842" y2="0.254" width="0.1524" layer="94"/>
<wire x1="5.842" y1="1.524" x2="6.604" y2="1.524" width="0.1524" layer="94"/>
<wire x1="6.35" y1="1.778" x2="5.842" y2="1.524" width="0.1524" layer="94"/>
<wire x1="5.842" y1="1.524" x2="6.35" y2="1.27" width="0.1524" layer="94"/>
<wire x1="6.35" y1="1.27" x2="6.35" y2="1.778" width="0.1524" layer="94"/>
<wire x1="5.842" y1="0.508" x2="6.604" y2="0.508" width="0.1524" layer="94"/>
<wire x1="6.604" y1="0.508" x2="6.604" y2="1.524" width="0.1524" layer="94"/>
<wire x1="5.334" y1="2.794" x2="5.334" y2="1.524" width="0.1524" layer="94"/>
<wire x1="5.334" y1="1.524" x2="5.334" y2="0.254" width="0.1524" layer="94"/>
<wire x1="5.334" y1="1.524" x2="4.572" y2="1.524" width="0.1524" layer="94"/>
<wire x1="5.842" y1="-2.794" x2="5.842" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="5.842" y1="-1.778" x2="5.842" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="5.842" y1="-1.524" x2="5.842" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="5.842" y1="-0.762" x2="5.842" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="5.842" y1="-0.508" x2="5.842" y2="-0.254" width="0.1524" layer="94"/>
<wire x1="5.842" y1="-1.524" x2="6.604" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-1.778" x2="5.842" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="5.842" y1="-1.524" x2="6.35" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-1.27" x2="6.35" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="5.842" y1="-0.508" x2="6.604" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="6.604" y1="-0.508" x2="6.604" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="6.604" y1="0.508" x2="6.604" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="5.334" y1="-2.794" x2="5.334" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="5.334" y1="-1.524" x2="5.334" y2="-0.254" width="0.1524" layer="94"/>
<wire x1="5.334" y1="-1.524" x2="4.572" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="4.572" y1="1.524" x2="4.572" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.842" y1="2.54" x2="7.874" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.842" y1="-2.54" x2="7.62" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-7.62" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-7.62" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0.762" x2="-0.254" y2="-0.508" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-0.508" x2="-0.254" y2="1.27" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="1.27" x2="2.794" y2="-0.762" width="0.2032" layer="94"/>
<text x="-7.62" y="6.35" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-7.62" y="-7.62" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-6.35" y1="-1.016" x2="-3.81" y2="-0.762" layer="94"/>
<pin name="A" x="-10.16" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="K" x="-10.16" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="DS1" x="10.16" y="2.54" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="DS2" x="10.16" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<polygon width="0.1524" layer="94">
<vertex x="-6.35" y="1.016"/>
<vertex x="-3.81" y="1.016"/>
<vertex x="-5.08" y="-0.762"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="1.778" y="0.508"/>
<vertex x="1.27" y="-0.254"/>
<vertex x="2.794" y="-0.762"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="AQY*" prefix="K">
<description>&lt;b&gt;PhotoMOS Relay&lt;/b&gt; NAiS&lt;p&gt;
Source: http://www.panasonic-electric-works.com/catalogues/downloads/photomos/ds_x615_en_aqz10_20.pdf</description>
<gates>
<gate name="G$1" symbol="PHOTOMOS_2D" x="0" y="0"/>
</gates>
<devices>
<device name="SOP" package="SOP04">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="DS1" pad="4"/>
<connect gate="G$1" pin="DS2" pad="3"/>
<connect gate="G$1" pin="K" pad="2"/>
</connects>
<technologies>
<technology name="21">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="22">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="41">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD" package="AQY27-SMD04">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="DS1" pad="4"/>
<connect gate="G$1" pin="DS2" pad="3"/>
<connect gate="G$1" pin="K" pad="2"/>
</connects>
<technologies>
<technology name="27">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="AQY27" package="AQY27">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="DS1" pad="4"/>
<connect gate="G$1" pin="DS2" pad="3"/>
<connect gate="G$1" pin="K" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="st-microelectronics">
<description>&lt;b&gt;ST Microelectronics Devices&lt;/b&gt;&lt;p&gt;
Microcontrollers,  I2C components, linear devices&lt;p&gt;
http://www.st.com&lt;p&gt;
&lt;i&gt;Include st-microelectronics-2.lbr, st-microelectronics-3.lbr.&lt;p&gt;&lt;/i&gt;

&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL16">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="10.16" y1="2.921" x2="-10.16" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="10.16" y1="2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="2.921" x2="-10.16" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="-10.16" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.016" x2="-10.16" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="-6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="-8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-10.541" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-7.493" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="L293D">
<wire x1="-10.16" y1="20.32" x2="-10.16" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-20.32" x2="10.16" y2="-20.32" width="0.254" layer="94"/>
<wire x1="10.16" y1="-20.32" x2="10.16" y2="20.32" width="0.254" layer="94"/>
<wire x1="10.16" y1="20.32" x2="-10.16" y2="20.32" width="0.254" layer="94"/>
<text x="-10.16" y="21.336" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-22.86" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1-2EN" x="-15.24" y="17.78" length="middle" direction="in"/>
<pin name="1A" x="-15.24" y="12.7" length="middle" direction="in"/>
<pin name="1Y" x="-15.24" y="7.62" length="middle" direction="out"/>
<pin name="GND1" x="-15.24" y="2.54" length="middle" direction="pwr"/>
<pin name="GND2" x="-15.24" y="-2.54" length="middle" direction="pwr"/>
<pin name="2Y" x="-15.24" y="-7.62" length="middle" direction="out"/>
<pin name="2A" x="-15.24" y="-12.7" length="middle" direction="in"/>
<pin name="VCC2" x="-15.24" y="-17.78" length="middle" direction="pwr"/>
<pin name="VCC1" x="15.24" y="17.78" length="middle" direction="pwr" rot="R180"/>
<pin name="4A" x="15.24" y="12.7" length="middle" direction="in" rot="R180"/>
<pin name="4Y" x="15.24" y="7.62" length="middle" direction="out" rot="R180"/>
<pin name="GND3" x="15.24" y="2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="GND4" x="15.24" y="-2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="3Y" x="15.24" y="-7.62" length="middle" direction="out" rot="R180"/>
<pin name="3A" x="15.24" y="-12.7" length="middle" direction="in" rot="R180"/>
<pin name="3-4EN" x="15.24" y="-17.78" length="middle" direction="in" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="L293D" prefix="IC">
<description>&lt;b&gt;PUSH-PULL 4 CHANNEL DRIVER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="L293D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DIL16">
<connects>
<connect gate="G$1" pin="1-2EN" pad="1"/>
<connect gate="G$1" pin="1A" pad="2"/>
<connect gate="G$1" pin="1Y" pad="3"/>
<connect gate="G$1" pin="2A" pad="7"/>
<connect gate="G$1" pin="2Y" pad="6"/>
<connect gate="G$1" pin="3-4EN" pad="9"/>
<connect gate="G$1" pin="3A" pad="10"/>
<connect gate="G$1" pin="3Y" pad="11"/>
<connect gate="G$1" pin="4A" pad="15"/>
<connect gate="G$1" pin="4Y" pad="14"/>
<connect gate="G$1" pin="GND1" pad="4"/>
<connect gate="G$1" pin="GND2" pad="5"/>
<connect gate="G$1" pin="GND3" pad="13"/>
<connect gate="G$1" pin="GND4" pad="12"/>
<connect gate="G$1" pin="VCC1" pad="16"/>
<connect gate="G$1" pin="VCC2" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="L293D" constant="no"/>
<attribute name="OC_FARNELL" value="9589619" constant="no"/>
<attribute name="OC_NEWARK" value="56P8249" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-stocko">
<description>&lt;b&gt;STOCKO Connector&lt;/b&gt;&lt;p&gt;
http://www.stocko.de&lt;br&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MKS1854-6-0-404">
<description>&lt;b&gt;Serie MKS 1850&lt;/b&gt; Lötstifte In-line, Raster 2.5 mm, Stiftwannen, stehend, mit Rastlasche&lt;p&gt;
Source: http://www.stocko.de/de/index.php?seite=ka_rfk2_mks1850.html&amp;rubrik=Katalog+-+RFK+2&lt;br&gt;
rfk_1850.pdf</description>
<wire x1="-5.2283" y1="-3.6954" x2="-5.9912" y2="-3.6954" width="0.2032" layer="21"/>
<wire x1="-5.9912" y1="-3.6954" x2="-7.231" y2="-2.4556" width="0.2032" layer="21" curve="-90"/>
<wire x1="-7.231" y1="-2.4556" x2="-7.231" y2="2.4558" width="0.2032" layer="21"/>
<wire x1="-7.231" y1="2.4558" x2="-5.9912" y2="3.6956" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5.9912" y1="3.6956" x2="5.9672" y2="3.6956" width="0.2032" layer="21"/>
<wire x1="5.9672" y1="3.6956" x2="7.2547" y2="2.4081" width="0.2032" layer="21" curve="-90"/>
<wire x1="7.2547" y1="2.4081" x2="7.2547" y2="-2.4079" width="0.2032" layer="21"/>
<wire x1="7.2547" y1="-2.4079" x2="5.9672" y2="-3.6954" width="0.2032" layer="21" curve="-90"/>
<wire x1="5.9672" y1="-3.6954" x2="5.2281" y2="-3.6954" width="0.2032" layer="21"/>
<wire x1="5.2281" y1="-3.6954" x2="5.2281" y2="-2.1695" width="0.2032" layer="21"/>
<wire x1="5.2281" y1="-2.1695" x2="4.7036" y2="-1.645" width="0.2032" layer="21" curve="90"/>
<wire x1="4.7036" y1="-1.645" x2="-4.7038" y2="-1.645" width="0.2032" layer="21"/>
<wire x1="-4.7038" y1="-1.645" x2="-5.2283" y2="-2.1695" width="0.2032" layer="21" curve="90"/>
<wire x1="-5.2283" y1="-2.1695" x2="-5.2283" y2="-3.6954" width="0.2032" layer="21"/>
<wire x1="-5.2521" y1="-2.6702" x2="-5.9674" y2="-2.6702" width="0.2032" layer="21"/>
<wire x1="-5.9674" y1="-2.6702" x2="-6.325" y2="-2.3126" width="0.2032" layer="21" curve="-90"/>
<wire x1="-6.325" y1="-2.3126" x2="-6.325" y2="2.3605" width="0.2032" layer="21"/>
<wire x1="-6.325" y1="2.3605" x2="-6.0151" y2="2.6704" width="0.2032" layer="21" curve="-90"/>
<wire x1="-6.0151" y1="2.6704" x2="5.9911" y2="2.6704" width="0.2032" layer="21"/>
<wire x1="5.9911" y1="2.6704" x2="6.3249" y2="2.3366" width="0.2032" layer="21" curve="-90"/>
<wire x1="6.3249" y1="2.3366" x2="6.3249" y2="-2.3364" width="0.2032" layer="21"/>
<wire x1="6.3249" y1="-2.3364" x2="5.9911" y2="-2.6702" width="0.2032" layer="21" curve="-90"/>
<wire x1="5.9911" y1="-2.6702" x2="5.3235" y2="-2.6702" width="0.2032" layer="21"/>
<wire x1="-3.481" y1="3.6479" x2="-3.481" y2="2.7419" width="0.2032" layer="21"/>
<wire x1="3.457" y1="3.6241" x2="3.457" y2="2.7419" width="0.2032" layer="21"/>
<wire x1="2.9563" y1="3.6717" x2="2.9563" y2="3.1472" width="0.2032" layer="21"/>
<wire x1="2.9563" y1="3.1472" x2="-2.9803" y2="3.1472" width="0.2032" layer="21"/>
<wire x1="-2.9803" y1="3.1472" x2="-2.9803" y2="3.6241" width="0.2032" layer="21"/>
<wire x1="-3.0042" y1="3.1234" x2="-3.3856" y2="2.7419" width="0.2032" layer="21"/>
<wire x1="2.9802" y1="3.1234" x2="3.314" y2="2.7658" width="0.2032" layer="21"/>
<wire x1="-5.0853" y1="-1.7165" x2="-5.0853" y2="1.4544" width="0.2032" layer="21"/>
<wire x1="-5.0853" y1="1.4544" x2="5.1089" y2="1.4544" width="0.2032" layer="21"/>
<wire x1="5.1089" y1="1.4544" x2="5.1089" y2="-1.7404" width="0.2032" layer="21"/>
<pad name="1" x="-3.75" y="0" drill="1" diameter="1.4224"/>
<pad name="2" x="-1.25" y="0" drill="1" diameter="1.4224"/>
<pad name="3" x="1.25" y="0" drill="1" diameter="1.4224"/>
<pad name="4" x="3.75" y="0" drill="1" diameter="1.4224"/>
<text x="-3.81" y="-3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="4.445" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="PIN4">
<wire x1="-2.54" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="-2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-10.16" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<text x="-2.54" y="3.81" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-2.54" y="-12.7" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="pin" length="short" direction="in"/>
<pin name="2" x="-5.08" y="-2.54" visible="pin" length="short" direction="in"/>
<pin name="3" x="-5.08" y="-5.08" visible="pin" length="short" direction="in"/>
<pin name="4" x="-5.08" y="-7.62" visible="pin" length="short" direction="in"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MKS1854-6-0-404" prefix="X">
<description>&lt;b&gt;Serie MKS 1850&lt;/b&gt; Lötstifte In-line, Raster 2.5 mm, Stiftwannen, stehend, mit Rastlasche&lt;p&gt;
Source: http://www.stocko.de/de/index.php?seite=ka_rfk2_mks1850.html&amp;rubrik=Katalog+-+RFK+2&lt;br&gt;
rfk_1850.pdf</description>
<gates>
<gate name="G$1" symbol="PIN4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MKS1854-6-0-404">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="1141862" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rectifier">
<description>&lt;b&gt;Rectifiers&lt;/b&gt;&lt;p&gt;
General Instrument, Semikron, Diotec, Fagor&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SOIC-4">
<description>&lt;b&gt;SOIC-4 Package&lt;/b&gt;&lt;p&gt;
Source: http://www.fairchildsemi.com/products/discrete/pdf/soic4_dim.pdf</description>
<wire x1="-2.35" y1="2" x2="2.35" y2="2" width="0.2032" layer="21"/>
<wire x1="2.35" y1="2" x2="2.35" y2="-2" width="0.2032" layer="21"/>
<wire x1="2.35" y1="-2" x2="-2.35" y2="-2" width="0.2032" layer="21"/>
<wire x1="-2.35" y1="-2" x2="-2.35" y2="2" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="0.97" x2="-1.27" y2="0.97" width="0.1016" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.635" width="0.1016" layer="21"/>
<wire x1="0.955" y1="0.945" x2="1.59" y2="0.945" width="0.1016" layer="21"/>
<wire x1="-1.9" y1="-1.275" x2="-1.6" y2="-1.275" width="0.1016" layer="21" curve="-99.939481"/>
<wire x1="-1.3" y1="-1.275" x2="-1.6" y2="-1.275" width="0.1016" layer="21" curve="-99.939481"/>
<wire x1="1.275" y1="-1.275" x2="1.575" y2="-1.275" width="0.1016" layer="21" curve="-99.939481"/>
<wire x1="1.875" y1="-1.275" x2="1.575" y2="-1.275" width="0.1016" layer="21" curve="-99.939481"/>
<smd name="-" x="-1.3" y="3.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="+" x="1.3" y="3.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="~2" x="1.3" y="-3.05" dx="1.2" dy="1.2" layer="1" rot="R180"/>
<smd name="~1" x="-1.3" y="-3.05" dx="1.2" dy="1.2" layer="1" rot="R180"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.75" y1="2.05" x2="-0.95" y2="3.45" layer="51"/>
<rectangle x1="0.95" y1="2.05" x2="1.75" y2="3.45" layer="51"/>
<rectangle x1="0.95" y1="-3.45" x2="1.75" y2="-2.05" layer="51" rot="R180"/>
<rectangle x1="-1.75" y1="-3.45" x2="-0.95" y2="-2.05" layer="51" rot="R180"/>
<rectangle x1="-2.05" y1="1.5" x2="2.05" y2="1.8" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="DB">
<wire x1="-1.905" y1="-3.175" x2="-4.064" y2="-2.794" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-3.175" x2="-2.286" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-2.7178" y1="-4.0386" x2="-0.9398" y2="-2.2606" width="0.254" layer="94"/>
<wire x1="-1.905" y1="3.175" x2="-4.064" y2="2.794" width="0.254" layer="94"/>
<wire x1="-1.905" y1="3.175" x2="-2.286" y2="1.016" width="0.254" layer="94"/>
<wire x1="-2.7178" y1="4.0386" x2="-1.0668" y2="2.3876" width="0.254" layer="94"/>
<wire x1="3.175" y1="1.905" x2="2.794" y2="4.064" width="0.254" layer="94"/>
<wire x1="3.175" y1="1.905" x2="1.016" y2="2.286" width="0.254" layer="94"/>
<wire x1="2.3622" y1="1.016" x2="4.1402" y2="2.794" width="0.254" layer="94"/>
<wire x1="3.175" y1="-1.905" x2="2.794" y2="-4.064" width="0.254" layer="94"/>
<wire x1="3.175" y1="-1.905" x2="1.016" y2="-2.286" width="0.254" layer="94"/>
<wire x1="2.3622" y1="-1.0668" x2="4.1402" y2="-2.8448" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="3.175" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="-1.905" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="-3.2766" y1="-1.8034" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.064" y1="-2.794" x2="-2.286" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.2766" y2="1.8034" width="0.1524" layer="94"/>
<wire x1="-4.064" y1="2.794" x2="-2.286" y2="1.016" width="0.254" layer="94"/>
<wire x1="-1.905" y1="3.175" x2="0" y2="5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="5.08" x2="1.8034" y2="3.2766" width="0.1524" layer="94"/>
<wire x1="1.016" y1="2.286" x2="2.794" y2="4.064" width="0.254" layer="94"/>
<wire x1="3.175" y1="1.905" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="1.8034" y2="-3.2766" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-2.286" x2="2.794" y2="-4.064" width="0.254" layer="94"/>
<text x="5.08" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<pin name="AC1" x="0" y="5.08" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="+" x="5.08" y="0" visible="off" length="point" direction="pas" rot="R180"/>
<pin name="AC2" x="0" y="-5.08" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="-" x="-5.08" y="0" visible="off" length="point" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MB*S" prefix="B">
<description>&lt;b&gt;Bridge Rectifier&lt;/b&gt;&lt;p&gt;
Source: http://www.fairchildsemi.com/ds/MB/MB6S.pdf</description>
<gates>
<gate name="G$1" symbol="DB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC-4">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
<connect gate="G$1" pin="AC1" pad="~1"/>
<connect gate="G$1" pin="AC2" pad="~2"/>
</connects>
<technologies>
<technology name="1"/>
<technology name="2"/>
<technology name="4"/>
<technology name="6"/>
<technology name="8"/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="surface-mount-headers">
<packages>
<package name="1X04">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="0" y1="1.27" x2="0.635" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.905" x2="1.905" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.905" x2="2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.905" x2="-3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-1.905" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.905" x2="-0.635" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.905" x2="0" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.905" x2="-5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.905" x2="4.445" y2="1.905" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.905" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.905" x2="2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<text x="-5.1562" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<smd name="1" x="-3.81" y="0" dx="1.27" dy="3.175" layer="1" rot="R180"/>
<smd name="2" x="-1.27" y="0" dx="1.27" dy="3.175" layer="1" rot="R180"/>
<smd name="3" x="1.27" y="0" dx="1.27" dy="3.175" layer="1" rot="R180"/>
<smd name="4" x="3.81" y="0" dx="1.27" dy="3.175" layer="1" rot="R180"/>
</package>
<package name="1X04/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-3.81" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-5.715" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="6.985" y="-4.445" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
</package>
<package name="1X03">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.175" y1="1.905" x2="-1.905" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.905" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-0.635" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.905" x2="0.635" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.905" x2="1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="-3.81" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.905" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.905" x2="3.81" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<text x="-3.8862" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<smd name="1" x="-2.54" y="0" dx="1.27" dy="3.175" layer="1"/>
<smd name="2" x="0" y="0" dx="1.27" dy="3.175" layer="1"/>
<smd name="3" x="2.54" y="0" dx="1.27" dy="3.175" layer="1"/>
</package>
<package name="1X03/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.81" y1="-1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="6.985" x2="-2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="6.985" x2="0" y2="1.27" width="0.762" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="6.985" x2="2.54" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="0" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-4.445" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="5.715" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.921" y1="0.635" x2="-2.159" y2="1.143" layer="21"/>
<rectangle x1="-0.381" y1="0.635" x2="0.381" y2="1.143" layer="21"/>
<rectangle x1="2.159" y1="0.635" x2="2.921" y2="1.143" layer="21"/>
<rectangle x1="-2.921" y1="-2.921" x2="-2.159" y2="-1.905" layer="21"/>
<rectangle x1="-0.381" y1="-2.921" x2="0.381" y2="-1.905" layer="21"/>
<rectangle x1="2.159" y1="-2.921" x2="2.921" y2="-1.905" layer="21"/>
</package>
<package name="1X05">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="1.905" y1="1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.905" x2="3.81" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-0.635" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.905" x2="0.635" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.905" x2="1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.905" x2="1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.905" x2="-4.445" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.905" x2="-3.81" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.27" x2="-3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="-1.905" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.905" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="1.27" x2="-6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.905" x2="-6.35" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.905" x2="5.715" y2="1.905" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.905" x2="6.35" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.27" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.905" x2="3.81" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<text x="-6.4262" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<smd name="1" x="-5.08" y="0" dx="1.27" dy="3.175" layer="1"/>
<smd name="2" x="-2.54" y="0" dx="1.27" dy="3.175" layer="1"/>
<smd name="3" x="0" y="0" dx="1.27" dy="3.175" layer="1"/>
<smd name="4" x="2.54" y="0" dx="1.27" dy="3.175" layer="1"/>
<smd name="5" x="5.08" y="0" dx="1.27" dy="3.175" layer="1"/>
</package>
<package name="1X05/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-6.35" y1="-1.905" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="6.985" x2="-5.08" y2="1.27" width="0.762" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="6.985" x2="-2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="6.985" x2="0" y2="1.27" width="0.762" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="6.985" x2="2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-1.905" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0.635" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="6.985" x2="5.08" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-5.08" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="0" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="5.08" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-6.985" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="8.255" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-5.461" y1="0.635" x2="-4.699" y2="1.143" layer="21"/>
<rectangle x1="-2.921" y1="0.635" x2="-2.159" y2="1.143" layer="21"/>
<rectangle x1="-0.381" y1="0.635" x2="0.381" y2="1.143" layer="21"/>
<rectangle x1="2.159" y1="0.635" x2="2.921" y2="1.143" layer="21"/>
<rectangle x1="4.699" y1="0.635" x2="5.461" y2="1.143" layer="21"/>
<rectangle x1="-5.461" y1="-2.921" x2="-4.699" y2="-1.905" layer="21"/>
<rectangle x1="-2.921" y1="-2.921" x2="-2.159" y2="-1.905" layer="21"/>
<rectangle x1="-0.381" y1="-2.921" x2="0.381" y2="-1.905" layer="21"/>
<rectangle x1="2.159" y1="-2.921" x2="2.921" y2="-1.905" layer="21"/>
<rectangle x1="4.699" y1="-2.921" x2="5.461" y2="-1.905" layer="21"/>
</package>
<package name="1X02">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-1.905" y1="1.905" x2="-0.635" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.905" x2="0" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.905" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.27" x2="0.635" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.905" x2="1.905" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.905" x2="2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<text x="-2.6162" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<smd name="1" x="-1.27" y="0" dx="1.27" dy="3.175" layer="1" rot="R180"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="3.175" layer="1" rot="R180"/>
</package>
<package name="1X02/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-3.175" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="4.445" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="PINHD4">
<wire x1="-6.35" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="7.62" x2="-6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINHD3">
<wire x1="-6.35" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINHD5">
<wire x1="-6.35" y1="-7.62" x2="1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="7.62" x2="-6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-7.62" width="0.4064" layer="94"/>
<text x="-6.35" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINHD2">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X4" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X04">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X04/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X3" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X03">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X03/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X5" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD5" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X05">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X05/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X2" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X02/90">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="XMEGA" library="Droplet" deviceset="XMEGA_A3" device="L"/>
<part name="C1" library="Droplet" deviceset="CAPACITOR" device="0402" value=".1uF"/>
<part name="C2" library="Droplet" deviceset="CAPACITOR" device="0402" value=".1uF"/>
<part name="C3" library="Droplet" deviceset="CAPACITOR" device="0402" value=".1uF"/>
<part name="C4" library="Droplet" deviceset="CAPACITOR" device="0402" value=".1uF"/>
<part name="C5" library="Droplet" deviceset="CAPACITOR" device="0402" value=".1uF"/>
<part name="C6" library="Droplet" deviceset="CAPACITOR" device="0402" value=".1uF"/>
<part name="V1" library="Droplet" deviceset="REGULATOR" device="" value="3.3V"/>
<part name="C7" library="Droplet" deviceset="CAPACITOR" device="0402" value="1uF"/>
<part name="C8" library="Droplet" deviceset="CAPACITOR" device="0402" value="1uF"/>
<part name="C9" library="Droplet" deviceset="CAPACITOR" device="0603" value="10uF"/>
<part name="L1" library="Droplet" deviceset="INDUCTOR" device="0805" value="10uH"/>
<part name="L2" library="Droplet" deviceset="INDUCTOR" device="0603" value=""/>
<part name="C10" library="Droplet" deviceset="CAPACITOR" device="0603" value="10uF"/>
<part name="V2" library="Droplet" deviceset="REGULATOR_5V" device="" value="5V"/>
<part name="L3" library="Droplet" deviceset="INDUCTOR" device="" value="4.7uH"/>
<part name="C11" library="Droplet" deviceset="CAPACITOR" device="0603" value="10uF"/>
<part name="C12" library="Droplet" deviceset="CAPACITOR" device="0603" value="10uF"/>
<part name="R1" library="Droplet" deviceset="RESISTOR" device="" value="845K"/>
<part name="R2" library="Droplet" deviceset="RESISTOR" device="" value="270K"/>
<part name="LED1" library="Droplet" deviceset="RGB_LED" device=""/>
<part name="R21" library="Droplet" deviceset="RESISTOR" device="" value="10K"/>
<part name="F1" library="Droplet" deviceset="MOSFET-N_X2" device=""/>
<part name="F2" library="Droplet" deviceset="MOSFET-N_X2" device=""/>
<part name="R15" library="Droplet" deviceset="RESISTOR" device="" value="75"/>
<part name="R16" library="Droplet" deviceset="RESISTOR" device="" value="75"/>
<part name="R17" library="Droplet" deviceset="RESISTOR" device="" value="75"/>
<part name="LM358D" library="linear" deviceset="LM358" device="D" value=""/>
<part name="R4" library="Droplet" deviceset="RESISTOR" device="" value="2.7k"/>
<part name="R5" library="Droplet" deviceset="RESISTOR" device="" value="33k"/>
<part name="R6" library="Droplet" deviceset="RESISTOR" device="" value="6.2k"/>
<part name="R7" library="Droplet" deviceset="RESISTOR" device="" value="100k"/>
<part name="R8" library="Droplet" deviceset="RESISTOR" device="" value="100k"/>
<part name="R9" library="Droplet" deviceset="RESISTOR" device="" value="10k"/>
<part name="C13" library="Droplet" deviceset="CAPACITOR" device="0402" value="1uF"/>
<part name="C14" library="Droplet" deviceset="CAPACITOR" device="0402" value="470pF"/>
<part name="C15" library="Droplet" deviceset="CAPACITOR" device="0402" value="10uF"/>
<part name="R10" library="Droplet" deviceset="RESISTOR" device="" value="2.2k"/>
<part name="MIC" library="microphon" deviceset="ELECTRET_MICROPHON-" device="WM-63PR" value=""/>
<part name="C16" library="Droplet" deviceset="CAPACITOR" device="0402" value="1uF"/>
<part name="PDI" library="pinhead" deviceset="PINHD-2X3" device=""/>
<part name="U2" library="SparkFun-Sensors" deviceset="MPR121" device="QFN20" value="MPR121"/>
<part name="MOTOR" library="pinhead" deviceset="PINHD-1X4" device=""/>
<part name="CAMERA" library="gameboy camera" deviceset="M09" device="1.5MM-TH" value="con: B9B-ZR(LF)(SN)"/>
<part name="PHOTORESISTOR1" library="SparkFun-Sensors" deviceset="LTR-301" device=""/>
<part name="K1" library="relay" deviceset="AQY*" device="SOP" technology="22"/>
<part name="ELEC0" library="SparkFun-Connectors" deviceset="M06" device="SIP"/>
<part name="ELEC1" library="SparkFun-Connectors" deviceset="M06" device="SIP"/>
<part name="PHOTORESISTOR2" library="SparkFun-Sensors" deviceset="LTR-301" device=""/>
<part name="LED2" library="Droplet" deviceset="RGB_LED" device=""/>
<part name="F3" library="Droplet" deviceset="MOSFET-N_X2" device=""/>
<part name="R29" library="Droplet" deviceset="RESISTOR" device="" value="75"/>
<part name="R30" library="Droplet" deviceset="RESISTOR" device="" value="75"/>
<part name="R31" library="Droplet" deviceset="RESISTOR" device="" value="75"/>
<part name="ACCEL" library="Droplet" deviceset="ACCELEROMETER" device=""/>
<part name="MEMORY" library="Droplet" deviceset="I2C_MEMORY" device=""/>
<part name="UART" library="pinhead" deviceset="PINHD-1X4" device=""/>
<part name="RESET" library="Droplet" deviceset="SMD_SWITCH" device=""/>
<part name="LCD" library="pinhead" deviceset="PINHD-1X18" device=""/>
<part name="R32" library="Droplet" deviceset="RESISTOR" device="" value="75"/>
<part name="R33" library="Droplet" deviceset="RESISTOR" device="" value="75"/>
<part name="R34" library="Droplet" deviceset="RESISTOR" device="" value="75"/>
<part name="C17" library="Droplet" deviceset="CAPACITOR" device="0402" value=".1uF"/>
<part name="C18" library="Droplet" deviceset="CAPACITOR" device="0402" value=".1uF"/>
<part name="R35" library="Droplet" deviceset="RESISTOR" device="" value="4.7k"/>
<part name="R36" library="Droplet" deviceset="RESISTOR" device="" value="4.7k"/>
<part name="U$4" library="Droplet" deviceset="MP3_MODULE_DEVICE" device=""/>
<part name="SPEAKER" library="pinhead" deviceset="PINHD-1X2" device=""/>
<part name="IC2" library="st-microelectronics" deviceset="L293D" device="" value="SN754410"/>
<part name="R12" library="Droplet" deviceset="RESISTOR" device="" value="motors"/>
<part name="R13" library="Droplet" deviceset="RESISTOR" device="" value="motors"/>
<part name="R14" library="Droplet" deviceset="RESISTOR" device="" value="fans"/>
<part name="R18" library="Droplet" deviceset="RESISTOR" device="" value="fans"/>
<part name="R19" library="Droplet" deviceset="RESISTOR" device="" value="fans"/>
<part name="R20" library="Droplet" deviceset="RESISTOR" device="" value="fans"/>
<part name="TEMP/HUM" library="con-stocko" deviceset="MKS1854-6-0-404" device="" value="DHT22"/>
<part name="B1" library="rectifier" deviceset="MB*S" device="" technology="6"/>
<part name="R22" library="Droplet" deviceset="RESISTOR" device="" value="1k"/>
<part name="R23" library="Droplet" deviceset="RESISTOR" device="" value="1.2M"/>
<part name="R26" library="Droplet" deviceset="RESISTOR" device="" value="10k"/>
<part name="R27" library="Droplet" deviceset="RESISTOR" device="" value="6.2k"/>
<part name="R37" library="Droplet" deviceset="RESISTOR" device="" value="10k"/>
<part name="WIFI" library="pinhead" deviceset="PINHD-2X4" device=""/>
<part name="NODE5" library="surface-mount-headers" deviceset="PINHD-1X4" device=""/>
<part name="NODE4" library="surface-mount-headers" deviceset="PINHD-1X4" device=""/>
<part name="NODE1" library="surface-mount-headers" deviceset="PINHD-1X4" device=""/>
<part name="NODE2" library="surface-mount-headers" deviceset="PINHD-1X4" device=""/>
<part name="SERVO1" library="surface-mount-headers" deviceset="PINHD-1X3" device=""/>
<part name="SERVO2" library="surface-mount-headers" deviceset="PINHD-1X3" device=""/>
<part name="SERVO3" library="pinhead" deviceset="PINHD-1X3" device=""/>
<part name="SERVO4" library="pinhead" deviceset="PINHD-1X3" device=""/>
<part name="R11" library="Droplet" deviceset="RESISTOR" device="" value="servo"/>
<part name="R38" library="Droplet" deviceset="RESISTOR" device="" value="servo"/>
<part name="R39" library="Droplet" deviceset="RESISTOR" device="" value="servo"/>
<part name="R40" library="Droplet" deviceset="RESISTOR" device="" value="servo"/>
<part name="FAN1" library="surface-mount-headers" deviceset="PINHD-1X3" device=""/>
<part name="FAN2" library="surface-mount-headers" deviceset="PINHD-1X3" device=""/>
<part name="C21" library="Droplet" deviceset="CAPACITOR" device="0402" value=".1uF"/>
<part name="C19" library="Droplet" deviceset="CAPACITOR" device="0402" value=".1uF"/>
<part name="R41" library="Droplet" deviceset="RESISTOR" device="" value="10K ??"/>
<part name="R42" library="Droplet" deviceset="RESISTOR" device="" value="unk?"/>
<part name="R24" library="Droplet" deviceset="RESISTOR" device="" value="10K??"/>
<part name="R25" library="Droplet" deviceset="RESISTOR" device="" value="10K??"/>
<part name="USBPOWER" library="Droplet" deviceset="RESISTOR" device="" value="0"/>
<part name="NODE0" library="surface-mount-headers" deviceset="PINHD-1X5" device=""/>
<part name="NODE3" library="surface-mount-headers" deviceset="PINHD-1X4" device=""/>
<part name="R3" library="Droplet" deviceset="RESISTOR" device="" value="abt 0.8k"/>
<part name="R28" library="Droplet" deviceset="RESISTOR" device="" value="abt 9.5k"/>
<part name="I2C3V3" library="surface-mount-headers" deviceset="PINHD-1X4" device=""/>
<part name="I2C5V" library="surface-mount-headers" deviceset="PINHD-1X4" device=""/>
<part name="C20" library="Droplet" deviceset="CAPACITOR" device="0402" value="opt"/>
<part name="R43" library="Droplet" deviceset="RESISTOR" device="" value="10k"/>
<part name="R44" library="Droplet" deviceset="RESISTOR" device="" value="opt"/>
<part name="75VAC" library="surface-mount-headers" deviceset="PINHD-1X2" device=""/>
<part name="PDLC" library="surface-mount-headers" deviceset="PINHD-1X2" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="-139.7" y1="297.18" x2="-139.7" y2="208.28" width="0.1524" layer="98"/>
<wire x1="-139.7" y1="208.28" x2="-33.02" y2="208.28" width="0.1524" layer="98"/>
<wire x1="-33.02" y1="208.28" x2="-33.02" y2="297.18" width="0.1524" layer="98"/>
<wire x1="-33.02" y1="297.18" x2="-139.7" y2="297.18" width="0.1524" layer="98"/>
<text x="-139.7" y="299.72" size="2.54" layer="98">Power Regulation and Filtering</text>
<wire x1="-111.76" y1="144.78" x2="-111.76" y2="91.44" width="0.1524" layer="98"/>
<wire x1="-111.76" y1="91.44" x2="-7.62" y2="91.44" width="0.1524" layer="98"/>
<wire x1="-7.62" y1="91.44" x2="-7.62" y2="144.78" width="0.1524" layer="98"/>
<wire x1="-111.76" y1="200.66" x2="-111.76" y2="152.4" width="0.1524" layer="98"/>
<wire x1="-111.76" y1="152.4" x2="-40.64" y2="152.4" width="0.1524" layer="98"/>
<wire x1="-40.64" y1="152.4" x2="-40.64" y2="200.66" width="0.1524" layer="98"/>
<wire x1="-40.64" y1="200.66" x2="-111.76" y2="200.66" width="0.1524" layer="98"/>
<wire x1="-7.62" y1="144.78" x2="-111.76" y2="144.78" width="0.1524" layer="98"/>
<text x="-109.22" y="147.32" size="2.54" layer="98">RGB Emitter</text>
<wire x1="-25.4" y1="149.86" x2="106.68" y2="149.86" width="0.1524" layer="98"/>
<wire x1="106.68" y1="149.86" x2="106.68" y2="297.18" width="0.1524" layer="98"/>
<wire x1="106.68" y1="297.18" x2="-25.4" y2="297.18" width="0.1524" layer="98"/>
<text x="-22.86" y="299.72" size="2.54" layer="98">Microcontroller and External Connections</text>
<wire x1="-25.4" y1="297.18" x2="-25.4" y2="149.86" width="0.1524" layer="98"/>
<wire x1="-411.48" y1="83.82" x2="-411.48" y2="0" width="0.1524" layer="98"/>
<wire x1="-411.48" y1="0" x2="-299.72" y2="0" width="0.1524" layer="98"/>
<wire x1="-299.72" y1="0" x2="-299.72" y2="83.82" width="0.1524" layer="98"/>
<wire x1="-299.72" y1="83.82" x2="-411.48" y2="83.82" width="0.1524" layer="98"/>
<text x="-408.94" y="86.36" size="2.54" layer="98">Microphone</text>
<wire x1="-119.38" y1="91.44" x2="-299.72" y2="91.44" width="0.1524" layer="98"/>
<wire x1="-299.72" y1="91.44" x2="-299.72" y2="200.66" width="0.1524" layer="98"/>
<wire x1="-299.72" y1="200.66" x2="-185.42" y2="200.66" width="0.1524" layer="98"/>
<text x="-297.18" y="203.2" size="2.54" layer="98">External Connections</text>
<wire x1="-180.34" y1="200.66" x2="-119.38" y2="200.66" width="0.1524" layer="98"/>
<wire x1="-119.38" y1="91.44" x2="-119.38" y2="152.4" width="0.1524" layer="98"/>
<wire x1="-119.38" y1="160.02" x2="-119.38" y2="200.66" width="0.1524" layer="98"/>
<wire x1="-411.48" y1="91.44" x2="-307.34" y2="91.44" width="0.1524" layer="98"/>
<wire x1="-307.34" y1="91.44" x2="-307.34" y2="200.66" width="0.1524" layer="98"/>
<wire x1="-307.34" y1="200.66" x2="-411.48" y2="200.66" width="0.1524" layer="98"/>
<wire x1="-411.48" y1="200.66" x2="-411.48" y2="91.44" width="0.1524" layer="98"/>
<text x="-408.94" y="203.2" size="2.54" layer="98">Capacitive Touch</text>
<wire x1="-302.26" y1="297.18" x2="-208.28" y2="297.18" width="0.1524" layer="98"/>
<wire x1="-208.28" y1="297.18" x2="-208.28" y2="259.08" width="0.1524" layer="98"/>
<wire x1="-147.32" y1="297.18" x2="-147.32" y2="254" width="0.1524" layer="98"/>
<wire x1="-147.32" y1="254" x2="-203.2" y2="254" width="0.1524" layer="98"/>
<wire x1="-203.2" y1="254" x2="-203.2" y2="297.18" width="0.1524" layer="98"/>
<text x="-297.18" y="299.72" size="2.54" layer="98">SD Card Player &amp; Speaker</text>
<wire x1="-302.26" y1="259.08" x2="-302.26" y2="297.18" width="0.1524" layer="98"/>
<wire x1="-226.06" y1="83.82" x2="-226.06" y2="0" width="0.1524" layer="98"/>
<wire x1="-226.06" y1="0" x2="-7.62" y2="0" width="0.1524" layer="98"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="83.82" width="0.1524" layer="98"/>
<wire x1="-7.62" y1="83.82" x2="-226.06" y2="83.82" width="0.1524" layer="98"/>
<text x="-223.52" y="86.36" size="2.54" layer="98">Stepper Motor Driver</text>
<wire x1="-299.72" y1="251.46" x2="-299.72" y2="208.28" width="0.1524" layer="98"/>
<wire x1="-299.72" y1="208.28" x2="-208.28" y2="208.28" width="0.1524" layer="98"/>
<wire x1="-200.66" y1="243.84" x2="-200.66" y2="208.28" width="0.1524" layer="98"/>
<wire x1="-200.66" y1="208.28" x2="-147.32" y2="208.28" width="0.1524" layer="98"/>
<wire x1="-147.32" y1="208.28" x2="-147.32" y2="243.84" width="0.1524" layer="98"/>
<wire x1="-147.32" y1="243.84" x2="-200.66" y2="243.84" width="0.1524" layer="98"/>
<wire x1="-208.28" y1="208.28" x2="-208.28" y2="251.46" width="0.1524" layer="98"/>
<wire x1="-208.28" y1="251.46" x2="-299.72" y2="251.46" width="0.1524" layer="98"/>
<text x="-297.18" y="254" size="2.54" layer="98">Gameboy Camera</text>
<wire x1="0" y1="142.24" x2="106.68" y2="142.24" width="0.1524" layer="98"/>
<wire x1="106.68" y1="142.24" x2="106.68" y2="91.44" width="0.1524" layer="98"/>
<wire x1="106.68" y1="91.44" x2="0" y2="91.44" width="0.1524" layer="98"/>
<wire x1="0" y1="91.44" x2="0" y2="142.24" width="0.1524" layer="98"/>
<text x="2.54" y="144.78" size="2.54" layer="98">Temp &amp; Light</text>
<text x="15.24" y="129.54" size="1.778" layer="98">Temp: internal to uC</text>
<text x="12.7" y="134.62" size="1.778" layer="98">light: voltage divider/ photoResistor</text>
<wire x1="-411.48" y1="297.18" x2="-307.34" y2="297.18" width="0.1524" layer="98"/>
<wire x1="-307.34" y1="297.18" x2="-307.34" y2="208.28" width="0.1524" layer="98"/>
<wire x1="-307.34" y1="208.28" x2="-411.48" y2="208.28" width="0.1524" layer="98"/>
<wire x1="-411.48" y1="208.28" x2="-411.48" y2="297.18" width="0.1524" layer="98"/>
<text x="-408.94" y="299.72" size="2.54" layer="98">120 V Solid State Relay</text>
<wire x1="106.68" y1="83.82" x2="0" y2="83.82" width="0.1524" layer="98"/>
<wire x1="0" y1="83.82" x2="0" y2="0" width="0.1524" layer="98"/>
<wire x1="0" y1="0" x2="106.68" y2="0" width="0.1524" layer="98"/>
<wire x1="106.68" y1="0" x2="106.68" y2="83.82" width="0.1524" layer="98"/>
<text x="2.54" y="86.36" size="2.54" layer="98">16 x 2 RGB LCD</text>
<text x="-109.22" y="203.2" size="2.54" layer="98">Accelerometer</text>
<text x="-198.12" y="246.38" size="2.54" layer="98">External Memory</text>
<text x="-93.98" y="195.58" size="1.778" layer="98">MMA7660FC</text>
<text x="-182.88" y="236.22" size="1.778" layer="98">M24M01</text>
<wire x1="-223.52" y1="279.4" x2="-220.98" y2="279.4" width="0.1524" layer="94"/>
<wire x1="-220.98" y1="279.4" x2="-220.98" y2="276.86" width="0.1524" layer="94"/>
<wire x1="-220.98" y1="276.86" x2="-223.52" y2="276.86" width="0.1524" layer="94"/>
<rectangle x1="-220.98" y1="274.32" x2="-218.44" y2="279.4" layer="94"/>
<rectangle x1="-220.98" y1="279.4" x2="-218.44" y2="281.94" layer="94"/>
<wire x1="-220.98" y1="279.4" x2="-215.9" y2="281.94" width="0.1524" layer="94"/>
<wire x1="-215.9" y1="281.94" x2="-215.9" y2="274.32" width="0.1524" layer="94"/>
<wire x1="-215.9" y1="274.32" x2="-220.98" y2="276.86" width="0.1524" layer="94"/>
<wire x1="-302.26" y1="259.08" x2="-208.28" y2="259.08" width="0.1524" layer="98"/>
<wire x1="-147.32" y1="297.18" x2="-203.2" y2="297.18" width="0.1524" layer="98"/>
<text x="-200.66" y="299.72" size="2.54" layer="98">Temp/Humidity Sensor</text>
<wire x1="-292.1" y1="0" x2="-292.1" y2="83.82" width="0.1524" layer="98"/>
<wire x1="-233.68" y1="0" x2="-233.68" y2="83.82" width="0.1524" layer="98"/>
<wire x1="-233.68" y1="83.82" x2="-292.1" y2="83.82" width="0.1524" layer="98"/>
<wire x1="-233.68" y1="0" x2="-292.1" y2="0" width="0.1524" layer="98"/>
<text x="-289.56" y="86.36" size="2.54" layer="98">Servos</text>
<wire x1="-185.42" y1="200.66" x2="-185.42" y2="152.4" width="0.1524" layer="98"/>
<wire x1="-185.42" y1="152.4" x2="-119.38" y2="152.4" width="0.1524" layer="98"/>
<wire x1="-180.34" y1="160.02" x2="-119.38" y2="160.02" width="0.1524" layer="98"/>
<wire x1="-180.34" y1="200.66" x2="-180.34" y2="160.02" width="0.1524" layer="98"/>
<text x="-381" y="256.54" size="1.778" layer="98">part: G3VM-351G1</text>
<text x="-355.6" y="284.48" size="1.778" layer="98">part: MB6S-TP (MB6S-TPMSCT-ND)</text>
<text x="20.32" y="76.2" size="2.54" layer="98">HD44780 compatible</text>
</plain>
<instances>
<instance part="XMEGA" gate="G$1" x="27.94" y="223.52"/>
<instance part="C1" gate="C1" x="-116.84" y="226.06"/>
<instance part="C2" gate="C1" x="-109.22" y="226.06"/>
<instance part="C3" gate="C1" x="-101.6" y="226.06"/>
<instance part="C4" gate="C1" x="-93.98" y="226.06"/>
<instance part="C5" gate="C1" x="-86.36" y="226.06"/>
<instance part="C6" gate="C1" x="-73.66" y="226.06"/>
<instance part="V1" gate="G$1" x="-104.14" y="251.46"/>
<instance part="C7" gate="C1" x="-124.46" y="248.92"/>
<instance part="C8" gate="C1" x="-91.44" y="248.92"/>
<instance part="C9" gate="C1" x="-71.12" y="248.92"/>
<instance part="L1" gate="G$1" x="-81.28" y="256.54"/>
<instance part="L2" gate="G$1" x="-60.96" y="256.54"/>
<instance part="C10" gate="C1" x="-50.8" y="248.92"/>
<instance part="V2" gate="G$1" x="-83.82" y="281.94"/>
<instance part="L3" gate="G$1" x="-104.14" y="279.4"/>
<instance part="C11" gate="C1" x="-124.46" y="279.4"/>
<instance part="C12" gate="C1" x="-55.88" y="279.4"/>
<instance part="R1" gate="G$1" x="-71.12" y="281.94" rot="R90"/>
<instance part="R2" gate="G$1" x="-63.5" y="274.32"/>
<instance part="LED1" gate="G$1" x="-81.28" y="132.08" rot="R270"/>
<instance part="R21" gate="G$1" x="-167.64" y="104.14" rot="R180"/>
<instance part="F1" gate="G$1" x="-91.44" y="101.6" smashed="yes"/>
<instance part="F1" gate="G$2" x="-66.04" y="101.6" smashed="yes"/>
<instance part="F2" gate="G$1" x="76.2" y="22.86" smashed="yes"/>
<instance part="R15" gate="G$1" x="-45.72" y="111.76" rot="R180"/>
<instance part="R16" gate="G$1" x="-73.66" y="116.84" rot="R180"/>
<instance part="R17" gate="G$1" x="-88.9" y="116.84" rot="R90"/>
<instance part="LM358D" gate="A" x="-353.06" y="43.18" rot="MR180"/>
<instance part="LM358D" gate="P" x="-353.06" y="43.18" rot="R180"/>
<instance part="R4" gate="G$1" x="-370.84" y="45.72" rot="R180"/>
<instance part="R5" gate="G$1" x="-353.06" y="63.5" rot="R180"/>
<instance part="R6" gate="G$1" x="-337.82" y="35.56" rot="R270"/>
<instance part="R7" gate="G$1" x="-373.38" y="20.32" rot="R180"/>
<instance part="R8" gate="G$1" x="-355.6" y="20.32" rot="R180"/>
<instance part="R9" gate="G$1" x="-322.58" y="35.56" rot="R270"/>
<instance part="C13" gate="C1" x="-383.54" y="45.72" rot="R270"/>
<instance part="C14" gate="C1" x="-350.52" y="71.12" rot="R90"/>
<instance part="C15" gate="C1" x="-365.76" y="10.16"/>
<instance part="R10" gate="G$1" x="-388.62" y="58.42" rot="R270"/>
<instance part="MIC" gate="G$1" x="-401.32" y="45.72" rot="R180"/>
<instance part="C16" gate="C1" x="-330.2" y="43.18" rot="R90"/>
<instance part="PDI" gate="A" x="-160.02" y="114.3"/>
<instance part="U2" gate="G$1" x="-340.36" y="134.62"/>
<instance part="MOTOR" gate="A" x="-25.4" y="60.96"/>
<instance part="CAMERA" gate="G$1" x="-210.82" y="231.14" rot="R180"/>
<instance part="PHOTORESISTOR1" gate="G$1" x="33.02" y="109.22"/>
<instance part="K1" gate="G$1" x="-368.3" y="246.38"/>
<instance part="ELEC0" gate="G$1" x="-368.3" y="185.42" rot="R180"/>
<instance part="ELEC1" gate="G$1" x="-325.12" y="185.42" rot="R180"/>
<instance part="PHOTORESISTOR2" gate="G$1" x="63.5" y="119.38"/>
<instance part="LED2" gate="G$1" x="-22.86" y="129.54" rot="R270"/>
<instance part="F3" gate="G$1" x="88.9" y="22.86" smashed="yes"/>
<instance part="F3" gate="G$2" x="-40.64" y="101.6" smashed="yes"/>
<instance part="R29" gate="G$1" x="-20.32" y="111.76"/>
<instance part="R30" gate="G$1" x="-30.48" y="116.84"/>
<instance part="R31" gate="G$1" x="-45.72" y="121.92"/>
<instance part="ACCEL" gate="G$1" x="-86.36" y="175.26"/>
<instance part="MEMORY" gate="G$1" x="-172.72" y="226.06"/>
<instance part="UART" gate="A" x="-218.44" y="134.62" rot="R90"/>
<instance part="RESET" gate="G$1" x="-185.42" y="104.14" rot="R270"/>
<instance part="LCD" gate="A" x="5.08" y="40.64" rot="R180"/>
<instance part="R32" gate="G$1" x="58.42" y="48.26" rot="R180"/>
<instance part="R33" gate="G$1" x="58.42" y="55.88" rot="R180"/>
<instance part="R34" gate="G$1" x="58.42" y="63.5" rot="R180"/>
<instance part="C17" gate="C1" x="-88.9" y="157.48" rot="R90"/>
<instance part="C18" gate="C1" x="-60.96" y="157.48" rot="R90"/>
<instance part="R35" gate="G$1" x="-226.06" y="175.26"/>
<instance part="R36" gate="G$1" x="-226.06" y="167.64"/>
<instance part="U$4" gate="G$1" x="-251.46" y="276.86"/>
<instance part="SPEAKER" gate="G$1" x="-223.52" y="276.86"/>
<instance part="IC2" gate="G$1" x="-109.22" y="30.48"/>
<instance part="R12" gate="G$1" x="-182.88" y="60.96" rot="R180"/>
<instance part="R13" gate="G$1" x="-185.42" y="43.18" rot="R180"/>
<instance part="R14" gate="G$1" x="-213.36" y="55.88" rot="R270"/>
<instance part="R18" gate="G$1" x="-172.72" y="55.88" rot="R270"/>
<instance part="R19" gate="G$1" x="-208.28" y="38.1" rot="R270"/>
<instance part="R20" gate="G$1" x="-167.64" y="38.1" rot="R270"/>
<instance part="TEMP/HUM" gate="G$1" x="-162.56" y="279.4"/>
<instance part="B1" gate="G$1" x="-340.36" y="274.32"/>
<instance part="R22" gate="G$1" x="-398.78" y="271.78" rot="R270"/>
<instance part="R23" gate="G$1" x="-327.66" y="274.32"/>
<instance part="R26" gate="G$1" x="-320.04" y="266.7" rot="R90"/>
<instance part="R27" gate="G$1" x="-271.78" y="238.76" rot="R270"/>
<instance part="R37" gate="G$1" x="-271.78" y="223.52" rot="R270"/>
<instance part="WIFI" gate="A" x="-149.86" y="132.08"/>
<instance part="NODE5" gate="A" x="-276.86" y="134.62" rot="R90"/>
<instance part="NODE4" gate="A" x="-256.54" y="134.62" rot="R90"/>
<instance part="NODE1" gate="A" x="-261.62" y="187.96" rot="R90"/>
<instance part="NODE2" gate="A" x="-243.84" y="187.96" rot="R90"/>
<instance part="SERVO1" gate="A" x="-241.3" y="68.58"/>
<instance part="SERVO2" gate="A" x="-241.3" y="50.8"/>
<instance part="SERVO3" gate="A" x="-241.3" y="33.02"/>
<instance part="SERVO4" gate="A" x="-241.3" y="15.24"/>
<instance part="R11" gate="G$1" x="-261.62" y="71.12" rot="R180"/>
<instance part="R38" gate="G$1" x="-261.62" y="53.34" rot="R180"/>
<instance part="R39" gate="G$1" x="-261.62" y="35.56" rot="R180"/>
<instance part="R40" gate="G$1" x="-261.62" y="17.78" rot="R180"/>
<instance part="FAN1" gate="A" x="-25.4" y="40.64"/>
<instance part="FAN2" gate="A" x="-25.4" y="17.78"/>
<instance part="C21" gate="C1" x="-363.22" y="157.48" rot="R90"/>
<instance part="C19" gate="C1" x="-381" y="124.46" rot="R180"/>
<instance part="R41" gate="G$1" x="-386.08" y="152.4" rot="R270"/>
<instance part="R42" gate="G$1" x="-363.22" y="119.38" rot="R270"/>
<instance part="R24" gate="G$1" x="50.8" y="124.46" rot="R180"/>
<instance part="R25" gate="G$1" x="20.32" y="114.3" rot="R180"/>
<instance part="USBPOWER" gate="G$1" x="-213.36" y="114.3" rot="R270"/>
<instance part="NODE0" gate="A" x="-281.94" y="187.96" rot="R90"/>
<instance part="F2" gate="G$2" x="63.5" y="22.86" smashed="yes"/>
<instance part="NODE3" gate="A" x="-238.76" y="134.62" rot="R90"/>
<instance part="R3" gate="G$1" x="48.26" y="15.24" rot="R180"/>
<instance part="R28" gate="G$1" x="45.72" y="22.86" rot="R180"/>
<instance part="I2C3V3" gate="A" x="-213.36" y="187.96" rot="R90"/>
<instance part="I2C5V" gate="A" x="-193.04" y="187.96" rot="R90"/>
<instance part="LM358D" gate="B" x="-149.86" y="180.34"/>
<instance part="C20" gate="C1" x="-129.54" y="180.34" rot="R90"/>
<instance part="R43" gate="G$1" x="-165.1" y="175.26" rot="R270"/>
<instance part="R44" gate="G$1" x="-170.18" y="185.42"/>
<instance part="75VAC" gate="G$1" x="-314.96" y="246.38"/>
<instance part="PDLC" gate="G$1" x="-314.96" y="233.68"/>
</instances>
<busses>
</busses>
<nets>
<net name="VCC" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="VCC1"/>
<wire x1="-22.86" y1="241.3" x2="-12.7" y2="241.3" width="0.1524" layer="91"/>
<pinref part="XMEGA" gate="G$1" pin="VCC5"/>
<wire x1="-12.7" y1="241.3" x2="-5.08" y2="241.3" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="241.3" x2="-12.7" y2="238.76" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="238.76" x2="-12.7" y2="236.22" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="236.22" x2="-12.7" y2="233.68" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="233.68" x2="-12.7" y2="231.14" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="231.14" x2="-5.08" y2="231.14" width="0.1524" layer="91"/>
<junction x="-12.7" y="241.3"/>
<pinref part="XMEGA" gate="G$1" pin="VCC4"/>
<wire x1="-5.08" y1="233.68" x2="-12.7" y2="233.68" width="0.1524" layer="91"/>
<junction x="-12.7" y="233.68"/>
<pinref part="XMEGA" gate="G$1" pin="VCC3"/>
<wire x1="-5.08" y1="236.22" x2="-12.7" y2="236.22" width="0.1524" layer="91"/>
<junction x="-12.7" y="236.22"/>
<pinref part="XMEGA" gate="G$1" pin="VCC2"/>
<wire x1="-5.08" y1="238.76" x2="-12.7" y2="238.76" width="0.1524" layer="91"/>
<junction x="-12.7" y="238.76"/>
<label x="-22.86" y="241.3" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C1" gate="C1" pin="1"/>
<wire x1="-116.84" y1="231.14" x2="-116.84" y2="233.68" width="0.1524" layer="91"/>
<pinref part="C5" gate="C1" pin="1"/>
<wire x1="-116.84" y1="233.68" x2="-109.22" y2="233.68" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="233.68" x2="-101.6" y2="233.68" width="0.1524" layer="91"/>
<wire x1="-101.6" y1="233.68" x2="-93.98" y2="233.68" width="0.1524" layer="91"/>
<wire x1="-93.98" y1="233.68" x2="-86.36" y2="233.68" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="233.68" x2="-86.36" y2="231.14" width="0.1524" layer="91"/>
<pinref part="C4" gate="C1" pin="1"/>
<wire x1="-93.98" y1="231.14" x2="-93.98" y2="233.68" width="0.1524" layer="91"/>
<junction x="-93.98" y="233.68"/>
<pinref part="C3" gate="C1" pin="1"/>
<wire x1="-101.6" y1="231.14" x2="-101.6" y2="233.68" width="0.1524" layer="91"/>
<junction x="-101.6" y="233.68"/>
<pinref part="C2" gate="C1" pin="1"/>
<wire x1="-109.22" y1="231.14" x2="-109.22" y2="233.68" width="0.1524" layer="91"/>
<junction x="-109.22" y="233.68"/>
<label x="-121.92" y="233.68" size="1.778" layer="95"/>
<wire x1="-116.84" y1="233.68" x2="-121.92" y2="233.68" width="0.1524" layer="91"/>
<junction x="-116.84" y="233.68"/>
</segment>
<segment>
<pinref part="L1" gate="G$1" pin="2"/>
<pinref part="L2" gate="G$1" pin="1"/>
<wire x1="-73.66" y1="256.54" x2="-71.12" y2="256.54" width="0.1524" layer="91"/>
<pinref part="C9" gate="C1" pin="1"/>
<wire x1="-71.12" y1="256.54" x2="-68.58" y2="256.54" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="256.54" x2="-71.12" y2="254" width="0.1524" layer="91"/>
<junction x="-71.12" y="256.54"/>
<wire x1="-71.12" y1="256.54" x2="-71.12" y2="261.62" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="261.62" x2="-73.66" y2="261.62" width="0.1524" layer="91"/>
<label x="-73.66" y="261.62" size="1.778" layer="95"/>
<wire x1="-71.12" y1="261.62" x2="-68.58" y2="261.62" width="0.1524" layer="91"/>
<junction x="-71.12" y="261.62"/>
</segment>
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="-162.56" y1="104.14" x2="-162.56" y2="99.06" width="0.1524" layer="91"/>
<label x="-162.56" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VDD"/>
<wire x1="-358.14" y1="149.86" x2="-368.3" y2="149.86" width="0.1524" layer="91"/>
<label x="-375.92" y="149.86" size="1.778" layer="95"/>
<wire x1="-368.3" y1="149.86" x2="-375.92" y2="149.86" width="0.1524" layer="91"/>
<wire x1="-368.3" y1="157.48" x2="-368.3" y2="149.86" width="0.1524" layer="91"/>
<junction x="-368.3" y="149.86"/>
<pinref part="C21" gate="C1" pin="1"/>
</segment>
<segment>
<wire x1="-386.08" y1="157.48" x2="-386.08" y2="162.56" width="0.1524" layer="91"/>
<label x="-386.08" y="162.56" size="1.778" layer="95"/>
<pinref part="R41" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="MEMORY" gate="G$1" pin="VCC"/>
<wire x1="-157.48" y1="231.14" x2="-149.86" y2="231.14" width="0.1524" layer="91"/>
<label x="-154.94" y="231.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ACCEL" gate="G$1" pin="DVDD"/>
<wire x1="-53.34" y1="182.88" x2="-45.72" y2="182.88" width="0.1524" layer="91"/>
<label x="-50.8" y="182.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R35" gate="G$1" pin="1"/>
<wire x1="-231.14" y1="175.26" x2="-233.68" y2="175.26" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="175.26" x2="-233.68" y2="167.64" width="0.1524" layer="91"/>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="-233.68" y1="167.64" x2="-233.68" y2="165.1" width="0.1524" layer="91"/>
<wire x1="-231.14" y1="167.64" x2="-233.68" y2="167.64" width="0.1524" layer="91"/>
<label x="-233.68" y="165.1" size="1.778" layer="95" rot="R90"/>
<wire x1="-218.44" y1="180.34" x2="-233.68" y2="180.34" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="180.34" x2="-233.68" y2="175.26" width="0.1524" layer="91"/>
<junction x="-233.68" y="167.64"/>
<junction x="-233.68" y="175.26"/>
<pinref part="I2C3V3" gate="A" pin="1"/>
<wire x1="-218.44" y1="180.34" x2="-218.44" y2="185.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C18" gate="C1" pin="2"/>
<wire x1="-58.42" y1="157.48" x2="-50.8" y2="157.48" width="0.1524" layer="91"/>
<label x="-55.88" y="157.48" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-264.16" y1="271.78" x2="-276.86" y2="271.78" width="0.1524" layer="91"/>
<label x="-276.86" y="271.78" size="1.778" layer="95"/>
<pinref part="U$4" gate="G$1" pin="P07"/>
</segment>
<segment>
<wire x1="-264.16" y1="284.48" x2="-276.86" y2="284.48" width="0.1524" layer="91"/>
<label x="-276.86" y="284.48" size="1.778" layer="95"/>
<pinref part="U$4" gate="G$1" pin="P02"/>
</segment>
<segment>
<wire x1="-264.16" y1="281.94" x2="-276.86" y2="281.94" width="0.1524" layer="91"/>
<label x="-276.86" y="281.94" size="1.778" layer="95"/>
<pinref part="U$4" gate="G$1" pin="P03"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="VCC2"/>
<wire x1="-124.46" y1="12.7" x2="-139.7" y2="12.7" width="0.1524" layer="91"/>
<label x="-139.7" y="12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="TEMP/HUM" gate="G$1" pin="1"/>
<wire x1="-167.64" y1="279.4" x2="-187.96" y2="279.4" width="0.1524" layer="91"/>
<label x="-187.96" y="279.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="WIFI" gate="A" pin="2"/>
<wire x1="-144.78" y1="137.16" x2="-132.08" y2="137.16" width="0.1524" layer="91"/>
<label x="-137.16" y="137.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PDI" gate="A" pin="2"/>
<wire x1="-154.94" y1="116.84" x2="-147.32" y2="116.84" width="0.1524" layer="91"/>
<label x="-147.32" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="AVCC" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="AVCC"/>
<wire x1="-5.08" y1="254" x2="-22.86" y2="254" width="0.1524" layer="91"/>
<label x="-22.86" y="254" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C6" gate="C1" pin="1"/>
<wire x1="-73.66" y1="231.14" x2="-73.66" y2="233.68" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="233.68" x2="-78.74" y2="233.68" width="0.1524" layer="91"/>
<label x="-78.74" y="233.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="L2" gate="G$1" pin="2"/>
<pinref part="C10" gate="C1" pin="1"/>
<wire x1="-53.34" y1="256.54" x2="-50.8" y2="256.54" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="256.54" x2="-50.8" y2="254" width="0.1524" layer="91"/>
<label x="-45.72" y="256.54" size="1.778" layer="95"/>
<wire x1="-50.8" y1="256.54" x2="-40.64" y2="256.54" width="0.1524" layer="91"/>
<junction x="-50.8" y="256.54"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="-378.46" y1="20.32" x2="-388.62" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-388.62" y1="20.32" x2="-388.62" y2="22.86" width="0.1524" layer="91"/>
<label x="-388.62" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LM358D" gate="P" pin="V+"/>
<wire x1="-353.06" y1="35.56" x2="-353.06" y2="30.48" width="0.1524" layer="91"/>
<label x="-353.06" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ACCEL" gate="G$1" pin="AVDD"/>
<wire x1="-99.06" y1="177.8" x2="-106.68" y2="177.8" width="0.1524" layer="91"/>
<label x="-106.68" y="177.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C17" gate="C1" pin="1"/>
<wire x1="-93.98" y1="157.48" x2="-101.6" y2="157.48" width="0.1524" layer="91"/>
<label x="-101.6" y="157.48" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="15.24" y1="114.3" x2="5.08" y2="114.3" width="0.1524" layer="91"/>
<label x="5.08" y="114.3" size="1.778" layer="95"/>
<pinref part="R25" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="45.72" y1="124.46" x2="35.56" y2="124.46" width="0.1524" layer="91"/>
<label x="35.56" y="124.46" size="1.778" layer="95"/>
<pinref part="R24" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PDI_CLK" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="RESET"/>
<wire x1="-5.08" y1="289.56" x2="-22.86" y2="289.56" width="0.1524" layer="91"/>
<label x="-22.86" y="289.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PDI" gate="A" pin="5"/>
<wire x1="-162.56" y1="111.76" x2="-172.72" y2="111.76" width="0.1524" layer="91"/>
<label x="-185.42" y="111.76" size="1.778" layer="95"/>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="-172.72" y1="104.14" x2="-172.72" y2="111.76" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="111.76" x2="-180.34" y2="111.76" width="0.1524" layer="91"/>
<junction x="-172.72" y="111.76"/>
<wire x1="-180.34" y1="111.76" x2="-185.42" y2="111.76" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="104.14" x2="-180.34" y2="111.76" width="0.1524" layer="91"/>
<junction x="-180.34" y="111.76"/>
<pinref part="RESET" gate="G$1" pin="P1"/>
</segment>
</net>
<net name="PDI_DATA" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PDI_DATA"/>
<wire x1="-5.08" y1="287.02" x2="-22.86" y2="287.02" width="0.1524" layer="91"/>
<label x="-22.86" y="287.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PDI" gate="A" pin="1"/>
<wire x1="-162.56" y1="116.84" x2="-185.42" y2="116.84" width="0.1524" layer="91"/>
<label x="-185.42" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX0" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PC2"/>
<wire x1="60.96" y1="238.76" x2="83.82" y2="238.76" width="0.1524" layer="91"/>
<label x="63.5" y="238.76" size="1.778" layer="95"/>
</segment>
<segment>
<label x="-284.48" y="162.56" size="1.778" layer="95" rot="R90"/>
<wire x1="-284.48" y1="185.42" x2="-284.48" y2="162.56" width="0.1524" layer="91"/>
<pinref part="NODE0" gate="A" pin="2"/>
</segment>
</net>
<net name="TX0" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PC3"/>
<wire x1="60.96" y1="236.22" x2="83.82" y2="236.22" width="0.1524" layer="91"/>
<label x="63.5" y="236.22" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-281.94" y1="185.42" x2="-281.94" y2="162.56" width="0.1524" layer="91"/>
<label x="-281.94" y="162.56" size="1.778" layer="95" rot="R90"/>
<pinref part="NODE0" gate="A" pin="3"/>
</segment>
</net>
<net name="RX2" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PD2"/>
<wire x1="60.96" y1="215.9" x2="83.82" y2="215.9" width="0.1524" layer="91"/>
<label x="63.5" y="215.9" size="1.778" layer="95"/>
</segment>
<segment>
<label x="-246.38" y="162.56" size="1.778" layer="95" rot="R90"/>
<pinref part="NODE2" gate="A" pin="2"/>
<wire x1="-246.38" y1="185.42" x2="-246.38" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TX2" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PD3"/>
<wire x1="60.96" y1="213.36" x2="83.82" y2="213.36" width="0.1524" layer="91"/>
<label x="63.5" y="213.36" size="1.778" layer="95"/>
</segment>
<segment>
<label x="-243.84" y="162.56" size="1.778" layer="95" rot="R90"/>
<pinref part="NODE2" gate="A" pin="3"/>
<wire x1="-243.84" y1="185.42" x2="-243.84" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RX4" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PE2"/>
<wire x1="60.96" y1="193.04" x2="83.82" y2="193.04" width="0.1524" layer="91"/>
<label x="63.5" y="193.04" size="1.778" layer="95"/>
</segment>
<segment>
<label x="-259.08" y="106.68" size="1.778" layer="95" rot="R90"/>
<pinref part="NODE4" gate="A" pin="2"/>
<wire x1="-259.08" y1="132.08" x2="-259.08" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TX4" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PE3"/>
<wire x1="60.96" y1="190.5" x2="83.82" y2="190.5" width="0.1524" layer="91"/>
<label x="63.5" y="190.5" size="1.778" layer="95"/>
</segment>
<segment>
<label x="-256.54" y="106.68" size="1.778" layer="95" rot="R90"/>
<pinref part="NODE4" gate="A" pin="3"/>
<wire x1="-256.54" y1="132.08" x2="-256.54" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RX_EXT" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PF2"/>
<wire x1="60.96" y1="170.18" x2="83.82" y2="170.18" width="0.1524" layer="91"/>
<label x="63.5" y="170.18" size="1.778" layer="95"/>
</segment>
<segment>
<label x="-220.98" y="106.68" size="1.778" layer="95" rot="R90"/>
<pinref part="UART" gate="A" pin="2"/>
<wire x1="-220.98" y1="106.68" x2="-220.98" y2="132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="WIFI" gate="A" pin="4"/>
<wire x1="-144.78" y1="134.62" x2="-127" y2="134.62" width="0.1524" layer="91"/>
<label x="-137.16" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="TX_EXT" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PF3"/>
<wire x1="60.96" y1="167.64" x2="83.82" y2="167.64" width="0.1524" layer="91"/>
<label x="63.5" y="167.64" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-218.44" y1="132.08" x2="-218.44" y2="106.68" width="0.1524" layer="91"/>
<label x="-218.44" y="106.68" size="1.778" layer="95" rot="R90"/>
<pinref part="UART" gate="A" pin="3"/>
</segment>
<segment>
<pinref part="WIFI" gate="A" pin="6"/>
<wire x1="-144.78" y1="132.08" x2="-127" y2="132.08" width="0.1524" layer="91"/>
<label x="-137.16" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<pinref part="V2" gate="G$1" pin="VIN"/>
<wire x1="-93.98" y1="289.56" x2="-96.52" y2="289.56" width="0.1524" layer="91"/>
<pinref part="V2" gate="G$1" pin="EN"/>
<wire x1="-96.52" y1="289.56" x2="-114.3" y2="289.56" width="0.1524" layer="91"/>
<wire x1="-114.3" y1="289.56" x2="-124.46" y2="289.56" width="0.1524" layer="91"/>
<wire x1="-124.46" y1="289.56" x2="-132.08" y2="289.56" width="0.1524" layer="91"/>
<wire x1="-93.98" y1="284.48" x2="-96.52" y2="284.48" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="284.48" x2="-96.52" y2="289.56" width="0.1524" layer="91"/>
<junction x="-96.52" y="289.56"/>
<pinref part="L3" gate="G$1" pin="1"/>
<wire x1="-111.76" y1="279.4" x2="-114.3" y2="279.4" width="0.1524" layer="91"/>
<wire x1="-114.3" y1="279.4" x2="-114.3" y2="289.56" width="0.1524" layer="91"/>
<junction x="-114.3" y="289.56"/>
<pinref part="C11" gate="C1" pin="1"/>
<wire x1="-124.46" y1="284.48" x2="-124.46" y2="289.56" width="0.1524" layer="91"/>
<junction x="-124.46" y="289.56"/>
<label x="-132.08" y="289.56" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-236.22" y1="101.6" x2="-254" y2="101.6" width="0.1524" layer="91"/>
<label x="-297.18" y="101.6" size="1.778" layer="95"/>
<wire x1="-254" y1="101.6" x2="-274.32" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-274.32" y1="101.6" x2="-297.18" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-254" y1="132.08" x2="-254" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-236.22" y1="132.08" x2="-236.22" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-236.22" y1="101.6" x2="-213.36" y2="101.6" width="0.1524" layer="91"/>
<junction x="-274.32" y="101.6"/>
<junction x="-254" y="101.6"/>
<junction x="-236.22" y="101.6"/>
<wire x1="-274.32" y1="132.08" x2="-274.32" y2="101.6" width="0.1524" layer="91"/>
<pinref part="NODE4" gate="A" pin="4"/>
<pinref part="USBPOWER" gate="G$1" pin="2"/>
<wire x1="-213.36" y1="101.6" x2="-213.36" y2="109.22" width="0.1524" layer="91"/>
<pinref part="NODE5" gate="A" pin="4"/>
<pinref part="NODE3" gate="A" pin="4"/>
</segment>
<segment>
<wire x1="-241.3" y1="157.48" x2="-259.08" y2="157.48" width="0.1524" layer="91"/>
<label x="-297.18" y="157.48" size="1.778" layer="95"/>
<wire x1="-259.08" y1="157.48" x2="-279.4" y2="157.48" width="0.1524" layer="91"/>
<wire x1="-279.4" y1="157.48" x2="-297.18" y2="157.48" width="0.1524" layer="91"/>
<wire x1="-279.4" y1="185.42" x2="-279.4" y2="157.48" width="0.1524" layer="91"/>
<wire x1="-259.08" y1="185.42" x2="-259.08" y2="157.48" width="0.1524" layer="91"/>
<junction x="-279.4" y="157.48"/>
<junction x="-259.08" y="157.48"/>
<pinref part="NODE1" gate="A" pin="4"/>
<pinref part="NODE2" gate="A" pin="4"/>
<wire x1="-241.3" y1="185.42" x2="-241.3" y2="157.48" width="0.1524" layer="91"/>
<pinref part="NODE0" gate="A" pin="4"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="V1" gate="G$1" pin="VOUT"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="-93.98" y1="256.54" x2="-91.44" y2="256.54" width="0.1524" layer="91"/>
<pinref part="C8" gate="C1" pin="1"/>
<wire x1="-91.44" y1="256.54" x2="-88.9" y2="256.54" width="0.1524" layer="91"/>
<wire x1="-91.44" y1="256.54" x2="-91.44" y2="254" width="0.1524" layer="91"/>
<junction x="-91.44" y="256.54"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="V2" gate="G$1" pin="VFB"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="-73.66" y1="274.32" x2="-71.12" y2="274.32" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-71.12" y1="274.32" x2="-68.58" y2="274.32" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="274.32" x2="-71.12" y2="276.86" width="0.1524" layer="91"/>
<junction x="-71.12" y="274.32"/>
</segment>
</net>
<net name="HVCC" class="0">
<segment>
<pinref part="V2" gate="G$1" pin="VOUT"/>
<pinref part="C12" gate="C1" pin="1"/>
<wire x1="-73.66" y1="289.56" x2="-71.12" y2="289.56" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="289.56" x2="-55.88" y2="289.56" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="289.56" x2="-55.88" y2="284.48" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="-71.12" y1="287.02" x2="-71.12" y2="289.56" width="0.1524" layer="91"/>
<junction x="-71.12" y="289.56"/>
<wire x1="-55.88" y1="289.56" x2="-40.64" y2="289.56" width="0.1524" layer="91"/>
<junction x="-55.88" y="289.56"/>
<label x="-45.72" y="289.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="ANODE"/>
<wire x1="-81.28" y1="139.7" x2="-88.9" y2="139.7" width="0.1524" layer="91"/>
<label x="-88.9" y="139.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="V1" gate="G$1" pin="VIN"/>
<wire x1="-114.3" y1="256.54" x2="-116.84" y2="256.54" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="256.54" x2="-124.46" y2="256.54" width="0.1524" layer="91"/>
<wire x1="-124.46" y1="256.54" x2="-132.08" y2="256.54" width="0.1524" layer="91"/>
<label x="-132.08" y="256.54" size="1.778" layer="95"/>
<pinref part="C7" gate="C1" pin="1"/>
<wire x1="-124.46" y1="254" x2="-124.46" y2="256.54" width="0.1524" layer="91"/>
<junction x="-124.46" y="256.54"/>
<pinref part="V1" gate="G$1" pin="CE"/>
<wire x1="-114.3" y1="251.46" x2="-116.84" y2="251.46" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="251.46" x2="-116.84" y2="256.54" width="0.1524" layer="91"/>
<junction x="-116.84" y="256.54"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="-388.62" y1="73.66" x2="-388.62" y2="63.5" width="0.1524" layer="91"/>
<label x="-388.62" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LED2" gate="G$1" pin="ANODE"/>
<wire x1="-22.86" y1="137.16" x2="-22.86" y2="139.7" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="139.7" x2="-30.48" y2="139.7" width="0.1524" layer="91"/>
<label x="-30.48" y="139.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LCD" gate="A" pin="2"/>
<wire x1="7.62" y1="22.86" x2="35.56" y2="22.86" width="0.1524" layer="91"/>
<wire x1="35.56" y1="22.86" x2="35.56" y2="10.16" width="0.1524" layer="91"/>
<junction x="35.56" y="22.86"/>
<label x="35.56" y="10.16" size="1.778" layer="95" rot="R90"/>
<pinref part="LCD" gate="A" pin="15"/>
<wire x1="7.62" y1="55.88" x2="35.56" y2="55.88" width="0.1524" layer="91"/>
<wire x1="35.56" y1="55.88" x2="35.56" y2="22.86" width="0.1524" layer="91"/>
<label x="17.78" y="55.88" size="1.778" layer="95"/>
<label x="17.78" y="22.86" size="1.778" layer="95"/>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="35.56" y1="22.86" x2="40.64" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-238.76" y1="284.48" x2="-233.68" y2="284.48" width="0.1524" layer="91"/>
<label x="-238.76" y="284.48" size="1.778" layer="95"/>
<pinref part="U$4" gate="G$1" pin="VDD"/>
</segment>
<segment>
<pinref part="CAMERA" gate="G$1" pin="1"/>
<wire x1="-220.98" y1="241.3" x2="-231.14" y2="241.3" width="0.1524" layer="91"/>
<label x="-231.14" y="241.3" size="1.778" layer="95"/>
</segment>
<segment>
<label x="-198.12" y="162.56" size="1.778" layer="95" rot="R90"/>
<pinref part="I2C5V" gate="A" pin="1"/>
<wire x1="-198.12" y1="185.42" x2="-198.12" y2="162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SERVO1" gate="A" pin="2"/>
<wire x1="-243.84" y1="68.58" x2="-256.54" y2="68.58" width="0.1524" layer="91"/>
<label x="-256.54" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SERVO2" gate="A" pin="2"/>
<wire x1="-243.84" y1="50.8" x2="-256.54" y2="50.8" width="0.1524" layer="91"/>
<label x="-256.54" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SERVO3" gate="A" pin="2"/>
<wire x1="-243.84" y1="33.02" x2="-256.54" y2="33.02" width="0.1524" layer="91"/>
<label x="-256.54" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SERVO4" gate="A" pin="2"/>
<wire x1="-243.84" y1="15.24" x2="-256.54" y2="15.24" width="0.1524" layer="91"/>
<label x="-256.54" y="15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="V2" gate="G$1" pin="SW"/>
<pinref part="L3" gate="G$1" pin="2"/>
<wire x1="-93.98" y1="279.4" x2="-96.52" y2="279.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RGB_R" class="0">
<segment>
<pinref part="F3" gate="G$2" pin="G"/>
<wire x1="-43.18" y1="99.06" x2="-53.34" y2="99.06" width="0.1524" layer="91"/>
<label x="-53.34" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PD5"/>
<wire x1="60.96" y1="208.28" x2="83.82" y2="208.28" width="0.1524" layer="91"/>
<label x="63.5" y="208.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="RGB_G" class="0">
<segment>
<pinref part="F1" gate="G$2" pin="G"/>
<wire x1="-68.58" y1="99.06" x2="-78.74" y2="99.06" width="0.1524" layer="91"/>
<label x="-78.74" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PE5"/>
<wire x1="60.96" y1="185.42" x2="83.82" y2="185.42" width="0.1524" layer="91"/>
<label x="63.5" y="185.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="RGB_B" class="0">
<segment>
<pinref part="F1" gate="G$1" pin="G"/>
<wire x1="-93.98" y1="99.06" x2="-104.14" y2="99.06" width="0.1524" layer="91"/>
<label x="-104.14" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PE4"/>
<wire x1="60.96" y1="187.96" x2="83.82" y2="187.96" width="0.1524" layer="91"/>
<label x="63.5" y="187.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="RGB_B_SUPPLY" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="-88.9" y1="111.76" x2="-88.9" y2="109.22" width="0.1524" layer="91"/>
<wire x1="-88.9" y1="109.22" x2="-55.88" y2="109.22" width="0.1524" layer="91"/>
<pinref part="R31" gate="G$1" pin="1"/>
<wire x1="-55.88" y1="109.22" x2="-55.88" y2="119.38" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="119.38" x2="-53.34" y2="121.92" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="121.92" x2="-50.8" y2="121.92" width="0.1524" layer="91"/>
<pinref part="F1" gate="G$1" pin="D"/>
<wire x1="-88.9" y1="106.68" x2="-88.9" y2="109.22" width="0.1524" layer="91"/>
<junction x="-88.9" y="109.22"/>
</segment>
</net>
<net name="RGB_G_SUPPLY" class="0">
<segment>
<pinref part="F1" gate="G$2" pin="D"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="-63.5" y1="106.68" x2="-63.5" y2="116.84" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="116.84" x2="-68.58" y2="116.84" width="0.1524" layer="91"/>
<pinref part="R30" gate="G$1" pin="1"/>
<wire x1="-35.56" y1="116.84" x2="-63.5" y2="116.84" width="0.1524" layer="91"/>
<junction x="-63.5" y="116.84"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<pinref part="LED1" gate="G$1" pin="B"/>
<wire x1="-88.9" y1="121.92" x2="-88.9" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<pinref part="LED1" gate="G$1" pin="G"/>
<wire x1="-78.74" y1="116.84" x2="-81.28" y2="119.38" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="119.38" x2="-81.28" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<pinref part="LED1" gate="G$1" pin="R"/>
<wire x1="-50.8" y1="111.76" x2="-58.42" y2="111.76" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="111.76" x2="-58.42" y2="121.92" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="121.92" x2="-60.96" y2="124.46" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="124.46" x2="-73.66" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MIC_CON/SPEAK" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="-388.62" y1="53.34" x2="-388.62" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C13" gate="C1" pin="2"/>
<wire x1="-388.62" y1="45.72" x2="-386.08" y2="45.72" width="0.1524" layer="91"/>
<pinref part="MIC" gate="G$1" pin="1"/>
<wire x1="-388.62" y1="45.72" x2="-396.24" y2="45.72" width="0.1524" layer="91"/>
<junction x="-388.62" y="45.72"/>
</segment>
<segment>
<pinref part="C20" gate="C1" pin="2"/>
<wire x1="-127" y1="180.34" x2="-124.46" y2="180.34" width="0.1524" layer="91"/>
<wire x1="-124.46" y1="180.34" x2="-124.46" y2="167.64" width="0.1524" layer="91"/>
<label x="-144.78" y="167.64" size="1.778" layer="95"/>
<wire x1="-124.46" y1="167.64" x2="-144.78" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="LM358D" gate="A" pin="-IN"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="-360.68" y1="45.72" x2="-363.22" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="-363.22" y1="45.72" x2="-365.76" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-363.22" y1="45.72" x2="-363.22" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-363.22" y1="63.5" x2="-358.14" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-363.22" y1="63.5" x2="-363.22" y2="71.12" width="0.1524" layer="91"/>
<pinref part="C14" gate="C1" pin="1"/>
<wire x1="-363.22" y1="71.12" x2="-355.6" y2="71.12" width="0.1524" layer="91"/>
<junction x="-363.22" y="63.5"/>
<junction x="-363.22" y="45.72"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="-347.98" y1="63.5" x2="-340.36" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-340.36" y1="63.5" x2="-340.36" y2="43.18" width="0.1524" layer="91"/>
<pinref part="LM358D" gate="A" pin="OUT"/>
<wire x1="-340.36" y1="43.18" x2="-345.44" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C14" gate="C1" pin="2"/>
<wire x1="-347.98" y1="71.12" x2="-340.36" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-340.36" y1="71.12" x2="-340.36" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C16" gate="C1" pin="1"/>
<wire x1="-340.36" y1="43.18" x2="-337.82" y2="43.18" width="0.1524" layer="91"/>
<junction x="-337.82" y="43.18"/>
<wire x1="-337.82" y1="43.18" x2="-335.28" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="-337.82" y1="43.18" x2="-337.82" y2="40.64" width="0.1524" layer="91"/>
<junction x="-340.36" y="63.5"/>
<junction x="-340.36" y="43.18"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="-368.3" y1="20.32" x2="-365.76" y2="20.32" width="0.1524" layer="91"/>
<pinref part="C15" gate="C1" pin="1"/>
<wire x1="-365.76" y1="20.32" x2="-365.76" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-365.76" y1="20.32" x2="-365.76" y2="40.64" width="0.1524" layer="91"/>
<pinref part="LM358D" gate="A" pin="+IN"/>
<wire x1="-365.76" y1="40.64" x2="-360.68" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="-360.68" y1="20.32" x2="-365.76" y2="20.32" width="0.1524" layer="91"/>
<junction x="-365.76" y="20.32"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="C13" gate="C1" pin="1"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="-378.46" y1="45.72" x2="-375.92" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MIC_AMP" class="0">
<segment>
<pinref part="C16" gate="C1" pin="2"/>
<wire x1="-327.66" y1="43.18" x2="-322.58" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="-322.58" y1="43.18" x2="-322.58" y2="40.64" width="0.1524" layer="91"/>
<junction x="-322.58" y="43.18"/>
<wire x1="-322.58" y1="43.18" x2="-314.96" y2="43.18" width="0.1524" layer="91"/>
<label x="-314.96" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PA4"/>
<wire x1="60.96" y1="279.4" x2="83.82" y2="279.4" width="0.1524" layer="91"/>
<label x="63.5" y="279.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="DAC_EXT(NOTCONNECTED)" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PB3"/>
<wire x1="60.96" y1="259.08" x2="83.82" y2="259.08" width="0.1524" layer="91"/>
<label x="63.5" y="259.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="LCD14_D4" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PB7"/>
<wire x1="60.96" y1="248.92" x2="83.82" y2="248.92" width="0.1524" layer="91"/>
<label x="63.5" y="248.92" size="1.778" layer="95"/>
</segment>
<segment>
<label x="17.78" y="53.34" size="1.778" layer="95"/>
<pinref part="LCD" gate="A" pin="14"/>
<wire x1="7.62" y1="53.34" x2="27.94" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SPK_CON-" class="0">
<segment>
<label x="-231.14" y="266.7" size="1.778" layer="95"/>
<wire x1="-238.76" y1="271.78" x2="-228.6" y2="271.78" width="0.1524" layer="91"/>
<wire x1="-228.6" y1="271.78" x2="-228.6" y2="276.86" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="SPK-"/>
<pinref part="SPEAKER" gate="G$1" pin="2"/>
<wire x1="-228.6" y1="276.86" x2="-226.06" y2="276.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CAM_START" class="0">
<segment>
<pinref part="CAMERA" gate="G$1" pin="2"/>
<wire x1="-220.98" y1="238.76" x2="-238.76" y2="238.76" width="0.1524" layer="91"/>
<label x="-238.76" y="238.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PF6"/>
<wire x1="60.96" y1="160.02" x2="83.82" y2="160.02" width="0.1524" layer="91"/>
<label x="63.5" y="160.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="VREG" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VREG"/>
<wire x1="-358.14" y1="144.78" x2="-381" y2="144.78" width="0.1524" layer="91"/>
<wire x1="-381" y1="144.78" x2="-381" y2="127" width="0.1524" layer="91"/>
<label x="-375.92" y="144.78" size="1.778" layer="95"/>
<pinref part="C19" gate="C1" pin="2"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<wire x1="-363.22" y1="124.46" x2="-363.22" y2="132.08" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="REXT"/>
<wire x1="-363.22" y1="132.08" x2="-358.14" y2="132.08" width="0.1524" layer="91"/>
<pinref part="R42" gate="G$1" pin="1"/>
</segment>
</net>
<net name="!CT_IRQ" class="0">
<segment>
<wire x1="-386.08" y1="147.32" x2="-386.08" y2="142.24" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="~IRQ"/>
<wire x1="-358.14" y1="142.24" x2="-386.08" y2="142.24" width="0.1524" layer="91"/>
<label x="-375.92" y="142.24" size="1.778" layer="95"/>
<pinref part="R41" gate="G$1" pin="2"/>
</segment>
</net>
<net name="CAM_VOUT" class="0">
<segment>
<pinref part="CAMERA" gate="G$1" pin="8"/>
<wire x1="-220.98" y1="223.52" x2="-238.76" y2="223.52" width="0.1524" layer="91"/>
<label x="-238.76" y="223.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PA0"/>
<wire x1="60.96" y1="289.56" x2="83.82" y2="289.56" width="0.1524" layer="91"/>
<label x="63.5" y="289.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="CAM_READ_HV" class="0">
<segment>
<pinref part="CAMERA" gate="G$1" pin="7"/>
<wire x1="-220.98" y1="226.06" x2="-264.16" y2="226.06" width="0.1524" layer="91"/>
<label x="-238.76" y="226.06" size="1.778" layer="95"/>
<wire x1="-264.16" y1="226.06" x2="-264.16" y2="246.38" width="0.1524" layer="91"/>
<wire x1="-264.16" y1="246.38" x2="-271.78" y2="246.38" width="0.1524" layer="91"/>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="-271.78" y1="246.38" x2="-271.78" y2="243.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="120VIN" class="0">
<segment>
<label x="-335.28" y="248.92" size="1.778" layer="95"/>
<pinref part="K1" gate="G$1" pin="DS1"/>
<pinref part="B1" gate="G$1" pin="AC1"/>
<wire x1="-355.6" y1="248.92" x2="-317.5" y2="248.92" width="0.1524" layer="91"/>
<wire x1="-355.6" y1="248.92" x2="-358.14" y2="248.92" width="0.1524" layer="91"/>
<wire x1="-340.36" y1="279.4" x2="-340.36" y2="281.94" width="0.1524" layer="91"/>
<wire x1="-340.36" y1="281.94" x2="-355.6" y2="281.94" width="0.1524" layer="91"/>
<wire x1="-355.6" y1="281.94" x2="-355.6" y2="248.92" width="0.1524" layer="91"/>
<junction x="-355.6" y="248.92"/>
<pinref part="75VAC" gate="G$1" pin="1"/>
</segment>
</net>
<net name="120VCONTROL" class="0">
<segment>
<pinref part="K1" gate="G$1" pin="A"/>
<wire x1="-378.46" y1="248.92" x2="-398.78" y2="248.92" width="0.1524" layer="91"/>
<label x="-398.78" y="248.92" size="1.778" layer="95"/>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="-398.78" y1="266.7" x2="-398.78" y2="248.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<wire x1="-358.14" y1="137.16" x2="-375.92" y2="137.16" width="0.1524" layer="91"/>
<label x="-375.92" y="137.16" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="SDA"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PE0"/>
<wire x1="60.96" y1="198.12" x2="83.82" y2="198.12" width="0.1524" layer="91"/>
<label x="63.5" y="198.12" size="1.778" layer="95"/>
</segment>
<segment>
<label x="-215.9" y="162.56" size="1.778" layer="95" rot="R90"/>
<pinref part="R35" gate="G$1" pin="2"/>
<wire x1="-215.9" y1="175.26" x2="-215.9" y2="162.56" width="0.1524" layer="91"/>
<wire x1="-220.98" y1="175.26" x2="-215.9" y2="175.26" width="0.1524" layer="91"/>
<junction x="-215.9" y="175.26"/>
<pinref part="I2C3V3" gate="A" pin="2"/>
<wire x1="-215.9" y1="175.26" x2="-215.9" y2="185.42" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="-195.58" y="162.56" size="1.778" layer="95" rot="R90"/>
<pinref part="I2C5V" gate="A" pin="2"/>
<wire x1="-195.58" y1="185.42" x2="-195.58" y2="162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ACCEL" gate="G$1" pin="SDA"/>
<wire x1="-53.34" y1="172.72" x2="-45.72" y2="172.72" width="0.1524" layer="91"/>
<label x="-50.8" y="172.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MEMORY" gate="G$1" pin="SDA"/>
<wire x1="-157.48" y1="220.98" x2="-149.86" y2="220.98" width="0.1524" layer="91"/>
<label x="-154.94" y="220.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<label x="-375.92" y="139.7" size="1.778" layer="95"/>
<wire x1="-358.14" y1="139.7" x2="-375.92" y2="139.7" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="SCL"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PE1"/>
<wire x1="60.96" y1="195.58" x2="83.82" y2="195.58" width="0.1524" layer="91"/>
<label x="63.5" y="195.58" size="1.778" layer="95"/>
</segment>
<segment>
<label x="-213.36" y="162.56" size="1.778" layer="95" rot="R90"/>
<pinref part="R36" gate="G$1" pin="2"/>
<wire x1="-213.36" y1="185.42" x2="-213.36" y2="167.64" width="0.1524" layer="91"/>
<wire x1="-213.36" y1="167.64" x2="-213.36" y2="162.56" width="0.1524" layer="91"/>
<wire x1="-220.98" y1="167.64" x2="-213.36" y2="167.64" width="0.1524" layer="91"/>
<junction x="-213.36" y="167.64"/>
<pinref part="I2C3V3" gate="A" pin="3"/>
</segment>
<segment>
<label x="-193.04" y="162.56" size="1.778" layer="95" rot="R90"/>
<pinref part="I2C5V" gate="A" pin="3"/>
<wire x1="-193.04" y1="185.42" x2="-193.04" y2="162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="MEMORY" gate="G$1" pin="SCL"/>
<wire x1="-157.48" y1="223.52" x2="-149.86" y2="223.52" width="0.1524" layer="91"/>
<label x="-154.94" y="223.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ACCEL" gate="G$1" pin="SCL"/>
<wire x1="-53.34" y1="167.64" x2="-45.72" y2="167.64" width="0.1524" layer="91"/>
<label x="-50.8" y="167.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPK_CON+" class="0">
<segment>
<label x="-231.14" y="287.02" size="1.778" layer="95"/>
<wire x1="-238.76" y1="274.32" x2="-236.22" y2="274.32" width="0.1524" layer="91"/>
<wire x1="-236.22" y1="274.32" x2="-236.22" y2="279.4" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="SPK+"/>
<pinref part="SPEAKER" gate="G$1" pin="1"/>
<wire x1="-236.22" y1="279.4" x2="-226.06" y2="279.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MOTOR1A" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PC0"/>
<wire x1="60.96" y1="243.84" x2="83.82" y2="243.84" width="0.1524" layer="91"/>
<label x="63.5" y="243.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="1A"/>
<wire x1="-124.46" y1="43.18" x2="-147.32" y2="43.18" width="0.1524" layer="91"/>
<label x="-147.32" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-218.44" y1="66.04" x2="-162.56" y2="66.04" width="0.1524" layer="91"/>
<label x="-218.44" y="66.04" size="1.778" layer="95"/>
<label x="-167.64" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOR1B" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="2A"/>
<wire x1="-124.46" y1="17.78" x2="-147.32" y2="17.78" width="0.1524" layer="91"/>
<label x="-147.32" y="17.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="-177.8" y1="60.96" x2="-172.72" y2="60.96" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="-172.72" y1="60.96" x2="-162.56" y2="60.96" width="0.1524" layer="91"/>
<junction x="-172.72" y="60.96"/>
<label x="-167.64" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOR2A" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PC4"/>
<wire x1="60.96" y1="233.68" x2="83.82" y2="233.68" width="0.1524" layer="91"/>
<label x="63.5" y="233.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="3A"/>
<wire x1="-93.98" y1="17.78" x2="-71.12" y2="17.78" width="0.1524" layer="91"/>
<label x="-81.28" y="17.78" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-218.44" y1="48.26" x2="-162.56" y2="48.26" width="0.1524" layer="91"/>
<label x="-218.44" y="48.26" size="1.778" layer="95"/>
<label x="-167.64" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOR2B" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="4A"/>
<wire x1="-93.98" y1="43.18" x2="-71.12" y2="43.18" width="0.1524" layer="91"/>
<label x="-81.28" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="-180.34" y1="43.18" x2="-167.64" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="-167.64" y1="43.18" x2="-162.56" y2="43.18" width="0.1524" layer="91"/>
<junction x="-167.64" y="43.18"/>
<label x="-167.64" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX1" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PC6"/>
<wire x1="60.96" y1="228.6" x2="83.82" y2="228.6" width="0.1524" layer="91"/>
<label x="63.5" y="228.6" size="1.778" layer="95"/>
</segment>
<segment>
<label x="-264.16" y="162.56" size="1.778" layer="95" rot="R90"/>
<pinref part="NODE1" gate="A" pin="2"/>
<wire x1="-264.16" y1="185.42" x2="-264.16" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TX1" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PC7"/>
<wire x1="60.96" y1="226.06" x2="83.82" y2="226.06" width="0.1524" layer="91"/>
<label x="63.5" y="226.06" size="1.778" layer="95"/>
</segment>
<segment>
<label x="-261.62" y="162.56" size="1.778" layer="95" rot="R90"/>
<pinref part="NODE1" gate="A" pin="3"/>
<wire x1="-261.62" y1="185.42" x2="-261.62" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ELE2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ELE2"/>
<wire x1="-322.58" y1="127" x2="-312.42" y2="127" width="0.1524" layer="91"/>
<label x="-320.04" y="127" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-373.38" y1="185.42" x2="-396.24" y2="185.42" width="0.1524" layer="91"/>
<label x="-396.24" y="185.42" size="1.778" layer="95"/>
<pinref part="ELEC0" gate="G$1" pin="3"/>
</segment>
</net>
<net name="ELE3" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ELE3"/>
<wire x1="-322.58" y1="129.54" x2="-312.42" y2="129.54" width="0.1524" layer="91"/>
<label x="-320.04" y="129.54" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-373.38" y1="182.88" x2="-396.24" y2="182.88" width="0.1524" layer="91"/>
<label x="-396.24" y="182.88" size="1.778" layer="95"/>
<pinref part="ELEC0" gate="G$1" pin="4"/>
</segment>
</net>
<net name="ELE4" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ELE4/LED0"/>
<wire x1="-322.58" y1="132.08" x2="-312.42" y2="132.08" width="0.1524" layer="91"/>
<label x="-320.04" y="132.08" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-373.38" y1="180.34" x2="-396.24" y2="180.34" width="0.1524" layer="91"/>
<label x="-396.24" y="180.34" size="1.778" layer="95"/>
<pinref part="ELEC0" gate="G$1" pin="5"/>
</segment>
</net>
<net name="ELE5" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ELE5/LED1"/>
<wire x1="-322.58" y1="134.62" x2="-312.42" y2="134.62" width="0.1524" layer="91"/>
<label x="-320.04" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-373.38" y1="177.8" x2="-396.24" y2="177.8" width="0.1524" layer="91"/>
<label x="-396.24" y="177.8" size="1.778" layer="95"/>
<pinref part="ELEC0" gate="G$1" pin="6"/>
</segment>
</net>
<net name="ELE6" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ELE6/LED2"/>
<wire x1="-322.58" y1="137.16" x2="-312.42" y2="137.16" width="0.1524" layer="91"/>
<label x="-320.04" y="137.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ELEC1" gate="G$1" pin="1"/>
<wire x1="-330.2" y1="190.5" x2="-353.06" y2="190.5" width="0.1524" layer="91"/>
<label x="-353.06" y="190.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="ELE7" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ELE7/LED3"/>
<wire x1="-322.58" y1="139.7" x2="-312.42" y2="139.7" width="0.1524" layer="91"/>
<label x="-320.04" y="139.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ELEC1" gate="G$1" pin="2"/>
<wire x1="-330.2" y1="187.96" x2="-353.06" y2="187.96" width="0.1524" layer="91"/>
<label x="-353.06" y="187.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="ELE1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ELE1"/>
<wire x1="-322.58" y1="124.46" x2="-312.42" y2="124.46" width="0.1524" layer="91"/>
<label x="-320.04" y="124.46" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-373.38" y1="187.96" x2="-396.24" y2="187.96" width="0.1524" layer="91"/>
<label x="-396.24" y="187.96" size="1.778" layer="95"/>
<pinref part="ELEC0" gate="G$1" pin="2"/>
</segment>
</net>
<net name="ELE0" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ELE0"/>
<wire x1="-322.58" y1="121.92" x2="-312.42" y2="121.92" width="0.1524" layer="91"/>
<label x="-320.04" y="121.92" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-373.38" y1="190.5" x2="-396.24" y2="190.5" width="0.1524" layer="91"/>
<label x="-396.24" y="190.5" size="1.778" layer="95"/>
<pinref part="ELEC0" gate="G$1" pin="1"/>
</segment>
</net>
<net name="ELE8" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ELE8/LED4"/>
<wire x1="-322.58" y1="142.24" x2="-312.42" y2="142.24" width="0.1524" layer="91"/>
<label x="-320.04" y="142.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ELEC1" gate="G$1" pin="3"/>
<wire x1="-330.2" y1="185.42" x2="-353.06" y2="185.42" width="0.1524" layer="91"/>
<label x="-353.06" y="185.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="ELE9" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ELE9/LED5"/>
<wire x1="-322.58" y1="144.78" x2="-312.42" y2="144.78" width="0.1524" layer="91"/>
<label x="-320.04" y="144.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ELEC1" gate="G$1" pin="4"/>
<wire x1="-330.2" y1="182.88" x2="-353.06" y2="182.88" width="0.1524" layer="91"/>
<label x="-353.06" y="182.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="ELE10" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ELE10/LED6"/>
<wire x1="-322.58" y1="147.32" x2="-312.42" y2="147.32" width="0.1524" layer="91"/>
<label x="-320.04" y="147.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ELEC1" gate="G$1" pin="5"/>
<wire x1="-330.2" y1="180.34" x2="-353.06" y2="180.34" width="0.1524" layer="91"/>
<label x="-353.06" y="180.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="ELE11" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ELE11/LED7"/>
<wire x1="-322.58" y1="149.86" x2="-312.42" y2="149.86" width="0.1524" layer="91"/>
<label x="-320.04" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ELEC1" gate="G$1" pin="6"/>
<wire x1="-330.2" y1="177.8" x2="-353.06" y2="177.8" width="0.1524" layer="91"/>
<label x="-353.06" y="177.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX3" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PD6"/>
<wire x1="60.96" y1="205.74" x2="83.82" y2="205.74" width="0.1524" layer="91"/>
<label x="63.5" y="205.74" size="1.778" layer="95"/>
</segment>
<segment>
<label x="-279.4" y="106.68" size="1.778" layer="95" rot="R90"/>
<wire x1="-279.4" y1="132.08" x2="-279.4" y2="106.68" width="0.1524" layer="91"/>
<pinref part="NODE5" gate="A" pin="2"/>
</segment>
</net>
<net name="TX3" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PD7"/>
<wire x1="60.96" y1="203.2" x2="83.82" y2="203.2" width="0.1524" layer="91"/>
<label x="63.5" y="203.2" size="1.778" layer="95"/>
</segment>
<segment>
<label x="-276.86" y="106.68" size="1.778" layer="95" rot="R90"/>
<wire x1="-276.86" y1="132.08" x2="-276.86" y2="106.68" width="0.1524" layer="91"/>
<pinref part="NODE5" gate="A" pin="3"/>
</segment>
</net>
<net name="RX5" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PE6"/>
<wire x1="60.96" y1="182.88" x2="83.82" y2="182.88" width="0.1524" layer="91"/>
<label x="63.5" y="182.88" size="1.778" layer="95"/>
</segment>
<segment>
<label x="-241.3" y="106.68" size="1.778" layer="95" rot="R90"/>
<wire x1="-241.3" y1="132.08" x2="-241.3" y2="106.68" width="0.1524" layer="91"/>
<pinref part="NODE3" gate="A" pin="2"/>
</segment>
</net>
<net name="TX5" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PE7"/>
<wire x1="60.96" y1="180.34" x2="83.82" y2="180.34" width="0.1524" layer="91"/>
<label x="63.5" y="180.34" size="1.778" layer="95"/>
</segment>
<segment>
<label x="-238.76" y="106.68" size="1.778" layer="95" rot="R90"/>
<wire x1="-238.76" y1="132.08" x2="-238.76" y2="106.68" width="0.1524" layer="91"/>
<pinref part="NODE3" gate="A" pin="3"/>
</segment>
</net>
<net name="PHOTO2" class="0">
<segment>
<pinref part="PHOTORESISTOR2" gate="G$1" pin="C"/>
<wire x1="55.88" y1="124.46" x2="63.5" y2="124.46" width="0.1524" layer="91"/>
<wire x1="63.5" y1="124.46" x2="63.5" y2="121.92" width="0.1524" layer="91"/>
<wire x1="63.5" y1="124.46" x2="71.12" y2="124.46" width="0.1524" layer="91"/>
<label x="66.04" y="124.46" size="1.778" layer="95"/>
<pinref part="R24" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PA1"/>
<wire x1="60.96" y1="287.02" x2="83.82" y2="287.02" width="0.1524" layer="91"/>
<label x="63.5" y="287.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="PHOTO1" class="0">
<segment>
<pinref part="PHOTORESISTOR1" gate="G$1" pin="C"/>
<wire x1="25.4" y1="114.3" x2="33.02" y2="114.3" width="0.1524" layer="91"/>
<wire x1="33.02" y1="114.3" x2="33.02" y2="111.76" width="0.1524" layer="91"/>
<wire x1="33.02" y1="114.3" x2="40.64" y2="114.3" width="0.1524" layer="91"/>
<label x="35.56" y="114.3" size="1.778" layer="95"/>
<pinref part="R25" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PA3"/>
<wire x1="60.96" y1="281.94" x2="83.82" y2="281.94" width="0.1524" layer="91"/>
<label x="63.5" y="281.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="-40.64" y1="121.92" x2="-30.48" y2="121.92" width="0.1524" layer="91"/>
<pinref part="LED2" gate="G$1" pin="B"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="R30" gate="G$1" pin="2"/>
<pinref part="LED2" gate="G$1" pin="G"/>
<wire x1="-25.4" y1="116.84" x2="-22.86" y2="119.38" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="119.38" x2="-22.86" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="-15.24" y1="111.76" x2="-15.24" y2="121.92" width="0.1524" layer="91"/>
<pinref part="LED2" gate="G$1" pin="R"/>
</segment>
</net>
<net name="LCD_R/SERVO_2" class="0">
<segment>
<pinref part="R38" gate="G$1" pin="2"/>
<wire x1="-266.7" y1="53.34" x2="-287.02" y2="53.34" width="0.1524" layer="91"/>
<label x="-287.02" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="58.42" y1="20.32" x2="58.42" y2="43.18" width="0.1524" layer="91"/>
<label x="58.42" y="22.86" size="1.778" layer="95" rot="R90"/>
<pinref part="F2" gate="G$2" pin="G"/>
<wire x1="58.42" y1="20.32" x2="60.96" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PD0"/>
<wire x1="60.96" y1="220.98" x2="83.82" y2="220.98" width="0.1524" layer="91"/>
<label x="63.5" y="220.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="LCD_G/SERVO_3" class="0">
<segment>
<pinref part="R39" gate="G$1" pin="2"/>
<wire x1="-266.7" y1="35.56" x2="-287.02" y2="35.56" width="0.1524" layer="91"/>
<label x="-287.02" y="35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="F2" gate="G$1" pin="G"/>
<wire x1="73.66" y1="20.32" x2="71.12" y2="20.32" width="0.1524" layer="91"/>
<wire x1="71.12" y1="20.32" x2="71.12" y2="43.18" width="0.1524" layer="91"/>
<label x="71.12" y="22.86" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PD1"/>
<wire x1="60.96" y1="218.44" x2="83.82" y2="218.44" width="0.1524" layer="91"/>
<label x="63.5" y="218.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="UART" gate="A" pin="4"/>
<wire x1="-215.9" y1="124.46" x2="-215.9" y2="132.08" width="0.1524" layer="91"/>
<wire x1="-215.9" y1="124.46" x2="-213.36" y2="121.92" width="0.1524" layer="91"/>
<pinref part="USBPOWER" gate="G$1" pin="1"/>
<wire x1="-213.36" y1="121.92" x2="-213.36" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LCD13_D3" class="0">
<segment>
<label x="17.78" y="50.8" size="1.778" layer="95"/>
<pinref part="LCD" gate="A" pin="13"/>
<wire x1="7.62" y1="50.8" x2="27.94" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PB6"/>
<wire x1="60.96" y1="251.46" x2="83.82" y2="251.46" width="0.1524" layer="91"/>
<label x="63.5" y="251.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="LCD12_D2" class="0">
<segment>
<label x="17.78" y="48.26" size="1.778" layer="95"/>
<pinref part="LCD" gate="A" pin="12"/>
<wire x1="7.62" y1="48.26" x2="27.94" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PB5"/>
<wire x1="60.96" y1="254" x2="83.82" y2="254" width="0.1524" layer="91"/>
<label x="63.5" y="254" size="1.778" layer="95"/>
</segment>
</net>
<net name="LCD11_D1" class="0">
<segment>
<wire x1="7.62" y1="45.72" x2="27.94" y2="45.72" width="0.1524" layer="91"/>
<label x="17.78" y="45.72" size="1.778" layer="95"/>
<pinref part="LCD" gate="A" pin="11"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PB4"/>
<wire x1="60.96" y1="256.54" x2="83.82" y2="256.54" width="0.1524" layer="91"/>
<label x="63.5" y="256.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="LCD18_B-" class="0">
<segment>
<pinref part="F3" gate="G$1" pin="D"/>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="63.5" y1="63.5" x2="91.44" y2="63.5" width="0.1524" layer="91"/>
<wire x1="91.44" y1="63.5" x2="91.44" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="F3" gate="G$1" pin="S"/>
<wire x1="91.44" y1="17.78" x2="91.44" y2="12.7" width="0.1524" layer="91"/>
<wire x1="91.44" y1="12.7" x2="78.74" y2="12.7" width="0.1524" layer="91"/>
<wire x1="78.74" y1="12.7" x2="76.2" y2="12.7" width="0.1524" layer="91"/>
<wire x1="76.2" y1="12.7" x2="76.2" y2="7.62" width="0.1524" layer="91"/>
<label x="76.2" y="7.62" size="1.778" layer="95"/>
<wire x1="66.04" y1="17.78" x2="66.04" y2="12.7" width="0.1524" layer="91"/>
<wire x1="66.04" y1="12.7" x2="76.2" y2="12.7" width="0.1524" layer="91"/>
<junction x="76.2" y="12.7"/>
<pinref part="F2" gate="G$2" pin="S"/>
<pinref part="F2" gate="G$1" pin="S"/>
<wire x1="78.74" y1="17.78" x2="78.74" y2="12.7" width="0.1524" layer="91"/>
<junction x="78.74" y="12.7"/>
</segment>
<segment>
<pinref part="F1" gate="G$1" pin="S"/>
<wire x1="-88.9" y1="96.52" x2="-88.9" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-88.9" y1="93.98" x2="-63.5" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="93.98" x2="-38.1" y2="93.98" width="0.1524" layer="91"/>
<pinref part="F1" gate="G$2" pin="S"/>
<wire x1="-63.5" y1="96.52" x2="-63.5" y2="93.98" width="0.1524" layer="91"/>
<junction x="-63.5" y="93.98"/>
<wire x1="-88.9" y1="93.98" x2="-106.68" y2="93.98" width="0.1524" layer="91"/>
<junction x="-88.9" y="93.98"/>
<label x="-106.68" y="93.98" size="1.778" layer="95"/>
<pinref part="F3" gate="G$2" pin="S"/>
<wire x1="-38.1" y1="93.98" x2="-38.1" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="GND1"/>
<wire x1="-5.08" y1="274.32" x2="-12.7" y2="274.32" width="0.1524" layer="91"/>
<pinref part="XMEGA" gate="G$1" pin="GND5"/>
<wire x1="-12.7" y1="274.32" x2="-22.86" y2="274.32" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="264.16" x2="-12.7" y2="264.16" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="264.16" x2="-12.7" y2="266.7" width="0.1524" layer="91"/>
<junction x="-12.7" y="274.32"/>
<pinref part="XMEGA" gate="G$1" pin="GND2"/>
<wire x1="-12.7" y1="266.7" x2="-12.7" y2="269.24" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="269.24" x2="-12.7" y2="271.78" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="271.78" x2="-12.7" y2="274.32" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="271.78" x2="-12.7" y2="271.78" width="0.1524" layer="91"/>
<junction x="-12.7" y="271.78"/>
<pinref part="XMEGA" gate="G$1" pin="GND3"/>
<wire x1="-5.08" y1="269.24" x2="-12.7" y2="269.24" width="0.1524" layer="91"/>
<junction x="-12.7" y="269.24"/>
<pinref part="XMEGA" gate="G$1" pin="GND4"/>
<wire x1="-5.08" y1="266.7" x2="-12.7" y2="266.7" width="0.1524" layer="91"/>
<junction x="-12.7" y="266.7"/>
<label x="-22.86" y="274.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="AGND"/>
<wire x1="-5.08" y1="248.92" x2="-22.86" y2="248.92" width="0.1524" layer="91"/>
<label x="-22.86" y="248.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="V1" gate="G$1" pin="VSS"/>
<wire x1="-114.3" y1="246.38" x2="-124.46" y2="246.38" width="0.1524" layer="91"/>
<label x="-132.08" y="246.38" size="1.778" layer="95"/>
<pinref part="C7" gate="C1" pin="2"/>
<wire x1="-124.46" y1="246.38" x2="-132.08" y2="246.38" width="0.1524" layer="91"/>
<junction x="-124.46" y="246.38"/>
</segment>
<segment>
<pinref part="C8" gate="C1" pin="2"/>
<wire x1="-91.44" y1="246.38" x2="-91.44" y2="243.84" width="0.1524" layer="91"/>
<wire x1="-91.44" y1="243.84" x2="-71.12" y2="243.84" width="0.1524" layer="91"/>
<pinref part="C9" gate="C1" pin="2"/>
<wire x1="-71.12" y1="243.84" x2="-71.12" y2="246.38" width="0.1524" layer="91"/>
<pinref part="C10" gate="C1" pin="2"/>
<wire x1="-71.12" y1="243.84" x2="-50.8" y2="243.84" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="243.84" x2="-50.8" y2="246.38" width="0.1524" layer="91"/>
<junction x="-71.12" y="243.84"/>
<label x="-81.28" y="243.84" size="1.778" layer="95"/>
<label x="-43.18" y="243.84" size="1.778" layer="95"/>
<wire x1="-50.8" y1="243.84" x2="-40.64" y2="243.84" width="0.1524" layer="91"/>
<junction x="-50.8" y="243.84"/>
</segment>
<segment>
<pinref part="V2" gate="G$1" pin="GND"/>
<wire x1="-93.98" y1="274.32" x2="-124.46" y2="274.32" width="0.1524" layer="91"/>
<pinref part="C11" gate="C1" pin="2"/>
<wire x1="-124.46" y1="274.32" x2="-132.08" y2="274.32" width="0.1524" layer="91"/>
<wire x1="-124.46" y1="276.86" x2="-124.46" y2="274.32" width="0.1524" layer="91"/>
<junction x="-124.46" y="274.32"/>
<label x="-132.08" y="274.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="C12" gate="C1" pin="2"/>
<wire x1="-58.42" y1="274.32" x2="-55.88" y2="274.32" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="274.32" x2="-55.88" y2="276.86" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="274.32" x2="-40.64" y2="274.32" width="0.1524" layer="91"/>
<junction x="-55.88" y="274.32"/>
<label x="-43.18" y="274.32" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-393.7" y1="48.26" x2="-393.7" y2="55.88" width="0.1524" layer="91"/>
<label x="-393.7" y="50.8" size="1.778" layer="95" rot="R90"/>
<pinref part="MIC" gate="G$1" pin="2"/>
<wire x1="-393.7" y1="48.26" x2="-396.24" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C15" gate="C1" pin="2"/>
<wire x1="-365.76" y1="7.62" x2="-365.76" y2="5.08" width="0.1524" layer="91"/>
<label x="-365.76" y="5.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="-350.52" y1="20.32" x2="-345.44" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-345.44" y1="20.32" x2="-345.44" y2="5.08" width="0.1524" layer="91"/>
<label x="-345.44" y="5.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LM358D" gate="P" pin="V-"/>
<wire x1="-353.06" y1="50.8" x2="-353.06" y2="55.88" width="0.1524" layer="91"/>
<label x="-353.06" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="-337.82" y1="30.48" x2="-337.82" y2="27.94" width="0.1524" layer="91"/>
<label x="-337.82" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="-322.58" y1="30.48" x2="-322.58" y2="27.94" width="0.1524" layer="91"/>
<label x="-322.58" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<label x="-297.18" y="160.02" size="1.778" layer="95"/>
<wire x1="-248.92" y1="160.02" x2="-266.7" y2="160.02" width="0.1524" layer="91"/>
<wire x1="-266.7" y1="160.02" x2="-287.02" y2="160.02" width="0.1524" layer="91"/>
<wire x1="-287.02" y1="160.02" x2="-297.18" y2="160.02" width="0.1524" layer="91"/>
<wire x1="-287.02" y1="185.42" x2="-287.02" y2="160.02" width="0.1524" layer="91"/>
<junction x="-287.02" y="160.02"/>
<junction x="-248.92" y="160.02"/>
<junction x="-266.7" y="160.02"/>
<wire x1="-248.92" y1="185.42" x2="-248.92" y2="160.02" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="185.42" x2="-210.82" y2="160.02" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="160.02" x2="-248.92" y2="160.02" width="0.1524" layer="91"/>
<wire x1="-190.5" y1="185.42" x2="-190.5" y2="160.02" width="0.1524" layer="91"/>
<wire x1="-190.5" y1="160.02" x2="-210.82" y2="160.02" width="0.1524" layer="91"/>
<junction x="-210.82" y="160.02"/>
<pinref part="NODE1" gate="A" pin="1"/>
<wire x1="-266.7" y1="185.42" x2="-266.7" y2="160.02" width="0.1524" layer="91"/>
<pinref part="NODE2" gate="A" pin="1"/>
<pinref part="NODE0" gate="A" pin="1"/>
<pinref part="I2C3V3" gate="A" pin="4"/>
<pinref part="I2C5V" gate="A" pin="4"/>
</segment>
<segment>
<pinref part="PDI" gate="A" pin="6"/>
<wire x1="-154.94" y1="111.76" x2="-147.32" y2="111.76" width="0.1524" layer="91"/>
<label x="-147.32" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-223.52" y1="104.14" x2="-243.84" y2="104.14" width="0.1524" layer="91"/>
<label x="-297.18" y="104.14" size="1.778" layer="95"/>
<wire x1="-243.84" y1="104.14" x2="-261.62" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-261.62" y1="104.14" x2="-281.94" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-281.94" y1="104.14" x2="-297.18" y2="104.14" width="0.1524" layer="91"/>
<label x="-223.52" y="106.68" size="1.778" layer="95" rot="R90"/>
<wire x1="-261.62" y1="132.08" x2="-261.62" y2="104.14" width="0.1524" layer="91"/>
<junction x="-281.94" y="104.14"/>
<junction x="-261.62" y="104.14"/>
<junction x="-243.84" y="104.14"/>
<pinref part="UART" gate="A" pin="1"/>
<wire x1="-243.84" y1="132.08" x2="-243.84" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-223.52" y1="104.14" x2="-223.52" y2="132.08" width="0.1524" layer="91"/>
<wire x1="-281.94" y1="132.08" x2="-281.94" y2="104.14" width="0.1524" layer="91"/>
<pinref part="NODE4" gate="A" pin="1"/>
<pinref part="NODE5" gate="A" pin="1"/>
<pinref part="NODE3" gate="A" pin="1"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VSS"/>
<wire x1="-358.14" y1="147.32" x2="-360.68" y2="147.32" width="0.1524" layer="91"/>
<label x="-375.92" y="147.32" size="1.778" layer="95"/>
<wire x1="-360.68" y1="147.32" x2="-375.92" y2="147.32" width="0.1524" layer="91"/>
<wire x1="-360.68" y1="147.32" x2="-360.68" y2="157.48" width="0.1524" layer="91"/>
<junction x="-360.68" y="147.32"/>
<pinref part="C21" gate="C1" pin="2"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="ADDR"/>
<wire x1="-358.14" y1="134.62" x2="-370.84" y2="134.62" width="0.1524" layer="91"/>
<wire x1="-370.84" y1="134.62" x2="-370.84" y2="111.76" width="0.1524" layer="91"/>
<wire x1="-381" y1="119.38" x2="-381" y2="111.76" width="0.1524" layer="91"/>
<wire x1="-381" y1="111.76" x2="-370.84" y2="111.76" width="0.1524" layer="91"/>
<wire x1="-370.84" y1="111.76" x2="-363.22" y2="111.76" width="0.1524" layer="91"/>
<wire x1="-363.22" y1="111.76" x2="-363.22" y2="114.3" width="0.1524" layer="91"/>
<wire x1="-370.84" y1="111.76" x2="-370.84" y2="101.6" width="0.1524" layer="91"/>
<label x="-370.84" y="101.6" size="1.778" layer="95" rot="R90"/>
<junction x="-370.84" y="111.76"/>
<pinref part="C19" gate="C1" pin="1"/>
<pinref part="R42" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="PHOTORESISTOR1" gate="G$1" pin="E"/>
<wire x1="33.02" y1="101.6" x2="33.02" y2="93.98" width="0.1524" layer="91"/>
<label x="33.02" y="99.06" size="1.778" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="K1" gate="G$1" pin="K"/>
<label x="-398.78" y="243.84" size="1.778" layer="95"/>
<wire x1="-378.46" y1="243.84" x2="-398.78" y2="243.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PHOTORESISTOR2" gate="G$1" pin="E"/>
<wire x1="63.5" y1="111.76" x2="63.5" y2="104.14" width="0.1524" layer="91"/>
<label x="63.5" y="109.22" size="1.778" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="-190.5" y1="99.06" x2="-190.5" y2="104.14" width="0.1524" layer="91"/>
<pinref part="RESET" gate="G$1" pin="P2"/>
<label x="-190.5" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MEMORY" gate="G$1" pin="VSS"/>
<wire x1="-187.96" y1="220.98" x2="-198.12" y2="220.98" width="0.1524" layer="91"/>
<label x="-198.12" y="220.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MEMORY" gate="G$1" pin="E2"/>
<wire x1="-187.96" y1="223.52" x2="-198.12" y2="223.52" width="0.1524" layer="91"/>
<label x="-198.12" y="223.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MEMORY" gate="G$1" pin="E1"/>
<wire x1="-187.96" y1="228.6" x2="-198.12" y2="228.6" width="0.1524" layer="91"/>
<label x="-198.12" y="228.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MEMORY" gate="G$1" pin="!WC"/>
<wire x1="-157.48" y1="228.6" x2="-149.86" y2="228.6" width="0.1524" layer="91"/>
<label x="-154.94" y="228.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ACCEL" gate="G$1" pin="AVSS"/>
<wire x1="-99.06" y1="172.72" x2="-106.68" y2="172.72" width="0.1524" layer="91"/>
<label x="-106.68" y="172.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ACCEL" gate="G$1" pin="DVSS"/>
<wire x1="-53.34" y1="177.8" x2="-45.72" y2="177.8" width="0.1524" layer="91"/>
<label x="-50.8" y="177.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C17" gate="C1" pin="2"/>
<wire x1="-86.36" y1="157.48" x2="-76.2" y2="157.48" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="157.48" x2="-76.2" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C18" gate="C1" pin="1"/>
<wire x1="-76.2" y1="157.48" x2="-66.04" y2="157.48" width="0.1524" layer="91"/>
<label x="-76.2" y="154.94" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-238.76" y1="281.94" x2="-233.68" y2="281.94" width="0.1524" layer="91"/>
<label x="-238.76" y="281.94" size="1.778" layer="95"/>
<pinref part="U$4" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="LCD" gate="A" pin="7"/>
<wire x1="7.62" y1="35.56" x2="33.02" y2="35.56" width="0.1524" layer="91"/>
<label x="17.78" y="35.56" size="1.778" layer="95"/>
<pinref part="LCD" gate="A" pin="8"/>
<wire x1="7.62" y1="38.1" x2="33.02" y2="38.1" width="0.1524" layer="91"/>
<label x="17.78" y="38.1" size="1.778" layer="95"/>
<pinref part="LCD" gate="A" pin="9"/>
<wire x1="7.62" y1="40.64" x2="33.02" y2="40.64" width="0.1524" layer="91"/>
<label x="17.78" y="40.64" size="1.778" layer="95"/>
<pinref part="LCD" gate="A" pin="10"/>
<wire x1="7.62" y1="43.18" x2="33.02" y2="43.18" width="0.1524" layer="91"/>
<label x="17.78" y="43.18" size="1.778" layer="95"/>
<wire x1="33.02" y1="35.56" x2="33.02" y2="20.32" width="0.1524" layer="91"/>
<pinref part="LCD" gate="A" pin="1"/>
<wire x1="33.02" y1="20.32" x2="7.62" y2="20.32" width="0.1524" layer="91"/>
<wire x1="33.02" y1="20.32" x2="33.02" y2="10.16" width="0.1524" layer="91"/>
<junction x="33.02" y="20.32"/>
<label x="33.02" y="10.16" size="1.778" layer="95" rot="R90"/>
<label x="17.78" y="20.32" size="1.778" layer="95"/>
<wire x1="33.02" y1="43.18" x2="33.02" y2="40.64" width="0.1524" layer="91"/>
<wire x1="33.02" y1="40.64" x2="33.02" y2="38.1" width="0.1524" layer="91"/>
<wire x1="33.02" y1="38.1" x2="33.02" y2="35.56" width="0.1524" layer="91"/>
<junction x="33.02" y="40.64"/>
<junction x="33.02" y="38.1"/>
<junction x="33.02" y="35.56"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="43.18" y1="15.24" x2="38.1" y2="20.32" width="0.1524" layer="91"/>
<wire x1="38.1" y1="20.32" x2="33.02" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="GND1"/>
<wire x1="-124.46" y1="33.02" x2="-139.7" y2="33.02" width="0.1524" layer="91"/>
<label x="-139.7" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="GND2"/>
<wire x1="-124.46" y1="27.94" x2="-139.7" y2="27.94" width="0.1524" layer="91"/>
<label x="-139.7" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="GND3"/>
<wire x1="-93.98" y1="33.02" x2="-78.74" y2="33.02" width="0.1524" layer="91"/>
<label x="-83.82" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="GND4"/>
<wire x1="-93.98" y1="27.94" x2="-78.74" y2="27.94" width="0.1524" layer="91"/>
<label x="-83.82" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<label x="-40.64" y="43.18" size="1.778" layer="95"/>
<pinref part="FAN1" gate="A" pin="1"/>
<wire x1="-27.94" y1="43.18" x2="-40.64" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="-40.64" y="20.32" size="1.778" layer="95"/>
<pinref part="FAN2" gate="A" pin="1"/>
<wire x1="-27.94" y1="20.32" x2="-40.64" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="-172.72" y1="50.8" x2="-172.72" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="-167.64" y1="33.02" x2="-167.64" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-167.64" y1="30.48" x2="-170.18" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-170.18" y1="30.48" x2="-172.72" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-170.18" y1="30.48" x2="-170.18" y2="25.4" width="0.1524" layer="91"/>
<label x="-170.18" y="25.4" size="1.778" layer="95"/>
<junction x="-170.18" y="30.48"/>
</segment>
<segment>
<pinref part="B1" gate="G$1" pin="-"/>
<wire x1="-345.44" y1="274.32" x2="-350.52" y2="274.32" width="0.1524" layer="91"/>
<wire x1="-350.52" y1="274.32" x2="-350.52" y2="264.16" width="0.1524" layer="91"/>
<label x="-353.06" y="261.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R26" gate="G$1" pin="1"/>
<wire x1="-320.04" y1="261.62" x2="-320.04" y2="259.08" width="0.1524" layer="91"/>
<wire x1="-320.04" y1="259.08" x2="-312.42" y2="259.08" width="0.1524" layer="91"/>
<label x="-314.96" y="256.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="TEMP/HUM" gate="G$1" pin="4"/>
<wire x1="-167.64" y1="271.78" x2="-187.96" y2="271.78" width="0.1524" layer="91"/>
<label x="-187.96" y="271.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CAMERA" gate="G$1" pin="9"/>
<wire x1="-220.98" y1="220.98" x2="-231.14" y2="220.98" width="0.1524" layer="91"/>
<label x="-231.14" y="220.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R37" gate="G$1" pin="2"/>
<wire x1="-271.78" y1="218.44" x2="-271.78" y2="213.36" width="0.1524" layer="91"/>
<label x="-271.78" y="213.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="WIFI" gate="A" pin="8"/>
<wire x1="-144.78" y1="129.54" x2="-132.08" y2="129.54" width="0.1524" layer="91"/>
<label x="-137.16" y="129.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SERVO1" gate="A" pin="3"/>
<wire x1="-243.84" y1="66.04" x2="-256.54" y2="66.04" width="0.1524" layer="91"/>
<label x="-256.54" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SERVO2" gate="A" pin="3"/>
<wire x1="-243.84" y1="48.26" x2="-256.54" y2="48.26" width="0.1524" layer="91"/>
<label x="-256.54" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SERVO3" gate="A" pin="3"/>
<wire x1="-243.84" y1="30.48" x2="-256.54" y2="30.48" width="0.1524" layer="91"/>
<label x="-256.54" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SERVO4" gate="A" pin="3"/>
<wire x1="-243.84" y1="12.7" x2="-256.54" y2="12.7" width="0.1524" layer="91"/>
<label x="-256.54" y="12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C6" gate="C1" pin="2"/>
<wire x1="-73.66" y1="220.98" x2="-73.66" y2="223.52" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="220.98" x2="-73.66" y2="220.98" width="0.1524" layer="91"/>
<pinref part="C5" gate="C1" pin="2"/>
<wire x1="-86.36" y1="223.52" x2="-86.36" y2="220.98" width="0.1524" layer="91"/>
<junction x="-86.36" y="220.98"/>
<wire x1="-93.98" y1="220.98" x2="-86.36" y2="220.98" width="0.1524" layer="91"/>
<pinref part="C4" gate="C1" pin="2"/>
<wire x1="-93.98" y1="223.52" x2="-93.98" y2="220.98" width="0.1524" layer="91"/>
<junction x="-93.98" y="220.98"/>
<wire x1="-101.6" y1="220.98" x2="-93.98" y2="220.98" width="0.1524" layer="91"/>
<pinref part="C3" gate="C1" pin="2"/>
<wire x1="-101.6" y1="223.52" x2="-101.6" y2="220.98" width="0.1524" layer="91"/>
<junction x="-101.6" y="220.98"/>
<wire x1="-109.22" y1="220.98" x2="-101.6" y2="220.98" width="0.1524" layer="91"/>
<pinref part="C2" gate="C1" pin="2"/>
<wire x1="-109.22" y1="223.52" x2="-109.22" y2="220.98" width="0.1524" layer="91"/>
<junction x="-109.22" y="220.98"/>
<pinref part="C1" gate="C1" pin="2"/>
<wire x1="-116.84" y1="223.52" x2="-116.84" y2="220.98" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="220.98" x2="-109.22" y2="220.98" width="0.1524" layer="91"/>
<junction x="-116.84" y="220.98"/>
<wire x1="-134.62" y1="220.98" x2="-116.84" y2="220.98" width="0.1524" layer="91"/>
<label x="-134.62" y="220.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R43" gate="G$1" pin="2"/>
<wire x1="-165.1" y1="170.18" x2="-165.1" y2="167.64" width="0.1524" layer="91"/>
<label x="-170.18" y="167.64" size="1.778" layer="95"/>
<wire x1="-165.1" y1="167.64" x2="-170.18" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CONTRAST" class="0">
<segment>
<pinref part="LCD" gate="A" pin="3"/>
<wire x1="7.62" y1="25.4" x2="38.1" y2="25.4" width="0.1524" layer="91"/>
<wire x1="38.1" y1="25.4" x2="40.64" y2="27.94" width="0.1524" layer="91"/>
<wire x1="40.64" y1="27.94" x2="50.8" y2="27.94" width="0.1524" layer="91"/>
<label x="17.78" y="25.4" size="1.778" layer="95"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="53.34" y1="15.24" x2="53.34" y2="25.4" width="0.1524" layer="91"/>
<wire x1="53.34" y1="25.4" x2="50.8" y2="27.94" width="0.1524" layer="91"/>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="50.8" y1="27.94" x2="50.8" y2="22.86" width="0.1524" layer="91"/>
<junction x="50.8" y="27.94"/>
</segment>
</net>
<net name="LCD17_G" class="0">
<segment>
<pinref part="LCD" gate="A" pin="17"/>
<label x="17.78" y="60.96" size="1.778" layer="95"/>
<pinref part="R33" gate="G$1" pin="2"/>
<wire x1="33.02" y1="60.96" x2="7.62" y2="60.96" width="0.1524" layer="91"/>
<wire x1="53.34" y1="55.88" x2="50.8" y2="55.88" width="0.1524" layer="91"/>
<wire x1="50.8" y1="55.88" x2="50.8" y2="60.96" width="0.1524" layer="91"/>
<wire x1="50.8" y1="60.96" x2="33.02" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LCD16_R" class="0">
<segment>
<pinref part="LCD" gate="A" pin="16"/>
<wire x1="48.26" y1="58.42" x2="7.62" y2="58.42" width="0.1524" layer="91"/>
<label x="17.78" y="58.42" size="1.778" layer="95"/>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="48.26" y1="58.42" x2="48.26" y2="48.26" width="0.1524" layer="91"/>
<wire x1="48.26" y1="48.26" x2="53.34" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LCD18_B" class="0">
<segment>
<pinref part="LCD" gate="A" pin="18"/>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="53.34" y1="63.5" x2="7.62" y2="63.5" width="0.1524" layer="91"/>
<label x="17.78" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="RGB_R_SUPPLY" class="0">
<segment>
<pinref part="F3" gate="G$2" pin="D"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="-38.1" y1="111.76" x2="-40.64" y2="111.76" width="0.1524" layer="91"/>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="-25.4" y1="111.76" x2="-38.1" y2="111.76" width="0.1524" layer="91"/>
<junction x="-38.1" y="111.76"/>
<wire x1="-38.1" y1="106.68" x2="-38.1" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LCD16_R-" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="1"/>
<wire x1="63.5" y1="48.26" x2="66.04" y2="48.26" width="0.1524" layer="91"/>
<wire x1="66.04" y1="48.26" x2="66.04" y2="27.94" width="0.1524" layer="91"/>
<pinref part="F2" gate="G$2" pin="D"/>
</segment>
</net>
<net name="MOTOR_ENABLE" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="1-2EN"/>
<wire x1="-124.46" y1="48.26" x2="-147.32" y2="48.26" width="0.1524" layer="91"/>
<label x="-147.32" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="3-4EN"/>
<wire x1="-93.98" y1="12.7" x2="-71.12" y2="12.7" width="0.1524" layer="91"/>
<label x="-88.9" y="12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PF7"/>
<wire x1="60.96" y1="157.48" x2="83.82" y2="157.48" width="0.1524" layer="91"/>
<label x="63.5" y="157.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="M_OUT1/FAN1" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="1Y"/>
<wire x1="-124.46" y1="38.1" x2="-147.32" y2="38.1" width="0.1524" layer="91"/>
<label x="-147.32" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<label x="-50.8" y="40.64" size="1.778" layer="95"/>
<pinref part="MOTOR" gate="A" pin="1"/>
<wire x1="-27.94" y1="40.64" x2="-58.42" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="66.04" x2="-58.42" y2="66.04" width="0.1524" layer="91"/>
<label x="-78.74" y="66.04" size="1.778" layer="95"/>
<wire x1="-58.42" y1="66.04" x2="-78.74" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="40.64" x2="-58.42" y2="66.04" width="0.1524" layer="91"/>
<junction x="-58.42" y="66.04"/>
<pinref part="FAN1" gate="A" pin="2"/>
</segment>
</net>
<net name="M_OUT4" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="4Y"/>
<wire x1="-93.98" y1="38.1" x2="-71.12" y2="38.1" width="0.1524" layer="91"/>
<label x="-81.28" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MOTOR" gate="A" pin="4"/>
<wire x1="-27.94" y1="58.42" x2="-78.74" y2="58.42" width="0.1524" layer="91"/>
<label x="-78.74" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="12V" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="VCC1"/>
<label x="-83.82" y="48.26" size="1.778" layer="95"/>
<wire x1="-93.98" y1="48.26" x2="-78.74" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-276.86" y1="185.42" x2="-276.86" y2="162.56" width="0.1524" layer="91"/>
<label x="-276.86" y="162.56" size="1.778" layer="95" rot="R90"/>
<pinref part="NODE0" gate="A" pin="5"/>
</segment>
</net>
<net name="M_OUT3/FAN2" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="3Y"/>
<wire x1="-93.98" y1="22.86" x2="-71.12" y2="22.86" width="0.1524" layer="91"/>
<label x="-81.28" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<label x="-50.8" y="17.78" size="1.778" layer="95"/>
<pinref part="MOTOR" gate="A" pin="3"/>
<wire x1="-27.94" y1="17.78" x2="-55.88" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="60.96" x2="-55.88" y2="60.96" width="0.1524" layer="91"/>
<label x="-78.74" y="60.96" size="1.778" layer="95"/>
<wire x1="-55.88" y1="60.96" x2="-78.74" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="17.78" x2="-55.88" y2="60.96" width="0.1524" layer="91"/>
<junction x="-55.88" y="60.96"/>
<pinref part="FAN2" gate="A" pin="2"/>
</segment>
</net>
<net name="M_OUT2" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="2Y"/>
<wire x1="-124.46" y1="22.86" x2="-147.32" y2="22.86" width="0.1524" layer="91"/>
<label x="-147.32" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MOTOR" gate="A" pin="2"/>
<wire x1="-27.94" y1="63.5" x2="-78.74" y2="63.5" width="0.1524" layer="91"/>
<label x="-78.74" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="FAN1SPEED" class="0">
<segment>
<label x="-50.8" y="38.1" size="1.778" layer="95"/>
<pinref part="FAN1" gate="A" pin="3"/>
<wire x1="-27.94" y1="38.1" x2="-50.8" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="-213.36" y1="50.8" x2="-213.36" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-213.36" y1="17.78" x2="-162.56" y2="17.78" width="0.1524" layer="91"/>
<label x="-175.26" y="17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="FAN2SPEED" class="0">
<segment>
<label x="-50.8" y="15.24" size="1.778" layer="95"/>
<pinref part="FAN2" gate="A" pin="3"/>
<wire x1="-27.94" y1="15.24" x2="-50.8" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="-208.28" y1="33.02" x2="-208.28" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-208.28" y1="12.7" x2="-162.56" y2="12.7" width="0.1524" layer="91"/>
<label x="-175.26" y="12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOR1B/FAN1SPEED" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="-187.96" y1="60.96" x2="-213.36" y2="60.96" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="-213.36" y1="60.96" x2="-218.44" y2="60.96" width="0.1524" layer="91"/>
<junction x="-213.36" y="60.96"/>
<label x="-218.44" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PC1"/>
<wire x1="60.96" y1="241.3" x2="83.82" y2="241.3" width="0.1524" layer="91"/>
<label x="63.5" y="241.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOR2B/FAN2SPEED" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="-190.5" y1="43.18" x2="-208.28" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="-208.28" y1="43.18" x2="-218.44" y2="43.18" width="0.1524" layer="91"/>
<junction x="-208.28" y="43.18"/>
<label x="-218.44" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PC5"/>
<wire x1="60.96" y1="231.14" x2="83.82" y2="231.14" width="0.1524" layer="91"/>
<label x="63.5" y="231.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="120VCONTROL_SAFE" class="0">
<segment>
<wire x1="-398.78" y1="287.02" x2="-398.78" y2="276.86" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="-398.78" y1="287.02" x2="-403.86" y2="287.02" width="0.1524" layer="91"/>
<label x="-403.86" y="287.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PB0"/>
<wire x1="60.96" y1="266.7" x2="83.82" y2="266.7" width="0.1524" layer="91"/>
<label x="63.5" y="266.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="120V_GND" class="0">
<segment>
<wire x1="-340.36" y1="246.38" x2="-340.36" y2="233.68" width="0.1524" layer="91"/>
<label x="-335.28" y="246.38" size="1.778" layer="95"/>
<label x="-335.28" y="233.68" size="1.778" layer="95"/>
<wire x1="-340.36" y1="246.38" x2="-340.36" y2="269.24" width="0.1524" layer="91"/>
<pinref part="B1" gate="G$1" pin="AC2"/>
<junction x="-340.36" y="246.38"/>
<pinref part="75VAC" gate="G$1" pin="2"/>
<wire x1="-340.36" y1="246.38" x2="-317.5" y2="246.38" width="0.1524" layer="91"/>
<pinref part="PDLC" gate="G$1" pin="2"/>
<wire x1="-340.36" y1="233.68" x2="-317.5" y2="233.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="120VOUT" class="0">
<segment>
<label x="-335.28" y="236.22" size="1.778" layer="95"/>
<pinref part="K1" gate="G$1" pin="DS2"/>
<wire x1="-358.14" y1="243.84" x2="-347.98" y2="243.84" width="0.1524" layer="91"/>
<wire x1="-347.98" y1="243.84" x2="-347.98" y2="236.22" width="0.1524" layer="91"/>
<pinref part="PDLC" gate="G$1" pin="1"/>
<wire x1="-347.98" y1="236.22" x2="-317.5" y2="236.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="B1" gate="G$1" pin="+"/>
<wire x1="-335.28" y1="274.32" x2="-332.74" y2="274.32" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="1"/>
</segment>
</net>
<net name="HV_SENSE" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<label x="-309.88" y="276.86" size="1.778" layer="95" rot="R180"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="-322.58" y1="274.32" x2="-320.04" y2="274.32" width="0.1524" layer="91"/>
<wire x1="-320.04" y1="274.32" x2="-320.04" y2="271.78" width="0.1524" layer="91"/>
<wire x1="-320.04" y1="274.32" x2="-312.42" y2="274.32" width="0.1524" layer="91"/>
<junction x="-320.04" y="274.32"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PA7"/>
<wire x1="60.96" y1="271.78" x2="83.82" y2="271.78" width="0.1524" layer="91"/>
<label x="63.5" y="271.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="CAM_LOAD" class="0">
<segment>
<pinref part="CAMERA" gate="G$1" pin="4"/>
<wire x1="-220.98" y1="233.68" x2="-238.76" y2="233.68" width="0.1524" layer="91"/>
<label x="-238.76" y="233.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PF5"/>
<wire x1="60.96" y1="162.56" x2="83.82" y2="162.56" width="0.1524" layer="91"/>
<label x="63.5" y="162.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="CAM_READ" class="0">
<segment>
<pinref part="R27" gate="G$1" pin="2"/>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="-271.78" y1="233.68" x2="-271.78" y2="231.14" width="0.1524" layer="91"/>
<wire x1="-271.78" y1="231.14" x2="-271.78" y2="228.6" width="0.1524" layer="91"/>
<wire x1="-271.78" y1="231.14" x2="-297.18" y2="231.14" width="0.1524" layer="91"/>
<label x="-297.18" y="231.14" size="1.778" layer="95"/>
<junction x="-271.78" y="231.14"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PA2"/>
<wire x1="60.96" y1="284.48" x2="83.82" y2="284.48" width="0.1524" layer="91"/>
<label x="63.5" y="284.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="WIFI_CTS/WIFI_MODE/WIFI_GPIO" class="0">
<segment>
<label x="-200.66" y="134.62" size="1.778" layer="95"/>
<pinref part="WIFI" gate="A" pin="3"/>
<wire x1="-152.4" y1="134.62" x2="-200.66" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LCD_B/SERVO_1" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="-266.7" y1="71.12" x2="-287.02" y2="71.12" width="0.1524" layer="91"/>
<label x="-287.02" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="F3" gate="G$1" pin="G"/>
<wire x1="86.36" y1="20.32" x2="83.82" y2="20.32" width="0.1524" layer="91"/>
<wire x1="83.82" y1="20.32" x2="83.82" y2="43.18" width="0.1524" layer="91"/>
<label x="83.82" y="22.86" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PD4"/>
<wire x1="60.96" y1="210.82" x2="83.82" y2="210.82" width="0.1524" layer="91"/>
<label x="63.5" y="210.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="CAM_RESET/MP3_RESET" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PF4"/>
<wire x1="60.96" y1="165.1" x2="83.82" y2="165.1" width="0.1524" layer="91"/>
<label x="63.5" y="165.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CAMERA" gate="G$1" pin="5"/>
<wire x1="-220.98" y1="231.14" x2="-261.62" y2="231.14" width="0.1524" layer="91"/>
<label x="-261.62" y="231.14" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-264.16" y1="287.02" x2="-297.18" y2="287.02" width="0.1524" layer="91"/>
<label x="-299.72" y="287.02" size="1.778" layer="95"/>
<pinref part="U$4" gate="G$1" pin="RESET"/>
</segment>
</net>
<net name="CAM_SIN/MP3_DI/SERVO_4" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PF1"/>
<wire x1="60.96" y1="172.72" x2="83.82" y2="172.72" width="0.1524" layer="91"/>
<label x="63.5" y="172.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R40" gate="G$1" pin="2"/>
<wire x1="-266.7" y1="17.78" x2="-269.24" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-269.24" y1="17.78" x2="-269.24" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-269.24" y1="22.86" x2="-287.02" y2="22.86" width="0.1524" layer="91"/>
<label x="-287.02" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CAMERA" gate="G$1" pin="3"/>
<wire x1="-220.98" y1="236.22" x2="-261.62" y2="236.22" width="0.1524" layer="91"/>
<label x="-261.62" y="236.22" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-264.16" y1="276.86" x2="-299.72" y2="276.86" width="0.1524" layer="91"/>
<label x="-299.72" y="276.86" size="1.778" layer="95"/>
<pinref part="U$4" gate="G$1" pin="P05"/>
</segment>
</net>
<net name="TEMP_DATA/CAM_XCK/MP3_CLK" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PF0"/>
<wire x1="60.96" y1="175.26" x2="83.82" y2="175.26" width="0.1524" layer="91"/>
<label x="63.5" y="175.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CAMERA" gate="G$1" pin="6"/>
<wire x1="-220.98" y1="228.6" x2="-261.62" y2="228.6" width="0.1524" layer="91"/>
<label x="-261.62" y="228.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="TEMP/HUM" gate="G$1" pin="2"/>
<wire x1="-167.64" y1="276.86" x2="-200.66" y2="276.86" width="0.1524" layer="91"/>
<label x="-200.66" y="276.86" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-264.16" y1="279.4" x2="-299.72" y2="279.4" width="0.1524" layer="91"/>
<label x="-299.72" y="279.4" size="1.778" layer="95"/>
<pinref part="U$4" gate="G$1" pin="P04"/>
</segment>
</net>
<net name="WIFI_RTS/WIFI_READY" class="0">
<segment>
<pinref part="WIFI" gate="A" pin="1"/>
<wire x1="-152.4" y1="137.16" x2="-200.66" y2="137.16" width="0.1524" layer="91"/>
<label x="-200.66" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="WIFI_LINK" class="0">
<segment>
<pinref part="WIFI" gate="A" pin="5"/>
<wire x1="-152.4" y1="132.08" x2="-180.34" y2="132.08" width="0.1524" layer="91"/>
<label x="-180.34" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="WIFI_RESET" class="0">
<segment>
<pinref part="WIFI" gate="A" pin="7"/>
<wire x1="-152.4" y1="129.54" x2="-180.34" y2="129.54" width="0.1524" layer="91"/>
<label x="-180.34" y="129.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="XMEGA" gate="G$1" pin="PR1"/>
<wire x1="-5.08" y1="215.9" x2="-22.86" y2="215.9" width="0.1524" layer="91"/>
<label x="-22.86" y="215.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="SERVO1" gate="A" pin="1"/>
<wire x1="-243.84" y1="71.12" x2="-256.54" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="SERVO2" gate="A" pin="1"/>
<pinref part="R38" gate="G$1" pin="1"/>
<wire x1="-243.84" y1="53.34" x2="-256.54" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="SERVO3" gate="A" pin="1"/>
<pinref part="R39" gate="G$1" pin="1"/>
<wire x1="-243.84" y1="35.56" x2="-256.54" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="SERVO4" gate="A" pin="1"/>
<pinref part="R40" gate="G$1" pin="1"/>
<wire x1="-243.84" y1="17.78" x2="-256.54" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R33" gate="G$1" pin="1"/>
<wire x1="63.5" y1="55.88" x2="78.74" y2="55.88" width="0.1524" layer="91"/>
<pinref part="F2" gate="G$1" pin="D"/>
<wire x1="78.74" y1="27.94" x2="78.74" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="LM358D" gate="B" pin="OUT"/>
<wire x1="-142.24" y1="180.34" x2="-139.7" y2="180.34" width="0.1524" layer="91"/>
<wire x1="-139.7" y1="180.34" x2="-139.7" y2="172.72" width="0.1524" layer="91"/>
<wire x1="-139.7" y1="172.72" x2="-160.02" y2="172.72" width="0.1524" layer="91"/>
<wire x1="-160.02" y1="172.72" x2="-160.02" y2="177.8" width="0.1524" layer="91"/>
<pinref part="LM358D" gate="B" pin="-IN"/>
<wire x1="-160.02" y1="177.8" x2="-157.48" y2="177.8" width="0.1524" layer="91"/>
<wire x1="-139.7" y1="180.34" x2="-134.62" y2="180.34" width="0.1524" layer="91"/>
<junction x="-139.7" y="180.34"/>
<pinref part="C20" gate="C1" pin="1"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="R43" gate="G$1" pin="1"/>
<wire x1="-165.1" y1="180.34" x2="-165.1" y2="182.88" width="0.1524" layer="91"/>
<pinref part="R44" gate="G$1" pin="2"/>
<pinref part="LM358D" gate="B" pin="+IN"/>
<wire x1="-165.1" y1="182.88" x2="-165.1" y2="185.42" width="0.1524" layer="91"/>
<wire x1="-157.48" y1="182.88" x2="-165.1" y2="182.88" width="0.1524" layer="91"/>
<junction x="-165.1" y="182.88"/>
</segment>
</net>
<net name="TAP_INT/DAC_AMP" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PB2"/>
<wire x1="60.96" y1="261.62" x2="83.82" y2="261.62" width="0.1524" layer="91"/>
<label x="63.5" y="261.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ACCEL" gate="G$1" pin="!INT"/>
<wire x1="-99.06" y1="167.64" x2="-109.22" y2="167.64" width="0.1524" layer="91"/>
<label x="-109.22" y="167.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R44" gate="G$1" pin="1"/>
<wire x1="-175.26" y1="185.42" x2="-177.8" y2="185.42" width="0.1524" layer="91"/>
<wire x1="-177.8" y1="185.42" x2="-177.8" y2="193.04" width="0.1524" layer="91"/>
<wire x1="-177.8" y1="193.04" x2="-165.1" y2="193.04" width="0.1524" layer="91"/>
<label x="-175.26" y="193.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="WIFI_RTS/WIFI_READY/LCD_RS" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PA5"/>
<wire x1="60.96" y1="276.86" x2="83.82" y2="276.86" width="0.1524" layer="91"/>
<label x="63.5" y="276.86" size="1.778" layer="95"/>
</segment>
<segment>
<label x="17.78" y="27.94" size="1.778" layer="95"/>
<wire x1="7.62" y1="27.94" x2="27.94" y2="27.94" width="0.1524" layer="91"/>
<pinref part="LCD" gate="A" pin="4"/>
</segment>
</net>
<net name="WIFI_CTS/WIFI_MODE/WIFI_GPIO/LCD_RW" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PA6"/>
<wire x1="60.96" y1="274.32" x2="83.82" y2="274.32" width="0.1524" layer="91"/>
<label x="63.5" y="274.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LCD" gate="A" pin="5"/>
<wire x1="30.48" y1="30.48" x2="7.62" y2="30.48" width="0.1524" layer="91"/>
<label x="17.78" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="WIFI_LINK/LCD_E" class="0">
<segment>
<pinref part="XMEGA" gate="G$1" pin="PB1"/>
<wire x1="60.96" y1="264.16" x2="83.82" y2="264.16" width="0.1524" layer="91"/>
<label x="63.5" y="264.16" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="7.62" y1="33.02" x2="27.94" y2="33.02" width="0.1524" layer="91"/>
<label x="17.78" y="33.02" size="1.778" layer="95"/>
<pinref part="LCD" gate="A" pin="6"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
