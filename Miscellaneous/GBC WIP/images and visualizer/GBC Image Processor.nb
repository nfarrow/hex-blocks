(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     15952,        426]
NotebookOptionsPosition[     14794,        382]
NotebookOutlinePosition[     15140,        397]
CellTagsIndexPosition[     15097,        394]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["\<\
MEMORY: the first successful allocation of 123*32 bytes used memory locations \
0x2bb5 to 0x3b14\
\>", "Subsubtitle",
 CellChangeTimes->{{3.616965248122285*^9, 3.6169652948915396`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"SetDirectory", "[", 
  RowBox[{"NotebookDirectory", "[", "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.617346559527206*^9, 3.6173465741134667`*^9}}],

Cell[BoxData["\<\"C:\\\\Users\\\\Toshiba\\\\Desktop\\\\GIT \
repos\\\\hex-blocks\\\\Miscellaneous\\\\GBC WIP\\\\images and \
visualizer\"\>"], "Output",
 CellChangeTimes->{{3.6173465759074144`*^9, 3.6173465787778544`*^9}, 
   3.617346852514523*^9, {3.6173472188383894`*^9, 3.6173472291345463`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
     RowBox[{"lets", " ", "get", " ", "100"}], " ", "=", " ", 
     RowBox[{"0", "b01100100", " ", "into", " ", "two", " ", "numbers"}]}], 
    ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"0", "b0110"}], " ", "=", " ", 
     RowBox[{"6", " ", 
      RowBox[{"(", 
       RowBox[{"high", " ", "nibble"}], ")"}]}]}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"0", "b0100"}], " ", "=", " ", 
     RowBox[{"4", " ", 
      RowBox[{"(", 
       RowBox[{"low", " ", "nibble"}], ")"}]}]}]}], " ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"HighNibble", "[", "x_", "]"}], ":=", 
    RowBox[{"IntegerPart", "[", 
     RowBox[{"x", "/", "16"}], "]"}]}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"LowNibble", "[", "x_", "]"}], ":=", 
    RowBox[{
     RowBox[{"FractionalPart", "[", 
      RowBox[{"x", "/", "16"}], "]"}], "*", "16"}]}], "\[IndentingNewLine]", 
   RowBox[{"HighNibble", "[", "100", "]"}], "\[IndentingNewLine]", 
   RowBox[{"LowNibble", "[", "100", "]"}]}]}]], "Input",
 CellChangeTimes->{{3.6169589367257357`*^9, 3.616958964353673*^9}, {
  3.616959060822861*^9, 3.616959155219453*^9}, {3.6169592096834517`*^9, 
  3.616959312901287*^9}, {3.6169594884803066`*^9, 3.616959512145809*^9}}],

Cell[BoxData["6"], "Output",
 CellChangeTimes->{{3.616958953714303*^9, 3.6169589575675397`*^9}, 
   3.616959073724187*^9, {3.616959111289352*^9, 3.616959132271557*^9}, 
   3.6169595126449404`*^9, 3.616963054553321*^9, 3.6169677989866858`*^9, 
   3.6169689193734217`*^9, 3.617346514489398*^9, 3.617346578887052*^9, 
   3.6173468526081133`*^9, {3.6173472189475894`*^9, 3.617347229243721*^9}}],

Cell[BoxData["4"], "Output",
 CellChangeTimes->{{3.616958953714303*^9, 3.6169589575675397`*^9}, 
   3.616959073724187*^9, {3.616959111289352*^9, 3.616959132271557*^9}, 
   3.6169595126449404`*^9, 3.616963054553321*^9, 3.6169677989866858`*^9, 
   3.6169689193734217`*^9, 3.617346514489398*^9, 3.617346578887052*^9, 
   3.6173468526081133`*^9, {3.6173472189475894`*^9, 3.617347229243721*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
     RowBox[{"lets", " ", "get", " ", "100"}], " ", "=", " ", 
     RowBox[{"0", "b01100100", " ", "into", " ", "four", " ", "numbers"}]}], 
    ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"0", "b0110"}], " ", "=", " ", 
     RowBox[{"6", " ", 
      RowBox[{"(", 
       RowBox[{"high", " ", "nibble"}], ")"}]}]}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"0", "b0100"}], " ", "=", " ", 
     RowBox[{"4", " ", 
      RowBox[{"(", 
       RowBox[{"low", " ", "nibble"}], ")"}]}]}]}], " ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"FirstCrumb", "[", "x_", "]"}], ":=", 
    RowBox[{"IntegerPart", "[", 
     RowBox[{
      RowBox[{"HighNibble", "[", "x", "]"}], "/", "4"}], "]"}]}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"SecondCrumb", "[", "x_", "]"}], ":=", 
    RowBox[{
     RowBox[{"FractionalPart", "[", 
      RowBox[{
       RowBox[{"HighNibble", "[", "x", "]"}], "/", "4"}], "]"}], "*", "4"}]}],
    "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"ThirdCrumb", "[", "x_", "]"}], ":=", 
    RowBox[{"IntegerPart", "[", 
     RowBox[{
      RowBox[{"LowNibble", "[", "x", "]"}], "/", "4"}], "]"}]}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"FourthCrumb", "[", "x_", "]"}], ":=", 
    RowBox[{
     RowBox[{"FractionalPart", "[", 
      RowBox[{
       RowBox[{"LowNibble", "[", "x", "]"}], "/", "4"}], "]"}], "*", "4"}]}], 
   "\[IndentingNewLine]", 
   RowBox[{"FirstCrumb", "[", "100", "]"}], "\[IndentingNewLine]", 
   RowBox[{"SecondCrumb", "[", "100", "]"}], "\[IndentingNewLine]", 
   RowBox[{"ThirdCrumb", "[", "100", "]"}], "\[IndentingNewLine]", 
   RowBox[{"FourthCrumb", "[", "100", "]"}]}]}]], "Input",
 CellChangeTimes->{{3.6169679596369743`*^9, 3.616968152064906*^9}}],

Cell[BoxData["1"], "Output",
 CellChangeTimes->{
  3.6169681056388597`*^9, 3.61696815351573*^9, 3.616968919467022*^9, 
   3.617346514614199*^9, 3.617346578918256*^9, 3.6173468526393156`*^9, {
   3.617347218963192*^9, 3.6173472292749214`*^9}}],

Cell[BoxData["2"], "Output",
 CellChangeTimes->{
  3.6169681056388597`*^9, 3.61696815351573*^9, 3.616968919467022*^9, 
   3.617346514614199*^9, 3.617346578918256*^9, 3.6173468526393156`*^9, {
   3.617347218963192*^9, 3.6173472292749214`*^9}}],

Cell[BoxData["1"], "Output",
 CellChangeTimes->{
  3.6169681056388597`*^9, 3.61696815351573*^9, 3.616968919467022*^9, 
   3.617346514614199*^9, 3.617346578918256*^9, 3.6173468526393156`*^9, {
   3.617347218963192*^9, 3.6173472292749214`*^9}}],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{
  3.6169681056388597`*^9, 3.61696815351573*^9, 3.616968919467022*^9, 
   3.617346514614199*^9, 3.617346578918256*^9, 3.6173468526393156`*^9, {
   3.617347218963192*^9, 3.6173472292749214`*^9}}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Load the file here:", "Subsubtitle",
 CellChangeTimes->{{3.616968416269085*^9, 3.6169684217603397`*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"data", "=", 
   RowBox[{"ReadList", "[", 
    RowBox[{"\"\<testimage5.dat\>\"", ",", "Word", ",", "\[IndentingNewLine]", 
     RowBox[{"WordSeparators", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{"\"\<,\>\"", ",", "\"\< \>\""}], "}"}]}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"RecordSeparators", "\[Rule]", 
      RowBox[{"{", "\"\<\\n\>\"", "}"}]}], ",", "\[IndentingNewLine]", 
     RowBox[{"RecordLists", "\[Rule]", "True"}]}], "\[IndentingNewLine]", 
    "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"dataHex", "=", 
   RowBox[{"data", "/.", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"x_", "?", "NumericQ"}], "\[RuleDelayed]", 
      RowBox[{"ToString", "[", "x", "]"}]}], "}"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"dataDec", "=", 
   RowBox[{"Map", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"FromDigits", "[", 
       RowBox[{"#", ",", "16"}], "]"}], "&"}], ",", "dataHex", ",", 
     RowBox[{"{", "2", "}"}]}], "]"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.6169580354113703`*^9, 3.6169580735693398`*^9}, 
   3.616958133536323*^9, {3.616958238681345*^9, 3.616958254765105*^9}, 
   3.616963131955674*^9, 3.6169631795879374`*^9, 3.616968164139433*^9, {
   3.6169689172050047`*^9, 3.616968917298678*^9}, {3.6173468514537*^9, 
   3.617346851562971*^9}, {3.6173472176528487`*^9, 3.6173472279177074`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
Process the data here (must break up the bytes to recover the pixel values):\
\>", "Subsubtitle",
 CellChangeTimes->{{3.6169684367364845`*^9, 3.6169684666887045`*^9}}],

Cell[BoxData[
 RowBox[{"(*", " ", 
  RowBox[{
   RowBox[{
    RowBox[{"THIS", " ", "USED", " ", "FOR", " ", "2"}], "-", 
    RowBox[{"nibble", " ", "BYTES"}]}], ",", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"dataImage", "=", 
     RowBox[{"{", "}"}]}], ";", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"For", "[", 
      RowBox[{
       RowBox[{"row", " ", "=", " ", "1"}], ",", " ", 
       RowBox[{"row", " ", "<=", " ", 
        RowBox[{"Length", "[", "dataDec", "]"}]}], ",", 
       RowBox[{"row", "+=", "1"}], ",", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"AppendTo", "[", 
         RowBox[{"dataImage", ",", 
          RowBox[{"{", "}"}]}], "]"}], ";", "\[IndentingNewLine]", 
        RowBox[{"For", "[", 
         RowBox[{
          RowBox[{"col", "=", "1"}], ",", 
          RowBox[{"col", "\[LessEqual]", 
           RowBox[{"Length", "[", 
            RowBox[{
            "dataDec", "\[LeftDoubleBracket]", "row", 
             "\[RightDoubleBracket]"}], "]"}]}], ",", 
          RowBox[{"col", "+=", "1"}], ",", "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{"AppendTo", "[", 
            RowBox[{
             RowBox[{
             "dataImage", "\[LeftDoubleBracket]", "row", 
              "\[RightDoubleBracket]"}], ",", 
             RowBox[{"HighNibble", "[", 
              RowBox[{"dataDec", "\[LeftDoubleBracket]", 
               RowBox[{"row", ",", "col"}], "\[RightDoubleBracket]"}], 
              "]"}]}], "]"}], ";", "\[IndentingNewLine]", 
           RowBox[{"AppendTo", "[", 
            RowBox[{
             RowBox[{
             "dataImage", "\[LeftDoubleBracket]", "row", 
              "\[RightDoubleBracket]"}], ",", 
             RowBox[{"LowNibble", "[", 
              RowBox[{"dataDec", "\[LeftDoubleBracket]", 
               RowBox[{"row", ",", "col"}], "\[RightDoubleBracket]"}], 
              "]"}]}], "]"}]}]}], "\[IndentingNewLine]", "]"}]}]}], 
      "\[IndentingNewLine]", "]"}], "\[IndentingNewLine]", "dataImage"}]}]}], 
  "\[IndentingNewLine]", "*)"}]], "Input",
 CellChangeTimes->{{3.6169682454006233`*^9, 3.616968248785861*^9}, {
  3.6169684804792385`*^9, 3.616968498201082*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"dataImage", "=", 
   RowBox[{"{", "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"For", "[", 
  RowBox[{
   RowBox[{"row", " ", "=", " ", "1"}], ",", " ", 
   RowBox[{"row", " ", "<=", " ", 
    RowBox[{"Length", "[", "dataDec", "]"}]}], ",", 
   RowBox[{"row", "+=", "1"}], ",", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"AppendTo", "[", 
     RowBox[{"dataImage", ",", 
      RowBox[{"{", "}"}]}], "]"}], ";", "\[IndentingNewLine]", 
    RowBox[{"For", "[", 
     RowBox[{
      RowBox[{"col", "=", "1"}], ",", 
      RowBox[{"col", "\[LessEqual]", 
       RowBox[{"Length", "[", 
        RowBox[{
        "dataDec", "\[LeftDoubleBracket]", "row", "\[RightDoubleBracket]"}], 
        "]"}]}], ",", 
      RowBox[{"col", "+=", "1"}], ",", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"AppendTo", "[", 
        RowBox[{
         RowBox[{
         "dataImage", "\[LeftDoubleBracket]", "row", 
          "\[RightDoubleBracket]"}], ",", 
         RowBox[{"FirstCrumb", "[", 
          RowBox[{"dataDec", "\[LeftDoubleBracket]", 
           RowBox[{"row", ",", "col"}], "\[RightDoubleBracket]"}], "]"}]}], 
        "]"}], ";", "\[IndentingNewLine]", 
       RowBox[{"AppendTo", "[", 
        RowBox[{
         RowBox[{
         "dataImage", "\[LeftDoubleBracket]", "row", 
          "\[RightDoubleBracket]"}], ",", 
         RowBox[{"SecondCrumb", "[", 
          RowBox[{"dataDec", "\[LeftDoubleBracket]", 
           RowBox[{"row", ",", "col"}], "\[RightDoubleBracket]"}], "]"}]}], 
        "]"}], ";", "\[IndentingNewLine]", 
       RowBox[{"AppendTo", "[", 
        RowBox[{
         RowBox[{
         "dataImage", "\[LeftDoubleBracket]", "row", 
          "\[RightDoubleBracket]"}], ",", 
         RowBox[{"ThirdCrumb", "[", 
          RowBox[{"dataDec", "\[LeftDoubleBracket]", 
           RowBox[{"row", ",", "col"}], "\[RightDoubleBracket]"}], "]"}]}], 
        "]"}], ";", "\[IndentingNewLine]", 
       RowBox[{"AppendTo", "[", 
        RowBox[{
         RowBox[{
         "dataImage", "\[LeftDoubleBracket]", "row", 
          "\[RightDoubleBracket]"}], ",", 
         RowBox[{"FourthCrumb", "[", 
          RowBox[{"dataDec", "\[LeftDoubleBracket]", 
           RowBox[{"row", ",", "col"}], "\[RightDoubleBracket]"}], "]"}]}], 
        "]"}]}]}], "\[IndentingNewLine]", "]"}]}]}], "\[IndentingNewLine]", 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{"dataImage", ";"}]}], "Input",
 CellChangeTimes->{{3.616968517311268*^9, 3.616968571662197*^9}, {
  3.617346533786953*^9, 3.6173465360957074`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"ArrayPlot", "[", "dataImage", "]"}]], "Input",
 CellChangeTimes->{{3.616963154560299*^9, 3.616963163156048*^9}, {
  3.616968580881853*^9, 3.6169685825822315`*^9}}],

Cell[BoxData[
 GraphicsBox[RasterBox[CompressedData["
1:eJztmIlx2zAQRQWgkrSUEtyAW0jpiUTiBnb/HqTsiZGRR8MQeH9PAPr18fn7
Iz4ejz/n52f8jHeOJBwxPj/HKF+U4/FcQwR/8Y3UOpKUfzjg6/LDHXxCQpAJ
UPLjNgcA/Ckx2OxXZkH4hw9PdrDw9/YDxodTh5qfjDUYYomTln9osI/vzPcQ
YOOra8CLrxPgyFfVYqgKTPyktT848ncaMGlW/s4HqGPM/I39t/HfbX+ickDC
t/RhB77BC9FwHhj8L1FR3v0KfG0EDPk3zrPzUSVn3x+e3ccflZzfNHWg9n8l
10c0f3kyP/l6DVMdKOzX+uCgt/GT4suMySqcXvxnsF/KbemmOph8L1BxzDfF
P6TnRzuq71sNMn7Q2394v/eB8Ewc7Of/uRvi9LCrPUzTYWs6dyB5DPZ81P7q
g3KMkdi/7T2wprz3930AdMCej7vgaB+v5UpDFvjA4/zdxCE/uY9fdYy9+F5+
6veyCApw55fTIBYDP37qYvAmfqr0u+OfE6/25AjkwDX82HjiRv9nHZkPnImv
4Lf7MpeGV/HbU8Ht/FT7X2Ly8Cr+KYLtBV782vX6hzkON/CXd6nYVCTJb/au
ZjKtrTvz1UDP2nI9kvyePtztOH7mFNZOBOh/QVSacyexZqnFXsNw/6/aDXwu
r2JTj/W8kFKXP6iKhjPZRsyf41/3DBm/XRP3QW5QXf4P9yhoEHwkDjkvJ6Ki
K+xrnFiN8pEAnf+s+GQuMHhIXBeD/cBMGdYVViPJ1/m6u1cAfHcNkdsH+nf5
M6eku74WTaJ65PhnLgg6rOx9hC/SUE6zcA5g/IjFIZ/rUQ+I7v9oLsQI14GI
j/qg3y/YpuXPb/dMxAfO/NTy+Tne/HZNjA/2AQU/Rj4HruEzZ1xCb0/8nvx2
Ta8cMPLNOXApPznz49iH7H3Agc/E4IrzUDOf2wqYHBDy48Q37gXK86BDH7DY
L+3Fvr8/jXXFxYDKAe39a7HG1g/kvcyXD73vw5/qkNQQNzkA8RdvTPuYci9w
s5/RsMsBgL/OqYU/+Tpw4m99Sd8P1poV/ifrmeLP827lJy8+1U/pX01E/M3/
MFu6KAes+//SAyR/WbcCOLSfxvq2O7+bu44FW4cu/K39kc0BHT/yuVfsB/uA
nI/Q8T6g4/MyqF4s4MfuG2h/t+62hwy//wO2H1EV8ImVcw4I/Y+aD9SAnA/D
EX6S8bl71JIP9CH4/AfX/qSBtAnhn54UsRHbUL6k7hf2730g4MutzxrYC/rF
fGZ9BK/IPdx+Ft++W/7FrheO2pL6XvM/jr/p8aI+
   "], {{0, 0}, {128, 128}}, {0, 3}],
  Frame->Automatic,
  FrameLabel->{None, None},
  FrameTicks->{{None, None}, {None, None}}]], "Output",
 CellChangeTimes->{
  3.616967802808719*^9, {3.6169685751565623`*^9, 3.6169685829722376`*^9}, 
   3.616968919701023*^9, 3.6173465150510063`*^9, 3.6173465791834564`*^9, 
   3.617346852857716*^9, {3.617347219197193*^9, 3.617347229493325*^9}}]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1040, 636},
WindowMargins->{{239, Automatic}, {-4, Automatic}},
FrontEndVersion->"9.0 for Microsoft Windows (64-bit) (January 25, 2013)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[579, 22, 195, 4, 33, "Subsubtitle"],
Cell[CellGroupData[{
Cell[799, 30, 172, 3, 31, "Input"],
Cell[974, 35, 298, 4, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1309, 44, 1321, 33, 152, "Input"],
Cell[2633, 79, 390, 5, 31, "Output"],
Cell[3026, 86, 390, 5, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3453, 96, 1841, 50, 232, "Input"],
Cell[5297, 148, 242, 4, 31, "Output"],
Cell[5542, 154, 242, 4, 31, "Output"],
Cell[5787, 160, 242, 4, 31, "Output"],
Cell[6032, 166, 242, 4, 31, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[6323, 176, 110, 1, 33, "Subsubtitle"],
Cell[6436, 179, 1405, 33, 152, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7878, 217, 177, 3, 33, "Subsubtitle"],
Cell[8058, 222, 2175, 51, 232, "Input"],
Cell[10236, 275, 2555, 63, 232, "Input"],
Cell[CellGroupData[{
Cell[12816, 342, 187, 3, 31, "Input"],
Cell[13006, 347, 1760, 31, 406, "Output"]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
