/**
 * @file       cam.c
 * @brief      gameboy camera interface for AT90S4433
 * 
 * @author     Laurent Saint-Marcel (lstmarcel@yahoo.fr)
 * @date       2005/07/05
 */

#ifndef __CAM_H__
#define __CAM_H__


// Initialise the IO ports for the camera
void camInit();

// locally set the value of a register but do not set it in the AR chip. You 
// must run camSendRegisters1 to write the register value in the chip
void camSetReg1(unsigned char reg, unsigned char data);

// Define the protocol use to send the image through the serial port
void camSetMode(unsigned char mode);

// Change the clock speed to allow taking photo with a smaller or bigger 
// exposure time than the one defined by default. Default is 0x0A = clock period = 5us 
void camSetClockSpeed(unsigned char clockSpeed);

// brief Define the the minimal value of pixels. Used by som camera modes
void camSetMinPixValue(unsigned char minV);

// Define the the maximal value of pixels. Used by som camera modes
void camSetMaxPixValue(unsigned char maxV);

// Send the 8 register values to the AR chip
void camSendRegisters1();

// Take a picture, read it and send it though the serial port. 
void camReadPicture();

// PRIVATE FUNCTIONS ///////////////////////////////////////////////////////////////

// Delay used between each signal sent to the AR (four per xck cycle).
void camStepDelay();

// Sends a 'reset' pulse to the AR chip.
void camResetAR();

// Sets one of the 8 8-bit registers in the AR chip.
void camSetRegAR(unsigned char regaddr, unsigned char regval);

// Sends a 'start' pulse to the AR chip.
void camStartAR();

// Sends a blank 'xck' pulse to the AR chip.
void camClockAR();

// Wait read signal from the AR chip
unsigned char camWaitRead();

// Read pixels from AR until READ signal is unraise
unsigned char camReadPixels();

// callback run when the ADC conversion is finished = read 1 pixel done
void camReadPixel();









#endif
