// MP3_player.cpp

/* LEAVE THIS FILE COMMENTED UNTIL WE ARE READY TO INCLUDE IT */


#include "MP3_player.h"

void MP3_Command(uint16_t bytes)
{
	//printf("mp3: ");
	
	MP3_PORT.OUTCLR = MP3_CLK_PIN_bm;	// clock falling edge
	_delay_ms(2);
	
	for (int8_t i = 15; i >= 0; i--)
	{
		_delay_us(50);
		if(((bytes>>i) & 0x0001) > 0)
		{
			MP3_PORT.OUTSET = MP3_DATA_PIN_bm;	// data pin HIGH
			//printf("1");
		}
		else
		{
			MP3_PORT.OUTCLR = MP3_DATA_PIN_bm;	// data pin LOW
			//printf("0");
		}
		_delay_us(50);
		MP3_PORT.OUTSET = MP3_CLK_PIN_bm;	// clock rising edge
		_delay_us(50);
		
		if(i > 0)
			MP3_PORT.OUTCLR = MP3_DATA_PIN_bm;	// data pin LOW
		else
			MP3_PORT.OUTSET = MP3_DATA_PIN_bm;	// data pin HIGH
		_delay_us(50);
		
		if(i > 0)
			MP3_PORT.OUTCLR = MP3_CLK_PIN_bm; // clock pin low
		else
			MP3_PORT.OUTSET = MP3_CLK_PIN_bm; // clock pin high
	}
	
	_delay_ms(20);

	//printf("\r\n");
}


// useful: https://docs.google.com/folderview?id=0BxdLxDCD6HidWlczbkFEVU1TbVE

void MP3_init(uint16_t init_id)
{
	if(init_id == MUSIC_BLOCK)
		printf_P(PSTR("MP3 approved\n\r"));
	else
		return;
	
	//printf("reset music\r\n");
	
	// Music Module pins as output
	MP3_PORT.DIRSET = MP3_CLK_PIN_bm;
	MP3_PORT.DIRSET = MP3_DATA_PIN_bm;
	MP3_PORT.DIRSET = MP3_RESET_PIN_bm;

	// All control pins high to begin 
	MP3_PORT.OUTSET = MP3_CLK_PIN_bm;
	MP3_PORT.OUTSET = MP3_DATA_PIN_bm;
	MP3_PORT.OUTSET = MP3_RESET_PIN_bm;

	MP3_PORT.OUTCLR = MP3_RESET_PIN_bm;	// reset low for 5 ms, then wait 300 ms
	//_delay_ms(10);
	_delay_ms(5);
	MP3_PORT.OUTSET = MP3_RESET_PIN_bm;

	//_delay_ms(100);
	_delay_ms(300);
	//MP3_Command(0xFFF7);		// volume 7
}

