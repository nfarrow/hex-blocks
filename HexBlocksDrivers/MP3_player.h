// MP3_player.h

#include <stdio.h>			// used for printf_P
#include <avr/pgmspace.h>	// used for PSTR
#include <avr/io.h>
#include <util/delay.h>

/* LEAVE THIS FILE COMMENTED UNTIL WE ARE READY TO INCLUDE IT */

#define MP3_PORT			PORTF
#define MP3_CLK_PIN_bm		PIN0_bm
#define MP3_DATA_PIN_bm		PIN1_bm
#define MP3_RESET_PIN_bm	PIN4_bm

#define MUSIC_BLOCK			0x3E40	// hex-id of block 9

void MP3_Command(uint16_t bytes);
 

void MP3_init(uint16_t init_id);

