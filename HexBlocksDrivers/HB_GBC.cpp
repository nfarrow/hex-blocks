// HB_GBC.cpp

// CURRENTLY PICTURES !PRINT! LEFT TO RIGHT ONE ROW AT A TIME
// ALL PIXELS ARE WRITTEN (128 columns x 123 rows), WITH 8-BIT PRECISION

// NOTE: This driver library is currently undergoing extensive refactoring (8/24/14)
//		 (11/14/14) refactoring ongoing with predicted completion soon

#include "HB_GBC.h"
#include <stdlib.h>	// for malloc


#define DEBUG_CAMERA 0

#define CAMERA_BLOCK_ID 0xDA4A

// GBC_VOUT is Analog Output from AR, image signal data output
// note: connected to ADC pin of XMEGA
#define GBC_VOUT_PORT		PORTA
#define GBC_VOUT_PIN_bm		PIN0_bm		// A0
#define GBC_ADC				ADCA					// GBC Vout on PA0
#define GBC_MUXPOS_gc		ADC_CH_MUXPOS_PIN0_gc	// GBC Vout on PA0

// GBC_READ is Digital Output from AR, indicate data output timing
// note: connected to asynch pin of XMEGA
#define GBC_READ_PORT		PORTA
#define GBC_READ_PIN_bm		PIN2_bm		// A2

// GBC_XCK is Digital Input to AR, system clock
// note: use overlaps with MP3 player module XCK
#define GBC_XCK_PORT		PORTF
#define GBC_XCK_PIN_bm		PIN0_bm		// F0

// GBC_SIN is Digital Input to AR, register data input
// note: use overlaps with MP3 player module DI (DATA)
#define GBC_SIN_PORT		PORTF
#define GBC_SIN_PIN_bm		PIN1_bm		// F1

// GBC_RESET is Digital Input to AR, register reset
// note: use overlaps with MP3 player module RESET
#define GBC_RESET_PORT		PORTF
#define GBC_RESET_PIN_bm	PIN4_bm		// F4

// GBC_LOAD is Digital Input to AR, validate register data input
#define GBC_LOAD_PORT		PORTF
#define GBC_LOAD_PIN_bm		PIN5_bm		// F5

// GBC_START is Digital Input to AR, image capture start signal
#define GBC_START_PORT		PORTF
#define GBC_START_PIN_bm	PIN6_bm		// F6

// memory requirements for image 3.84375 kB
#define NUM_IMAGE_MEM_ROWS	123
//#define NUM_IMAGE_COLS	64		// two pixels per byte! stored as nibbles
#define NUM_IMAGE_MEM_COLS	32		// four pixels per byte! stored as half-nibbles (crumbs)

#define NUM_PIXEL_ROWS 123
#define NUM_PIXEL_COLS 128

// image formats
#define PIXELS_AS_BYTES		0
#define PIXELS_AS_NIBBLES	1
#define PIXELS_AS_CRUMBS	2

uint8_t image_format = PIXELS_AS_CRUMBS;

// 123 * 32 = 3936
// 122*32 + 31 = 3935

// first run through semi-working code counted 16384 pixels per image request
// 16384 = 128 * 128 (success!)

// HEY! grabbing this large of a chunk of memory at compile time somehow screws up the program,
// probably by messing with the program counter or instruction pointer
// it is currently unknown how large of a block of memory is OK to allocate at compile time,
// for instance 23*64 bytes was OK, but 123*64 bytes was NOT
// we now use malloc() to allocate the memory after boot-up (uses about 8 kB!)
//uint8_t imagedata[NUM_IMAGE_ROWS][NUM_IMAGE_COLS];
uint8_t *imagedata;

uint16_t pixel_count;
uint8_t highest_pixel;
uint8_t lowest_pixel;
uint8_t camera_okay;	// boolean

//volatile unsigned char camReadPixelDone=FALSE; NO LONGER USED
// default value for registers
unsigned char camReg1[8] = { 0x80, 0x03, 0x00, 0x30, 0x01, 0x00, 0x01, 0x21 };	// TODO: DOCUMENT WHAT THESE VALUES ARE DOING

// possibly unused variables:
//#define CAM_MODE_STANDARD 2
//unsigned char camMode       = CAM_MODE_STANDARD;	// DONT KNOW IF THIS WAS IMPLEMENTED YET (see: image_format)
unsigned char camClockSpeed = 0x0A;					// FOR NOW, WE DONT CHANGE THE CLOCK SPEED

unsigned char camPixMin = 0;						// FOR NOW, WE DONT KNOW HIGH-LOW VALUES OF THE ADC
unsigned char camPixMax = 0xFF;						// FOR NOW, WE DONT KNOW HIGH-LOW VALUES OF THE ADC
//unsigned char camCompBuf= 0;	NEVER USED!
//unsigned char camCompI  = 0;	NEVER USED!

uint8_t accumulator; // for debugging camReadPixels()

// Initialise the IO ports for the camera
void GBC_init(uint16_t init_id)
{
	if(init_id == CAMERA_BLOCK_ID)
	{
		printf_P(PSTR("Block 1 GBC\r\n"));
	
		// try to allocate memory to store the image
		// malloc takes as input number of bytes to allocate, cast as size_t
		// malloc returns a pointer to the first byte of the allocated block of memory
		// if there is insufficient memory available, malloc will return a pointer to NULL

		imagedata = (uint8_t *)malloc((size_t) (NUM_IMAGE_MEM_ROWS+1)*(NUM_IMAGE_MEM_COLS+1));
	
		if(imagedata == NULL)
		{
			printf_P(PSTR("GBC could not allocate image memory\r\n"));
			camera_okay = 0;
		}
		else
		{
			// print the address bounds of the memory block
			printf_P(PSTR("GBC mem-OK: %p"), imagedata);
			printf_P(PSTR(" to %p\r\n"), &imagedata[(NUM_IMAGE_MEM_ROWS-1)*NUM_IMAGE_MEM_COLS + (NUM_IMAGE_MEM_COLS-1)]);
		
			camera_okay = 1;
		}
		
		printf_P(PSTR("CAMERA PINS>"));
		camInitPins();
	}
	else
	{
		printf_P(PSTR("no camera"));
		camera_okay = 0;
	}
}

void GBC_print_image()
{
	uint8_t val;
	
	if(camera_okay)
	{
		// printing takes awhile (more than a second), temporary suspend printing the time 
		deactivate_print_time();
	
		printf_P(PSTR("pic\r\n[%i][%i]:\r\n"), NUM_PIXEL_ROWS, NUM_PIXEL_COLS);
			
		// print pixels a row at a time
		for(uint8_t pixel_col = 0; pixel_col < NUM_PIXEL_COLS; pixel_col++)
		{
			for(uint8_t pixel_row = 0; pixel_row < NUM_PIXEL_ROWS; pixel_row++)
			{
				// always print full 0-255 values
				val = load_pixel(pixel_col, pixel_row);
				printf_P(PSTR("%u,"), val);
				_delay_us(800);	// TODO: this could be shorter, but how short?
			}
			printf_P(PSTR("\r\n")); // print a newline after the comma (makes parsing on the receiving side easier)
		}
		
		activate_print_time();
	}
	
	else
		printf_P(PSTR("camera uninitialized\r\n"));
	
}

void save_pixel(uint8_t pixel_x, uint8_t pixel_y, uint8_t val)
{
	if(DEBUG_CAMERA)
		printf_P(PSTR("save[%u][%u]: %02x\r\n"), pixel_x, pixel_y, val);
	
	uint8_t save_val;
	uint16_t mem_loc;
	uint8_t crumb_count;
	
	if(image_format == PIXELS_AS_BYTES)
	{
		mem_loc = pixel_y*NUM_IMAGE_MEM_COLS + pixel_x;
		save_val = val;
	}
	else if(image_format == PIXELS_AS_CRUMBS)
	{
		// TODO: VERIFY CORRECT MEMORY ACCESS
		mem_loc = pixel_y*NUM_IMAGE_MEM_COLS + pixel_x/4;
		
		crumb_count = pixel_x%4;			// 1-byte = |33|22|11|00| <- bits
		
		if(DEBUG_CAMERA)
			printf_P(PSTR("crumb: %u\r\n"), crumb_count);
			
		// thresholding
		if(val <= 70)
			save_val = 0;
		else if(val <= 110)
			save_val = 1;
		else if(val <= 130)
			save_val = 2;
		else
			save_val = 3;

		if(DEBUG_CAMERA)
			printf_P(PSTR("save scale: %02x\r\n"), save_val);
		
		save_val = (save_val << 2*crumb_count);
		
		if(DEBUG_CAMERA)
			printf_P(PSTR("save shift: %02x\r\n"), save_val);
		
		save_val = (imagedata[mem_loc] & ~(0b00000011 << 2*crumb_count)) | save_val;
		
		if(DEBUG_CAMERA)
			printf_P(PSTR("save merge: %02x\r\n\r\n"), save_val);
		
	}
	else
	{
		printf_P(PSTR("S-UIF:%i\r\n"), image_format); // S-UIF = Save, Unknown Image Format
		return;	
	}
	
	if(mem_loc > (NUM_IMAGE_MEM_ROWS+1)*(NUM_IMAGE_MEM_COLS+1))
		printf_P(PSTR("OUT OF RANGE %i > %i\r\n"), mem_loc, (NUM_IMAGE_MEM_ROWS+1)*(NUM_IMAGE_MEM_COLS+1));
	else
		imagedata[mem_loc] = save_val;
		
	return;
}

uint8_t load_pixel(uint8_t pixel_x, uint8_t pixel_y)
{
	if(DEBUG_CAMERA)
		printf_P(PSTR("load[%u][%u]: "), pixel_x, pixel_y);
	
	uint8_t load_val;
	uint16_t mem_loc;
	uint8_t val;
	uint8_t crumb_count;
	
	if(image_format == PIXELS_AS_BYTES)
	{
		mem_loc = pixel_y*NUM_IMAGE_MEM_COLS + pixel_x;
		val = imagedata[mem_loc];
	}
	else if(image_format == PIXELS_AS_CRUMBS)
	{
		// TODO: VERIFY CORRECT MEMORY ACCESS
		mem_loc = pixel_y*NUM_IMAGE_MEM_COLS + pixel_x/4;
		
		load_val = imagedata[mem_loc];
	
		if(DEBUG_CAMERA)
			printf_P(PSTR("%02x\r\n"), load_val);
	
		crumb_count = pixel_x%4;			// 1-byte = |33|22|11|00| <- bits
	
		if(DEBUG_CAMERA)
			printf_P(PSTR("crumb: %u\r\n"), crumb_count);
	
		load_val = load_val >> 2*crumb_count;
		
		if(DEBUG_CAMERA)
			printf_P(PSTR("load shift: %02x\r\n"), load_val);
		
		load_val &= 0b00000011;
	
		if(DEBUG_CAMERA)
			printf_P(PSTR("load chop: %02x\r\n"), load_val);
	
		val = load_val * 64; // CHEAT
	
		if(DEBUG_CAMERA)
			printf_P(PSTR("load scale: %u\r\n\r\n"), val);	
	}
	
	else
	{
		printf_P(PSTR("L-UIF:%i\r\n"), image_format);	// L-UIF = Load, Unknown Image Format
		val = rand()%100;
	}
	
	return val;
}

void GBC_fake_image()
{
	if(camera_okay)
	{
		for(uint8_t pixel_row = 0; pixel_row < NUM_PIXEL_ROWS; pixel_row++)
		{
			for(uint8_t pixel_col = 0; pixel_col < NUM_PIXEL_COLS; pixel_col++)
			{
				if((pixel_row == pixel_col)||(pixel_col == NUM_PIXEL_COLS/2))
					save_pixel(pixel_col, pixel_row, 0); // test pattern
				else
					save_pixel(pixel_col, pixel_row, pixel_row*2); // horizontal fade
					//save_pixel(pixel_col, pixel_row, 0xFF); // white out
			}
		}	
	}
	
	else
		printf_P(PSTR("no camera memory\r\n"));
}	

void GBC_clear_image()
{
	if(camera_okay)
	{
		for(uint8_t pixel_row = 0; pixel_row < NUM_IMAGE_MEM_ROWS; pixel_row++)
		{
			for(uint8_t pixel_col = 0; pixel_col < NUM_IMAGE_MEM_COLS; pixel_col++)
			{
				//save_pixel(pixel_row, pixel_col, 0);
				imagedata[pixel_row*NUM_IMAGE_MEM_COLS + pixel_col] = 0;
			}
		}
	}	
}

//############################################################################
//#### PUBLIC FUNCTIONS                                                   ####
//############################################################################
// Initialize the IO ports for the camera
void camInitPins()
{
	printf_P(PSTR("<INIT\r\n"));
	//outp(0xFF, CAM_DATA_DDR);      // if you change connections, do not forget to 
	//outp(0x40, CAM_LED_READ_DDR);  // change these 2 values
	
	// setup connections here OUTPUTS
	GBC_XCK_PORT.DIRSET = GBC_XCK_PIN_bm;
	GBC_RESET_PORT.DIRSET = GBC_RESET_PIN_bm;
	GBC_LOAD_PORT.DIRSET = GBC_LOAD_PIN_bm;
	GBC_START_PORT.DIRSET = GBC_START_PIN_bm;
	GBC_SIN_PORT.DIRSET = GBC_SIN_PIN_bm;
	
	// setup connections here INPUTS (TODO)
	GBC_VOUT_PORT.DIRCLR = GBC_VOUT_PIN_bm;
	GBC_READ_PORT.DIRCLR = GBC_READ_PIN_bm;

	// vout to ADC
	GBC_ADC.REFCTRL = ADC_REFSEL_VCC_gc;			// Vcc/1.6
	GBC_ADC.CTRLB = ADC_RESOLUTION_8BIT_gc;			// use 8 bit resolution
	GBC_ADC.PRESCALER = ADC_PRESCALER_DIV512_gc;
	GBC_ADC.CH0.CTRL = ADC_CH_INPUTMODE_SINGLEENDED_gc;
	GBC_ADC.CTRLA = ADC_ENABLE_bm;					// Enable ADC so that it may make measurements
	// connect the MUX:
	GBC_ADC.CH0.MUXCTRL &= ~0b01111000;	// clear out the old MUXPOS_gc (PROBABLY UNNECESSARY)
	GBC_ADC.CH0.MUXCTRL |= GBC_MUXPOS_gc;

	// read to interrupt?
	
	// set the output lines low to begin
	GBC_XCK_PORT.OUTCLR = GBC_XCK_PIN_bm;		// clock pin LOW	
	GBC_RESET_PORT.OUTCLR = GBC_RESET_PIN_bm;	// reset pin LOW
	GBC_LOAD_PORT.OUTCLR = GBC_LOAD_PIN_bm;		// load pin LOW
	GBC_START_PORT.OUTCLR = GBC_START_PIN_bm;	// start pin LOW
	GBC_SIN_PORT.OUTCLR = GBC_SIN_PIN_bm;		// SIN pin LOW
	
	_delay_ms(5);
	printf_P(PSTR("CAMERA PINS OK\r\n"));
	_delay_ms(5);
}

// locally set the value of a register but do not set it in the AR chip. You 
// must run camSendRegisters1 to write the register value in the chip
void camSetReg1(unsigned char reg, unsigned char data) 
{
	camReg1[reg] = data;
}

void camSetMode(unsigned char mode) 
{
	// data compression mode, not yet implemented
	// there is not enough RAM to store full 8-bit pictures, and maybe not even 4-bit pictures
	
	//camMode = mode;
	// camMode replaced with image_format
	image_format = mode;
}

// Change the clock speed to allow taking photo with a smaller or bigger 
// exposure time than the one defined by default
void camSetClockSpeed(unsigned char clockSpeed)
{
   // not implemented yet
   camClockSpeed=clockSpeed;
}

// Define the the minimal value of pixels
void camSetMinPixValue(uint8_t minV) 
{
	// not yet implemented, this needs to be worked out of the ADC and also depends on 'exposure time'
	camPixMin = minV;
}

// Define the the maximal value of pixels. Used by some camera modes
void camSetMaxPixValue(uint8_t maxV) 
{
	// not yet implemented, this needs to be worked out of the ADC and also depends on 'exposure time'
	camPixMax = maxV;
}

// Send the 8 register values to the AR chip
void camSendRegisters1()
{
	unsigned char reg;
	camResetAR();
	for(reg=0; reg<8; ++reg)
	{
		camSetRegAR(reg, camReg1[reg]);
	}
}

// Take a picture, read it and send it though the serial port. 
void camTakePicture()
{
	// set global (private) variables for the camera routine
	pixel_count = 0;
	highest_pixel = 0;
	lowest_pixel = 0xFF;

	uint8_t pixel_row = 0;
	uint8_t pixel_col = 0;

	uint8_t pixel_val = 0;

	GBC_clear_image();
	
	printf_P(PSTR("read pic\r\n"));
	
	// have to send registers to tell camera what kind of picture settings to use
	camSendRegisters1();	
	
	// Send a START pulse to AR, this is the Image Capture Start Signal
	camStartAR();	
	//printf_P(PSTR("START\r\n"));
	
	// wait AR READ signal
	while(!camWaitRead());	// waiting for AR to signal that image is ready
	//printf_P(PSTR("READ\r\n"));
	
	//uartWrite(CAM_COM_START_READ);
	//sbi(CAM_LED_PORT, CAM_LED_BIT);

	while(!camReadPixels(pixel_val))
	{
		// HEY, SAVE THE PIXELS INTO THE ARRAY HERE
		save_pixel(pixel_col, pixel_row, pixel_val);

		// increment the pixel counters
		pixel_col++;
		if(pixel_col == NUM_PIXEL_COLS)
		{
			pixel_col = 0;
			pixel_row++;
			
			if(pixel_row == NUM_PIXEL_ROWS)
				pixel_row = 0;
		}
		
		pixel_count++;
		
		if(pixel_count <= NUM_PIXEL_COLS*(NUM_PIXEL_ROWS-1))
		{
			if(pixel_val < lowest_pixel)
				lowest_pixel = pixel_val;

			if(pixel_val > highest_pixel)
				highest_pixel = pixel_val;
		}
	};
	
	//uartWrite(CAM_COM_STOP_READ);
	//cbi(CAM_LED_PORT, CAM_LED_BIT);

	//GBC_print_image();

	camResetAR();

	printf_P(PSTR("pixel %u\r\n"), pixel_count);
	printf_P(PSTR("H %u\r\n"), highest_pixel);
	printf_P(PSTR("L %u\r\n"), lowest_pixel);
}


//############################################################################
//####   PRIVATE FUNCTION : Communication with the AR chip                ####
//############################################################################
// cbi(port, bitId) = clear bit(port, bitId) = Set the signal Low
// sbi(port, bitId) = set bit(port, bitId) = Set the signal High
// Delay used between each signal sent to the AR (four per xck cycle).
void camStepDelay()
{ // the way this function was used in the orig code, 
  // I assume this is supposed to be a 4 us delay (minimum) 
  // I reasoned this out based on the 8 Mhz Arduino original implementation
  // play with this value to see what effects it has, SHOULD BE SIGNIFICANT 
	
	_delay_us(4);
	
}

// Sends a 'reset' pulse to the AR chip.
void camResetAR()
{
	GBC_XCK_PORT.OUTCLR = GBC_XCK_PIN_bm;		// clock pin LOW
	GBC_RESET_PORT.OUTCLR = GBC_RESET_PIN_bm;	// reset pin LOW
	camStepDelay();
	camStepDelay();

	GBC_XCK_PORT.OUTSET = GBC_XCK_PIN_bm;		// clock pin HIGH	
	camStepDelay();
	camStepDelay();
	GBC_RESET_PORT.OUTSET = GBC_RESET_PIN_bm;	//reset pin HIGH
}

// Sets one of the 8 8-bit registers in the AR chip.
void camSetRegAR(unsigned char regaddr, unsigned char regval)
{
	//printf_P(PSTR("cam set reg\r\n"));

	unsigned char bitmask;

	// Write 3-bit address.
	for(bitmask = 4; bitmask >= 1; bitmask >>= 1)
	{
		GBC_XCK_PORT.OUTCLR = GBC_XCK_PIN_bm;		// clock pin LOW
		camStepDelay();
		GBC_LOAD_PORT.OUTCLR = GBC_LOAD_PIN_bm;		// load pin LOW
		if(regaddr & bitmask)
			GBC_SIN_PORT.OUTSET = GBC_SIN_PIN_bm;	// SIN pin HIGH
		else 
			GBC_SIN_PORT.OUTCLR = GBC_SIN_PIN_bm;	// SIN pin LOW
		
		camStepDelay();

		GBC_XCK_PORT.OUTSET = GBC_XCK_PIN_bm;		// clock pin HIGH
		camStepDelay();
		GBC_SIN_PORT.OUTCLR = GBC_SIN_PIN_bm;		// SIN pin LOW
		camStepDelay();
	}

	// Write 7 most significant bits of 8-bit data.
	for(bitmask = 128; bitmask >= 2; bitmask>>=1)
	{
		GBC_XCK_PORT.OUTCLR = GBC_XCK_PIN_bm;		// clock pin LOW
		camStepDelay();
		if(regval & bitmask)
			GBC_SIN_PORT.OUTSET = GBC_SIN_PIN_bm;	// SIN pin HIGH
		else
			GBC_SIN_PORT.OUTCLR = GBC_SIN_PIN_bm;	// SIN pin LOW
		camStepDelay();

		GBC_XCK_PORT.OUTSET = GBC_XCK_PIN_bm;		// clock pin HIGH
		camStepDelay();
		GBC_SIN_PORT.OUTCLR = GBC_SIN_PIN_bm;		// SIN pin LOW
		camStepDelay();
	}

	// Write LSB of data along with 'load' signal and the rest of xck.
	GBC_XCK_PORT.OUTCLR = GBC_XCK_PIN_bm;		// clock pin LOW
	camStepDelay();
	if(regval & 1)
		GBC_SIN_PORT.OUTSET = GBC_SIN_PIN_bm;	// SIN pin HIGH
	else
		GBC_SIN_PORT.OUTCLR = GBC_SIN_PIN_bm;	// SIN pin LOW
	
	camStepDelay();

	GBC_XCK_PORT.OUTSET = GBC_XCK_PIN_bm;		// clock pin HIGH
	camStepDelay();
	GBC_LOAD_PORT.OUTSET = GBC_LOAD_PIN_bm;		// load pin HIGH // load asserted (on falling edge of xck)
	camStepDelay();
}

// Sends a 'start' pulse to the AR chip.
void camStartAR()
{
	GBC_XCK_PORT.OUTCLR = GBC_XCK_PIN_bm;		// clock pin LOW

	camStepDelay();

	GBC_SIN_PORT.OUTCLR = GBC_SIN_PIN_bm;		// SIN pin LOW
	GBC_LOAD_PORT.OUTCLR = GBC_LOAD_PIN_bm;		// load pin LOW
	GBC_START_PORT.OUTSET = GBC_START_PIN_bm;	// start pin HIGH

	camStepDelay();

	GBC_XCK_PORT.OUTSET = GBC_XCK_PIN_bm;		// clock pin HIGH
	camStepDelay();

	GBC_START_PORT.OUTCLR = GBC_START_PIN_bm;	// start pin LOW

	camStepDelay();
}

// Sends a blank 'xck' pulse to the AR chip.
void camClockAR()
{
	GBC_XCK_PORT.OUTCLR = GBC_XCK_PIN_bm;		// clock pin LOW
	camStepDelay();
	camStepDelay();

	GBC_XCK_PORT.OUTSET = GBC_XCK_PIN_bm;		// clock pin HIGH
	camStepDelay();
	camStepDelay();
}

// Wait read signal from the AR chip
unsigned char camWaitRead()
{	// FROM WHAT I CAN TELL, there is nothing 'wait' about this routine. 
	// It just checks the READ pin and returns its value
	// The 'wait' comes from calling it repeatedly, until it returns a non-zero value
	// The code could (should?) be refactored so that the waiting occurs within this function (TODO maybe?)
	// It would be good to check how much waiting is required, how often this is called, etc.

	printf_P(PSTR("W"));

	GBC_XCK_PORT.OUTCLR = GBC_XCK_PIN_bm;		// clock pin LOW
	camStepDelay();
	camStepDelay();

	GBC_XCK_PORT.OUTSET = GBC_XCK_PIN_bm;		// clock pin HIGH
	camStepDelay();
	//if((inp(CAM_READ_PIN) & (1 << CAM_READ_BIT)))	//reads status of READ pin on AR
	if(GBC_READ_PORT.IN & GBC_READ_PIN_bm)
	{
		camStepDelay();
		return 1;	// 1 = TRUE
	}
	else
	{
		camStepDelay();
		return 0;	// 0 = FALSE
	}
}

// Read pixels from AR until READ signal is unraise
unsigned char camReadPixels(uint8_t &pixel_val)
{	// FROM WHAT I CAN TELL, this function is called repeatedly in a while loop until it returns a 1
	
	//printf_P(PSTR("RP%u\r\n"), pixel_count);
	
	GBC_XCK_PORT.OUTCLR = GBC_XCK_PIN_bm;	// clock pin LOW
	camStepDelay();
	camStepDelay();

	GBC_XCK_PORT.OUTSET = GBC_XCK_PIN_bm;	// clock pin HIGH
	camStepDelay();
	
	//camReadPixelDone = 0;	// 0 = FALSE	DONT USE THIS ANYMORE
	
	//adcStart();
	// our ADC is really fast, so dont need to start and then come back later
	pixel_val = camReadPixel();	

	
	

	//if(!(inp(CAM_READ_IN) & (1 << CAM_READ_BIT)))
	if((GBC_READ_PORT.IN & GBC_READ_PIN_bm) == 0)
	{ // check to see if READ pin is low, which signals the end of the image from the AR
		camStepDelay(); // end of read sequence
		return 1;	// 1 = TRUE
	}
	else
	{
		//while(!camReadPixelDone)
		//{
			//__asm__ __volatile__ ("nop");
		//}
		return 0;	// 0 = FALSE
	}
}

//############################################################################
//#### PRIVATE FUNCTIONS : Pixel treatment                                ####
//############################################################################
// callback run when the ADC conversion is finished = read 1 pixel done
uint8_t camReadPixel()
{
	uint8_t meas;

	GBC_ADC.CTRLA |= ADC_CH0START_bm;
	while (GBC_ADC.CH0.INTFLAGS==0){};		// wait for 'complete flag' to be set
	GBC_ADC.CH0.INTFLAGS = 1;				// clear the complete flag
	meas = GBC_ADC.CH0.RES;

	return meas;
}

