

//#include "avr_compiler.h"		// (provides: <avr/io.h>, <avr/interrupt.h>, <avr/pgmspace.h>, <util/delay.h>)
#include "touch.h"





// PRIVATE VARIABLE:
//uint8_t last_touch_status;	// this assumes the block is not being touched upon initialization

static uint8_t touch_existance_confirmed = 0;	// boolean, set in init

//uint8_t keys_held;
//uint8_t keys_reported_held;

/* USER ACCESSIBLE FUNCTIONS */

uint8_t check_touch_exists(void)
{
	return I2C_check_device_exists(MPR121_ADDRESS);
}

uint8_t verify_touch_existance(void)
{
	return touch_existance_confirmed;
}


/* USER ACCESSIBLE FUNCTIONS */



uint16_t get_electrode_proximity_value(uint8_t elec_no)
{
	if(elec_no==0)
		return(touch_read_16(0x04)); // address for elec0
	if(elec_no==1)
		return(touch_read_16(0x06)); // address for elec1
	if(elec_no==2)
		return(touch_read_16(0x08)); // address for elec2
	if(elec_no==3)
		return(touch_read_16(0x0A)); // address for elec3
	if(elec_no==4)
		return(touch_read_16(0x0C)); // address for elec4
	if(elec_no==5)
		return(touch_read_16(0x0E)); // address for elec5

	else
		return 0;	// serious problem, an invalid parameter was passed
}

void print_proximity_values(void)
{
	uint16_t cap_value;

	uint16_t cap_sum = 0;
	
	printf_P(PSTR("\rCAP VALUES\r\n"));

	if(check_touch_exists())	// this is a duplicate check
	{
		for(uint8_t elec_no = 0; elec_no < 6; elec_no++)
		{
			cap_value = get_electrode_proximity_value(elec_no);
			printf_P(PSTR("cap %u: %u\r\n"), elec_no, cap_value);
			_delay_ms(1); // for printf to complete
			cap_sum += cap_value;
		}
		
		if(cap_value == 0)
		{
			 touch_soft_reset();
			 printf("RESET TOUCH\r\n");
		}
			
	}
	else
		printf("DOES NOT EXIST\r\n");
}

void blink_touch_leds(void)
{
	// blink the touch sensor MPR121 LEDs
	touch_write(GPIO_SET, 0b11111100);
	_delay_ms(2);
	touch_write(GPIO_CLEAR, 0b11111100);
	//printf("TOUCH LED BLINK!\r\n");
}

void touch_soft_reset(void)
{
	touch_write(SRST, 0x63);	// Ref: MPR121 datasheet, sec 5.13
}

/* INTERNAL FUNCTIONS */

uint8_t touch_read(uint8_t read_address)
{	
	return I2C_read(MPR121_ADDRESS, read_address);
}

uint16_t touch_read_16(uint8_t read_address)
{
	return I2C_read_16(MPR121_ADDRESS, read_address);
}

void touch_write(uint8_t addr, uint8_t data)
{
	I2C_write(MPR121_ADDRESS, addr, data);
}


/* INITIALIZATION */

void init_touch(void)
{
	if(check_touch_exists() == 1)
	{
		touch_existance_confirmed = 1;
		
		init_touch_LEDS();

		touch_quick_config();

		/*	// set to 0 upon declaration in handler
		last_touch_status = 0;	// this assumes the block is not being touched upon initialization
		keys_held = 0;
		keys_reported_held = 0;
		*/
	}
	
	else
	{
		touch_existance_confirmed = 0;	// redundant

		printf_P(PSTR("NO TOUCH FOUND\r\n"));
	}
	return;
}


void init_touch_LEDS(void)
{
	// Setup the GPIO pins to drive LEDs
	touch_write(GPIO_EN, 0xFF);	    // 0x77 is GPIO enable
	touch_write(GPIO_DIR, 0xFF);	// 0x76 is GPIO Dir
	touch_write(GPIO_CTRL0, 0xFF);	// Set to LED driver
	touch_write(GPIO_CTRL1, 0xFF);	// GPIO Control 1
	touch_write(GPIO_CLEAR, 0xFF);	// GPIO Data Clear (turn off all 8 GPIO pins, we are only using 6 of 8, is this a problem?)

	//touch_write(GPIO_SET, 0b11111100);	// this line will turn all 6 LEDs on
}

void touch_quick_config(void)
{
	// This section generally follows the MPR121 Quick Configuration settings (see the application notes)
	// THIS SECTION HAS NOT YET BEEN OPTIMIZED FOR THE HEX BLOCKS
	// more comments in this section are needed!
	// specifically, each line should state what it is doing, and preferably why this choice is superior to other options


	// Section A
	// This group controls filtering when data is > baseline.
	touch_write(MHD_R, 0x01);
	touch_write(NHD_R, 0x01);
	touch_write(NCL_R, 0x00);
	touch_write(FDL_R, 0x00);
	
	// Section B
	// This group controls filtering when data is < baseline.
	touch_write(MHD_F, 0x01);
	touch_write(NHD_F, 0x01);
	touch_write(NCL_F, 0xFF);
	touch_write(FDL_F, 0x02);
	
	// Section C
	// This group sets touch and release thresholds for each electrode
	touch_write(ELE0_T, TOU_THRESH);
	touch_write(ELE0_R, REL_THRESH);
	touch_write(ELE1_T, TOU_THRESH);
	touch_write(ELE1_R, REL_THRESH);
	touch_write(ELE2_T, TOU_THRESH);
	touch_write(ELE2_R, REL_THRESH);
	touch_write(ELE3_T, TOU_THRESH);
	touch_write(ELE3_R, REL_THRESH);
	touch_write(ELE4_T, TOU_THRESH);
	touch_write(ELE4_R, REL_THRESH);
	touch_write(ELE5_T, TOU_THRESH);
	touch_write(ELE5_R, REL_THRESH);
	
	// Section D
	// Set the Filter Configuration
	// Set ESI2
	touch_write(FIL_CFG, 0x04);
	
	// Section E
	// Electrode Configuration
	// Enable 6 Electrodes and set to run mode
	// Set ELE_CFG to 0x00 to return to standby mode
	// touch_write(ELE_CFG, 0x0C);	// Enables all 12 Electrodes
	touch_write(ELE_CFG, 0x06);		// Enable first 6 electrodes
	
	// Section F
	// Enable Auto Config and auto Reconfig
	/*
	touch_write(ATO_CFG0, 0x0B);
	touch_write(ATO_CFGU, 0xC9);	// USL = (Vdd-0.7)/vdd*256 = 0xC9 @3.3V
	touch_write(ATO_CFGL, 0x82);	// LSL = 0.65*USL = 0x82 @3.3V
	touch_write(ATO_CFGT, 0xB5);	// Target = 0.9*USL = 0xB5 @3.3V
	*/
}





