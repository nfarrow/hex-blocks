//lcd.h

// NOTE: There is a MAJOR fault with the LCD module which has not been elucidated,
// the initialization routine puts the module into 2-line mode, however this only works correctly
// upon first power on.  Reprogramming the XMEGA in-circuit  
// REF: http://web.alfredstate.edu/weimandn/lcd/lcd_initialization/lcd_initialization_index.html


#ifndef LCD_H
#define LCD_H



#include <avr/io.h>
#include <inttypes.h>
#include <avr/pgmspace.h>
#include <stdio.h>

#include <util/delay.h>


#include <string.h>


#ifdef __cplusplus
extern "C"
{
#endif


// macro for automatically storing string constant in program memory
#define lcd_puts_P(__s)         lcd_puts_p(PSTR(__s))


#define LCD_LINES           2     /**< number of visible lines of the display */
#define LCD_DISP_LENGTH    16     /**< visible characters per line of the display */
#define LCD_LINE_LENGTH  0x40     /**< internal line length of the display    */
#define LCD_START_LINE1  0x00     /**< DDRAM address of first char of line 1 */
#define LCD_START_LINE2  0x40     /**< DDRAM address of first char of line 2 */
#define LCD_START_LINE3  0x14     /**< DDRAM address of first char of line 3 */
#define LCD_START_LINE4  0x54     /**< DDRAM address of first char of line 4 */
#define LCD_WRAP_LINES      0     /**< 0: no wrap, 1: wrap at end of visible line */



#define LCD11_DATA0_PORT   PORTB        /**< port for 4bit data bit 0 */
#define LCD12_DATA1_PORT   PORTB        /**< port for 4bit data bit 1 */
#define LCD13_DATA2_PORT   PORTB        /**< port for 4bit data bit 2 */
#define LCD14_DATA3_PORT   PORTB		  /**< port for 4bit data bit 3 */
#define LCD11_DATA0_bm    0b00010000   /**< pin for 4bit data bit 0  */
#define LCD12_DATA1_bm    0b00100000   /**< pin for 4bit data bit 1  */
#define LCD13_DATA2_bm    0b01000000   /**< pin for 4bit data bit 2  */
#define LCD14_DATA3_bm    0b10000000   /**< pin for 4bit data bit 3  */
#define LCD04_RS_PORT      PORTA		  /**< port for RS line         */
#define LCD04_RS_bm       0b00100000   /**< pin  for RS line         */
#define LCD05_RW_PORT      PORTA	      /**< port for RW line         */		
#define LCD05_RW_bm       0b01000000   /**< pin  for RW line         */
#define LCD06_E_PORT       PORTB	      /**< port for Enable line     */
#define LCD06_E_bm        0b00000010   /**< pin  for Enable line     */





/* instruction register bit positions, see HD44780U data sheet */
#define LCD_CLR               0      /* DB0: clear display                  */
#define LCD_HOME              1      /* DB1: return to home position        */
#define LCD_ENTRY_MODE        2      /* DB2: set entry mode                 */
#define LCD_ENTRY_INC         1      /*   DB1: 1=increment, 0=decrement     */
#define LCD_ENTRY_SHIFT       0      /*   DB2: 1=display shift on           */
#define LCD_ON                3      /* DB3: turn lcd/cursor on             */
#define LCD_ON_DISPLAY        2      /*   DB2: turn display on              */
#define LCD_ON_CURSOR         1      /*   DB1: turn cursor on               */
#define LCD_ON_BLINK          0      /*     DB0: blinking cursor ?          */
#define LCD_MOVE              4      /* DB4: move cursor/display            */
#define LCD_MOVE_DISP         3      /*   DB3: move display (0-> cursor) ?  */
#define LCD_MOVE_RIGHT        2      /*   DB2: move right (0-> left) ?      */
#define LCD_FUNCTION          5      /* DB5: function set                   */
#define LCD_FUNCTION_8BIT     4      /*   DB4: set 8BIT mode (0->4BIT mode) */
#define LCD_FUNCTION_2LINES   3      /*   DB3: two lines (0->one line)      */
#define LCD_FUNCTION_10DOTS   2      /*   DB2: 5x10 font (0->5x7 font)      */
#define LCD_CGRAM             6      /* DB6: set CG RAM address             */
#define LCD_DDRAM             7      /* DB7: set DD RAM address             */
#define LCD_BUSY              7      /* DB7: LCD is busy                    */

/* set entry mode: display shift on/off, dec/inc cursor move direction */
#define LCD_ENTRY_DEC            0x04   /* display shift off, dec cursor move dir */
#define LCD_ENTRY_DEC_SHIFT      0x05   /* display shift on,  dec cursor move dir */
#define LCD_ENTRY_INC_           0x06   /* display shift off, inc cursor move dir */
#define LCD_ENTRY_INC_SHIFT      0x07   /* display shift on,  inc cursor move dir */

/* display on/off, cursor on/off, blinking char at cursor position */
#define LCD_DISP_OFF             0x08   /* display off                            */
#define LCD_DISP_ON              0x0C   /* display on, cursor off                 */
#define LCD_DISP_ON_BLINK        0x0D   /* display on, cursor off, blink char     */
#define LCD_DISP_ON_CURSOR       0x0E   /* display on, cursor on                  */
#define LCD_DISP_ON_CURSOR_BLINK 0x0F   /* display on, cursor on, blink char      */

/* move cursor/shift display */
#define LCD_MOVE_CURSOR_LEFT     0x10   /* move cursor left  (decrement)          */
#define LCD_MOVE_CURSOR_RIGHT    0x14   /* move cursor right (increment)          */
#define LCD_MOVE_DISP_LEFT       0x18   /* shift display left                     */
#define LCD_MOVE_DISP_RIGHT      0x1C   /* shift display right                    */

/* function set: set interface data length and number of display lines */
#define LCD_FUNCTION_4BIT_1LINE  0x20   /* 4-bit interface, single line, 5x7 dots */
#define LCD_FUNCTION_4BIT_2LINES 0x28   /* 4-bit interface, dual line,   5x7 dots */
#define LCD_FUNCTION_8BIT_1LINE  0x30   /* 8-bit interface, single line, 5x7 dots */
#define LCD_FUNCTION_8BIT_2LINES 0x38   /* 8-bit interface, dual line,   5x7 dots */


// USER ACCESSABLE FUNCTIONS:
uint8_t lcd_put_chars(const char* s);	// use '\n' to access the 2nd line of output
// ///// example calls:		///////////////////////////////////////////
//
// lcd_put_chars("LCD test");
//
// int num_as_int = 1234;
// char num_as_string[5];
// itoa(num_as_int, num_as_string, 10 /*base 10*/);
// lcd_put_chars(num_as_string);
// 
// ////////////////////////////////////////////////////////////////////

void lcd_clear_screen(void); // user must call this to clear the screen before writing new stuff

void lcd_init(void);

void lcd_set_backlight(uint8_t r, uint8_t g, uint8_t b);

void lcd_getxy(void);	// prints to stdout

void lcd_print_contents(void);


// INTERNAL FUNCTIONS (NOT TO BE CALLED BY USER):

void lcd_putc(char c);

void lcd_newline(void);

void lcd_write_command(uint8_t command_data);

void lcd_write_data(uint8_t data);

void lcd_send_byte(uint8_t data);

uint8_t lcd_waitbusy(void);

void toggle_e(void);

void lcd_reset(void);

// UNUSED FUNCTIONS:
/*
void lcd_gotoxy(uint8_t x, uint8_t y);

extern void lcd_home(void);
*/


#ifdef __cplusplus
}
#endif

#endif //LCD_H

