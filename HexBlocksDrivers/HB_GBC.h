// HB_GBC.h

// LEAVE THIS FILE COMMENTED UNTIL WE ARE READY TO INCLUDE IT

// NOTE: This driver library is currently undergoing extensive refactoring (8/24/14)

#include <stdio.h>			// used for printf_P
#include <avr/pgmspace.h>	// used for PSTR

#include <avr/io.h>
#include <util/delay.h>

#include "..\HexBlocksSource\time_manager.h"	// to disable printing of time while picture is clocked out

// Initialize the IO ports for the camera
void GBC_init(uint16_t init_id);

// Saves a fake image to image buffer,
// used to assess timing and printing requirements
void GBC_fake_image();

// Prints an image to the serial port,
// currently this may take 8 seconds to print the whole thing
// prints in hex
void GBC_print_image();

void save_pixel(uint8_t pixel_x, uint8_t pixel_y, uint8_t val);

uint8_t load_pixel(uint8_t pixel_x, uint8_t pixel_y);

// Initialize the IO ports for the camera
void camInitPins();

// locally set the value of a register but do not set it in the AR chip. You
// must run camSendRegisters1 to write the register value in the chip
void camSetReg1(unsigned char reg, unsigned char data);

// Define the protocol use to send the image through the serial port
//void camSetMode(unsigned char mode);

// Change the clock speed to allow taking photo with a smaller or bigger
// exposure time than the one defined by default. Default is 0x0A = clock period = 5us
//void camSetClockSpeed(unsigned char clockSpeed);

// Define the the minimal value of pixels. Used by som camera modes
//void camSetMinPixValue(unsigned char minV);

// Define the the maximal value of pixels. Used by som camera modes
//void camSetMaxPixValue(unsigned char maxV);

// Send the 8 register values to the AR chip
void camSendRegisters1();

// Take a picture, read it and send it though the serial port.
void camTakePicture();

// PRIVATE FUNCTIONS ///////////////////////////////////////////////////////////////

// Delay used between each signal sent to the AR (four per xck cycle).
void camStepDelay();

// Sends a 'reset' pulse to the AR chip.
void camResetAR();

// Sets one of the 8 8-bit registers in the AR chip.
void camSetRegAR(unsigned char regaddr, unsigned char regval);

// Sends a 'start' pulse to the AR chip.
void camStartAR();

// Sends a blank 'xck' pulse to the AR chip.
void camClockAR();

// Wait read signal from the AR chip
unsigned char camWaitRead();

// Read pixels from AR until READ signal is unraise
unsigned char camReadPixels(uint8_t &pixel_val);

// callback run when the ADC conversion is finished = read 1 pixel done
uint8_t camReadPixel();

