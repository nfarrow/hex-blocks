// touch.h

#include "HexBlockI2C.h"

#include <util/delay.h>
#include <stdio.h>			// used for printf_P
#include <avr/pgmspace.h>	// used for PSTR

/*
#ifdef __cplusplus
extern "C"
{
#endif
*/

// MPR121 Register Defines
#define MHD_R		0x2B
#define NHD_R		0x2C
#define	NCL_R 		0x2D
#define	FDL_R		0x2E
#define	MHD_F		0x2F
#define	NHD_F		0x30
#define	NCL_F		0x31
#define	FDL_F		0x32
#define	ELE0_T		0x41
#define	ELE0_R		0x42
#define	ELE1_T		0x43
#define	ELE1_R		0x44
#define	ELE2_T		0x45
#define	ELE2_R		0x46
#define	ELE3_T		0x47
#define	ELE3_R		0x48
#define	ELE4_T		0x49
#define	ELE4_R		0x4A
#define	ELE5_T		0x4B
#define	ELE5_R		0x4C
#define	ELE6_T		0x4D
#define	ELE6_R		0x4E
#define	ELE7_T		0x4F
#define	ELE7_R		0x50
#define	ELE8_T		0x51
#define	ELE8_R		0x52
#define	ELE9_T		0x53
#define	ELE9_R		0x54
#define	ELE10_T		0x55
#define	ELE10_R		0x56
#define	ELE11_T		0x57
#define	ELE11_R		0x58
#define	FIL_CFG		0x5D
#define	ELE_CFG		0x5E
#define GPIO_CTRL0	0x73
#define	GPIO_CTRL1	0x74
#define GPIO_DATA	0x75
#define	GPIO_DIR	0x76
#define	GPIO_EN		0x77
#define	GPIO_SET	0x78
#define	GPIO_CLEAR	0x79
#define	GPIO_TOGGLE	0x7A
#define	ATO_CFG0	0x7B
#define	ATO_CFGU	0x7D
#define	ATO_CFGL	0x7E
#define	ATO_CFGT	0x7F
#define SRST		0x80

// Global Constants
#define TOU_THRESH	0x0F
#define	REL_THRESH	0x0A


// see Table 1 of application note AN3895 for these values (ADDR pin is pin 4 of the 20-pin QFN package)
#define MPR121_I2C_ADDR_PIN_ON_GND		0x5A
#define MPR121_I2C_ADDR_PIN_ON_VCC		0x5B
#define MPR121_I2C_ADDR_PIN_ON_SDA		0x5C
#define MPR121_I2C_ADDR_PIN_ON_SCL		0x5D

#define MPR121_ADDRESS					MPR121_I2C_ADDR_PIN_ON_GND	// this is a standard 7-BIT address!


/* USER ACCESSIBLE FUNCTIONS */

// only need to perform this once (during init)
uint8_t check_touch_exists(void);

// after calling check_touch_exists, you may call this to get the same result
// this prevents unnecessarily using the I2C line
uint8_t verify_touch_existance(void);

//input electrode number to get 16-bit electrode ADC value (elec_no range is 0 - 5)
uint16_t get_electrode_proximity_value(uint8_t elec_no); 

void print_proximity_values(void);

void blink_touch_leds(void);

void touch_soft_reset(void);

/* INTERNAL FUNCTIONS */

// read a register (8bit)
uint8_t touch_read(uint8_t register_address);

// read 2 consecutive registers (16 bit)
// register address is the first address, the next is auto-incremented
uint16_t touch_read_16(uint8_t register_address);

// write 8bit data into registers via i2c
void touch_write(uint8_t register_address, uint8_t data);


/* INITIALIZATION */

// initialize touch controller
void init_touch(void);

// initialize GPIO pins as LED drivers
// CALLED BY init_touch(), DO NOT CALL
void init_touch_LEDS(void);

// config touch controller for touch reading
// proximity sensing comes along with it??
// THIS SECTION HAS NOT YET BEEN OPTIMIZED FOR THE HEX BLOCKS
// CALLED BY init_touch(), DO NOT CALL
void touch_quick_config(void);

/*
#ifdef __cplusplus
}
#endif
*/