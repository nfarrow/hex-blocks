// lcd.c

// LCD TODO:

// 1) git rid of any lines of code that don't get called anywhere
// 2) add more strange delays so that printfs aren't necessary
// 3) write string parsing/comparison code that keeps track of what is on the screen
// 4) write a more usable user interface
// 5) polish the code and comments
// 6) figure out why strange delays are necessary 


// RS pin 4 of LCD stands for Register Select, it is used to distinguish commands from data
// COMMANDS (RS = 0) are things like 'clear the display' & 'move the cursor'
// DATA only (ONLY!) means single characters wither sent to display or read from display
// reading the busy flag is a COMMAND!



// it is reported (http://8515.avrfreaks.net/index.php?name=PNphpBB2&file=viewtopic&t=87612&view=previous)
// that if the AVR processor speed is > 1MHz, then it is too fast to use with the LCD:
// "Besides what speed your AVR is running at? The code most likely runs too fast too, 
// and you are violating the specifications how long RS and RW should be stable before E rises,
// how long should E be high, how long E should be low, and what should be the time between two rising E edges." 

// with an LCD module internal clock speed of 270 kHz, one clock pulse is 3.7 us, so it may take this long to stabilize
// after changing the R/W, RS, and E pins.  ** IMPORTATNT FOR TIMING AND WAITING ON BUSY FLAG **
// This could explain why the module reports being not busy immediately after issuing the clear screen command. !!! TODO
// REF: https://www.adafruit.com/datasheets/HD44780.pdf, pg. 38


#include "lcd.h"

//#define LCD_DEBUG_PRINTOUT_INTERMEDIATE
//#define LCD_DEBUG_PRINTOUT_WHEN_COMPLETE

#define E_DELAY_us 1				
#define CHAR_DELAY_us 160			// 160 us is recommended
#define LCD_BOOT_UP_DELAY_ms 16		// 16 ms is recommended, but may have been insufficient
#define CLEAR_SCREEN_DELAY_ms 4.1
#define EXTRA_DELAY_OF_UNKNOWN_ORIGIN_us 25//25

//#define F_CPU 32000000	// defined by makefile

uint8_t lcd_led_red;
uint8_t lcd_led_green;
uint8_t lcd_led_blue;

uint8_t cursor_x = 0;
uint8_t cursor_y = 0;

char lcd_internal_line[2][17];

uint16_t total_exe_time = 0;



// USER ACCESSABLE FUNCTIONS:

uint8_t lcd_put_chars(const char* s)
// returns the number of characters that were found in the string s
{
	total_exe_time = 0;
	
	//char c;
	uint8_t i;

	uint8_t terminal_char = 0;

	//printf_P(PSTR("lcd:"));	// garbage prints/crashes program
	#ifdef LCD_DEBUG_PRINTOUT_INTERMEDIATE
	printf("lcd:\r\n");	// prints OK
	#endif
	
	i = 0;
	
	while(s[i])
	{
		if(s[i] == '\n')
		lcd_newline();
		else
		lcd_putc(s[i]);
		
		i++;
	}
	terminal_char = i-1;

	#ifdef LCD_DEBUG_PRINTOUT_WHEN_COMPLETE
	printf("LCD exe time ");
	//_delay_ms(1);	// TODO replace this delay with a flush command
	fflush(stdout);
	printf("%u us\r\n",total_exe_time);
	#endif
	
	total_exe_time = 0;

	return terminal_char;
}

// Clear display and set cursor to home position
void lcd_clear_screen(void)
{
	total_exe_time = 0;
	
	lcd_write_command(0b00000001);	// this function does not return until busy flag clears

	// for some reason, the LCD module reports being done (not busy) before it is actually ready
	// to receive another input or command

	#ifdef LCD_DEBUG_PRINTOUT_WHEN_COMPLETE
	_delay_ms(1);
	printf("LCD clear time ");
	_delay_ms(1);
	printf("%u us\r\n",total_exe_time); // this, and the preceding print take a combined 2.71 ms
	#else
	
	//_delay_ms(1.5);
	_delay_ms(1.5);
	// experimentally, using the oscilloscope, this delay was measured to be:
	// _delay_ms(0.9) = 1.088 ms
	// _delay_ms(1.0) = 1.176 ms
	// _delay_ms(1.1) = 1.284 ms
	// _delay_ms(1.2) = 1.384 ms	(does not work - too short)
	// _delay_ms(1.3) = 1.596 ms	(works - threshold of working)
	// _delay_ms(1.4) = 1.688 ms
	// _delay_ms(1.5) = 1.796 ms

	// functional form:		ActualDelay = -0.0525714 + 1.23571(RequestedDelay)
	// note (perfect) :		ActualDelay = 0          + 1(RequestedDelay)

	#endif
	
	total_exe_time = 0;

	cursor_x = 0;
	cursor_y = 0;

	for(uint8_t i = 0; i < 16; i++)
	{
		lcd_internal_line[0][i] = '\0';
		lcd_internal_line[1][i] = '\0';
	}
}

void lcd_init(void)
{
	total_exe_time = 0;

	// ensure internal strings are always null terminated (when full)
	lcd_internal_line[0][16] = '\0';
	lcd_internal_line[1][16] = '\0';
	
	// Disables JTAG (Frees Pins B4-B7)
	CCP = CCP_IOREG_gc;
	MCU.MCUCR = 1;
	
	// Set backlight pins as output
	PORTD.DIRSET = 0b00000001;	// red backlight
	PORTD.DIRSET = 0b00010000;	// blue backlight
	PORTD.DIRSET = 0b00000010;	// green backlight

	lcd_led_red = 0;
	lcd_led_green = 0;
	lcd_led_blue = 0;

	lcd_set_backlight(0,1,0);

	//Init Pins for Output
	LCD04_RS_PORT.DIR |= LCD04_RS_bm;
	LCD05_RW_PORT.DIR |= LCD05_RW_bm;
	LCD06_E_PORT.DIR  |= LCD06_E_bm;
	LCD11_DATA0_PORT.DIR |= LCD11_DATA0_bm;
	LCD12_DATA1_PORT.DIR |= LCD12_DATA1_bm;
	LCD13_DATA2_PORT.DIR |= LCD13_DATA2_bm;
	LCD14_DATA3_PORT.DIR |= LCD14_DATA3_bm;

	// wait 16ms or more after power-on
	_delay_ms(LCD_BOOT_UP_DELAY_ms);
	total_exe_time += LCD_BOOT_UP_DELAY_ms*1000;
	
	lcd_reset();	// this is needed in the case of XMEGA reset without accompanying loss of power

	// the following is NOT data, the RS line should be LOW for commands (e.g. the following bytes)
	
	LCD04_RS_PORT.OUT &=~ LCD04_RS_bm;			// RS = 0 for command (not data)
	LCD05_RW_PORT.OUT &=~ LCD05_RW_bm;			// R/W = 0 for write

	// Initialize LCD to 4 bit I/O mode

	// 1st byte: 0010 0000		(configure 4-bit mode)
	// (the LCD module treats this first nibble as a single 8-bit instruction)
	
	//0010
	LCD14_DATA3_PORT.OUT &=~ LCD14_DATA3_bm;
	LCD13_DATA2_PORT.OUT &=~ LCD13_DATA2_bm;
	LCD12_DATA1_PORT.OUT |= LCD12_DATA1_bm;
	LCD11_DATA0_PORT.OUT &=~ LCD11_DATA0_bm;
	toggle_e();
	
	//actually, it is recommended to wait 100 us here, and not to check the busy flag
	_delay_us(100);

	// 2nd byte: 0010 1000		(Function Set: 4-bit characters(again), 2-line LCD, 5x8 dots)

	lcd_write_command(0b00101000);	// this function does not return until busy flag clears
	
	// 3rd byte: 0000 1111		(Display: display ON, cursor ON, cursor blinks is ON)
	// 3rd byte: 0000 1101		(Display: display ON, cursor OFF, cursor blinks is ON)
	// 3rd byte: 0000 1110		(Display: display ON, cursor ON, cursor blinks is OFF)
	// 3rd byte: 0000 1100		(Display: display ON, cursor OFF, cursor blinks is OFF)

	// note: 'cursor blinks' means that blinking 5x8 black box
	// note: 'cursor' means that solid 5x1 bar that floats under the characters

	lcd_write_command(0b00001100);	// this function does not return until busy flag clears
	
	// REF: http://mil.ufl.edu/4744/docs/lcdmanual/commands.html#Rbf

	// 4th byte: 0000 0110
	//   0 0 0 0 0 1  1  0	(I/D = 1; increment cursor position (write left-to-right), I/D = 0; decrement cursor postion (write right-to-left))
	// = 0 0 0 0 0 1 I/D S	(S = *matches I/D*; scrolling display, yeay!, S = *opposite of I/D*; stationary display)

	lcd_write_command(0b00000110);	// this function does not return until busy flag clears
	
	// this is the end of commands, from now on RS should be 1 (except clearing display and moving cursor manually, etc.)
	
	//LCD04_RS_PORT.OUT |= LCD04_RS_bm;			// RS = 1 for data (not command)
	
	#ifdef LCD_DEBUG_PRINTOUT_WHEN_COMPLETE
	printf("LCD init time ");
	fflush(stdout);
	printf("%u us\r\n",total_exe_time);
	#endif
	
	total_exe_time = 0;

	lcd_clear_screen();
}

void lcd_set_backlight(uint8_t r, uint8_t g, uint8_t b)
{
	if(r != lcd_led_red)
	{
		if(r)	PORTD.OUT |= 0b00000001;	// turn on the red backlight
		else	PORTD.OUT &= ~0b00000001;	// turn off the red backlight
		lcd_led_red = r;
	}

	if(g != lcd_led_green)
	{
		if(g)	PORTD.OUT |= 0b00000010;	// turn on the green backlight
		else	PORTD.OUT &= ~0b00000010;	// turn off the green backlight
		lcd_led_green = g;
	}

	if(b != lcd_led_blue)
	{
		if(b)	PORTD.OUT |= 0b00010000;	// turn on the blue backlight
		else	PORTD.OUT &= ~0b00010000;	// turn off the blue backlight
		lcd_led_blue = b;
	}
}

void lcd_getxy(void)
{
	printf("lcd x: %i\r\n", cursor_x);
	_delay_ms(1);
	printf("lcd y: %i\r\n", cursor_y);
}

void lcd_print_contents(void)
{
	printf("lcd 0: %s\r\n", lcd_internal_line[0]);
	_delay_ms(1);
	printf("lcd 1: %s\r\n", lcd_internal_line[1]);
}


// INTERNAL FUNCTIONS (NOT TO BE CALLED BY USER):

// ** DO NOT CALL DIRECTLY **
// Display character at current cursor position
// FUNCTIONS THAT CALL THIS: lcd_put_chars()
void lcd_putc(char c)
{
	// newline characters are handled BEFORE calling this function,
	// they are not sent to this functions, instead they trigger a call to lcd_newline()

	//printf_P(PSTR("%c"),c);		// garbage prints
	#ifdef LCD_DEBUG_PRINTOUT_INTERMEDIATE
	printf("%c ",c);					// prints ok
	#endif

	uint8_t data = c;
	

	lcd_write_data(data);	// this function does not return until busy flag clears

	if((cursor_x < 16)&&(cursor_y < 2))
	lcd_internal_line[cursor_y][cursor_x] = c;

	cursor_x++;
}

void lcd_newline(void)
{
	lcd_write_command(0b11000000);	// take cursor to 1st position on 2nd line
	//lcd_write_command(0b10000000);	// take cursor to 1st position on 1st line

	cursor_y = 1;
	cursor_x = 0;
}

// Send LCD controller instruction command
void lcd_write_command(uint8_t command_data)
{
	// commands are WRITE ONLY (RW = 0 for write)
	LCD05_RW_PORT.OUT &= ~LCD05_RW_bm;	// set LCD R/W input low

	// commands are instructions ONLY (RS = 0 for instruction, RS = 1 for data)
	LCD04_RS_PORT.OUT &= ~LCD04_RS_bm;	// set LCD RS input low (RS = register select)
	
	lcd_send_byte(command_data);	// this function does not return until busy flag clears
}


// Send data byte to LCD controller
void lcd_write_data(uint8_t data)
{
	// data characters are WRITE ONLY (RW = 0 for write)
	LCD05_RW_PORT.OUT &= ~LCD05_RW_bm;	// set LCD R/W input LOW

	// data characters are data ONLY (RS = 0 for instruction, RS = 1 for data)
	LCD04_RS_PORT.OUT |= LCD04_RS_bm;	// set LCD RS input HIGH
	
	lcd_send_byte(data);	// this function does not return until busy flag clears
}

void lcd_send_byte(uint8_t data)
{
	// output high nibble first
	
	if(data & 0x80)
	LCD14_DATA3_PORT.OUT |= LCD14_DATA3_bm;
	else
	LCD14_DATA3_PORT.OUT &= ~LCD14_DATA3_bm;
	if(data & 0x40)
	LCD13_DATA2_PORT.OUT |= LCD13_DATA2_bm;
	else
	LCD13_DATA2_PORT.OUT &= ~LCD13_DATA2_bm;
	if(data & 0x20)
	LCD12_DATA1_PORT.OUT |= LCD12_DATA1_bm;
	else
	LCD12_DATA1_PORT.OUT &= ~LCD12_DATA1_bm;
	if(data & 0x10)
	LCD11_DATA0_PORT.OUT |= LCD11_DATA0_bm;
	else
	LCD11_DATA0_PORT.OUT &= ~LCD11_DATA0_bm;
	toggle_e();
	
	// output low nibble
	
	if(data & 0x08)
	LCD14_DATA3_PORT.OUT |= LCD14_DATA3_bm;
	else
	LCD14_DATA3_PORT.OUT &= ~LCD14_DATA3_bm;
	if(data & 0x04)
	LCD13_DATA2_PORT.OUT |= LCD13_DATA2_bm;
	else
	LCD13_DATA2_PORT.OUT &= ~LCD13_DATA2_bm;
	if(data & 0x02)
	LCD12_DATA1_PORT.OUT |= LCD12_DATA1_bm;
	else
	LCD12_DATA1_PORT.OUT &= ~LCD12_DATA1_bm;
	if(data & 0x01)
	LCD11_DATA0_PORT.OUT |= LCD11_DATA0_bm;
	else
	LCD11_DATA0_PORT.OUT &= ~LCD11_DATA0_bm;
	toggle_e();
	
	// all data pins LOW (inactive)
	LCD14_DATA3_PORT.OUT &= ~LCD14_DATA3_bm;
	LCD13_DATA2_PORT.OUT &= ~LCD13_DATA2_bm;
	LCD12_DATA1_PORT.OUT &= ~LCD12_DATA1_bm;
	LCD11_DATA0_PORT.OUT &= ~LCD11_DATA0_bm;

	lcd_waitbusy();
}

uint8_t lcd_waitbusy(void)
{
	// woah, we need to make sure RS = 0!
	LCD04_RS_PORT.OUT &= ~LCD04_RS_bm;	// set LCD RS input low

	volatile uint8_t busy_wait_count = 0;
	
	// initialize Data7/BusyFlag pin low
	LCD14_DATA3_PORT.OUT &= ~LCD14_DATA3_bm;

	// set Data7/BusyFlag pin as input
	LCD14_DATA3_PORT.DIR &= ~LCD14_DATA3_bm;

	// set R/W pin high to READ
	LCD05_RW_PORT.OUT |= LCD05_RW_bm;


	do{	// wait until busy flag is cleared
	
		if(busy_wait_count >= 0)
		{
			LCD06_E_PORT.OUT &= ~LCD06_E_bm; // OFF

			_delay_us(E_DELAY_us);
			total_exe_time += E_DELAY_us;
		}
	
		busy_wait_count++;
	
		// manually strobe E to read the busy flag
		LCD06_E_PORT.OUT |= LCD06_E_bm;	// E-ON
		_delay_us(E_DELAY_us);	// 500 ns should be sufficient
		total_exe_time += E_DELAY_us;

		//clear_count++;	//debug

	}while((LCD14_DATA3_PORT.IN & LCD14_DATA3_bm) == LCD14_DATA3_bm);	// this read only valid when E is high?
	//}while(((LCD14_DATA3_PORT.IN & LCD14_DATA3_bm) == LCD14_DATA3_bm)||(clear_count < 10));	// this read only valid when E is high?

	//printf("cc:%u", clear_count);

	LCD06_E_PORT.OUT &= ~LCD06_E_bm; // E-OFF

	// set R/W pin low to WRITE again
	LCD05_RW_PORT.OUT &= ~LCD05_RW_bm;	// OFF

	// set Data7/BusyFlag pin as output again
	LCD14_DATA3_PORT.DIR |= LCD14_DATA3_bm;

	_delay_us(E_DELAY_us);
	total_exe_time += E_DELAY_us;


	#ifdef LCD_DEBUG_PRINTOUT_INTERMEDIATE
	printf("WB%u\r\n",busy_wait_count);
	//fflush(stdout);
	total_exe_time += EXTRA_DELAY_OF_UNKNOWN_ORIGIN_us;
	// the problem is that if you don't do these printfs, then the lcd
	// doesn't print the character string correctly, it omits chars, or replaces chars with garbage
	// the fix is to either printf, or have some extra delay here, which I have included in the #else below

	#else
	// some extra delay is necessary here,
	// sufficient delay(us): 100, 50(causes blinking display when writing?), 25 <- seems to work the best
	// insufficient delay(us): 10, 20
	_delay_us(EXTRA_DELAY_OF_UNKNOWN_ORIGIN_us);
	total_exe_time += EXTRA_DELAY_OF_UNKNOWN_ORIGIN_us;
	#endif

	return busy_wait_count;
}

// toggle Enable Pin to send contents of data pins to lcd controller
void toggle_e(void)
{
	LCD06_E_PORT.OUT |= LCD06_E_bm;	// ON

	_delay_us(E_DELAY_us);	// 500 ns should be sufficient
	total_exe_time += E_DELAY_us;

	LCD06_E_PORT.OUT &= ~LCD06_E_bm; // OFF

	_delay_us(E_DELAY_us);
	total_exe_time += E_DELAY_us;
}

void lcd_reset(void)
{
	LCD04_RS_PORT.OUT &=~ LCD04_RS_bm;			// RS = 0 for command (not data)
	LCD05_RW_PORT.OUT &=~ LCD05_RW_bm;			// R/W = 0 for write

	// 1st byte: 0011 0000
	// (yes, looks like we are setting 8-bit mode, doesn't matter b/c we are going to reset anyway)

	//0011
	LCD14_DATA3_PORT.OUT &=~ LCD14_DATA3_bm;
	LCD13_DATA2_PORT.OUT &=~ LCD13_DATA2_bm;
	LCD12_DATA1_PORT.OUT |= LCD12_DATA1_bm;
	LCD11_DATA0_PORT.OUT |= LCD11_DATA0_bm;
	toggle_e();

	_delay_ms(4.1);	// longer delay than usual

	// 2nd byte: 0011 0000 (repeat of the 1st)

	//0011
	LCD14_DATA3_PORT.OUT &=~ LCD14_DATA3_bm;
	LCD13_DATA2_PORT.OUT &=~ LCD13_DATA2_bm;
	LCD12_DATA1_PORT.OUT |= LCD12_DATA1_bm;
	LCD11_DATA0_PORT.OUT |= LCD11_DATA0_bm;
	toggle_e();

	_delay_us(100);

	// 3rd byte: 0011 0000 (repeat of the 1st)

	//0011
	LCD14_DATA3_PORT.OUT &=~ LCD14_DATA3_bm;
	LCD13_DATA2_PORT.OUT &=~ LCD13_DATA2_bm;
	LCD12_DATA1_PORT.OUT |= LCD12_DATA1_bm;
	LCD11_DATA0_PORT.OUT |= LCD11_DATA0_bm;
	toggle_e();

	_delay_us(100);

	// this should complete the reset process (equivalent to power cycling the module)
}

// UNUSED ROUTINES:

/*
// Set cursor to specified position
// (x = 0: left most position)
// (y = 0: first line)
void lcd_gotoxy(uint8_t x, uint8_t y)
{
	if ( y==0 )
	lcd_write_command((1<<LCD_DDRAM)+LCD_START_LINE1+x);
	else
	lcd_write_command((1<<LCD_DDRAM)+LCD_START_LINE2+x);
}

// Set cursor to home position
void lcd_home(void)
{
	lcd_write_command(0b00000010);	// this function does not return until busy flag clears
}
*/
