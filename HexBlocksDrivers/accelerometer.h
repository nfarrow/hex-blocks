// accelerometer.h

// NOTE: There is currently no application which uses the accelerometer
// this component is only installed on block 13 (I think), which is also intended
// to have battery backup operation. There is little use for the accelerometer while
// the blocks are in the wall, except possibly the tap/tilt functions.

#include "HexBlockI2C.h"

//#include "avr_compiler.h"		// (provides: <avr/io.h>, <avr/interrupt.h>, <avr/pgmspace.h>, <util/delay.h>)

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdio.h> // used for fflush and stdout
#include <math.h>


// Interrupts Declared:
// ISR(PORTB_INT0_vect)


#ifdef __cplusplus
extern "C"
{
#endif


#define ACCELEROMETER_ADDRESS			0b1001100	// this is a standard 7-BIT I2C address!
// we are using Freescale Semiconductor MMA7660FC 3-Axis Orientation/Motion Detection Sensor


/* USER ACCESSIBLE FUNCTIONS */

uint8_t check_accel_exists(void);	// return: 1 if exist, 0 if not

// read and print all measured data from the accelerometer
void accel_status(void);


/* INTERNAL FUNCTIONS */

// read a register (8bit) via i2c
uint8_t accel_read(uint8_t read_address);

// read a register (8bit) via i2c, request again if data self-reports as corrupt
uint8_t accel_read_and_verify(uint8_t read_address);

// write 8bit data into registers via i2c
void accel_write(uint8_t addr, uint8_t data);

// convert raw data from two's companion into decimal with sign (units are in g's) 
float accel_data_to_number(uint8_t axis_data);

// PCB (incl. accelerometer) is rotated +51 degrees w.r.t. true DOWN
// therefore, these functions convert to rotated coordinate system
float rotated_y(float y_accel, float z_accel);

float rotated_z(float y_accel, float z_accel);

//prints tap to stdout
void tap(uint8_t tilt);

//prints shake to stdout
void shake(uint8_t tilt);


/* INITIALIZATION */

// initialize accelerometer controller
void init_accel(void);



//ISR(PORTB_INT0_vect);

#ifdef __cplusplus
}
#endif