
#include <avr/io.h>
#include "high_voltage.h"

void init_HV(void)
{
	PORTB.DIRSET = PIN0_bm;		// pin B0 as output
	PORTB.OUTCLR = PIN0_bm;		// start low
}

void go_transparent(void)
{
	PORTB.OUTSET = PIN0_bm;		// set B0 high
	printf_P(PSTR("transparent\r\n"));
}

void go_opaque(void)
{
	PORTB.OUTCLR = PIN0_bm;		// set B0 low
	printf_P(PSTR("opaque\r\n"));
}

uint8_t is_transparent(void)
{
	if(PORTB.OUT & PIN0_bm)
		return 1;
	else
		return 0;
}

uint8_t is_opaque(void)
{
	if(PORTB.OUT & PIN0_bm)
		return 0;
	else
		return 1;
}