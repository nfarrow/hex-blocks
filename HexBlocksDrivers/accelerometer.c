// accelerometer.c

#include "accelerometer.h"

// Internal Register Addresses
#define X_OUT		0x00
#define Y_OUT		0x01
#define Z_OUT		0x02
#define TILT		0x03
#define INTSU		0x06	// Interrupt Setup Register	(pg. 16)
#define MODE		0x07
#define PDET		0x09	// Tap/Pulse Detection Register (pg. 21)

// used for reading XOUT, YOUT, or ZOUT registers (ref: pg. 14, datasheet)
#define ACCEL_VALUE_bm	0b00011111
#define ACCEL_SIGN_bm	0b00100000

// used for reading TILT register
#define SHAKE_bm		0b10000000
#define TAP_bm			0b00100000

// valid for use with XOUT, YOUT, ZOUT or TILT register
#define ALERT_bm		0b01000000

uint8_t accelerometer_existance_confirmed = 0;	// boolean, set in init

/* USER ACCESSIBLE FUNCTIONS */

uint8_t check_accel_exists()
{
	return I2C_check_device_exists(ACCELEROMETER_ADDRESS);
}

// prints the measurements of the accelerometer to stdout
void accel_status(void)
{
	uint8_t x_data, y_data, z_data, tilt_data;
	float x_value, y_value, z_value, y_value_rotated, z_value_rotated;

	printf("accelerometer\r\n");
	
	if(accelerometer_existance_confirmed)
	{
		//gather raw data, request again if data is corrupt
		x_data = accel_read_and_verify(X_OUT);
		y_data = accel_read_and_verify(Y_OUT);
		z_data = accel_read_and_verify(Z_OUT);
		tilt_data = accel_read_and_verify(TILT);

		x_value = accel_data_to_number(x_data);
		y_value = accel_data_to_number(y_data);
		z_value = accel_data_to_number(z_data);

		y_value_rotated = rotated_y(y_value, z_value); // rotate axis
		z_value_rotated = rotated_z(y_value, z_value);

		printf("X RAW: %u\r\n", x_data);		//fflush(stdout);
		printf("Y RAW: %u\r\n", y_data);		//fflush(stdout);
		printf("Z RAW: %u\r\n", z_data);		//fflush(stdout);
		
		printf("X-AXIS: %f\r\n", x_value);			//fflush(stdout);
		_delay_ms(1);
		printf("Y-AXIS: %f\r\n", y_value_rotated);	//fflush(stdout);
		_delay_ms(1);
		printf("Z-AXIS: %f\r\n", z_value_rotated);	//fflush(stdout);
		_delay_ms(1);
		
		// print the tilt value in binary (why?)
		// note: itoa() is a nonstandard function, whose definition is built
		// into 'most' compilers (e.g. it is not provided by a library include)
		// so this may not work in all compiles.  An alternative is to print
		// inside a for loop using bit shift operations.
		char tilt_binary[9];
		itoa(tilt_data, tilt_binary, 2);
		printf("TILT: %s\r\n", tilt_binary);		//fflush(stdout);

		//test for tap and shake
		tap(tilt_data);
		shake(tilt_data);
	}
	
	else
		printf("DOES NOT EXIST\r\n");
}


/* INTERNAL FUNCTIONS */

uint8_t accel_read(uint8_t read_address)
{
	return I2C_read(ACCELEROMETER_ADDRESS, read_address);
}

uint8_t accel_read_and_verify(uint8_t read_address)
{
	uint8_t data;
	uint8_t request_attempts = 0; // for debug

	do
	{
		data = I2C_read(ACCELEROMETER_ADDRESS, read_address);
		request_attempts++;

		if(request_attempts > 1)
		printf("Alert!");
	} while (data & ALERT_bm);
	
	return data;
}

void accel_write(uint8_t addr, uint8_t data)
{
	I2C_write(ACCELEROMETER_ADDRESS, addr, data);
}

// convert raw data from two's companion into decimal with sign and units G
float accel_data_to_number(uint8_t axis_data)
{
	uint8_t accel_value;
	float answer;

	accel_value = axis_data & ACCEL_VALUE_bm;
	
	// accel data is reported in 6-bit 2's complement form
	// check to see if sign bit is set indicating a negative value

	if(axis_data & ACCEL_SIGN_bm)
	{	// then data is negative, so negate the number using standard 2's complement rules
		uint8_t temp;
		// rules:	take the complement of the binary number
		//			add 1, and discard the overflow bit (if applicable)
		temp = (~accel_value + 1)&ACCEL_VALUE_bm;	// bitmask is for handling the overflow bit
		answer = (temp/21.33)*-1.0;		//21.33 constant comes from the 'sensitivity' of the sensor (Ref: pg. 8, datasheet)
	}
	
	else
	answer = accel_value/21.33;

	return answer;
}

// Covert y axis measurement to true y axis,
// PCB (incl. accelerometer) is rotated +51 degrees w.r.t. true DOWN
float rotated_y(float y_accel, float z_accel)
{
	// standard rotation matrix
	float y_component = y_accel * cos((M_PI/180)*-51);
	float z_component = z_accel * -sin((M_PI/180)*-51);
	
	return y_component + z_component;
}

// Convert z axis measurement to true z axis
float rotated_z(float y_accel, float z_accel)
{
	// standard rotation matrix
	float y_component = y_accel * sin((M_PI/180)*-51);
	float z_component = z_accel * cos((M_PI/180)*-51);

	return y_component + z_component;
}



/* INITIALIZATION */

void init_accel()
{
	
	//printf("a");
	
	
	// set up interrupt
	PORTB.INT0MASK |= 0b00000100;	// sets portB's interrupt 0 to be tied to pin 2 (B2)
	
	PORTB.PIN2CTRL |= 0b01000000;	// inverts the logic on portBpin2

	PORTB.PIN2CTRL |= 0b00010000;	// enables pull-down resistor on portBpin2
	//PORTB.PIN2CTRL |= 0b00011000;	// enables pull-up resistor on portBpin2
	
	//PORTB.PIN2CTRL |= 0b00000010;	// trigger on falling-edge 
	PORTB.PIN2CTRL |= 0b00000001;	// trigger on rising-edge
	//PORTB.PIN2CTRL |= 0b00000000;	// trigger on both edges
	
	PORTB.INTCTRL  |= 0b00000001;	// enables INT0 at LOW-priority

	// note: the interrupt output is connected to portBpin2 (pin8 of MCU)
	// this can also be accessed on the distal pad of R44 (next to the op-amp)

	if(check_accel_exists() == 1)
	{
		accelerometer_existance_confirmed = 1;
		
		//printf("b");
		accel_write(MODE, 0b10000000);			   // sets interrupt output pin so that is is active when HIGH
		//printf("c");
		accel_write(INTSU, 0b11100100);			   // Enables interrupts for Shakes in X,Y,Z Axis, as well as taps
		//printf("d");
		accel_write(PDET, 0b00000001);			   // taps enables in x,y,z axis, tap threshold is +/- 1 counts
		//printf("e");
		accel_write(MODE, 0b00000001);			   // Set current mode to Active Mode (vs. Standby Mode or Test Mode)
		//printf("f");
	}

	else
		accelerometer_existance_confirmed = 0;	// redundant

		return;
	
}


// detect a shake to the block, shake is stored in the 7 bit of tilt
void shake(uint8_t tilt)
{						
	uint8_t shake = tilt & SHAKE_bm;
	if(shake == SHAKE_bm){
		printf("SHAKE\r\n");
	}
}

// detect a tap, tap is in bit 5 of tilt
void tap(uint8_t tilt)
{							
	uint8_t tap = tilt & TAP_bm;
	if(tap == TAP_bm){
		printf("TAP\r\n");
	}
}




//interrupt function for shake
ISR(PORTB_INT0_vect)
{		
	// it is currently unknown if this interrupt is working properly
	printf("SHAKE INT!!\r\n");
}
