
#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>			// used for printf_P
#include <avr/pgmspace.h>	// used for PSTR

void init_HV(void);

void go_transparent(void);

void go_opaque(void);

uint8_t is_transparent(void);

uint8_t is_opaque(void);

#ifdef __cplusplus
}
#endif 