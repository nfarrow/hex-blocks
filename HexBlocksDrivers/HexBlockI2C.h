// HexBlockI2C.h

#ifdef __cplusplus
extern "C"
{
#endif

#include <avr/io.h>
//#include <util/delay.h>	


// initialize I2C, currently only initializes PORTE for internal (on-PCB) components
// there are I2C lines on PORTC as well for external (off-PCB) components, but these are not yet implemented
void init_I2C(void); 

// returns 1 if device acknowledged, 0 o/w
uint8_t I2C_check_device_exists(uint8_t device_address);

uint8_t I2C_read(uint8_t device_address, uint8_t register_read_address); // read a register (8bit) via i2c

uint16_t I2C_read_16(uint8_t device_address, uint8_t register_read_address); //read 2 consecutive registers (16 bit) via i2c

void I2C_write(uint8_t device_address, uint8_t register_write_address, uint8_t data); // write 8bit data into registers via i2c

void check_all_of_I2C(void);

#ifdef __cplusplus
}
#endif