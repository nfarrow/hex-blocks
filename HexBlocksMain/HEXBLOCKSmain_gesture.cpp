
#if PROGMEM_SIZE > 0x010000
#define PGM_READ_BYTE pgm_read_byte_far
#else
#define PGM_READ_BYTE pgm_read_byte_near
#endif


#include "HEXBLOCKSmain_gesture.h"



#include "xgrid_maintenance.h"


//#include "touch.h"
//#include "mpr121.h"

// USART
#define USART_TX_BUF_SIZE 64
#define USART_RX_BUF_SIZE 64
char usart_txbuf[USART_TX_BUF_SIZE];
char usart_rxbuf[USART_RX_BUF_SIZE];
CREATE_USART(usart, UART_DEVICE_PORT);
//extern FILE usart_stream;
FILE usart_stream;

#define NODE_TX_BUF_SIZE 32
#define NODE_RX_BUF_SIZE 64
char usart_n0_txbuf[NODE_TX_BUF_SIZE];
char usart_n0_rxbuf[NODE_RX_BUF_SIZE];
CREATE_USART(usart_n0, USART_N0_DEVICE_PORT);
char usart_n1_txbuf[NODE_TX_BUF_SIZE];
char usart_n1_rxbuf[NODE_RX_BUF_SIZE];
CREATE_USART(usart_n1, USART_N1_DEVICE_PORT);
char usart_n2_txbuf[NODE_TX_BUF_SIZE];
char usart_n2_rxbuf[NODE_RX_BUF_SIZE];
CREATE_USART(usart_n2, USART_N2_DEVICE_PORT);
char usart_n3_txbuf[NODE_TX_BUF_SIZE];
char usart_n3_rxbuf[NODE_RX_BUF_SIZE];
CREATE_USART(usart_n3, USART_N3_DEVICE_PORT);
char usart_n4_txbuf[NODE_TX_BUF_SIZE];
char usart_n4_rxbuf[NODE_RX_BUF_SIZE];
CREATE_USART(usart_n4, USART_N4_DEVICE_PORT);
char usart_n5_txbuf[NODE_TX_BUF_SIZE];
char usart_n5_rxbuf[NODE_RX_BUF_SIZE];
CREATE_USART(usart_n5, USART_N5_DEVICE_PORT);

Xgrid xgrid;

volatile unsigned long milliseconds = 0; // Timer



void swarm_initialization();
void swarm_communication_send();
void swarm_command_handler();
void swarm_interaction(int i, int j, int nei);

void get_new_rands(int& rand1, int& rand2);

void init(void);
uint32_t prev_id=0;


#define MESSAGE_TYPE_PING 0
#define MESSAGE_TYPE_COMMAND 1
#define MESSAGE_TYPE_COLOR 2
#define MESSAGE_TYPE_QUERY 3
#define MESSAGE_TYPE_REPLY 4
#define MESSAGE_TYPE_SENSE 55
#define MESSAGE_TYPE_ORDER 66
#define MESSAGE_TYPE_VEC 77
#define MESSAGE_TYPE_TRANSPARENCY 88

#define SWARM_MODE_RANDOM 0
#define SWARM_MODE_WHITE 1
#define SWARM_MODE_UPDOWN 2			// gives an indication of where the block thinks it is (green = top, red = bottom, blue = middle)
#define SWARM_MODE_SIDE1 3
#define SWARM_MODE_SIDE2 4
#define SWARM_MODE_COLOR_DROPS 5
#define SWARM_MODE_RED 6

#define INIT_MODE SWARM_MODE_WHITE
//#define INIT_MODE SWARM_MODE_RED
//#define INIT_MODE SWARM_MODE_SIDE2

#define SWARM_POSITION_UNKNOWN			0
#define SWARM_POSITION_TOP				1
#define SWARM_POSITION_BOTTOM			2
#define SWARM_POSITION_MIDDLE			3
#define SWARM_POSITION_TOP_AND_BOTTOM	4

int i, j, t;	// moved from swarm_initialization() to global

#define PI 3.14159
#define RADtoDEG 57.29578
#define delayX 10
#define SIZEX 10
#define SIZEY 10
#define NUM_NEIGHBORS 6

#define BOTTOM		0	
#define LEFT_BOT	1
#define LEFT_TOP	2      
#define TOP			3  
#define RIGHT_TOP	4 
#define RIGHT_BOT	5     

#define BOTTOM_MASK				0b00000001 
#define LEFT_BOT_MASK			0b00000010 
#define LEFT_TOP_MASK			0b00000100 
#define TOP_MASK				0b00001000 
#define RIGHT_TOP_MASK			0b00010000 
#define RIGHT_BOT_MASK			0b00100000 

float theta;
//float my_x, my_y;


extern double euc_dist[12];

uint8_t my_red;
uint8_t my_green;
uint8_t my_blue;
extern int sense;
uint8_t my_rank;
uint8_t my_rank2;
uint8_t max_rank;

int swarm_id;
int rand1, rand2;
int swarm_mode;

	
bool command_received;
char command;
uint8_t command_source;
uint8_t message_distance;

bool print_servo_info = false;
bool update_allowed = false;
	
	uint8_t elec_array[6];
	uint8_t elec_array_first[6];
	uint8_t latter[6]; 
	

bool is_top = false;
bool is_bottom = false;
bool is_side1_top = false;
bool is_side1_bottom = false;
bool is_side2_top = false;
bool is_side2_bottom = false;

bool provisional_is_top = true;
bool provisional_is_bottom = true;
bool provisional_is_side1_top = true;
bool provisional_is_side1_bottom = true;
bool provisional_is_side2_top = true;
bool provisional_is_side2_bottom = true;

uint8_t my_swarm_position = SWARM_POSITION_UNKNOWN;

uint32_t message_count[NUM_NEIGHBORS];
uint8_t line_direction;
uint8_t send_mask;
uint8_t receive_mask;	// NOT USED

uint8_t turns_since_dropped = 0;

uint8_t test_data=0;
uint32_t count=0;
uint16_t message_timer;



/**
 * Data transmitted to neighbors
 */
struct color_struct {
	//float x, y;
	uint8_t red;
	uint8_t green;
	uint8_t blue;
};

color_struct color_msg_data;

void rx_pkt(Xgrid::Packet *pkt)
{
		//LED_PORT.OUTTGL = LED_USR_2_PIN_bm;		// toggle green light when receive packet
		//GREEN_LED_PORT.OUTTGL = GREEN_LED_PIN_bm;		// toggle green light when receive packet
		
		if((pkt->rx_node >=0)&&(pkt->rx_node < NUM_NEIGHBORS))
		{
			message_count[pkt->rx_node]++;
			
			if((pkt->rx_node == 0)&&provisional_is_bottom)
				provisional_is_bottom = false;

			if((pkt->rx_node == 1)&&provisional_is_side1_bottom)
				provisional_is_side1_bottom = false;

			if((pkt->rx_node == 2)&&provisional_is_side2_top)
				provisional_is_side2_top = false;

			if((pkt->rx_node == 3)&&provisional_is_top)
				provisional_is_top = false;

			if((pkt->rx_node == 4)&&provisional_is_side1_top)
				provisional_is_side1_top = false;

			if((pkt->rx_node == 5)&&provisional_is_side2_bottom)
				provisional_is_side2_bottom = false;
		}
		
		else
			fprintf_P(&usart_stream, PSTR("UNKNOWN packet source: %i\n\r"), pkt->rx_node);
		
		if(pkt->type == MESSAGE_TYPE_PING)
		{;}

		else if(pkt->type == MESSAGE_TYPE_COLOR)
		{
			color_struct* color_ptr = (color_struct*) pkt->data;
			//mchip.neix[pkt->source_id] = pt_ptr->x;
			//mchip.neiy[pkt->source_id] = pt_ptr->y;
			
			if(swarm_mode == SWARM_MODE_COLOR_DROPS)
			{
				// send packet to neighbor below with "my" old color
				{
					color_msg_data.red = my_red;
					color_msg_data.green = my_green;
					color_msg_data.blue = my_blue;
									
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_COLOR;
					pkt.flags = 0;
					pkt.radius = 1;
					pkt.data = (uint8_t *)&color_msg_data;
					pkt.data_len = sizeof(color_msg_data);
									
					xgrid.send_packet(&pkt, 0b00000001);
				}

				my_red = color_ptr->red;
				my_green = color_ptr->green;
				my_blue = color_ptr->blue;

			//	set_rgb(my_red, my_green, my_blue);
			}
		}
		
		else if(pkt->type == MESSAGE_TYPE_COMMAND)
		{
			char* char_ptr = (char*) pkt->data;
			command_received = true;
			command = *char_ptr;
			command_source = pkt->rx_node;
			
			fprintf_P(&usart_stream, PSTR("command source: %i "), command_source);
			switch(command_source)
			{
				case BOTTOM:		fprintf_P(&usart_stream, PSTR("0, BOTTOM\n\r"));	break;
				case LEFT_BOT:		fprintf_P(&usart_stream, PSTR("1, LEFT BOT\n\r"));	break;
				case LEFT_TOP:		fprintf_P(&usart_stream, PSTR("2, LEFT TOP\n\r"));	break;
				case TOP:		fprintf_P(&usart_stream, PSTR("3, TOP\n\r"));		break;
				case RIGHT_BOT:		fprintf_P(&usart_stream, PSTR("4, RIGHT TOP\n\r"));	break;
				case RIGHT_TOP:		fprintf_P(&usart_stream, PSTR("5, RIGHT BOT\n\r"));	break;
				default:			fprintf_P(&usart_stream, PSTR("UNKNOWN\n\r"));		break;
			}
		}

		else if(pkt->type == MESSAGE_TYPE_QUERY)
		{
			char inquiry;
			uint8_t inquiry_source;
			char* char_ptr = (char*) pkt->data;
			inquiry = *char_ptr;
			inquiry_source = pkt->rx_node;
			
			int reply_num;

			fprintf_P(&usart_stream, PSTR("inquiry received: %c "), inquiry);
			fprintf_P(&usart_stream, PSTR("inquiry source: %i "), inquiry_source);
			switch(inquiry)
			{
				case 'I':
					fprintf_P(&usart_stream, PSTR("reply: swarm_id: %i\n\r"), swarm_id);	

					//char str[] = "D";

					
					reply_num = swarm_id;
					Xgrid::Packet reply_pkt;
					reply_pkt.type = MESSAGE_TYPE_REPLY;
					reply_pkt.flags = 0;
					reply_pkt.radius = 1;
					//pkt.data = (uint8_t *)str;
					reply_pkt.data = (uint8_t *)&reply_num;
					reply_pkt.data_len = sizeof(swarm_id);	// maybe?
					
					xgrid.send_packet(&reply_pkt);

					break;

				default:
					fprintf_P(&usart_stream, PSTR("UNKNOWN\n\r"));
					break;
			}
		}
		
		else if(pkt->type == MESSAGE_TYPE_REPLY)
		{
			int reply;
			uint8_t reply_source;
			int* int_ptr = (int*) pkt->data;
			reply = *int_ptr;
			reply_source = pkt->rx_node;
			
			fprintf_P(&usart_stream, PSTR("reply received: %i\r\n"), reply);
			_delay_ms(1);
			fprintf_P(&usart_stream, PSTR("reply source: %i\r\r\n"), reply_source);
			
		}
		
		else if(pkt->type == MESSAGE_TYPE_SENSE)
		{
			sense=1;
			fprintf_P(&usart_stream, PSTR("packet cometh from %u! \n\r"),pkt->source_id);
			if(!test_data)
			{
				prev_id=pkt->source_id;
				fprintf_P(&usart_stream, PSTR("prev id: %u! \n\r"),prev_id);
			}
			if(!latter[0])
			{
				prev_id=pkt->source_id;
				fprintf_P(&usart_stream, PSTR("prev id: %u! \n\r"),prev_id);
			}			
			
		}
		else if(pkt->type == MESSAGE_TYPE_ORDER)
		{
			if(prev_id==pkt->source_id)
			{
			my_rank=*(pkt->data);
			my_rank=my_rank+1;
			if(latter[0]==0)
			{
				my_rank2=my_rank;
				my_rank=1;
			}				
			fprintf_P(&usart_stream, PSTR("rank: %i\n\r"),my_rank);
			fprintf_P(&usart_stream, PSTR("count: %x\n\r"),count);
						
			Xgrid::Packet pkt2;
			pkt2.type = MESSAGE_TYPE_ORDER;
			pkt2.flags = 0;
			pkt2.radius = 1;
			pkt2.data = (uint8_t *)&my_rank;
			pkt2.data_len = sizeof(my_rank);
			if(latter[0])
			xgrid.send_packet(&pkt2);
			}			
			
		}
		else if(pkt->type == MESSAGE_TYPE_VEC)
		{
			vector_rank* temp_vec_rank;
			temp_vec_rank=(vector_rank*)pkt->data;
			if((temp_vec_rank->rank)>max_rank)
			max_rank=temp_vec_rank->rank;
			fprintf_P(&usart_stream, PSTR("vec_rank: %i\n\r"),temp_vec_rank->rank);
			/*fprintf_P(&usart_stream, PSTR("source id: %u! \n\r"),pkt->source_id);
			fprintf_P(&usart_stream, PSTR("count: %x\n\r"),count);
			fprintf_P(&usart_stream, PSTR("vec_x: %lf\n\r"),temp_vec_rank->vec_message.vec_x);
			fprintf_P(&usart_stream, PSTR("vec_y: %lf\n\r"),temp_vec_rank->vec_message.vec_y);*/
			vec_in[temp_vec_rank->rank-1]=temp_vec_rank->vec_message;
			
		}
		else if(pkt->type == MESSAGE_TYPE_TRANSPARENCY)
		{
			bool transparency;
			transparency=*(pkt->data);
			if(transparency)
			go_transparent();
			else
			go_opaque();
			
		}


		else
		{
			fprintf_P(&usart_stream, PSTR("UNKNOWN packet type received: %i\n\r"), pkt->type);
		}
}

void swarm_initialization()
{
	RGB_LED_init();
	
	uint32_t b = 0;
	uint32_t crc = 0;
	
	// calculate local id
	// simply crc of user sig row
	// likely to be unique and constant for each chip
	NVM_CMD = NVM_CMD_READ_CALIB_ROW_gc;
	
	for (uint32_t i = 0x08; i <= 0x15; i++)
	{
			b = PGM_READ_BYTE(i);
			crc = _crc16_update(crc, b);
	}
	
	 NVM_CMD = NVM_CMD_NO_OPERATION_gc;
	
	swarm_id = crc;
	
	if(swarm_id < 0)
		swarm_id*=-1;
	
	srand(swarm_id);
	
	my_red = 0;
	my_green = 0;
	my_blue = 0;

	swarm_mode = INIT_MODE;

	if(swarm_mode == SWARM_MODE_RED){my_red = 255;}
	else if(swarm_mode == SWARM_MODE_WHITE){my_red = 255;my_green = 255;my_blue = 255;}
//	set_rgb(my_red, my_green, my_blue);

	// blindly assume my neighbors have the same value that I do
	for(t=0;t<NUM_NEIGHBORS;t++)
	{
		message_count[t]=0;
	}
	
	command_received = false;
	
	message_distance = 1;
	line_direction = 0;
}

void swarm_communication_send()
{
	
	//NEW
	color_msg_data.red = my_red;
	color_msg_data.green = my_green;
	color_msg_data.blue = my_blue;
	
	Xgrid::Packet pkt;

	pkt.type = MESSAGE_TYPE_PING;
	pkt.flags = 0;	
	pkt.radius = 1;
	pkt.data = (uint8_t *)&color_msg_data;
	pkt.data_len = sizeof(color_msg_data);
	
	xgrid.send_packet(&pkt);
}

void swarm_command_handler()
{
	if(command_received)
	{
		if(command == 'D')
		{
			swarm_mode = SWARM_MODE_COLOR_DROPS;
			fprintf_P(&usart_stream, PSTR("cmd COLOR DROPS MODE\n\r"));
		}

		if(command == 'O')
		{
			swarm_mode = SWARM_MODE_UPDOWN;
			fprintf_P(&usart_stream, PSTR("cmd UPDOWN MODE: top %i, bottom %i\n\r"), is_top, is_bottom);
		}
		
		else if(command == 'R')
		{
			swarm_mode = SWARM_MODE_RANDOM;
			fprintf_P(&usart_stream, PSTR("cmd RANDOM MODE\n\r"));
		}

		else if(command == 'W')
		{
			swarm_mode = SWARM_MODE_WHITE;
			fprintf_P(&usart_stream, PSTR("cmd WHITE MODE\n\r"));

			my_red = 255;
			my_green = 255;
			my_blue = 255;
			set_blue_led(my_red);
			set_blue_led(my_green);
			set_blue_led(my_blue);
		}

		else if(command == '1')
		{
			swarm_mode = SWARM_MODE_SIDE1;
			fprintf_P(&usart_stream, PSTR("cmd SIDE1 MODE: top %i, bottom %i\n\r"), is_side1_top, is_side1_bottom);
		}
		
		else if(command == '2')
		{
			swarm_mode = SWARM_MODE_SIDE2;
			fprintf_P(&usart_stream, PSTR("cmd SIDE2 MODE: top %i, bottom %i\n\r"), is_side2_top, is_side2_bottom);
		}
		
		else
		{
			fprintf_P(&usart_stream, PSTR("UNKNOWN command received: %c\n\r"), command);
		}

		command_received = false;
	}
}





void get_new_rands(int& rand1, int& rand2)
{
	rand1 = rand();
	rand2 = rand();
}


int main(void)
{
    uint32_t j = 0, touch;
	
	uint16_t iter;
	uint16_t j_iter;
	my_rank=0;
	my_rank2=0;
	max_rank=0;
	bool elec_array_sem=0;
	bool touch_stop=0;
	
	uint8_t* temp_latter;
	uint8_t* temp_elec_array;
	uint8_t* temp_elec_array_first;
	
	uint8_t transparency;
        
	char input_char;
	char first_char;
	
	float x=0.0001;
	
	
	
	
	
	temp_latter=latter;
	temp_elec_array=elec_array;
	
	       
    init();
	init_HV();
	go_transparent();

	
	fprintf_P(&usart_stream, PSTR("done!!!! \n \r"));
        
    xgrid.rx_pkt = &rx_pkt;

    fprintf_P(&usart_stream, PSTR("avr-xgrid build %ld\r\n"), (unsigned long) &__BUILD_NUMBER);
	
	swarm_initialization();
	init_I2C();
	init_touch();
	
	

	
	if (usart.available())
	{
		first_char = usart.get();
	}	
		
	//lcd_init(LCD_DISP_ON);
	//fprintf_P(&usart_stream, PSTR("LCD INIT\r\n"));
	//lcd_puts("LCD Test");

	fprintf_P(&usart_stream, PSTR("done \n \r"));
	while (1)
	{
		touch = touch_read(0x00);
	//	fprintf_P(&usart_stream, PSTR("transparent = %u "),touch);
		


		if(touch !=0)
		{
			//fprintf_P(&usart_stream, PSTR("done 12 \n \r"));
			test_data=1;
			Xgrid::Packet pkt1;
			pkt1.type = MESSAGE_TYPE_SENSE;
			pkt1.flags = 0;
			pkt1.radius = 1;
			pkt1.data = (uint8_t *)&test_data;
			pkt1.data_len = 1;
			xgrid.send_packet(&pkt1);
			
		
		//	fprintf_P(&usart_stream, PSTR("packet sent \n\r"));			
		}
	
		
		if(test_data)
		{
			count++;
					
		}

		if (count==0x18FF)
		{
			fprintf_P(&usart_stream, PSTR("woohoo!0 "));
			count =0;
			test_data=0;
			sense=0;
			max_rank=0;
			my_rank=0;
			my_rank2=0;
			prev_id=0;
			temp_elec_array=elec_array;
			temp_latter=latter;
			touch_stop=0;
			for(iter=0;iter<6;iter++)
			{
				elec_array[iter]=0;
				elec_array_first[iter]=0;
				latter[iter]=0;
				vec_in[2*i].vec_x=0;
				vec_in[2*i].vec_y=0;
				vec_in[(2*i)+1].vec_x=0;
				vec_in[(2*i)+1].vec_y=0;
		
			}
		}
		if (count==0x9ff)
		{
			set_blue_led(200);
			touch_stop=1;
			if(latter[0]==0)
			{
				fprintf_P(&usart_stream, PSTR("woohoo!12 "));
				
				max_rank=1;
				my_rank=1;
				Xgrid::Packet pkt2;
				pkt2.type = MESSAGE_TYPE_ORDER;
				pkt2.flags = 0;
				pkt2.radius = 1;
				pkt2.data = (uint8_t *)&my_rank;
				pkt2.data_len = sizeof(my_rank);
				xgrid.send_packet(&pkt2);
			}
	
		}
		if (count==0xbff)
		{
		/*	for(iter=0;iter<6;iter++)
			{
				fprintf_P(&usart_stream, PSTR("latter[%u] %u "),iter,latter[iter]);
				_delay_ms(10);
				fprintf_P(&usart_stream, PSTR("elec_array_first[%u] %u "),iter,elec_array_first[iter]);
				_delay_ms(10);
				fprintf_P(&usart_stream, PSTR("elec_array[%u] %u \n \r"),iter,elec_array[iter]);
				_delay_ms(10);
			}*/
			temp_elec_array_first=elec_array_first;
			if(latter[0]==0)
			{
				for(iter=0;iter<6;iter++)
				{
					if(latter[iter]==1)
					{
					*temp_elec_array_first=elec_array[iter];
					elec_array[iter]=0;
					*temp_elec_array_first++;
					}					
				}
							
			}			
			
		}
				 
		if (count==0xfff)
		{
	
			for(iter=0;iter<6;iter++)
			{
				fprintf_P(&usart_stream, PSTR("elec_array_first[%u] %u "),iter,elec_array_first[iter]);
				_delay_ms(10);
				fprintf_P(&usart_stream, PSTR("elec_array[%u] %u \n \r"),iter,elec_array[iter]);
				_delay_ms(10);
			}
			if(elec_array[0]!=0)
			{
				vector_rank temp_vec_rank;
				Xgrid::Packet pkt3;
				pkt3.type = MESSAGE_TYPE_VEC;
				temp_vec_rank.rank=my_rank;
				fprintf_P(&usart_stream, PSTR("\n \r **%u** 1\n \r"),my_rank);
				temp_vec_rank.vec_message=get_vectors(elec_array,my_rank);
				vec_in[my_rank-1]=temp_vec_rank.vec_message;
				fprintf_P(&usart_stream, PSTR("\n \r %lf %lf \n \r"),temp_vec_rank.vec_message.vec_x,temp_vec_rank.vec_message.vec_y);
				pkt3.flags = 0;
				pkt3.radius = 5;
				pkt3.data = (uint8_t *)&temp_vec_rank;
				pkt3.data_len = sizeof(temp_vec_rank);
				xgrid.send_packet(&pkt3);
			}
			if (elec_array_first[0]!=0)
			{
				vector_rank temp_vec_rank;
				Xgrid::Packet pkt3;
				pkt3.type = MESSAGE_TYPE_VEC;
				temp_vec_rank.rank=my_rank2;
				fprintf_P(&usart_stream, PSTR("\n \r **%u** 2\n \r"),my_rank2);
				temp_vec_rank.vec_message=get_vectors(elec_array_first,my_rank2);
				vec_in[my_rank2-1]=temp_vec_rank.vec_message;
				fprintf_P(&usart_stream, PSTR("\n \r %lf %lf \n \r"),temp_vec_rank.vec_message.vec_x,temp_vec_rank.vec_message.vec_y);
				pkt3.flags = 0;
				pkt3.radius = 5;
				pkt3.data = (uint8_t *)&temp_vec_rank;
				pkt3.data_len = sizeof(temp_vec_rank);
				xgrid.send_packet(&pkt3);
			}			 
		 }
 
		 if (count==0x14ff)
		 {
			 if(my_rank>max_rank)
			 max_rank=my_rank;
			 if((my_rank2>max_rank)&&(elec_array_first[0]!=0))
			 max_rank=my_rank2;
			 fprintf_P(&usart_stream, PSTR("max rank: %i\n\r"),max_rank);
			 for(iter=0;iter<max_rank;iter++)
			 {
				fprintf_P(&usart_stream, PSTR("vec_x[%d] %lf\n\r"),iter,vec_in[iter].vec_x);
				_delay_ms(40);
				fprintf_P(&usart_stream, PSTR("vec_y[%d] %lf\n\r"),iter,vec_in[iter].vec_y); 
				_delay_ms(40);
			 }
			 
			
			interpolation(max_rank,vec_in);
			fprintf_P(&usart_stream, PSTR("{"));
			for(iter=0;iter<SAMPLE_SIZE;iter++)
			{
				fprintf_P(&usart_stream, PSTR(" %lf,"),vec_out[iter].vec_x);
				_delay_ms(40);
			}
			fprintf_P(&usart_stream, PSTR("}\n\r"));
			fprintf_P(&usart_stream, PSTR("{"));
			for(iter=0;iter<SAMPLE_SIZE;iter++)
			{
				fprintf_P(&usart_stream, PSTR("%lf,"),vec_out[iter].vec_y);
				_delay_ms(40);
			}
			fprintf_P(&usart_stream, PSTR("}\n\r"));
			if ((euclidean_distance()>=0)&&(euclidean_distance()<6))
			{
				fprintf_P(&usart_stream, PSTR("C"));
				go_opaque();
				transparency=0;
				Xgrid::Packet pkt4;
				pkt4.type = MESSAGE_TYPE_TRANSPARENCY;
				pkt4.flags = 0;
				pkt4.radius = 5;
				pkt4.data = (uint8_t *)&transparency;
				pkt4.data_len = sizeof(transparency);
				xgrid.send_packet(&pkt4);
			}
			else if ((euclidean_distance()>=6)&&(euclidean_distance()<12))
			{
				fprintf_P(&usart_stream, PSTR("O")); 
				go_transparent();
				transparency=1;
				Xgrid::Packet pkt4;
				pkt4.type = MESSAGE_TYPE_TRANSPARENCY;
				pkt4.flags = 0;
				pkt4.radius = 5;
				pkt4.data = (uint8_t *)&transparency;
				pkt4.data_len = sizeof(transparency);
				xgrid.send_packet(&pkt4);
				
			}
			else if ((euclidean_distance()>=12)&&(euclidean_distance()<18))
			{
				fprintf_P(&usart_stream, PSTR("A"));
			}				
			else if ((euclidean_distance()>=18)&&(euclidean_distance()<24))
			{
				fprintf_P(&usart_stream, PSTR("J"));
			}
			else if ((euclidean_distance()>=24)&&(euclidean_distance()<30))
			{
				fprintf_P(&usart_stream, PSTR("T"));
			}
			_delay_ms(40);
			
		 }
 
	/*	uint8_t test1[6]={1,0,0,0,0,0};
		uint8_t test2[6]={4,5,0,0,0,0};
		uint8_t test3[6]={2,3,0,0,0,0};
		uint8_t test4[6]={6,0,0,0,0,0};
		vec_in[0]=get_vectors(test1,1);
		vec_in[1]=get_vectors(test2,2);
		vec_in[2]=get_vectors(test3,3);
		vec_in[3]=get_vectors(test4,4);
		 for(iter=0;iter<4;iter++)
		 {
			 fprintf_P(&usart_stream, PSTR("vec_x[%d] %lf\n\r"),iter,vec_in[iter].vec_x);
			 _delay_ms(40);
			 fprintf_P(&usart_stream, PSTR("vec_y[%d] %lf\n\r"),iter,vec_in[iter].vec_y);
			 _delay_ms(40);
		 }
		interpolation(4,vec_in);
		 for(iter=0;iter<SAMPLE_SIZE;iter++)
		 {
			 fprintf_P(&usart_stream, PSTR("vec_x[%d] %lf\n\r"),iter,vec_out[iter].vec_x);
			 _delay_ms(40);
			 fprintf_P(&usart_stream, PSTR("vec_y[%d] %lf\n\r"),iter,vec_out[iter].vec_y);
			 _delay_ms(40);
		 }
		fprintf_P(&usart_stream, PSTR("%u \n\r"),euclidean_distance()); 
		for (iter=0;iter<12;iter++)
		{
			fprintf_P(&usart_stream, PSTR("%lf \n\r"), euc_dist[iter]);
			_delay_ms(40);
		}*/
		if(!touch_stop)
		{
		
		if(touch & 0b00000001)
		{
			//				touch_toggle |= 0b01000000;
			if(sense)
			{
				set_rgb(0,255,0);
				if(elec_array_sem==0)
				{
					
					*temp_latter=1;
					*temp_elec_array=1;
					temp_latter++;
					temp_elec_array++;
					elec_array_sem=1;
				}
			}
			else
			{
				set_rgb(0,255,0);
	//			go_transparent();
				if(elec_array_sem==0)
				{
					*temp_latter=0;
					*temp_elec_array=1;
					temp_latter++;
					temp_elec_array++;
					elec_array_sem=1;
				}
			}
		}

		if(touch & 0b00000010)
		{
			//				touch_toggle |= 0b10000000;
			if(sense)
			{
				set_rgb(255,0,0);
				if(elec_array_sem==0)
				{
					*temp_latter=1;
					*temp_elec_array=2;
					temp_latter++;
					temp_elec_array++;
					elec_array_sem=1;
					fprintf_P(&usart_stream, PSTR("woohoo!2 "));
				}
			}
			else
			{
				set_rgb(255,0,0);
		//		go_opaque();
				if(elec_array_sem==0)
				{
					*temp_latter=0;
					*temp_elec_array=2;
					temp_latter++;
					temp_elec_array++;
					elec_array_sem=1;
					fprintf_P(&usart_stream, PSTR("woohoo!2 "));
				}
			}
		}


		if(touch & 0b00000100)
		{
			//				touch_toggle |= 0b00100000;
			if(sense)
			{
				set_rgb(255,0,255);
				if(elec_array_sem==0)
				{
					*temp_latter=1;
					*temp_elec_array=3;
					temp_latter++;
					temp_elec_array++;
					elec_array_sem=1;
				}
			}
			else
			{
				set_rgb(255,0,255);
				if(elec_array_sem==0)
				{
					*temp_latter=0;
					*temp_elec_array=3;
					temp_latter++;
					temp_elec_array++;
					elec_array_sem=1;
				}
			}
		}


		if(touch & 0b00001000)
		{
			//				touch_toggle |= 0b00010000;
			if(sense)
			{
				set_rgb(255,255,0);
				if(elec_array_sem==0)
				{
					
					*temp_latter=1;
					*temp_elec_array=4;
					temp_latter++;
					temp_elec_array++;
					elec_array_sem=1;
				}
			}
			else
			{
				set_rgb(255,255,0);
				if(elec_array_sem==0)
				{
					*temp_latter=0;
					*temp_elec_array=4;
					temp_latter++;
					temp_elec_array++;
					elec_array_sem=1;
				}
			}
		}

		if(touch & 0b00010000)
		{
			//				touch_toggle |= 0b00001000;
			if(sense)
			{
				set_rgb(0,255,125);
				if(elec_array_sem==0)
				{
					*temp_latter=1;
					*temp_elec_array=5;
					temp_latter++;
					temp_elec_array++;
					elec_array_sem=1;
				}
			}
			else
			{
				set_rgb(0,255,125);
				if(elec_array_sem==0)
				{
					*temp_latter=0;
					*temp_elec_array=5;
					temp_latter++;
					temp_elec_array++;
					elec_array_sem=1;
				}
			}
		}

		if(touch & 0b00100000)
		{
			//				touch_toggle |= 0b00000100;
			if(sense)
			{
				set_rgb(0,0,255);
				if(elec_array_sem==0)
				{
					*temp_latter=1;
					*temp_elec_array=6;
					temp_latter++;
					temp_elec_array++;
					elec_array_sem=1;
				}
			}
			else
			{
				set_rgb(0,0,255);
				if(elec_array_sem==0)
				{
					*temp_latter=0;
					*temp_elec_array=6;
					temp_latter++;
					temp_elec_array++;
					elec_array_sem=1;
				}
			}
		}

		if(touch==0)
		{
			set_rgb(0,0,0);
			elec_array_sem=0;

		}
		}		
		
		


//		fprintf_P(&usart_stream, PSTR("%x "),touch);
//fprintf_P(&usart_stream, PSTR("%x \n \r"),get_elec_touch(0));

/*

for(i=0;i<3;i++)
{

	if(i==0)
	{
	vec_in[i]=get_vectors(test1);

	}	
	if(i==1)
	{
	vec_in[i]=get_vectors(test2);

	}	
	if(i==2)
	{
	vec_in[i]=get_vectors(test3);

	}	
	
	
}

*/



//interpolation(i,vec_in);



  /*          j = milliseconds + 20; // from here, you have 20 ms to reach the bottom of this loop
				
			if (usart.available())
			{
				input_char = usart.get();
			}	
							
			// main loop
            if (input_char == 0x1b)
                    xboot_reset();
				
			else if(input_char != 0x00)
			{
				fprintf_P(&usart_stream, PSTR("CPU: %c\r\n"), input_char);
					
				if (input_char == 'c')
				{
					fprintf_P(&usart_stream, PSTR("message count:\r\n"));
						
					fprintf_P(&usart_stream, PSTR("0 [BOTTOM]:\t%i\r\n"),message_count[0]);
					_delay_ms(1);	// delay for printf to catch up
						
					fprintf_P(&usart_stream, PSTR("1 [LEFT BOT]:\t%i\r\n"),message_count[1]);
					_delay_ms(1);	// delay for printf to catch up
						
					fprintf_P(&usart_stream, PSTR("2 [LEFT TOP]:\t%i\r\n"),message_count[2]);
					_delay_ms(1);	// delay for printf to catch up
						
					fprintf_P(&usart_stream, PSTR("3 [TOP]:\t%i\r\n"),message_count[3]);
					_delay_ms(1);	// delay for printf to catch up
						
					fprintf_P(&usart_stream, PSTR("4 [RIGHT TOP]:\t%i\r\n"),message_count[4]);
					_delay_ms(1);	// delay for printf to catch up
						
					fprintf_P(&usart_stream, PSTR("5 [RIGHT BOT]:\t%i\r\n"),message_count[5]);
					_delay_ms(1);	// delay for printf to catch up
				}
					
				else if (input_char == 'C')
				{	
					fprintf_P(&usart_stream, PSTR("MY COLOR: %i,%i,%i\n\r"), my_red, my_green, my_blue);
				}
					
				else if (input_char == 'd')
				{
					message_distance++;
						
					if(message_distance > 7)
						message_distance = 1;
						
					fprintf_P(&usart_stream, PSTR("message distance:%i\n\r"), message_distance);
				}

				else if(input_char == 'D')
				{
					swarm_mode = SWARM_MODE_COLOR_DROPS;
					fprintf_P(&usart_stream, PSTR("send cmd COLOR DROPS MODE\n\r"));
					
					char str[] = "D";
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_COMMAND;
					pkt.flags = 0;
					pkt.radius = message_distance;
					pkt.data = (uint8_t *)str;
					pkt.data_len = 1;
					
					xgrid.send_packet(&pkt);
				}

				else if(input_char == 'h')
				{
					fprintf_P(&usart_stream, PSTR("send color down: %i,%i,%i\n\r"), my_red, my_green, my_blue);
					color_msg_data.red = my_red;
					color_msg_data.green = my_green;
					color_msg_data.blue = my_blue;
					
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_COLOR;
					pkt.flags = 0;
					pkt.radius = 1;
					pkt.data = (uint8_t *)&color_msg_data;
					pkt.data_len = sizeof(color_msg_data);
					
					xgrid.send_packet(&pkt, 0b00000001);
				}

				else if (input_char == 'i')
				{
					//fprintf_P(&usart_stream, PSTR("my info:\r\n"));
						
					fprintf_P(&usart_stream, PSTR("swarm_id:%i\r\n"),swarm_id);
				}
					
				else if (input_char == 'j')
				{
					fprintf_P(&usart_stream, PSTR("my calibration byte:\r\n"));
					fprintf_P(&usart_stream, PSTR("0:%i\r\n"),SP_ReadCalibrationByte(0));
					fprintf_P(&usart_stream, PSTR("1:%i\r\n"),SP_ReadCalibrationByte(1));
					fprintf_P(&usart_stream, PSTR("2:%i\r\n"),SP_ReadCalibrationByte(2));
					fprintf_P(&usart_stream, PSTR("3:%i\r\n"),SP_ReadCalibrationByte(3));
					fprintf_P(&usart_stream, PSTR("4:%i\r\n"),SP_ReadCalibrationByte(4));
				}
					
				else if(input_char == 'l')
				{
				/*	line_direction++;
						
					if(line_direction >= NUM_NEIGHBORS)
						line_direction = 0;
						
					fprintf_P(&usart_stream, PSTR("line direction: "));
					switch(line_direction)
					{
						case BOTTOM:	fprintf_P(&usart_stream, PSTR("0, BOTTOM\n\r"));	break;
						case LEFT_BOT:	fprintf_P(&usart_stream, PSTR("1, LEFT BOT\n\r"));	break;
						case LEFT_TOP:		fprintf_P(&usart_stream, PSTR("2, LEFT TOP\n\r"));		break;
						case TOP:	fprintf_P(&usart_stream, PSTR("3, TOP\n\r"));	break;
						case RIGHT_TOP:	fprintf_P(&usart_stream, PSTR("4, RIGHT TOP\n\r"));	break;
						case RIGHT_BOT:		fprintf_P(&usart_stream, PSTR("5, RIGHT BOT\n\r"));	break;
						default:			fprintf_P(&usart_stream, PSTR("UNKNOWN\n\r"));		break;
					}*/

/*

				}
					
				else if(input_char == 'm')
				{
					#define SWARM_MODE_RANDOM 0
					#define SWARM_MODE_WHITE 1
					#define SWARM_MODE_UPDOWN 2			// gives an indication of where the block thinks it is (green = top, red = bottom, blue = middle)
					#define SWARM_MODE_SIDE1 3
					#define SWARM_MODE_SIDE2 4
					#define SWARM_MODE_COLOR_DROPS 5
					fprintf_P(&usart_stream, PSTR("SWARM MODE: %i\r\n"), swarm_mode);
					fprintf_P(&usart_stream, PSTR("0=RAND\r\n"), swarm_mode);
					fprintf_P(&usart_stream, PSTR("1=WHITE\r\n"), swarm_mode);
					_delay_ms(1);
					fprintf_P(&usart_stream, PSTR("2=UPDOWN\r\n"), swarm_mode);
					fprintf_P(&usart_stream, PSTR("3=SIDE1\r\n"), swarm_mode);
					fprintf_P(&usart_stream, PSTR("4=SIDE2\r\n"), swarm_mode);
					fprintf_P(&usart_stream, PSTR("5=COL DROP\r\n"), swarm_mode);
				}
				
				else if(input_char == 'n')
				{	
					char str[] = "I";
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_QUERY;
					pkt.flags = 0;
					pkt.radius = 1;
					pkt.data = (uint8_t *)str;
					pkt.data_len = 1;
					
					xgrid.send_packet(&pkt);
				}
					
				else if(input_char == 'p')
				{
					fprintf_P(&usart_stream, PSTR("provisional position\n\r"));
					fprintf_P(&usart_stream, PSTR("bottom: %u\n\r"), provisional_is_bottom);
					_delay_ms(1);
					fprintf_P(&usart_stream, PSTR("left bot: %u\n\r"), provisional_is_side1_bottom);
					fprintf_P(&usart_stream, PSTR("left top: %u\n\r"), provisional_is_side2_top);
					fprintf_P(&usart_stream, PSTR("top: %u\n\r"), provisional_is_top);
					_delay_ms(1);
					fprintf_P(&usart_stream, PSTR("right top: %u\n\r"), provisional_is_side1_top);
					fprintf_P(&usart_stream, PSTR("right bot: %u\n\r"), provisional_is_side2_bottom);
				}

				else if(input_char == 'P')
				{
					fprintf_P(&usart_stream, PSTR("known position\n\r"));
					fprintf_P(&usart_stream, PSTR("bottom: %u\n\r"), is_bottom);
					_delay_ms(1);
					fprintf_P(&usart_stream, PSTR("left bot: %u\n\r"), is_side1_bottom);
					fprintf_P(&usart_stream, PSTR("left top: %u\n\r"), is_side2_top);
					fprintf_P(&usart_stream, PSTR("top: %u\n\r"), is_top);
					_delay_ms(1);
					fprintf_P(&usart_stream, PSTR("right top: %u\n\r"), is_side1_top);
					fprintf_P(&usart_stream, PSTR("right bot: %u\n\r"), is_side2_bottom);
				}

				else if(input_char == 'r')
				{
					if(swarm_mode == SWARM_MODE_RANDOM)
					{
						fprintf_P(&usart_stream, PSTR("RANDOM MODE\n\r"));
					}
						
					else
					{
						swarm_mode = SWARM_MODE_RANDOM;
						fprintf_P(&usart_stream, PSTR("now RANDOM MODE\n\r"));
					}
				}

				else if(input_char == 'R')
				{
					swarm_mode = SWARM_MODE_RANDOM;
					fprintf_P(&usart_stream, PSTR("send cmd RANDOM MODE\n\r"));
					
					char str[] = "R";
                    Xgrid::Packet pkt;
                    pkt.type = MESSAGE_TYPE_COMMAND;
                    pkt.flags = 0;
                    pkt.radius = message_distance;
                    pkt.data = (uint8_t *)str;
                    pkt.data_len = 1;
                        
                    xgrid.send_packet(&pkt);			
				}

				else if(input_char == 't')
				{
					fprintf_P(&usart_stream, PSTR("two's complement tutorial\r\n"));

					for(uint16_t i = 0; i <= 255; i++)
					{
						fprintf_P(&usart_stream, PSTR("%i : %u\r\n"), (int8_t)i, i);
						_delay_ms(1);
					}

				}

				else if(input_char == 'T')
				{
					fprintf_P(&usart_stream, PSTR("time: %i (ms)\r\n"), milliseconds);
				}

				else if(input_char == 'u')
				{
					if(update_allowed)
						update_allowed = false;
					else
						update_allowed = true;
							
					if(update_allowed)
						fprintf_P(&usart_stream, PSTR("update allowed"));
					else
						fprintf_P(&usart_stream, PSTR("update NOT allowed"));
				}
					
				else if(input_char == 'v')
				{
					fprintf_P(&usart_stream, PSTR("avr-xgrid build %ld\r\n"), (unsigned long) &__BUILD_NUMBER);
				}
				
				else if(input_char == 'W')
				{
					swarm_mode = SWARM_MODE_WHITE;
					fprintf_P(&usart_stream, PSTR("send cmd WHITE MODE\n\r"));
					
					my_red = 255;
					my_green = 255;
					my_blue = 255;
					set_blue_led(my_red);
					set_blue_led(my_green);
					set_blue_led(my_blue);

					char str[] = "W";
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_COMMAND;
					pkt.flags = 0;
					pkt.radius = message_distance;
					pkt.data = (uint8_t *)str;
					pkt.data_len = 1;
					
					xgrid.send_packet(&pkt);
				}

				else if(input_char == '1')
				{
					swarm_mode = SWARM_MODE_SIDE1;
					fprintf_P(&usart_stream, PSTR("send cmd SIDE1 MODE: top %i, bottom %i\n\r"), is_side1_top, is_side1_bottom);
					
					char str[] = "1";
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_COMMAND;
					pkt.flags = 0;
					pkt.radius = message_distance;
					pkt.data = (uint8_t *)str;
					pkt.data_len = 1;
					
					xgrid.send_packet(&pkt);
				}

				else if(input_char == '2')
				{
					swarm_mode = SWARM_MODE_SIDE2;
					fprintf_P(&usart_stream, PSTR("send cmd SIDE2 MODE: top %i, bottom %i\n\r"), is_side2_top, is_side2_bottom);
					
					char str[] = "2";
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_COMMAND;
					pkt.flags = 0;
					pkt.radius = message_distance;
					pkt.data = (uint8_t *)str;
					pkt.data_len = 1;
					
					xgrid.send_packet(&pkt);
				}

				else if(input_char == '3')
				{
					swarm_mode = SWARM_MODE_UPDOWN;
					fprintf_P(&usart_stream, PSTR("send cmd UPDOWN MODE: top %i, bottom %i\n\r"), is_top, is_bottom);
					
					char str[] = "O";
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_COMMAND;
					pkt.flags = 0;
					pkt.radius = message_distance;
					pkt.data = (uint8_t *)str;
					pkt.data_len = 1;
					
					xgrid.send_packet(&pkt);
				}

				input_char = 0x00;	// clear the most recent computer input
			}
				
	/*		if(milliseconds > j)	// if TRUE, then we took too long to get here, halt program
			{
				cli();	//disable_interrupts
				//LED_PORT.OUTSET &= ~LED_USR_2_PIN_bm;	// turn green light off and keep it off
				//LED_PORT.OUTSET = LED_USR_0_PIN_bm;		// turn red light on and keep it on
				//LED_PORT.OUTSET = LED_USR_1_PIN_bm;		// turn yellow light on and keep it on
					
				set_red_led(255);
				set_green_led(0);
				set_blue_led(255);

				//GREEN_LED_PORT.OUTSET = GREEN_LED_PIN_bm;
				//RED_LED_PORT.OUTCLR = RED_LED_PIN_bm;
				//BLUE_LED_PORT.OUTSET = BLUE_LED_PIN_bm;
					
				while(1==1)
				{};
			}					
				
            while (j > milliseconds) { };*/
    }  
}

// Init everything
void init(void)
{
        // clock
        OSC.CTRL |= OSC_RC32MEN_bm; // turn on 32 MHz oscillator
        while (!(OSC.STATUS & OSC_RC32MRDY_bm)) { }; // wait for it to start
        CCP = CCP_IOREG_gc;
        CLK.CTRL = CLK_SCLKSEL_RC32M_gc; // switch osc
        DFLLRC32M.CTRL = DFLL_ENABLE_bm; // turn on DFLL
        
        // disable JTAG
        CCP = CCP_IOREG_gc;
        MCU.MCUCR = 1;
        
        // Init pins
        //LED_PORT.OUTCLR = LED_USR_0_PIN_bm | LED_USR_1_PIN_bm | LED_USR_2_PIN_bm;
        //LED_PORT.DIRSET = LED_USR_0_PIN_bm | LED_USR_1_PIN_bm | LED_USR_2_PIN_bm;
        /*
		RED_LED_PORT.OUTCLR = RED_LED_PIN_bm;
		RED_LED_PORT.DIRSET = RED_LED_PIN_bm;
		GREEN_LED_PORT.OUTCLR = GREEN_LED_PIN_bm;
		GREEN_LED_PORT.DIRSET = GREEN_LED_PIN_bm;
		BLUE_LED_PORT.OUTCLR = BLUE_LED_PIN_bm;
		BLUE_LED_PORT.DIRSET = BLUE_LED_PIN_bm;
		*/

        // Init buttons
        BTN_PORT.DIRCLR = BTN_PIN_bm;
        
        // UARTs
        usart.set_tx_buffer(usart_txbuf, USART_TX_BUF_SIZE);
        usart.set_rx_buffer(usart_rxbuf, USART_RX_BUF_SIZE);
        usart.begin(UART_BAUD_RATE);
        usart.setup_stream(&usart_stream);
        
        usart_n0.set_tx_buffer(usart_n0_txbuf, NODE_TX_BUF_SIZE);
        usart_n0.set_rx_buffer(usart_n0_rxbuf, NODE_RX_BUF_SIZE);
        usart_n0.begin(NODE_BAUD_RATE);
        xgrid.add_node(&usart_n0);
        
		usart_n1.set_tx_buffer(usart_n1_txbuf, NODE_TX_BUF_SIZE);
        usart_n1.set_rx_buffer(usart_n1_rxbuf, NODE_RX_BUF_SIZE);
        usart_n1.begin(NODE_BAUD_RATE);
        xgrid.add_node(&usart_n1);
        
		usart_n2.set_tx_buffer(usart_n2_txbuf, NODE_TX_BUF_SIZE);
        usart_n2.set_rx_buffer(usart_n2_rxbuf, NODE_RX_BUF_SIZE);
        usart_n2.begin(NODE_BAUD_RATE);
        xgrid.add_node(&usart_n2);
        
		usart_n3.set_tx_buffer(usart_n3_txbuf, NODE_TX_BUF_SIZE);
        usart_n3.set_rx_buffer(usart_n3_rxbuf, NODE_RX_BUF_SIZE);
        usart_n3.begin(NODE_BAUD_RATE);
        xgrid.add_node(&usart_n3);
        
		usart_n4.set_tx_buffer(usart_n4_txbuf, NODE_TX_BUF_SIZE);
        usart_n4.set_rx_buffer(usart_n4_rxbuf, NODE_RX_BUF_SIZE);
        usart_n4.begin(NODE_BAUD_RATE);
        xgrid.add_node(&usart_n4);
        
		usart_n5.set_tx_buffer(usart_n5_txbuf, NODE_TX_BUF_SIZE);
        usart_n5.set_rx_buffer(usart_n5_rxbuf, NODE_RX_BUF_SIZE);
        usart_n5.begin(NODE_BAUD_RATE);
        xgrid.add_node(&usart_n5);

        // TCC
        TCC0.CTRLA = TC_CLKSEL_DIV256_gc;
        TCC0.CTRLB = 0;
        TCC0.CTRLC = 0;
        TCC0.CTRLD = 0;
        TCC0.CTRLE = 0;
        TCC0.INTCTRLA = TC_OVFINTLVL_LO_gc;
        TCC0.INTCTRLB = 0;
        TCC0.CNT = 0;
        TCC0.PER = 125;
        
        // Interrupts
        PMIC.CTRL = PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm;
        
        sei();
}


// Timer tick ISR (1 kHz)
ISR(TCC0_OVF_vect)
{
	// Timers
	milliseconds++;
	
	if (milliseconds % 50 == 0)
	{
		if(swarm_mode == SWARM_MODE_COLOR_DROPS)
		{
			
		}
	}
	
	if (milliseconds % 100 == 0)
	{

	}
	
	if (milliseconds % 200 == 0)
	{
		
		swarm_command_handler();		// looks for new commands
			
		int rand1, rand2;

		get_new_rands(rand1, rand2);

		if(swarm_mode == SWARM_MODE_RANDOM)
		{
			uint8_t color_diff = 10;
			if(rand2%3 == 0)
			{
				if((rand1%2==0)&&(my_blue > color_diff))
					my_blue-=color_diff;
				else if(my_blue < 255-color_diff)
					my_blue+=color_diff;
				set_blue_led(my_blue);
			}
				
			else if(rand2%3 == 1)
			{
				if((rand1%2==0)&&(my_green > color_diff))
					my_green-=color_diff;
				else if(my_green < 255-color_diff)
					my_green+=color_diff;
				set_green_led(my_green);
			}
				
			else if(rand2%3 == 2)
			{
				if((rand1%2==0)&&(my_red > color_diff))
					my_red-=color_diff;
				else if(my_red < 255-color_diff)
					my_red+=color_diff;
				set_red_led(my_red);
			}
		}
	}
	
	if (milliseconds % 250 == 0)	// was 500
	{
		//swarm_communication_send();
		//swarm_calculation();		// looks for new commands
		//servo_motor_control();

		if((swarm_mode == SWARM_MODE_COLOR_DROPS)&&is_top)
		{
			//if(turns_since_dropped > 10)
			//{
				if((my_red == 0)&&(my_green == 0)&&(my_blue == 0))
				{
					if(rand()%18 == 0)
						my_blue = 255;
					if(rand()%18 == 0)
						my_green = 255;
					if(rand()%18 == 0)
						my_red = 255;

					else
					{
						fprintf_P(&usart_stream, PSTR("send color down: %i,%i,%i\n\r"), my_red, my_green, my_blue);
						color_msg_data.red = my_red;
						color_msg_data.green = my_green;
						color_msg_data.blue = my_blue;
						
						Xgrid::Packet pkt;
						pkt.type = MESSAGE_TYPE_COLOR;
						pkt.flags = 0;
						pkt.radius = 1;
						pkt.data = (uint8_t *)&color_msg_data;
						pkt.data_len = sizeof(color_msg_data);
						
						xgrid.send_packet(&pkt, 0b00000001);
					}
				
				}

				else
				{
					fprintf_P(&usart_stream, PSTR("send color down: %i,%i,%i\n\r"), my_red, my_green, my_blue);
					color_msg_data.red = my_red;
					color_msg_data.green = my_green;
					color_msg_data.blue = my_blue;
				
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_COLOR;
					pkt.flags = 0;
					pkt.radius = 1;
					pkt.data = (uint8_t *)&color_msg_data;
					pkt.data_len = sizeof(color_msg_data);
				
					xgrid.send_packet(&pkt, 0b00000001);

					if(my_red > 99)
						my_red-=100;
					else
						my_red = 0;

					if(my_green > 99)
						my_green-=100;
					else
						my_green = 0;

					if(my_blue > 99)
						my_blue-=100;
					else
						my_blue = 0;
				}

			//	set_rgb(my_red, my_green, my_blue);

		}

		else
			swarm_communication_send();

	}

	if (milliseconds % 2000 == 0)	// swap provisional 
	{
		is_top = provisional_is_top;
		is_bottom = provisional_is_bottom;
		is_side1_top = provisional_is_side1_top;
		is_side1_bottom = provisional_is_side1_bottom;
		is_side2_top = provisional_is_side2_top;
		is_side2_bottom = provisional_is_side2_bottom;

		// color gives an indication of where the block thinks it is (green = top, red = bottom, blue = middle)
		
		if((swarm_mode == SWARM_MODE_UPDOWN)&&((is_top)&&(!is_bottom)))
		{
			my_red = 0;
			my_green = 255;
			my_blue = 0;
		}

		if((swarm_mode == SWARM_MODE_UPDOWN)&&((!is_top)&&(is_bottom)))
		{
			
			my_red = 255;
			my_green = 0;
			my_blue = 0;
		}

		if((swarm_mode == SWARM_MODE_UPDOWN)&&((is_top)&&(is_bottom)))
		{
			my_red = 255;
			my_green = 255;
			my_blue = 0;
		}

		if((swarm_mode == SWARM_MODE_UPDOWN)&&((!is_top)&&(!is_bottom)))
		{
			my_red = 0;
			my_green = 0;
			my_blue = 255;
		}

		if((swarm_mode == SWARM_MODE_SIDE1)&&((is_side1_top)&&(!is_side1_bottom)))
		{
			my_red = 0;
			my_green = 255;
			my_blue = 0;
		}

		if((swarm_mode == SWARM_MODE_SIDE1)&&((!is_side1_top)&&(is_side1_bottom)))
		{
			
			my_red = 255;
			my_green = 0;
			my_blue = 0;
		}

		if((swarm_mode == SWARM_MODE_SIDE1)&&((is_side1_top)&&(is_side1_bottom)))
		{
			my_red = 255;
			my_green = 255;
			my_blue = 0;
		}

		if((swarm_mode == SWARM_MODE_SIDE1)&&((!is_side1_top)&&(!is_side1_bottom)))
		{
			my_red = 0;
			my_green = 0;
			my_blue = 255;
		}

		if((swarm_mode == SWARM_MODE_SIDE2)&&((is_side2_top)&&(!is_side2_bottom)))
		{
			my_red = 0;
			my_green = 255;
			my_blue = 0;
		}

		if((swarm_mode == SWARM_MODE_SIDE2)&&((!is_side2_top)&&(is_side2_bottom)))
		{
			my_red = 255;
			my_green = 0;
			my_blue = 0;
		}

		if((swarm_mode == SWARM_MODE_SIDE2)&&((is_side2_top)&&(is_side2_bottom)))
		{
			my_red = 255;
			my_green = 255;
			my_blue = 0;
		}

		if((swarm_mode == SWARM_MODE_SIDE2)&&((!is_side2_top)&&(!is_side2_bottom)))
		{
			my_red = 0;
			my_green = 0;
			my_blue = 255;
		}

	//	set_red_led(my_red);
	//	set_green_led(my_green);
	//	set_blue_led(my_blue);

		provisional_is_bottom = true;	// assume you are top/bottom until proven otherwise
		provisional_is_top = true;
		provisional_is_side1_top = true;
		provisional_is_side1_bottom = true;
		provisional_is_side2_top = true;
		provisional_is_side2_bottom = true;
	}
	
	xgrid.process();
}
