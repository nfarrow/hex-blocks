
#ifndef __MAIN_H
#define __MAIN_H

#include <math.h>
#include <avr/io.h>
//#include <avr/pgmspace.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include <stdio.h>
#include <stdlib.h> // used for rand()

//#include "board.h"	// this is generic board not hex-blocks boards
#include "HEXBLOCKSboard.h"	// this is generic board not hex-blocks boards
#include "usart.h"
//#include "spi.h"
//#include "i2c.h"
#include "eeprom.h"
#include "xgrid.h"
//#include "servo.h"
#include "../xboot/xbootapi.h"
#include "../HexBlocksSource/accelerometer.h"
#include "../HexBlocksSource/clock.h"
#include "../HexBlocksSource/lcd.h"
#include "../HexBlocksSource/touch.h"
#include "../HexBlocksSource/high_voltage.h"
#include "../HexBlocksSource/RGB_LED.h"
//#include "../HexBlockSource/lcd.h"
//#include "RGB_LED.h"

// Prototypes
void init(void);
int main(void);
uint8_t SP_ReadCalibrationByte( uint8_t index );
uint8_t SP_ReadUserSigRow( uint8_t index );

#ifndef ADCACAL0_offset

#define ADCACAL0_offset 0x20
#define ADCACAL1_offset 0x21
#define ADCBCAL0_offset 0x24
#define ADCBCAL1_offset 0x25

#endif

#endif // __MAIN_H



