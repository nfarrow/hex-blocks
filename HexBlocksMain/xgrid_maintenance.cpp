// xgrid_maintenance.cpp

#include "xgrid_maintenance.h"

// Build information
extern char   __BUILD_DATE;
extern char   __BUILD_NUMBER;

uint16_t swarm_id;

uint16_t get_swarm_id(void)
{
	return swarm_id;
}

void print_swarm_id(void)
{
	// TODO: identify if either of these print statements is more 'efficient
	// memory-wise (compiles into fewer lines), and switch all prints program-wide
	// to be the better call.	
	//fprintf_P(&usart_stream, PSTR("swarm_id: %04X\r\n"),swarm_id);
	printf_P(PSTR("swarm_id: %04X\r\n"),swarm_id);	
}

void calculate_swarm_id()
{
	// note: swarm_id is uint16_t and is global
	uint32_t b = 0;
	uint32_t crc = 0;
	
	// calculate local id
	// simply crc of user sig row
	// likely to be unique and constant for each chip
	NVM_CMD = NVM_CMD_READ_CALIB_ROW_gc;
	
	for (uint32_t i = 0x08; i <= 0x15; i++)
	{
		b = pgm_read_byte_far(i);
		//b = PGM_READ_BYTE(i);
		crc = _crc16_update(crc, b);
	}
	
	NVM_CMD = NVM_CMD_NO_OPERATION_gc;
	
	swarm_id = crc;
	
	if(swarm_id < 0)
	swarm_id*=-1;
}

void print_calibration_bytes(uint8_t begin_byte, uint8_t end_byte)
{
	uint8_t i;
	printf_P(PSTR("calibration bytes\r\n"));
	
	for(i = begin_byte; i <= end_byte; i++)
	{
		printf_P(PSTR("%u:%i\r\n"), i, SP_ReadCalibrationByte(i));
	}
}

void halt_program(uint8_t red, uint8_t green, uint8_t blue)
{
	_delay_ms(5); // wrap up any print statements that are ongoing
	
	set_red_led(red);
	set_green_led(green);
	set_blue_led(blue);
	
	printf_P(PSTR("HALTED, must reboot, debug your program\r\n"));
	
	cli();	//disable_interrupts
	
	while(1==1)
	{/* infinite fail loop */};
}

void complain_no_halt()
{
	_delay_ms(5); // wrap up any print statements that are ongoing
	
	printf_P(PSTR("no-HALT, speed things up next time!\r\n"));	
}

void print_uint8_binary(uint8_t byte)
{
	// TODO: implement
	// targeted uses: accelerometer (and more)
}


// Production signature row access
uint8_t SP_ReadCalibrationByte( uint8_t index )
{
	uint8_t result;
	NVM_CMD = NVM_CMD_READ_CALIB_ROW_gc;
	result = pgm_read_byte(index);
	NVM_CMD = NVM_CMD_NO_OPERATION_gc;
	return result;
}

// User signature row access
uint8_t SP_ReadUserSigRow( uint8_t index )
{
	uint8_t result;
	NVM_CMD = NVM_CMD_READ_USER_SIG_ROW_gc;
	result = pgm_read_byte(index);
	NVM_CMD = NVM_CMD_NO_OPERATION_gc;
	return result;
}
