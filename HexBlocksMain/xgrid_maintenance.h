
#include <avr/io.h>			// for uint8_t
#include <avr/pgmspace.h>	// for pgm_read_byte
#include <avr/interrupt.h>	// for cli
#include <stdio.h>			// for printf_P
#include <util/delay.h>		// for crash loop
#include <util/crc16.h>		// for crc_update

#include "../HexBlocksDrivers/RGB_LED.h"	// to access the LED at low level


uint16_t get_swarm_id(void);

void print_swarm_id(void);

void calculate_swarm_id(void);

void print_calibration_bytes(uint8_t begin_byte, uint8_t end_byte);

void halt_program(uint8_t red, uint8_t green, uint8_t blue);

void complain_no_halt(void);

uint8_t SP_ReadCalibrationByte( uint8_t index );

uint8_t SP_ReadUserSigRow( uint8_t index );

