
#ifndef __MAIN_H
#define __MAIN_H

#include <math.h>
#include <avr/io.h>
//#include <avr/pgmspace.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include <stdio.h>
//#include <stdlib.h> // used for rand()


#include "HEXBLOCKSboard.h"	
#include "xgrid_usart.h"
//#include "eeprom.h"
#include "xgrid.h"

#include "../xboot/xbootapi.h"

#include "../HexBlocksDrivers/accelerometer.h"
#include "../HexBlocksDrivers/HexBlockI2C.h"
#include "../HexBlocksDrivers/high_voltage.h"
#include "../HexBlocksDrivers/lcd.h"
#include "../HexBlocksDrivers/RGB_LED.h"
#include "../HexBlocksDrivers/touch.h"
#include "../HexBlocksDrivers/MP3_player.h"
#include "../HexBlocksDrivers/HB_GBC.h"

#include "../HexBlocksSource/touch_handler.h"
#include "../HexBlocksSource/appearance_control.h"
#include "../HexBlocksSource/lcd_handler.h"
#include "../HexBlocksSource/position_control.h"
#include "../HexBlocksSource/communication_handler.h"
#include "../HexBlocksSource/mode_control.h"
#include "../HexBlocksSource/work_stack.h"
#include "../HexBlocksSource/time_manager.h"
#include "../HexBlocksSource/serial_handler.h"



// Prototypes
void init(void);
int main(void);
uint8_t SP_ReadCalibrationByte( uint8_t index );
uint8_t SP_ReadUserSigRow( uint8_t index );

#ifndef ADCACAL0_offset

#define ADCACAL0_offset 0x20
#define ADCACAL1_offset 0x21
#define ADCBCAL0_offset 0x24
#define ADCBCAL1_offset 0x25

#endif

#endif // __MAIN_H



