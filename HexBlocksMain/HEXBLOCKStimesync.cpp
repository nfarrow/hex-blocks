
#if PROGMEM_SIZE > 0x010000
#define PGM_READ_BYTE pgm_read_byte_far
#else
#define PGM_READ_BYTE pgm_read_byte_near
#endif


#include "HEXBLOCKStimesync.h"


// USART
#define USART_TX_BUF_SIZE 64
#define USART_RX_BUF_SIZE 64
char usart_txbuf[USART_TX_BUF_SIZE];
char usart_rxbuf[USART_RX_BUF_SIZE];
CREATE_USART(usart, UART_DEVICE_PORT);
FILE usart_stream;

#define NODE_TX_BUF_SIZE 32
#define NODE_RX_BUF_SIZE 64
char usart_n0_txbuf[NODE_TX_BUF_SIZE];
char usart_n0_rxbuf[NODE_RX_BUF_SIZE];
CREATE_USART(usart_n0, USART_N0_DEVICE_PORT);
char usart_n1_txbuf[NODE_TX_BUF_SIZE];
char usart_n1_rxbuf[NODE_RX_BUF_SIZE];
CREATE_USART(usart_n1, USART_N1_DEVICE_PORT);
char usart_n2_txbuf[NODE_TX_BUF_SIZE];
char usart_n2_rxbuf[NODE_RX_BUF_SIZE];
CREATE_USART(usart_n2, USART_N2_DEVICE_PORT);
char usart_n3_txbuf[NODE_TX_BUF_SIZE];
char usart_n3_rxbuf[NODE_RX_BUF_SIZE];
CREATE_USART(usart_n3, USART_N3_DEVICE_PORT);
char usart_n4_txbuf[NODE_TX_BUF_SIZE];
char usart_n4_rxbuf[NODE_RX_BUF_SIZE];
CREATE_USART(usart_n4, USART_N4_DEVICE_PORT);
char usart_n5_txbuf[NODE_TX_BUF_SIZE];
char usart_n5_rxbuf[NODE_RX_BUF_SIZE];
CREATE_USART(usart_n5, USART_N5_DEVICE_PORT);

Xgrid xgrid;

volatile unsigned long milliseconds = 0; // Timer



void swarm_initialization();
void swarm_communication_send();
void swarm_command_handler();
void swarm_interaction(int i, int j, int nei);

void get_new_rands(int& rand1, int& rand2);

void init(void);


#define MESSAGE_TYPE_PING 0
#define MESSAGE_TYPE_COMMAND 1
#define MESSAGE_TYPE_COLOR 2
#define MESSAGE_TYPE_QUERY 3
#define MESSAGE_TYPE_REPLY 4
#define MESSAGE_TYPE_COOR_ASSIGN 5
#define MESSAGE_TYPE_TIME 6
#define MESSAGE_TYPE_GESTURE_DATA 7

#define SWARM_MODE_RANDOM 0
#define SWARM_MODE_WHITE 1
#define SWARM_MODE_UPDOWN 2			// gives an indication of where the block thinks it is (green = top, red = bottom, blue = middle)
#define SWARM_MODE_SIDE1 3
#define SWARM_MODE_SIDE2 4
#define SWARM_MODE_COLOR_DROPS 5
#define SWARM_MODE_RED 6
#define SWARM_MODE_X_GRADIENT 7 // Grid Test
#define SWARM_MODE_Y_GRADIENT 8 // Grid Test

//#define INIT_MODE SWARM_MODE_WHITE
#define INIT_MODE SWARM_MODE_RED
//#define INIT_MODE SWARM_MODE_SIDE2

#define SWARM_POSITION_UNKNOWN			0
#define SWARM_POSITION_TOP				1
#define SWARM_POSITION_BOTTOM			2
#define SWARM_POSITION_MIDDLE			3
#define SWARM_POSITION_TOP_AND_BOTTOM	4

int i, j, t;	// moved from swarm_initialization() to global

#define PI 3.14159
#define RADtoDEG 57.29578
#define delayX 10
#define SIZEX 10
#define SIZEY 10
#define NUM_NEIGHBORS 6

#define BOTTOM		0	
#define LEFT_BOT	1
#define LEFT_TOP	2      
#define TOP			3  
#define RIGHT_TOP	4 
#define RIGHT_BOT	5     

// looking from the back of the block: 
#define BOTTOM_MASK				0b00000001 
#define LEFT_BOT_MASK			0b00000010 
#define LEFT_TOP_MASK			0b00000100 
#define TOP_MASK				0b00001000 
#define RIGHT_TOP_MASK			0b00010000 
#define RIGHT_BOT_MASK			0b00100000 

float theta;
//float my_x, my_y;

uint8_t my_red;
uint8_t my_green;
uint8_t my_blue;

uint16_t swarm_id;
int rand1, rand2;
int swarm_mode;

bool command_received;
char command;
uint8_t command_source;
uint8_t message_distance;

bool print_servo_info = false;
bool update_allowed = false;

bool is_top = false;
bool is_bottom = false;
bool is_side1_top = false;
bool is_side1_bottom = false;
bool is_side2_top = false;
bool is_side2_bottom = false;

bool provisional_is_top = true;
bool provisional_is_bottom = true;
bool provisional_is_side1_top = true;
bool provisional_is_side1_bottom = true;
bool provisional_is_side2_top = true;
bool provisional_is_side2_bottom = true;

uint8_t my_swarm_position = SWARM_POSITION_UNKNOWN;

uint32_t message_count[NUM_NEIGHBORS];
uint8_t line_direction;
uint8_t send_mask;
uint8_t receive_mask;	// NOT USED

uint8_t turns_since_dropped = 0;

CLOCK clock;

/**
 * Data transmitted to neighbors
 */
struct color_struct {
	//float x, y;
	uint8_t red;
	uint8_t green;
	uint8_t blue;
};

struct coordinates {
	 float x_coor; // Coordinates on grid for center and touch plates
 float y_coor;
bool assigned;

 float touchbot_x ;
 float touchbot_y ;

 float touchlb_x;
 float touchlb_y ;

 float touchlt_x ;
 float touchlt_y;

 float touchtop_x ;
 float touchtop_y ;

 float touchrt_x ;
 float touchrt_y ;

 float touchrb_x ;
 float touchrb_y ;

bool touch_assigned;
};

struct assign_coordinates{
	 uint16_t swarm_id;
	 float x_coor; // Coordinates on grid for center and touch plates
	 float y_coor;
	bool assigned;
	};


struct time{
	uint8_t ms;
	uint8_t sec;
	uint8_t min;
	uint8_t hr;
	float distance;
};

struct gesture_data{
	float x_coor;
	float y_coor;
	uint32_t time_stamp;
	};
	
gesture_data sensor_array[30];
gesture_data* sensor_array_temp;
gesture_data temp_gest_data;


time prev_sync;
color_struct color_msg_data;
coordinates coor;
assign_coordinates sent_coor;

uint8_t touchstatus;
bool time_set = false;

void rx_pkt(Xgrid::Packet *pkt)
{
		//LED_PORT.OUTTGL = LED_USR_2_PIN_bm;		// toggle green light when receive packet
		//GREEN_LED_PORT.OUTTGL = GREEN_LED_PIN_bm;		// toggle green light when receive packet
		
		if((pkt->rx_node >=0)&&(pkt->rx_node < NUM_NEIGHBORS))
		{
			message_count[pkt->rx_node]++;
			
			if((pkt->rx_node == 0)&&provisional_is_bottom)
				provisional_is_bottom = false;

			if((pkt->rx_node == 1)&&provisional_is_side1_bottom)
				provisional_is_side1_bottom = false;

			if((pkt->rx_node == 2)&&provisional_is_side2_top)
				provisional_is_side2_top = false;

			if((pkt->rx_node == 3)&&provisional_is_top)
				provisional_is_top = false;

			if((pkt->rx_node == 4)&&provisional_is_side1_top)
				provisional_is_side1_top = false;

			if((pkt->rx_node == 5)&&provisional_is_side2_bottom)
				provisional_is_side2_bottom = false;
		}
		
		else
			fprintf_P(&usart_stream, PSTR("UNKNOWN packet source: %i\n\r"), pkt->rx_node);
		
		if(pkt->type == MESSAGE_TYPE_PING)
		{
			if(*pkt->data==55)
				fprintf_P(&usart_stream, PSTR("woohoo %x \n\r"),pkt->source_id);
		}

		else if(pkt->type == MESSAGE_TYPE_COLOR)
		{
			color_struct* color_ptr = (color_struct*) pkt->data;
			//mchip.neix[pkt->source_id] = pt_ptr->x;
			//mchip.neiy[pkt->source_id] = pt_ptr->y;
			
			if(swarm_mode == SWARM_MODE_COLOR_DROPS)
			{
				// send packet to neighbor below with "my" old color
				{
					color_msg_data.red = my_red;
					color_msg_data.green = my_green;
					color_msg_data.blue = my_blue;
									
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_COLOR;
					pkt.flags = 0;
					pkt.radius = 1;
					pkt.data = (uint8_t *)&color_msg_data;
					pkt.data_len = sizeof(color_msg_data);
									
					xgrid.send_packet(&pkt, 0b00000001);
				}

				my_red = color_ptr->red;
				my_green = color_ptr->green;
				my_blue = color_ptr->blue;

				set_rgb(my_red, my_green, my_blue);
			}
		}
		
		else if(pkt->type == MESSAGE_TYPE_COOR_ASSIGN){
			assign_coordinates* received_coor = (assign_coordinates*) pkt->data;
				if(coor.assigned == false){
					coor.x_coor = received_coor->x_coor;
					coor.y_coor = received_coor->y_coor; 
					coor.assigned = true;
				}
				//fprintf_P(&usart_stream, PSTR("swarm_id: %04X\r\n"),received_coor->swarm_id);			
		}
		
		else if(pkt->type == MESSAGE_TYPE_COMMAND)
		{
			char* char_ptr = (char*) pkt->data;
			command_received = true;
			command = *char_ptr;
			command_source = pkt->rx_node;
			
			fprintf_P(&usart_stream, PSTR("command source: %i "), command_source);
			switch(command_source)
			{
				case BOTTOM:		fprintf_P(&usart_stream, PSTR("0, BOTTOM\n\r"));	break;
				case LEFT_BOT:		fprintf_P(&usart_stream, PSTR("1, LEFT BOT\n\r"));	break;
				case LEFT_TOP:		fprintf_P(&usart_stream, PSTR("2, LEFT TOP\n\r"));	break;
				case TOP:		fprintf_P(&usart_stream, PSTR("3, TOP\n\r"));		break;
				case RIGHT_BOT:		fprintf_P(&usart_stream, PSTR("4, RIGHT TOP\n\r"));	break;
				case RIGHT_TOP:		fprintf_P(&usart_stream, PSTR("5, RIGHT BOT\n\r"));	break;
				default:			fprintf_P(&usart_stream, PSTR("UNKNOWN\n\r"));		break;
			}
		}
		else if (pkt->type == MESSAGE_TYPE_TIME){
				int m	= RTC_getMonth();
				int d	= RTC_getDays();
				int yr	= RTC_getYears();
				int hr	= RTC_getHours();
				int min	= RTC_getMinutes();
				int sec	= RTC_getSeconds();
				int ms  = RTC_getMs();
				
				time* received_time = (time*) pkt->data;
				if(received_time->distance <= prev_sync.distance && swarm_id != 0xCE46){
		
				int time_offset = (0.0526*pkt->data_len)+1.55;
				fprintf_P(&usart_stream, PSTR("offset %u\r"),time_offset);
				
						int gap = (clock.millisecond + clock.second*1000) - (received_time->ms+time_offset +  received_time->sec*1000);
						int offset = ((received_time->sec - prev_sync.sec)*1000 + (received_time->ms+time_offset - prev_sync.ms))/gap;
						
						int milli = received_time->ms+time_offset;
						
						int s = received_time->sec;
						
						int minutes = received_time->min;
						
						int hours = received_time->hr;
						
						int year = RTC_getYears();
						int month = RTC_getMonth();
						int day = RTC_getDays();
						init_date_and_time(month,day,year,hours,minutes,s,milli,0);	
						prev_sync.sec = received_time->sec;
						prev_sync.min = received_time->min;
						prev_sync.hr = received_time->hr;
						prev_sync.distance = received_time->distance;
									
		
				m	= RTC_getMonth();
				d	= RTC_getDays();
				yr	= RTC_getYears();
				hr	= RTC_getHours();
				min	= RTC_getMinutes();
				sec	= RTC_getSeconds();
				ms  = RTC_getMs();
				fprintf_P(&usart_stream, PSTR("RECIEVE %i/%i/%i %i:%i:%i.%i\r\n"),m,d,yr,hr,min,sec,ms);
			}
		}
				
		else if(pkt->type == MESSAGE_TYPE_QUERY)
		{
			char inquiry;
			uint8_t inquiry_source;
			char* char_ptr = (char*) pkt->data;
			inquiry = *char_ptr;
			inquiry_source = pkt->rx_node;
			
			int reply_num;

			fprintf_P(&usart_stream, PSTR("inquiry received: %c "), inquiry);
			fprintf_P(&usart_stream, PSTR("inquiry source: %i "), inquiry_source);
			switch(inquiry)
			{
				case 'I':
					fprintf_P(&usart_stream, PSTR("reply: swarm_id: %04X\n\r"), swarm_id);	

					//char str[] = "D";

					
					reply_num = swarm_id;
					Xgrid::Packet reply_pkt;
					reply_pkt.type = MESSAGE_TYPE_REPLY;
					reply_pkt.flags = 0;
					reply_pkt.radius = 1;
					//pkt.data = (uint8_t *)str;
					reply_pkt.data = (uint8_t *)&reply_num;
					reply_pkt.data_len = sizeof(swarm_id);	// maybe?
					
					xgrid.send_packet(&reply_pkt);

					break;
				
				case 'g':
					fprintf_P(&usart_stream, PSTR("reply: swarm_id: %04X\n\r"), swarm_id);
					fprintf_P(&usart_stream, PSTR("X COOR: %i\r\n"),coor.x_coor);
					fprintf_P(&usart_stream, PSTR("Y COOR: %i\r\n"),coor.y_coor);
					fprintf_P(&usart_stream, PSTR("Assigned? %i\r\n"),coor.assigned);
					//char str[] = "D";

				
					reply_num = swarm_id;
					Xgrid::Packet coor_id_reply_pkt;
					reply_pkt.type = MESSAGE_TYPE_REPLY;
					reply_pkt.flags = 0;
					reply_pkt.radius = 1;
					//pkt.data = (uint8_t *)str;
					reply_pkt.data = (uint8_t *)&reply_num;
					reply_pkt.data_len = sizeof(coor);	// maybe?
				
					xgrid.send_packet(&reply_pkt);
					
					reply_num = coor.x_coor;
					Xgrid::Packet coor_x_reply_pkt;
					reply_pkt.type = MESSAGE_TYPE_REPLY;
					reply_pkt.flags = 0;
					reply_pkt.radius = 1;
					//pkt.data = (uint8_t *)str;
					reply_pkt.data = (uint8_t *)&reply_num;
					reply_pkt.data_len = sizeof(coor);	// maybe?
					
					xgrid.send_packet(&reply_pkt);
					
					reply_num = coor.y_coor;
					Xgrid::Packet coor_y_reply_pkt;
					reply_pkt.type = MESSAGE_TYPE_REPLY;
					reply_pkt.flags = 0;
					reply_pkt.radius = 1;
					//pkt.data = (uint8_t *)str;
					reply_pkt.data = (uint8_t *)&reply_num;
					reply_pkt.data_len = sizeof(coor);	// maybe?
					
					xgrid.send_packet(&reply_pkt);
					
					reply_num = coor.assigned;
					Xgrid::Packet coor_assigned_reply_pkt;
					reply_pkt.type = MESSAGE_TYPE_REPLY;
					reply_pkt.flags = 0;
					reply_pkt.radius = 1;
					//pkt.data = (uint8_t *)str;
					reply_pkt.data = (uint8_t *)&reply_num;
					reply_pkt.data_len = sizeof(coor);	// maybe?
					
					xgrid.send_packet(&reply_pkt);

				break;
				

				default:
					fprintf_P(&usart_stream, PSTR("UNKNOWN\n\r"));
					break;
			}
		}
		
		else if(pkt->type == MESSAGE_TYPE_REPLY)
		{
			int reply;
			uint8_t reply_source;
			int* int_ptr = (int*) pkt->data;
			reply = *int_ptr;
			reply_source = pkt->rx_node;
			
			fprintf_P(&usart_stream, PSTR("reply received: %04X\r\n"), reply);	// this reply contains a swarm_id from the neighbor
			_delay_ms(1);
			fprintf_P(&usart_stream, PSTR("reply source: %i\r\r\n"), reply_source);
			
		}
		
		else if(pkt->type == MESSAGE_TYPE_GESTURE_DATA)
		{
			
			//gesture_data* pkt_gest_temp=(gesture_data*)pkt->data;
			//*sensor_array_temp=*pkt_gest_temp;
			temp_gest_data =*(gesture_data*)pkt->data;
			
			
			fprintf_P(&usart_stream, PSTR("x_coor=%lf"), temp_gest_data.x_coor);	
			_delay_ms(1);
			fprintf_P(&usart_stream, PSTR("y_coor=%lf\r\r\n"), temp_gest_data.y_coor);
			sensor_array_temp++;
		}
		
		else
		{
			fprintf_P(&usart_stream, PSTR("UNKNOWN packet type received: %i\n\r"), pkt->type);
		}
}

void master_coor_assign() { //coordinates for the master block only
		coor.x_coor = 0;
		coor.y_coor = 0;
		coor.assigned = true;
}

void master_init() {
	master_coor_assign();
	//init_touch_i2c();
	//init_accel_i2c();
	
	//init_touch_LEDS();
	//lcd_init(LCD_DISP_ON);
}

void swarm_initialization()
{
	RGB_LED_init();
	
	uint32_t b = 0;
	uint32_t crc = 0;
	
	// calculate local id
	// simply crc of user sig row
	// likely to be unique and constant for each chip
	NVM_CMD = NVM_CMD_READ_CALIB_ROW_gc;
	
	for (uint32_t i = 0x08; i <= 0x15; i++)
	{
			b = PGM_READ_BYTE(i);
			crc = _crc16_update(crc, b);
	}
	
	 NVM_CMD = NVM_CMD_NO_OPERATION_gc;
	
	swarm_id = crc;
	fprintf_P(&usart_stream, PSTR("--%x-- \n\r"),swarm_id);
	if(swarm_id < 0)
		swarm_id*=-1;
	
	srand(swarm_id);
	
	my_red = 0;
	my_green = 0;
	my_blue = 0;

	swarm_mode = INIT_MODE;

	if(swarm_mode == SWARM_MODE_RED){my_red = 0; my_green = 255;my_blue = 125;}
	else if(swarm_mode == SWARM_MODE_WHITE){my_red = 255;my_green = 255;my_blue = 255;}
	set_rgb(my_red, my_green, my_blue);

	// blindly assume my neighbors have the same value that I do
	for(t=0;t<NUM_NEIGHBORS;t++)
	{
		message_count[t]=0;
	}
	
	command_received = false;
	
	message_distance = 1;
	line_direction = 0;
	
	if(swarm_id ==  0x1643){
		fprintf_P(&usart_stream, PSTR("--%x-- \n\r"),swarm_id);
		 master_init();
		/* Xgrid::Packet pkt;
		 b=55;
		 pkt.type = MESSAGE_TYPE_PING;
		 pkt.flags = 0;
		 pkt.radius = 7;
		 pkt.data = (uint8_t *)&b;
		 pkt.data_len = sizeof(b);
		 
		 xgrid.send_packet(&pkt);*/
	}
	init_date_and_time(6,12,2013,4,15,00,0000,0);
	prev_sync.distance = 1000;		 
//hello	
}

void swarm_communication_send()
{
	
	//NEW
	color_msg_data.red = my_red;
	color_msg_data.green = my_green;
	color_msg_data.blue = my_blue;
	
	Xgrid::Packet pkt;

	pkt.type = MESSAGE_TYPE_PING;
	pkt.flags = 0;	
	pkt.radius = 1;
	pkt.data = (uint8_t *)&color_msg_data;
	pkt.data_len = sizeof(color_msg_data);
	
	xgrid.send_packet(&pkt);
}

void swarm_command_handler()
{
	if(command_received)
	{
		if(command == 'D')
		{
			swarm_mode = SWARM_MODE_COLOR_DROPS;
			fprintf_P(&usart_stream, PSTR("cmd COLOR DROPS MODE\n\r"));
		}

		if(command == 'O')
		{
			swarm_mode = SWARM_MODE_UPDOWN;
			fprintf_P(&usart_stream, PSTR("cmd UPDOWN MODE: top %i, bottom %i\n\r"), is_top, is_bottom);
		}
		
		
		else if(command == 'R')
		{
			swarm_mode = SWARM_MODE_RANDOM;
			fprintf_P(&usart_stream, PSTR("cmd RANDOM MODE\n\r"));
		}

		else if(command == 'W')
		{
			swarm_mode = SWARM_MODE_WHITE;
			fprintf_P(&usart_stream, PSTR("cmd WHITE MODE\n\r"));

			my_red = 255;
			my_green = 255;
			my_blue = 255;
			set_blue_led(my_red);
			set_blue_led(my_green);
			set_blue_led(my_blue);
		}

		else if(command == '1')
		{
			swarm_mode = SWARM_MODE_SIDE1;
			fprintf_P(&usart_stream, PSTR("cmd SIDE1 MODE: top %i, bottom %i\n\r"), is_side1_top, is_side1_bottom);
		}
		
		else if(command == '2')
		{
			swarm_mode = SWARM_MODE_SIDE2;
			fprintf_P(&usart_stream, PSTR("cmd SIDE2 MODE: top %i, bottom %i\n\r"), is_side2_top, is_side2_bottom);
		}
		
		else if(command == '4')
		{
				swarm_mode = SWARM_MODE_X_GRADIENT;
				my_red = 150 - coor.x_coor;
				my_blue = 50 - coor.x_coor*2;
				my_green = 125 - coor.x_coor*3;
				
				set_red_led(my_red);
				set_green_led(my_green);
				set_blue_led(my_blue);
				fprintf_P(&usart_stream, PSTR("X GRADIENT\n\r"));
		}
		
		else if(command == '5')
		{
				swarm_mode = SWARM_MODE_Y_GRADIENT;
				my_red = 150 - coor.y_coor*2;
				my_blue = 50 - coor.y_coor*3;
				my_green = 125 - coor.y_coor;
				
				set_red_led(my_red);
				set_green_led(my_green);
				set_blue_led(my_blue);
				fprintf_P(&usart_stream, PSTR("X GRADIENT\n\r"));
		}
		
		
		else
		{
			fprintf_P(&usart_stream, PSTR("UNKNOWN command received: %c\n\r"), command);
		}

		command_received = false;
	}
}





void get_new_rands(int& rand1, int& rand2)
{
	rand1 = rand();
	rand2 = rand();
}

void touch_coor(coordinates*){
	if(coor.assigned == true){
		if(coor.touch_assigned == false){
			coor.touchtop_x = coor.x_coor;
			coor.touchtop_y = coor.y_coor + 11.91514;
		
			coor.touchrt_x = coor.x_coor - 13.58174322;
			coor.touchrt_y = coor.y_coor + 5.7560591;
		
			coor.touchrb_x = coor.x_coor - 13.58174322;
			coor.touchrb_y = coor.y_coor - 5.7560591;
		
			coor.touchbot_x = coor.x_coor;
			coor.touchbot_y = coor.y_coor - 11.91514;
		
			coor.touchlb_x = coor.x_coor + 13.58174322;
			coor.touchlb_y = coor.y_coor - 5.7560591;
		
			coor.touchlt_x = coor.x_coor + 13.58174322;
			coor.touchlt_y = coor.y_coor + 5.7560591;
			
			coor.touch_assigned = true;
		}
	}	
}

void assign_coor(uint8_t bitmask){
	if(coor.assigned == true){
		sent_coor.swarm_id = swarm_id;
		if(bitmask == TOP_MASK){
			
			sent_coor.x_coor = coor.x_coor;
			sent_coor.y_coor = coor.y_coor + 23.830026;
			sent_coor.assigned = true;
		}
		else if(bitmask == LEFT_TOP_MASK){
			sent_coor.x_coor = coor.x_coor + 38.0111;
			sent_coor.y_coor = coor.y_coor + 11.59764;
			sent_coor.assigned = true;
		}
		
		else if(bitmask == LEFT_BOT_MASK){
			sent_coor.x_coor = coor.x_coor + 38.0111;
			sent_coor.y_coor = coor.y_coor - 11.59764;
			sent_coor.assigned = true;
		}
		
		else if(bitmask == BOTTOM_MASK){
			sent_coor.x_coor = coor.x_coor;
			sent_coor.y_coor = coor.y_coor - 23.830026;
			sent_coor.assigned = true;
		}
		
		else if(bitmask == RIGHT_BOT_MASK){
			sent_coor.x_coor = coor.x_coor - 38.0111;
			sent_coor.y_coor = coor.y_coor - 11.59764;
			sent_coor.assigned = true;
		}
		
		else if(bitmask == RIGHT_TOP_MASK){
			sent_coor.x_coor = coor.x_coor - 38.0111;
			sent_coor.y_coor = coor.y_coor + 11.59764;
			sent_coor.assigned = true;
		}
		
		Xgrid::Packet pkt;
		pkt.type = MESSAGE_TYPE_COOR_ASSIGN;
		pkt.flags = 0;
		pkt.radius = 1;
		pkt.data = (uint8_t *)&sent_coor;
		pkt.data_len = sizeof(sent_coor);
		xgrid.send_packet(&pkt, bitmask);
	}
}

 void assign_all(){
	 assign_coor(BOTTOM_MASK);
	 assign_coor(LEFT_BOT_MASK);
	 assign_coor(LEFT_TOP_MASK);
	 assign_coor(TOP_MASK);
	 assign_coor(RIGHT_TOP_MASK);
	 assign_coor(RIGHT_BOT_MASK);
 }
 
void master_block(){
	//lcd_clock();
	//if(touchstatus == 34){
	//	reset_clock();
	//	lcd_clrscr();
	//	lcd_puts("                                                ");
	//}
	//accel_status();
	//_delay_ms(1000);
	//lcd_clrscr();
	
}
 
void clock_sync() {
	
	time current_time;
	current_time.hr = RTC_getHours();
	current_time.min = RTC_getMinutes();
	current_time.sec = RTC_getSeconds();
	current_time.ms = RTC_getMs();
	current_time.distance = sqrt(pow(coor.x_coor, 2.0) + pow(coor.y_coor, 2.0));
	fprintf_P(&usart_stream, PSTR("DISTANCE: %f\r\n"),current_time.distance);
	
	Xgrid::Packet pkt;
	pkt.type = MESSAGE_TYPE_TIME;
	pkt.flags = 0;
	pkt.radius = 1;
	pkt.data =(uint8_t *)&current_time;
	pkt.data_len = sizeof(current_time);
	
	xgrid.send_packet(&pkt);
	int m	= RTC_getMonth();
	int d	= RTC_getDays();
	int yr	= RTC_getYears();
	int hr	= RTC_getHours();
	int min	= RTC_getMinutes();
	int sec	= RTC_getSeconds();
	int ms  = RTC_getMs();
	fprintf_P(&usart_stream, PSTR("SEND:  %i/%i/%i %i:%i:%i.%i\r\n"),m,d,yr,hr,min,sec,ms);
}
 	 
int main(void)
{
	prev_sync.distance = 612;
		
    uint32_t j = 0;
	uint8_t elec_array_sem=0;
    uint8_t send_message_sem=0;    
	char input_char;
	char first_char;
	
	gesture_data gest_pkt;
		
    coor.touch_assigned=false;        
    init();
    init_HV();
    
    go_transparent();    
    xgrid.rx_pkt = &rx_pkt;

    fprintf_P(&usart_stream, PSTR("avr-xgrid build %ld\r\n"), (unsigned long) &__BUILD_NUMBER);
		
	swarm_initialization();
	
	if (usart.available())
	{
		
		first_char = usart.get();
	}
	fprintf_P(&usart_stream, PSTR(" woohoo1\n\r"));	
	//stdout=&usart_stream;
	init_I2C();
	fprintf_P(&usart_stream, PSTR(" woohoo2\n\r"));	
	init_touch(); 	
	fprintf_P(&usart_stream, PSTR(" woohoo\n\r"));	
		
	
		
	//lcd_init(LCD_DISP_ON);
	//fprintf_P(&usart_stream, PSTR("LCD INIT\r\n"));
	//lcd_puts("LCD Test");

	
    while (1)
    {	
		
			/*int sec = RTC_getSeconds();
			if (sec % 10 == 0){
				clock_sync();
			}
			if(sec == 0){
				my_red = 100;
				my_green = 150;
				my_blue = 255;
			}
			else if(sec == 10){
				my_red = 225;
				my_green = 0;
				my_blue = 0;
			}
			else if(sec == 20){
				my_red = 0;
				my_green = 255;
				my_blue = 0;
			}
			
			else if(sec == 30){
				my_red = 255;
				my_green = 255;
				my_blue = 0;
			}
			
			else if(sec == 40){
				my_red = 225;
				my_green = 225;
				my_blue = 225;
			}	
			
			else if(sec == 50){
				my_red = 0;
				my_green = 0;
				my_blue = 100;
			}		 
			if (clock.millisecond % 500 == 0){
				Xgrid::Packet pkt;
				pkt.type = MESSAGE_TYPE_TIME;
				pkt.flags = 0;
				pkt.radius = 1;
				pkt.data =(uint8_t*)&clock;
				pkt.data_len = sizeof(clock);
			}		*/	
				
            if(!coor.touch_assigned==true)
			{
				touch_coor(&coor);
				fprintf_P(&usart_stream, PSTR(" woohoo1\n\r"));
				
			}
			
			
			
			
			
			//things to be done(akshay)
			/* 1. co-ordinates assignment as per request by block
			   2. dynamic allotment of master
			   3. getting sensor data from the gesture
			   4. timestamp of sensors
			   5. sorting of gesture sensor data according to timestamp			   
			   6. gesture recognition
			   
			   
			*/
			
			
			
			
			touchstatus = touch_read(0x00);	// Read touch status
			//fprintf_P(&usart_stream, PSTR(" touchstatus=%2x\n\r"),touchstatus);	
			
			
			//touchstatus = touch_update();
			if((touchstatus)&&(coor.touch_assigned==true))
			{
				
				if(touchstatus & RIGHT_TOP_MASK)
				{
						set_rgb(0,255,0);
						//			go_transparent();
						if(elec_array_sem==0)
						{
							gest_pkt.x_coor=coor.touchlb_x;
							gest_pkt.y_coor=coor.touchlb_y;
							elec_array_sem=1;
							//send_message_sem=1;
						}
					
				}

				if(touchstatus & TOP_MASK)
				{
						set_rgb(255,0,0);
						//		go_opaque();
						if(elec_array_sem==0)
						{
							gest_pkt.x_coor=coor.touchlt_x;
							gest_pkt.y_coor=coor.touchlt_y;
							elec_array_sem=1;
							//send_message_sem=1;
						}
					
				}


				if(touchstatus & LEFT_TOP_MASK)
				{
						set_rgb(255,0,255);
						if(elec_array_sem==0)
						{
							gest_pkt.x_coor=coor.touchtop_x;
							gest_pkt.y_coor=coor.touchtop_y;
							elec_array_sem=1;
							//send_message_sem=1;
						}
					
				}


				if(touchstatus & LEFT_BOT_MASK)
				{
						set_rgb(255,255,0);
						if(elec_array_sem==0)
						{
							gest_pkt.x_coor=coor.touchrt_x;
							gest_pkt.y_coor=coor.touchrt_y;
							elec_array_sem=1;
							//send_message_sem=1;
						}
					
				}

				if(touchstatus & BOTTOM_MASK)
				{
					
						set_rgb(0,255,125);
						if(elec_array_sem==0)
						{
							gest_pkt.x_coor=coor.touchrb_x;
							gest_pkt.y_coor=coor.touchrb_y;
							elec_array_sem=1;
							//send_message_sem=1;
						}
					
				}

				if(touchstatus & RIGHT_BOT_MASK)
				{
					
						set_rgb(0,0,255);
						if(elec_array_sem==0)
						{
							gest_pkt.x_coor=coor.touchbot_x;
							gest_pkt.y_coor=coor.touchbot_y;
							elec_array_sem=1;
							//send_message_sem=1;
						}
					
				}
				if(touchstatus&&send_message_sem)
				{
					
					Xgrid::Packet pkt_gest;
					pkt_gest.type = MESSAGE_TYPE_GESTURE_DATA;
					pkt_gest.flags = 0;
					pkt_gest.radius = 6;
					pkt_gest.data = (uint8_t *)&gest_pkt;
					pkt_gest.data_len = sizeof(gest_pkt);
					xgrid.send_packet(&pkt_gest);
					send_message_sem=0;
					fprintf_P(&usart_stream, PSTR("x_coor=%lf"), gest_pkt.x_coor);
					_delay_ms(1);
					fprintf_P(&usart_stream, PSTR("y_coor=%lf\r\r\n"), gest_pkt.y_coor);
					_delay_ms(1);
				}

				
				
				
			}
			if(touchstatus==0)
			{
				//fprintf_P(&usart_stream, PSTR(" yello\n\r"));
				set_rgb(0,0,0);
				elec_array_sem=0;
				send_message_sem=1;

			}
		//	fprintf_P(&usart_stream, PSTR("woohoo\n\r"));
			
			j = milliseconds + 20; // from here, you have 20 ms to reach the bottom of this loop
			assign_all();				
			if (usart.available())
			{
				input_char = usart.get();
			}	
							
			// main loop
			if(swarm_id ==  0xae47){
				master_block();
			}
            if (input_char == 0x1b)
                    xboot_reset();
				
			else if(input_char != 0x00)
			{
				fprintf_P(&usart_stream, PSTR("CPU: %c\r\n"), input_char);
					
				if (input_char == 'c')
				{
					fprintf_P(&usart_stream, PSTR("message count:\r\n"));
						
					fprintf_P(&usart_stream, PSTR("0 [BOTTOM]:\t%i\r\n"),message_count[0]);
					_delay_ms(1);	// delay for printf to catch up
						
					fprintf_P(&usart_stream, PSTR("1 [LEFT BOT]:\t%i\r\n"),message_count[1]);
					_delay_ms(1);	// delay for printf to catch up
						
					fprintf_P(&usart_stream, PSTR("2 [LEFT TOP]:\t%i\r\n"),message_count[2]);
					_delay_ms(1);	// delay for printf to catch up
						
					fprintf_P(&usart_stream, PSTR("3 [TOP]:\t%i\r\n"),message_count[3]);
					_delay_ms(1);	// delay for printf to catch up
						
					fprintf_P(&usart_stream, PSTR("4 [RIGHT TOP]:\t%i\r\n"),message_count[4]);
					_delay_ms(1);	// delay for printf to catch up
						
					fprintf_P(&usart_stream, PSTR("5 [RIGHT BOT]:\t%i\r\n"),message_count[5]);
					_delay_ms(1);	// delay for printf to catch up
				}
					
				else if (input_char == 'C')
				{	
					fprintf_P(&usart_stream, PSTR("MY COLOR: %i,%i,%i\n\r"), my_red, my_green, my_blue);
				}
					
				else if (input_char == 'd')
				{
					message_distance++;
						
					if(message_distance > 7)
						message_distance = 1;
						
					fprintf_P(&usart_stream, PSTR("message distance:%i\n\r"), message_distance);
				}

				else if(input_char == 'D')
				{
					swarm_mode = SWARM_MODE_COLOR_DROPS;
					fprintf_P(&usart_stream, PSTR("send cmd COLOR DROPS MODE\n\r"));
					
					char str[] = "D";
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_COMMAND;
					pkt.flags = 0;
					pkt.radius = message_distance;
					pkt.data = (uint8_t *)str;
					pkt.data_len = 1;
					
					xgrid.send_packet(&pkt);
				}
				
				else if(input_char == 'g'){
					fprintf_P(&usart_stream, PSTR("swarm_id: %04X\r\n"),swarm_id);
					fprintf_P(&usart_stream, PSTR("X COOR: %f\r\n"),coor.x_coor);
					fprintf_P(&usart_stream, PSTR("Y COOR: %f\r\n"),coor.y_coor);
					fprintf_P(&usart_stream, PSTR("ASSIGNED? %i\r\n"),coor.assigned);
				}		
			
				else if(input_char == 'G'){
					char str[] = "g";
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_QUERY;
					pkt.flags = 0;
					pkt.radius = 1;
					pkt.data = (uint8_t *)str;
					pkt.data_len = 1;
					
					xgrid.send_packet(&pkt);
				}			
				 
				else if(input_char == 'h')
				{
					fprintf_P(&usart_stream, PSTR("send color down: %i,%i,%i\n\r"), my_red, my_green, my_blue);
					color_msg_data.red = my_red;
					color_msg_data.green = my_green;
					color_msg_data.blue = my_blue;
					
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_COLOR;
					pkt.flags = 0;
					pkt.radius = 1;
					pkt.data = (uint8_t *)&color_msg_data;
					pkt.data_len = sizeof(color_msg_data);
					
					xgrid.send_packet(&pkt, 0b00000001);
				}

				else if (input_char == 'i')
				{
					//fprintf_P(&usart_stream, PSTR("my info:\r\n"));
						
					fprintf_P(&usart_stream, PSTR("swarm_id: %04X\r\n"),swarm_id);
				}
					
				else if (input_char == 'j')
				{
					fprintf_P(&usart_stream, PSTR("my calibration byte:\r\n"));
					fprintf_P(&usart_stream, PSTR("0:%i\r\n"),SP_ReadCalibrationByte(0));
					fprintf_P(&usart_stream, PSTR("1:%i\r\n"),SP_ReadCalibrationByte(1));
					fprintf_P(&usart_stream, PSTR("2:%i\r\n"),SP_ReadCalibrationByte(2));
					fprintf_P(&usart_stream, PSTR("3:%i\r\n"),SP_ReadCalibrationByte(3));
					fprintf_P(&usart_stream, PSTR("4:%i\r\n"),SP_ReadCalibrationByte(4));
				}
				
				else if( input_char == 'l'){
					
					fprintf_P(&usart_stream, PSTR("TOUCH BOTTOM X: %f\r\n"),coor.touchbot_x);
					fprintf_P(&usart_stream, PSTR("TOUCH BOTTOM LEFT X: %f\r\n"),coor.touchlb_x);
					fprintf_P(&usart_stream, PSTR("TOUCH TOP LEFT X: %f\r\n"),coor.touchlt_x);
					fprintf_P(&usart_stream, PSTR("TOUCH TOP X: %f\r\n"),coor.touchtop_x);
					fprintf_P(&usart_stream, PSTR("TOUCH TOP RIGHT X: %f\r\n"),coor.touchrt_x);
					fprintf_P(&usart_stream, PSTR("TOUCH BOTTOM RIGHT X: %f\r\n"),coor.touchrb_x);
					fprintf_P(&usart_stream, PSTR("TOUCH BOTTOM Y: %f\r\n"),coor.touchbot_y);
					fprintf_P(&usart_stream, PSTR("TOUCH BOTTOM LEFT Y: %f\r\n"),coor.touchlb_y);
					fprintf_P(&usart_stream, PSTR("TOUCH TOP LEFT Y: %f\r\n"),coor.touchlt_y);
					fprintf_P(&usart_stream, PSTR("TOUCH TOP Y: %f\r\n"),coor.touchtop_y);
					fprintf_P(&usart_stream, PSTR("TOUCH TOP RIGHT Y: %f\r\n"),coor.touchrt_y);
					fprintf_P(&usart_stream, PSTR("TOUCH BOTTOM RIGHT Y: %f\r\n"),coor.touchrb_y);
					fprintf_P(&usart_stream, PSTR("TOUCH ASSIGNED? %i\r\n"),coor.touch_assigned);
				}
				
				else if(input_char == 'm')
				{
					#define SWARM_MODE_RANDOM 0
					#define SWARM_MODE_WHITE 1
					#define SWARM_MODE_UPDOWN 2			// gives an indication of where the block thinks it is (green = top, red = bottom, blue = middle)
					#define SWARM_MODE_SIDE1 3
					#define SWARM_MODE_SIDE2 4
					#define SWARM_MODE_COLOR_DROPS 5
					fprintf_P(&usart_stream, PSTR("SWARM MODE: %i\r\n"), swarm_mode);
					fprintf_P(&usart_stream, PSTR("0=RAND\r\n"), swarm_mode);
					fprintf_P(&usart_stream, PSTR("1=WHITE\r\n"), swarm_mode);
					_delay_ms(1);
					fprintf_P(&usart_stream, PSTR("2=UPDOWN\r\n"), swarm_mode);
					fprintf_P(&usart_stream, PSTR("3=SIDE1\r\n"), swarm_mode);
					fprintf_P(&usart_stream, PSTR("4=SIDE2\r\n"), swarm_mode);
					fprintf_P(&usart_stream, PSTR("5=COL DROP\r\n"), swarm_mode);
				}
				
				else if(input_char == 'n')
				{	
					char str[] = "I";
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_QUERY;
					pkt.flags = 0;
					pkt.radius = 1;
					pkt.data = (uint8_t *)str;
					pkt.data_len = 1;
					
					xgrid.send_packet(&pkt);
				}
					
				else if(input_char == 'p')
				{
					fprintf_P(&usart_stream, PSTR("provisional position\n\r"));
					fprintf_P(&usart_stream, PSTR("bottom: %u\n\r"), provisional_is_bottom);
					_delay_ms(1);
					fprintf_P(&usart_stream, PSTR("left bot: %u\n\r"), provisional_is_side1_bottom);
					fprintf_P(&usart_stream, PSTR("left top: %u\n\r"), provisional_is_side2_top);
					fprintf_P(&usart_stream, PSTR("top: %u\n\r"), provisional_is_top);
					_delay_ms(1);
					fprintf_P(&usart_stream, PSTR("right top: %u\n\r"), provisional_is_side1_top);
					fprintf_P(&usart_stream, PSTR("right bot: %u\n\r"), provisional_is_side2_bottom);
				}

				else if(input_char == 'P')
				{
					fprintf_P(&usart_stream, PSTR("known position\n\r"));
					fprintf_P(&usart_stream, PSTR("bottom: %u\n\r"), is_bottom);
					_delay_ms(1);
					fprintf_P(&usart_stream, PSTR("left bot: %u\n\r"), is_side1_bottom);
					fprintf_P(&usart_stream, PSTR("left top: %u\n\r"), is_side2_top);
					fprintf_P(&usart_stream, PSTR("top: %u\n\r"), is_top);
					_delay_ms(1);
					fprintf_P(&usart_stream, PSTR("right top: %u\n\r"), is_side1_top);
					fprintf_P(&usart_stream, PSTR("right bot: %u\n\r"), is_side2_bottom);
				}

				else if(input_char == 'r')
				{
					if(swarm_mode == SWARM_MODE_RANDOM)
					{
						fprintf_P(&usart_stream, PSTR("RANDOM MODE\n\r"));
					}
						
					else
					{
						swarm_mode = SWARM_MODE_RANDOM;
						fprintf_P(&usart_stream, PSTR("now RANDOM MODE\n\r"));
					}
				}

				else if(input_char == 'R')
				{
					swarm_mode = SWARM_MODE_RANDOM;
					fprintf_P(&usart_stream, PSTR("send cmd RANDOM MODE\n\r"));
					
					char str[] = "R";
                    Xgrid::Packet pkt;
                    pkt.type = MESSAGE_TYPE_COMMAND;
                    pkt.flags = 0;
                    pkt.radius = message_distance;
                    pkt.data = (uint8_t *)str;
                    pkt.data_len = 1;
                        
                    xgrid.send_packet(&pkt);			
				}
				
				else if(input_char == 's'){
					time current_time;
					current_time.hr = RTC_getHours();
					current_time.min = RTC_getMinutes();
					current_time.sec = RTC_getSeconds();
					current_time.ms = RTC_getMs();
					
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_TIME;
					pkt.flags = 0;
					pkt.radius = 1;
					pkt.data =(uint8_t *)&current_time;
					pkt.data_len = sizeof(current_time);
					
					xgrid.send_packet(&pkt);
					int m	= RTC_getMonth();
					int d	= RTC_getDays();
					int yr	= RTC_getYears();
					int hr	= RTC_getHours();
					int min	= RTC_getMinutes();
					int sec	= RTC_getSeconds();
					int ms  = RTC_getMs();
					fprintf_P(&usart_stream, PSTR("%i/%i/%i %i:%i:%i.%i\r\n"),m,d,yr,hr,min,sec,ms);
					
				}
				else if(input_char == 't')
				{
					fprintf_P(&usart_stream, PSTR("two's complement tutorial\r\n"));

					for(uint16_t i = 0; i <= 255; i++)
					{
						fprintf_P(&usart_stream, PSTR("%i : %u\r\n"), (int8_t)i, i);
						_delay_ms(1);
					}
				}

				else if(input_char == 'T')
				{
					fprintf_P(&usart_stream, PSTR("time: %i (ms)\r\n"), milliseconds);
				}

				else if(input_char == 'u')
				{
					if(update_allowed)
						update_allowed = false;
					else
						update_allowed = true;
							
					if(update_allowed)
						fprintf_P(&usart_stream, PSTR("update allowed"));
					else
						fprintf_P(&usart_stream, PSTR("update NOT allowed"));
				}
				else if(input_char == 'U'){
				int m	= RTC_getMonth();
				int d	= RTC_getDays();
				int yr	= RTC_getYears();
				int hr	= RTC_getHours();
				int min	= RTC_getMinutes();
				int sec	= RTC_getSeconds();
				int ms  = RTC_getMs();
				fprintf_P(&usart_stream, PSTR("%i/%i/%i %i:%i:%i.%i\r\n"),m,d,yr,hr,min,sec,ms);
					
				}				
				else if(input_char == 'v')
				{
					fprintf_P(&usart_stream, PSTR("avr-xgrid build %ld\r\n"), (unsigned long) &__BUILD_NUMBER);
				}
				
				else if(input_char == 'W')
				{
					swarm_mode = SWARM_MODE_WHITE;
					fprintf_P(&usart_stream, PSTR("send cmd WHITE MODE\n\r"));
					
					my_red = 255;
					my_green = 255;
					my_blue = 255;
					set_blue_led(my_red);
					set_blue_led(my_green);
					set_blue_led(my_blue);

					char str[] = "W";
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_COMMAND;
					pkt.flags = 0;
					pkt.radius = message_distance;
					pkt.data = (uint8_t *)str;
					pkt.data_len = 1;
					
					xgrid.send_packet(&pkt);
				}

				else if(input_char == '1')
				{
					swarm_mode = SWARM_MODE_SIDE1;
					fprintf_P(&usart_stream, PSTR("send cmd SIDE1 MODE: top %i, bottom %i\n\r"), is_side1_top, is_side1_bottom);
					
					char str[] = "1";
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_COMMAND;
					pkt.flags = 0;
					pkt.radius = message_distance;
					pkt.data = (uint8_t *)str;
					pkt.data_len = 1;
					
					xgrid.send_packet(&pkt);
				}

				else if(input_char == '2')
				{
					swarm_mode = SWARM_MODE_SIDE2;
					fprintf_P(&usart_stream, PSTR("send cmd SIDE2 MODE: top %i, bottom %i\n\r"), is_side2_top, is_side2_bottom);
					
					char str[] = "2";
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_COMMAND;
					pkt.flags = 0;
					pkt.radius = message_distance;
					pkt.data = (uint8_t *)str;
					pkt.data_len = 1;
					
					xgrid.send_packet(&pkt);
				}

				else if(input_char == '3')
				{
					swarm_mode = SWARM_MODE_UPDOWN;
					fprintf_P(&usart_stream, PSTR("send cmd UPDOWN MODE: top %i, bottom %i\n\r"), is_top, is_bottom);
					
					char str[] = "O";
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_COMMAND;
					pkt.flags = 0;
					pkt.radius = message_distance;
					pkt.data = (uint8_t *)str;
					pkt.data_len = 1;
					
					xgrid.send_packet(&pkt);
				}
				
				else if(input_char == '4')
				{
					char str[] = "4";
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_COMMAND;
					pkt.flags = 0;
					pkt.radius = 10;
					pkt.data = (uint8_t *)str;
					pkt.data_len = 1;
					
					xgrid.send_packet(&pkt);
				}
				
				else if(input_char == '5')
				{
					char str[] = "5";
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_COMMAND;
					pkt.flags = 0;
					pkt.radius = 10;
					pkt.data = (uint8_t *)str;
					pkt.data_len = 1;
					
					xgrid.send_packet(&pkt);
				}


				input_char = 0x00;	// clear the most recent computer input
			}
				
		/*	if(milliseconds > j)	// if TRUE, then we took too long to get here, halt program
			{
				fprintf_P(&usart_stream, PSTR("milliseconds > j\n\r"));
				
				cli();	//disable_interrupts
					
				set_red_led(255);
				set_green_led(0);
				set_blue_led(255);
					
				while(1==1)
				{};
			}					
			
            while (j > milliseconds) { fprintf_P(&usart_stream, PSTR("milliseconds > j\n\r"));};*/
			
    }  
}

// Init everything
void init(void)
{
        // clock
        OSC.CTRL |= OSC_RC32MEN_bm; // turn on 32 MHz oscillator
        while (!(OSC.STATUS & OSC_RC32MRDY_bm)) { }; // wait for it to start
        CCP = CCP_IOREG_gc;
        CLK.CTRL = CLK_SCLKSEL_RC32M_gc; // switch osc
        DFLLRC32M.CTRL = DFLL_ENABLE_bm; // turn on DFLL
        
        // disable JTAG
        CCP = CCP_IOREG_gc;
        MCU.MCUCR = 1;

        // Init buttons
        //BTN_PORT.DIRCLR = BTN_PIN_bm;
        
        // UARTs
        usart.set_tx_buffer(usart_txbuf, USART_TX_BUF_SIZE);
        usart.set_rx_buffer(usart_rxbuf, USART_RX_BUF_SIZE);
        usart.begin(UART_BAUD_RATE);
        usart.setup_stream(&usart_stream);
        
        usart_n0.set_tx_buffer(usart_n0_txbuf, NODE_TX_BUF_SIZE);
        usart_n0.set_rx_buffer(usart_n0_rxbuf, NODE_RX_BUF_SIZE);
        usart_n0.begin(NODE_BAUD_RATE);
        xgrid.add_node(&usart_n0);
        
		usart_n1.set_tx_buffer(usart_n1_txbuf, NODE_TX_BUF_SIZE);
        usart_n1.set_rx_buffer(usart_n1_rxbuf, NODE_RX_BUF_SIZE);
        usart_n1.begin(NODE_BAUD_RATE);
        xgrid.add_node(&usart_n1);
        
		usart_n2.set_tx_buffer(usart_n2_txbuf, NODE_TX_BUF_SIZE);
        usart_n2.set_rx_buffer(usart_n2_rxbuf, NODE_RX_BUF_SIZE);
        usart_n2.begin(NODE_BAUD_RATE);
        xgrid.add_node(&usart_n2);
        
		usart_n3.set_tx_buffer(usart_n3_txbuf, NODE_TX_BUF_SIZE);
        usart_n3.set_rx_buffer(usart_n3_rxbuf, NODE_RX_BUF_SIZE);
        usart_n3.begin(NODE_BAUD_RATE);
        xgrid.add_node(&usart_n3);
        
		usart_n4.set_tx_buffer(usart_n4_txbuf, NODE_TX_BUF_SIZE);
        usart_n4.set_rx_buffer(usart_n4_rxbuf, NODE_RX_BUF_SIZE);
        usart_n4.begin(NODE_BAUD_RATE);
        xgrid.add_node(&usart_n4);
        
		usart_n5.set_tx_buffer(usart_n5_txbuf, NODE_TX_BUF_SIZE);
        usart_n5.set_rx_buffer(usart_n5_rxbuf, NODE_RX_BUF_SIZE);
        usart_n5.begin(NODE_BAUD_RATE);
        xgrid.add_node(&usart_n5);

        // TCC
        TCC0.CTRLA = TC_CLKSEL_DIV256_gc;
        TCC0.CTRLB = 0;
        TCC0.CTRLC = 0;
        TCC0.CTRLD = 0;
        TCC0.CTRLE = 0;
        TCC0.INTCTRLA = TC_OVFINTLVL_LO_gc;
        TCC0.INTCTRLB = 0;
        TCC0.CNT = 0;
        TCC0.PER = 125;
        
        // Interrupts
        PMIC.CTRL = PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm;
        
        sei();
		//RTC
		RTC_Init();
		//PLL_init();
}


// Timer tick ISR (1 kHz)
ISR(TCC0_OVF_vect)
{
	// Timers
	milliseconds++;
	
	if (milliseconds % 50 == 0)
	{
		if(swarm_mode == SWARM_MODE_COLOR_DROPS)
		{
			
		}
	}
	
	if (milliseconds % 100 == 0)
	{

	}
	
	if (milliseconds % 200 == 0)
	{
		
		swarm_command_handler();		// looks for new commands
		
		int rand1, rand2;

		get_new_rands(rand1, rand2);

		if(swarm_mode == SWARM_MODE_RANDOM)
		{
			uint8_t color_diff = 10;
			if(rand2%3 == 0)
			{
				if((rand1%2==0)&&(my_blue > color_diff))
					my_blue-=color_diff;
				else if(my_blue < 255-color_diff)
					my_blue+=color_diff;
		//		set_blue_led(my_blue);
			}
				
			else if(rand2%3 == 1)
			{
				if((rand1%2==0)&&(my_green > color_diff))
					my_green-=color_diff;
				else if(my_green < 255-color_diff)
					my_green+=color_diff;
		//		set_green_led(my_green);
			}
				
			else if(rand2%3 == 2)
			{
				if((rand1%2==0)&&(my_red > color_diff))
					my_red-=color_diff;
				else if(my_red < 255-color_diff)
					my_red+=color_diff;
		//		set_red_led(my_red);
			}
		}
	}
	
	if (milliseconds % 250 == 0)	// was 500
	{
		//swarm_communication_send();
		//swarm_calculation();		// looks for new commands
		//servo_motor_control();

		if((swarm_mode == SWARM_MODE_COLOR_DROPS)&&is_top)
		{
			//if(turns_since_dropped > 10)
			//{
				if((my_red == 0)&&(my_green == 0)&&(my_blue == 0))
				{
					if(rand()%18 == 0)
						my_blue = 255;
					if(rand()%18 == 0)
						my_green = 255;
					if(rand()%18 == 0)
						my_red = 255;

					else
					{
						fprintf_P(&usart_stream, PSTR("send color down: %i,%i,%i\n\r"), my_red, my_green, my_blue);
						color_msg_data.red = my_red;
						color_msg_data.green = my_green;
						color_msg_data.blue = my_blue;
						
						Xgrid::Packet pkt;
						pkt.type = MESSAGE_TYPE_COLOR;
						pkt.flags = 0;
						pkt.radius = 1;
						pkt.data = (uint8_t *)&color_msg_data;
						pkt.data_len = sizeof(color_msg_data);
						
						xgrid.send_packet(&pkt, 0b00000001);
					}
				
				}

				else
				{
					fprintf_P(&usart_stream, PSTR("send color down: %i,%i,%i\n\r"), my_red, my_green, my_blue);
					color_msg_data.red = my_red;
					color_msg_data.green = my_green;
					color_msg_data.blue = my_blue;
				
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_COLOR;
					pkt.flags = 0;
					pkt.radius = 1;
					pkt.data = (uint8_t *)&color_msg_data;
					pkt.data_len = sizeof(color_msg_data);
				
					xgrid.send_packet(&pkt, 0b00000001);

					if(my_red > 99)
						my_red-=100;
					else
						my_red = 0;

					if(my_green > 99)
						my_green-=100;
					else
						my_green = 0;

					if(my_blue > 99)
						my_blue-=100;
					else
						my_blue = 0;
				}

		//		set_rgb(my_red, my_green, my_blue);

		}

		else
			swarm_communication_send();
	
	}
	
	if (milliseconds % 2000 == 0)	// swap provisional 
	{
		is_top = provisional_is_top;
		is_bottom = provisional_is_bottom;
		is_side1_top = provisional_is_side1_top;
		is_side1_bottom = provisional_is_side1_bottom;
		is_side2_top = provisional_is_side2_top;
		is_side2_bottom = provisional_is_side2_bottom;

		// color gives an indication of where the block thinks it is (green = top, red = bottom, blue = middle)
		
		if((swarm_mode == SWARM_MODE_UPDOWN)&&((is_top)&&(!is_bottom)))
		{
			my_red = 0;
			my_green = 255;
			my_blue = 0;
		}

		if((swarm_mode == SWARM_MODE_UPDOWN)&&((!is_top)&&(is_bottom)))
		{
			
			my_red = 255;
			my_green = 0;
			my_blue = 0;
		}

		if((swarm_mode == SWARM_MODE_UPDOWN)&&((is_top)&&(is_bottom)))
		{
			my_red = 255;
			my_green = 255;
			my_blue = 0;
		}

		if((swarm_mode == SWARM_MODE_UPDOWN)&&((!is_top)&&(!is_bottom)))
		{
			my_red = 0;
			my_green = 0;
			my_blue = 255;
		}

		if((swarm_mode == SWARM_MODE_SIDE1)&&((is_side1_top)&&(!is_side1_bottom)))
		{
			my_red = 0;
			my_green = 255;
			my_blue = 0;
		}

		if((swarm_mode == SWARM_MODE_SIDE1)&&((!is_side1_top)&&(is_side1_bottom)))
		{
			
			my_red = 255;
			my_green = 0;
			my_blue = 0;
		}

		if((swarm_mode == SWARM_MODE_SIDE1)&&((is_side1_top)&&(is_side1_bottom)))
		{
			my_red = 255;
			my_green = 255;
			my_blue = 0;
		}

		if((swarm_mode == SWARM_MODE_SIDE1)&&((!is_side1_top)&&(!is_side1_bottom)))
		{
			my_red = 0;
			my_green = 0;
			my_blue = 255;
		}

		if((swarm_mode == SWARM_MODE_SIDE2)&&((is_side2_top)&&(!is_side2_bottom)))
		{
			my_red = 0;
			my_green = 255;
			my_blue = 0;
		}

		if((swarm_mode == SWARM_MODE_SIDE2)&&((!is_side2_top)&&(is_side2_bottom)))
		{
			my_red = 255;
			my_green = 0;
			my_blue = 0;
		}

		if((swarm_mode == SWARM_MODE_SIDE2)&&((is_side2_top)&&(is_side2_bottom)))
		{
			my_red = 255;
			my_green = 255;
			my_blue = 0;
		}

		if((swarm_mode == SWARM_MODE_SIDE2)&&((!is_side2_top)&&(!is_side2_bottom)))
		{
			my_red = 0;
			my_green = 0;
			my_blue = 255;
		}

	//	set_red_led(my_red);
	//	set_green_led(my_green);
	//	set_blue_led(my_blue);

		provisional_is_bottom = true;	// assume you are top/bottom until proven otherwise
		provisional_is_top = true;
		provisional_is_side1_top = true;
		provisional_is_side1_bottom = true;
		provisional_is_side2_top = true;
		provisional_is_side2_bottom = true;
	}
	
	xgrid.process();
}


