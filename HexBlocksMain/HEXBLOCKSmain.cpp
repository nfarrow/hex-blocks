/*
#if PROGMEM_SIZE > 0x010000	// = 65536 (128a3u = 139264)
#define PGM_READ_BYTE pgm_read_byte_far
#else
#define PGM_READ_BYTE pgm_read_byte_near
#endif
*/

#include "HEXBLOCKSmain.h"

// USART
#define USART_TX_BUF_SIZE 64
#define USART_RX_BUF_SIZE 64
char usart_txbuf[USART_TX_BUF_SIZE];
char usart_rxbuf[USART_RX_BUF_SIZE];
CREATE_USART(usart, UART_DEVICE_PORT);
FILE usart_stream;

#define NODE_TX_BUF_SIZE 32
#define NODE_RX_BUF_SIZE 64
char usart_n0_txbuf[NODE_TX_BUF_SIZE];
char usart_n0_rxbuf[NODE_RX_BUF_SIZE];
CREATE_USART(usart_n0, USART_N0_DEVICE_PORT);
char usart_n1_txbuf[NODE_TX_BUF_SIZE];
char usart_n1_rxbuf[NODE_RX_BUF_SIZE];
CREATE_USART(usart_n1, USART_N1_DEVICE_PORT);
char usart_n2_txbuf[NODE_TX_BUF_SIZE];
char usart_n2_rxbuf[NODE_RX_BUF_SIZE];
CREATE_USART(usart_n2, USART_N2_DEVICE_PORT);
char usart_n3_txbuf[NODE_TX_BUF_SIZE];
char usart_n3_rxbuf[NODE_RX_BUF_SIZE];
CREATE_USART(usart_n3, USART_N3_DEVICE_PORT);
char usart_n4_txbuf[NODE_TX_BUF_SIZE];
char usart_n4_rxbuf[NODE_RX_BUF_SIZE];
CREATE_USART(usart_n4, USART_N4_DEVICE_PORT);
char usart_n5_txbuf[NODE_TX_BUF_SIZE];
char usart_n5_rxbuf[NODE_RX_BUF_SIZE];
CREATE_USART(usart_n5, USART_N5_DEVICE_PORT);


Xgrid xgrid;

volatile uint16_t j; // Timer compare (critical xgrid variable)

//void swarm_color_ping_send();

//void swarm_id_ping_send();

void init(void);


#define TIMEOUT_TIME_MS	40

#define INIT_MODE SWARM_MODE_DISPLAY_VERSION_NUMBER

#define PI 3.14159
#define RADtoDEG 57.29578

//char command;
//uint8_t command_source;

uint8_t my_swarm_position = SWARM_POSITION_UNKNOWN;

// //////////  Data structures transmitted to neighbors  /////////////////
 
struct color_struct {
	uint8_t red;
	uint8_t green;
	uint8_t blue;
};

color_struct color_msg_data;

void rx_pkt(Xgrid::Packet *pkt);

void init_PLL(void);

void init(void);


int main(void)
{
    stdout = &usart_stream;
	
	uint32_t j = 0;
        
	char input_char;

	struct work_struct current_work_notice;
	//char work_detail;
	//uint8_t work_source; // (node that told of work (0-5), or keyboard(6), whichever)
	
    _delay_ms(50);
        
    init();

    xgrid.rx_pkt = &rx_pkt;	// what does this do?

	_delay_ms(50);

    fprintf_P(&usart_stream, PSTR("avr-xgrid build %ld\r\n"), (unsigned long) &__BUILD_NUMBER);
		
	printf_P(PSTR("stdout OK\r\n"));

	uint8_t touch_val;

    while (1)
    {
        /* VERIFY TIME ROLLOVER IS WORKING OKAY, THEN DELETE THIS (TODO)
		
		NOTE: THERE *ARE* CURRENTLY ISSUES WITH THE CLOCK ROLLOVER AT THE MINUTES, NEEDS TO BE FIXED
		
		if(milliseconds > 60000)	//roll over after 1 minute, this also keeps j from ever being smaller than milliseconds
		{
			milliseconds-=60000;
			minutes++;
		}*/ 
		
		j = get_milliseconds() + TIMEOUT_TIME_MS; // from here, you have TIMEOUT_TIME_MS ms to reach the bottom of this loop

		if(j >= 60000)
			j = j - 60000;

	//*** (input phase)	***/////////////////////////////////////////////////////////
		
		// get INPUT from PC /////////////////////////////////////////////
		if (usart.available())
			input_char = usart.get();
			
		// get INPUT from user /////////////////////////////////////////////				
		touch_val = touch_update();
		
		// get INPUT from devices //////////////////////////////////////////

		// accelerometer

		// microphone

		// camera
	
	//*** (process the input)	***/////////////////////////////////////////////////////////

		// process INPUT from user /////////////////////////////////////////
		handle_touch(touch_val);
		
		// process INPUT from PC /////////////////////////////////////////   
		if(input_char)
		{
			serial_char_process(input_char);
		
			input_char = 0x00;	// clear the most recent computer input
		}

		if(work_queue_has_work())
		{
			current_work_notice = pop_work_queue();
			
			execute_task(current_work_notice);
		}



		if((j > TIMEOUT_TIME_MS)&&(get_milliseconds() > j))
		// if TRUE, then we took too long to get here, halt program
		// (TODO) there is currently nothing to handle the case where j <= TIMEOUT_TIME_MS so this
		// case currently gets a free pass, this is a rare event, but code should be written to handle it
		{
			//halt_program(255,0,255);
			complain_no_halt();
		}					
				
		handle_lcd();

        while (j > get_milliseconds())
		{/* killing time here */};
    }  
}

void init_PLL(void)
{	
	// this code is unverified, check that it works TODO

	// phase lock loop (increases stability of CPU clock)
	OSC.PLLCTRL = OSC_PLLSRC_RC32M_gc | 0x06;
	OSC.CTRL |= OSC_PLLEN_bm;
	while (!(OSC.STATUS & OSC_PLLRDY_bm));
	CCP = CCP_IOREG_gc;
	CLK.CTRL = CLK_SCLKSEL_PLL_gc;
}

// Init everything
void init(void)
{
	// clock
	OSC.CTRL |= OSC_RC32MEN_bm; // turn on 32 MHz oscillator
    while (!(OSC.STATUS & OSC_RC32MRDY_bm)) { }; // wait for it to start
    CCP = CCP_IOREG_gc;
    CLK.CTRL = CLK_SCLKSEL_RC32M_gc; // switch osc
    DFLLRC32M.CTRL = DFLL_ENABLE_bm; // turn on DFLL
       
	//init_PLL(); // this init code is unverified and may cause errors
	    
    // disable JTAG
    CCP = CCP_IOREG_gc;
    MCU.MCUCR = 1;

    
	//init_uarts(); // usart manager

	// UARTs
    usart.set_tx_buffer(usart_txbuf, USART_TX_BUF_SIZE);
    usart.set_rx_buffer(usart_rxbuf, USART_RX_BUF_SIZE);
    usart.begin(UART_BAUD_RATE);
    usart.setup_stream(&usart_stream);
        
    usart_n0.set_tx_buffer(usart_n0_txbuf, NODE_TX_BUF_SIZE);
    usart_n0.set_rx_buffer(usart_n0_rxbuf, NODE_RX_BUF_SIZE);
    usart_n0.begin(NODE_BAUD_RATE);
    xgrid.add_node(&usart_n0);
        
	usart_n1.set_tx_buffer(usart_n1_txbuf, NODE_TX_BUF_SIZE);
    usart_n1.set_rx_buffer(usart_n1_rxbuf, NODE_RX_BUF_SIZE);
    usart_n1.begin(NODE_BAUD_RATE);
    xgrid.add_node(&usart_n1);
        
	usart_n2.set_tx_buffer(usart_n2_txbuf, NODE_TX_BUF_SIZE);
    usart_n2.set_rx_buffer(usart_n2_rxbuf, NODE_RX_BUF_SIZE);
    usart_n2.begin(NODE_BAUD_RATE);
    xgrid.add_node(&usart_n2);
        
	usart_n3.set_tx_buffer(usart_n3_txbuf, NODE_TX_BUF_SIZE);
    usart_n3.set_rx_buffer(usart_n3_rxbuf, NODE_RX_BUF_SIZE);
    usart_n3.begin(NODE_BAUD_RATE);
    xgrid.add_node(&usart_n3);
        
	usart_n4.set_tx_buffer(usart_n4_txbuf, NODE_TX_BUF_SIZE);
    usart_n4.set_rx_buffer(usart_n4_rxbuf, NODE_RX_BUF_SIZE);
    usart_n4.begin(NODE_BAUD_RATE);
    xgrid.add_node(&usart_n4);
        
	usart_n5.set_tx_buffer(usart_n5_txbuf, NODE_TX_BUF_SIZE);
    usart_n5.set_rx_buffer(usart_n5_rxbuf, NODE_RX_BUF_SIZE);
    usart_n5.begin(NODE_BAUD_RATE);
    xgrid.add_node(&usart_n5);
		

    // TCC
    TCC0.CTRLA = TC_CLKSEL_DIV256_gc;
    TCC0.CTRLB = 0;
    TCC0.CTRLC = 0;
    TCC0.CTRLD = 0;
    TCC0.CTRLE = 0;
    TCC0.INTCTRLA = TC_OVFINTLVL_LO_gc;		// this is an overflow interrupt (ref: doc8331.pdf, pg 181)
    //TCC0.INTCTRLA = TC_OVFINTLVL_HI_gc;
	TCC0.INTCTRLB = 0;						// this is for clock compare interrupts (there are 4 avail)
    TCC0.CNT = 0;
    TCC0.PER = 125;
        
    // Interrupts
    //PMIC.CTRL = PMIC_HILVLEN_bm | PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm;
	PMIC.CTRL = PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm;		// ref: doc8331.pdf, pg 138
        
    sei();

	calculate_swarm_id();

	init_I2C();
	fprintf_P(&usart_stream, PSTR("I2C INIT\r\n"));

	init_touch();
	fprintf_P(&usart_stream, PSTR("TOUCH INIT\r\n"));

	init_accel();
	fprintf_P(&usart_stream, PSTR("ACCEL INIT\r\n"));
		
	init_HV();
	fprintf_P(&usart_stream, PSTR("TRANSPARENCY INIT\r\n"));
		
	lcd_init();
	fprintf_P(&usart_stream, PSTR("LCD INIT\r\n"));
	lcd_put_chars("LCD Test"); // TODO fix WARNING: deprecated conversion from string constant to 'char*' [-Wwrite-strings]

	RTC_init_new();
	fprintf_P(&usart_stream, PSTR("RTC INIT\r\n"));

	RGB_LED_init();

	// THESE need to happen AFTER ID assignment:

	//TODO: only block 9 should init for Music Module
	//music functions disabled until ready for use
	//MP3_init(get_swarm_id());
	//fprintf_P(&usart_stream, PSTR("MUSIC INIT\r\n"));
	
	// only block 1 | swarm_id = 'DA4A' | should init for GBC
	GBC_init(get_swarm_id());
	_delay_ms(10);
	fprintf_P(&usart_stream, PSTR("GBC INIT\r\n"));
	
	// init the random number generator
	srand(get_swarm_id());
	
	init_message_tally();

	init_coordinates(get_swarm_id());	
	fprintf_P(&usart_stream, PSTR("COORD INIT\r\n"));

	// set init color as function of version #
	change_swarm_mode(INIT_MODE);
	init_appearance(get_swarm_mode());

}

// Timer tick ISR (1 kHz)
ISR(TCC0_OVF_vect)	// CAUTION: TCC0 is also expected to be used with MOTOR1/FAN1
{
	// TODO (PRIORITY): TRY TO CLEAN OUT THIS ISR, MOVE STUFF ELSEWHERE
	// possibly move stuff to main loop before the wait at the bottom
	// milliseconds increment should be moved to the RTC control
	// xgrid_process should be moved to main loop, but called only once per millisecond, ideally
	
	// Timers
	increment_milliseconds();

	if(is_clock_synchronized_to_neighbor())
	{	
		stay_synchronized_check_and_update();
	}

	if (get_milliseconds() % 2000 == 0)	// was 500, WOAH! TODO
	{
		//swarm_color_ping_send();
		swarm_id_ping_send();
	}	

	// DO BELOW ELSEWHERE !
	
	
	if (get_milliseconds() % 2000 == 0)	// swap provisional 
	{
		update_neighbors_part2();

		uint8_t mode = get_swarm_mode();

		if((mode == SWARM_MODE_UPDOWN)||(mode == SWARM_MODE_SIDE1)||(mode == SWARM_MODE_SIDE2))
		{
			color_by_position();
			printf_P(PSTR("DEBUG mode %u\n\r"), get_swarm_mode());
		}
	}
	
	xgrid.process();
}
