FIRST TIME USERS...

You will need to install on your Windows machine:
- Atmel Studio 6.0 or newer (free from Atmel website, may need to register your email)
- WinAVR: http://sourceforge.net/projects/winavr/files/WinAVR/20100110/
- AVRDUDE: http://www.nongnu.org/avrdude/
- TeraTerm: http://en.sourceforge.jp/projects/ttssh2/releases/
- TeraTerm is recommended, but any terminal emulator program will work

Before you compile, the following files need to be added into your /HexBlocksMain file:
Makefile (literally, the file named Makefile without a filetype) 
buildnum.txt

