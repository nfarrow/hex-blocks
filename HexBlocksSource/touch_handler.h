


#include <avr/io.h>			// used for uint8_t
#include <stdio.h>			// used for printf_P
#include <avr/pgmspace.h>	// used for PSTR

#include "..\HexBlocksDrivers\touch.h"
#include "work_stack.h"
#include "..\HexBlocksMain\xgrid.h"

// CALLED FROM MAIN EVERYTIME THROUGH MAIN LOOP
// this reads the current touch status for the 6 electrodes,
// this ALSO toggles the GPIO LEDs to indicate visually the touch status,
// finally, the touch status is returned (note: will always return 0b00xxxxxx, two MSBs are always 0)
uint8_t touch_update(void);


// CALLED FROM MAIN EVERYTIME THROUGH MAIN LOOP
// this function is part of the "CONTROLLER" processes
// *** ADD YOUR CODE to this routine when key events happen *** 
void handle_touch(uint8_t touch_info);


// DO NOT CALL DIRECTLY
// this routine is called by touch_update()
uint8_t process_touch_single_electrode(uint8_t touch_info, uint8_t electrode_num);

uint8_t check_for_held_keys();


uint8_t electrode_num_from_bm(uint8_t electrode_bm);



uint8_t electrode_bm_from_num(uint8_t electrode_num);



uint16_t get_touch_duration(uint8_t electrode_num);