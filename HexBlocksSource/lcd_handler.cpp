#include "lcd_handler.h"

extern Xgrid xgrid;

uint8_t lcd_seconds;
uint8_t previous_lcd_seconds = 0;

void handle_lcd(void)
{
	lcd_seconds = get_milliseconds()/1000;
	if(lcd_seconds != previous_lcd_seconds)
	{
		previous_lcd_seconds = lcd_seconds;

		char seconds_array[9];
		itoa(lcd_seconds,seconds_array,10);

		lcd_clear_screen();
		
		lcd_put_chars("  SECONDS: "); 
		if(lcd_seconds < 10)
			lcd_put_chars("0"); 
		lcd_put_chars(seconds_array);
		
		// note: the code for time printing to the serial port is in time_manager.cpp (9/7/14)
	}
}