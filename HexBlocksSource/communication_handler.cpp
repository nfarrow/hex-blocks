// communication_handler.cpp

#include "communication_handler.h"


extern Xgrid xgrid;
uint8_t message_distance = 5; 
 
uint32_t message_count[NUM_NEIGHBORS];

// private variables:
char command;
uint8_t command_source;
extern FILE usart_stream; // in main

uint8_t query_context[6];

uint8_t node_bitmask(uint8_t dir)
{
	return (0b00000001 << dir);
}

void init_message_tally()
{
	for(uint8_t t = 0; t < NUM_NEIGHBORS; t++)
		message_count[t]=0;
}

uint8_t tally_message(uint8_t source_node)
{
	if((source_node >=0)&&(source_node < NUM_NEIGHBORS))
		message_count[source_node]++;
	else
		return 0;	// failed tally

	return 1;		// successful tally
}

void inquire_all_neighbors(void)
{
	printf_P(PSTR("neighbor check\n\r"));
	char str[] = "I";	// "I" for id
	Xgrid::Packet pkt;
	pkt.type = MESSAGE_TYPE_QUERY;
	pkt.flags = 0;
	pkt.radius = 1;
	pkt.data = (uint8_t *)str;
	pkt.data_len = 1;
	
	xgrid.send_packet(&pkt);
}

void print_message_count(void)
{
	printf_P(PSTR("message count:\r\n"));
	
	printf_P(PSTR("dir 0:\t%i\r\n"),message_count[0]);
	_delay_ms(1);	// delay for printf to catch up
	
	printf_P(PSTR("dir 1:\t%i\r\n"),message_count[1]);
	_delay_ms(1);	// delay for printf to catch up
	
	printf_P(PSTR("dir 2:\t%i\r\n"),message_count[2]);
	_delay_ms(1);	// delay for printf to catch up
	
	printf_P(PSTR("dir 3:\t%i\r\n"),message_count[3]);
	_delay_ms(1);	// delay for printf to catch up
	
	printf_P(PSTR("dir 4:\t%i\r\n"),message_count[4]);
	_delay_ms(1);	// delay for printf to catch up
	
	printf_P(PSTR("dir 5:\t%i\r\n"),message_count[5]);
	_delay_ms(1);	// delay for printf to catch up
}

void pack_and_send_message(char* message_string, uint8_t msg_type, uint8_t msg_radius)
{
	Xgrid::Packet generic_pkt;

	if (msg_type == MESSAGE_TYPE_QUERY)
	{
		query_context[0] = message_string[0];
		query_context[1] = message_string[0];
		query_context[2] = message_string[0];
		query_context[3] = message_string[0];
		query_context[4] = message_string[0];
		query_context[5] = message_string[0];	
	}	
	if(msg_type == MESSAGE_TYPE_COMMAND)
	{
		printf_P(PSTR("send cmd \"%s\"\n\r"),message_string);
	}
	
	uint8_t length = string_length(message_string);

	//printf("msg: %s (%i)\r\n", message_string, length);

	generic_pkt.type = msg_type;
	generic_pkt.flags = 0;
	generic_pkt.radius = msg_radius;
	generic_pkt.data = (uint8_t*)message_string;	// 'data' field is of type uint8_t*
	generic_pkt.data_len = length;

	xgrid.send_packet(&generic_pkt);
}

void pack_and_send_directional_message(char* message_string, uint8_t msg_type, uint8_t send_mask)
{
	Xgrid::Packet generic_pkt;
	
	uint8_t length = string_length(message_string);

	//printf("dmsg: %s (%i)\r\n", message_string, length);

	generic_pkt.type = msg_type;
	generic_pkt.flags = 0;
	generic_pkt.radius = 1;
	generic_pkt.data = (uint8_t*)message_string;	// 'data' field is of type uint8_t*
	generic_pkt.data_len = length;

	xgrid.send_packet(&generic_pkt, (uint16_t)send_mask);
}

void pack_and_send_directional_message_generic(uint8_t* message_data, uint8_t message_len, uint8_t msg_type, uint8_t send_mask)
{
	Xgrid::Packet generic_pkt;

	//printf("dmsg: %x (%i)\r\n", message_data, message_len);	//??

	generic_pkt.type = msg_type;
	generic_pkt.flags = 0;
	generic_pkt.radius = 1;
	generic_pkt.data = message_data;	// 'data' field is of type uint8_t*
	generic_pkt.data_len = message_len;

	xgrid.send_packet(&generic_pkt, (uint16_t)send_mask);
}

/*
	push_function none and uint8_t are fairly specific packets so we don't need many params; we know it is a message type command 
	data* is the pointer to the beginnng of the command struct which is fed into a buffer
	len is the size of the struct
	struct.detail == 'c' signifies a no arg function/ 'C' is a uint8_t function
	these are read in communication_handler.cpp::rx_pkt
*/
void pack_and_push_function_none(void (*function_ptr)(void), uint8_t send_mask )
{
	
	Xgrid::Packet generic_pkt;

	f_struct address_struct;
	address_struct.f_address = (uint16_t) function_ptr;
	address_struct.arg1 = 0;
	address_struct.detail = (uint8_t) 'c';
	
	uint16_t length = sizeof(address_struct);
	
	generic_pkt.data = (uint8_t *)&address_struct; 
	generic_pkt.flags = 0;
	generic_pkt.type = MESSAGE_TYPE_COMMAND;
	generic_pkt.radius = 1;
	generic_pkt.data_len = length;

	xgrid.send_packet(&generic_pkt, (uint16_t)send_mask);	
}


void pack_and_push_function_uint8(void (*function_ptr)(uint8_t), uint8_t arg, uint8_t send_mask )
{
	
	Xgrid::Packet generic_pkt;
	
	f_struct address_struct;
	address_struct.f_address = (uint16_t) function_ptr;
	address_struct.arg1 = arg;
	address_struct.detail = (uint8_t) 'C';
	
	uint16_t length = sizeof(address_struct);
	
	generic_pkt.data = (uint8_t *)&address_struct;
	generic_pkt.flags = 0;
	generic_pkt.type = MESSAGE_TYPE_COMMAND;
	generic_pkt.radius = 1;
	generic_pkt.data_len = length;

	xgrid.send_packet(&generic_pkt, (uint16_t)send_mask);	

}

uint8_t string_length(char* message_string)
{
	uint8_t i = 0;
	
	while(message_string[i])
	{
		i++;

		if(i == 0)	// this catches a wrap-around condition on uint8
			break;
	}
	return i;
}

void swarm_id_ping_send()
{
	Xgrid::Packet pkt;

	uint16_t my_id = get_swarm_id();

	pkt.type = MESSAGE_TYPE_PING;
	pkt.flags = 0;
	pkt.radius = 1;
	pkt.data = (uint8_t *)&my_id;
	pkt.data_len = sizeof(my_id);
	
	xgrid.send_packet(&pkt);
}

void increment_message_distance(void)
{
	message_distance++;
	
	if(message_distance > 7)
		message_distance = 1;

	printf_P(PSTR("message distance:%i\n\r"), message_distance);
}

uint8_t get_message_distance(void)
{
	return message_distance;
}

// moved from HEXBLOCKSmain.cpp 11/4/14
// declaration for this in xgrid.h line 198:
// void (*rx_pkt)(Packet *pkt);
void rx_pkt(Xgrid::Packet *pkt)
{
	uint8_t message_source_node = pkt->rx_node;
	
	tally_message(message_source_node);

	update_neighbors_part1(message_source_node);
	
	if(pkt->type == MESSAGE_TYPE_PING)
	{;}
	
	else if(pkt->type == MESSAGE_TYPE_COLOR)
	{
		fprintf_P(&usart_stream, PSTR("COLOR pkt [deprecated]\n\r"));
	}
	
	else if(pkt->type == MESSAGE_TYPE_COMMAND)
	{
		
		//receiving a char command
		if (pkt -> flags == 0)
		{
			
			
			char* char_ptr = (char*) pkt->data;
			
			//check if we are working with a function struct or a char
			uint8_t is_func = pkt ->data[0];
		
			command = *char_ptr;
			command_source = pkt->rx_node;
			
			//receiving a function ptr command: no params
			// 99 is the ascii integer for 'c' 
			//structs have uint8_t fields so 'c' is cast into 99, this is the first element of the function struct that has all info about the function
			if (is_func == 99)
			{
				//combining f_address into a uint16_t, is there a more clever way to do this?
				uint16_t func_left = pkt->data[2];
				func_left = func_left << 8;
				uint8_t func_right = pkt->data[1];
				uint16_t f_address = 0x0000;
				
				f_address =  (f_address | func_left) | func_right;
				
				//DEBUG: uncomment this if you want to see what address is being pushed
				//printf_P(PSTR("function(void) pointer received with address %p\r\n"), f_address);
				//_delay_ms(5);
				
				command_source = pkt -> rx_node;

				//recast from uint16 to void (*) (void)
				void (*func_ptr)(void) = (void(*)(void)) (f_address);

				// if a snake command, set the successor as whoever sent you the block 
				if (func_ptr == snake_head)
					//DEBUG (works fine as of 2/10/15)
					//printf_P(PSTR("Snake head setting successor as %d in communication handler"), command_source);
					//_delay_ms(5);
					{push_function_uint8(set_successor, command_source, '?', WORK_SOURCE_SELF, 0);}
		
		
				push_function_none(func_ptr, '?', command_source, 0);				
			}
			
			//67 is ascii code for 'C' which signifies command with arguments
			else if (is_func == 67)
			{
				uint16_t func_left = pkt->data[2];
				func_left = func_left << 8;
				uint8_t func_right = pkt->data[1];
				uint16_t f_address = 0x0000;
				
				f_address =  (f_address | func_left) | func_right;
				uint8_t f_arg = pkt->data[3];
				
				/* debug statements
				printf_P(PSTR("function(uint8_t) pointer received with address %p\r\n"), f_address);
				_delay_ms(5);
				printf_P(PSTR("Argument = %d\r\n"), f_arg);
				_delay_ms(5);			
				*/
				
				command_source = pkt -> rx_node;
				
				void (*func_ptr)(uint8_t) = (void(*)(uint8_t)) f_address;
			
				push_function_uint8(func_ptr, f_arg, '?', command_source, 0);		
					
			}
			
			//if not a function pointer, send to command decoder
			else
			{
				
			
			//fprintf_P(&usart_stream, PSTR("command source: "));print_rx_node_by_name(command_source);
			fprintf_P(&usart_stream, PSTR("cmd: '%c' src: "),command); print_rx_node_by_name(command_source);
	 
			fprintf_P(&usart_stream, PSTR("A COMMAND! what this?"));
		
			command_decoder(command, command_source);
			}			
		}	

		
	}

	else if(pkt->type == MESSAGE_TYPE_MENU_SYSTEM)
	{
		char* char_ptr = (char*) pkt->data;
	
		command = *char_ptr;
		command_source = pkt->rx_node;
		
		//fprintf_P(&usart_stream, PSTR("command source: "));print_rx_node_by_name(command_source);
		fprintf_P(&usart_stream, PSTR("MENU: '%c' src: "),command); print_rx_node_by_name(command_source);
		
		if(command == 'm')
		{
			go_to_menu_system(command_source);
		}
		else if(command == 'q')
			quit_second_color_mode();
		
		//TODO for now we are handling this here in main, integrate into the scheduler soon
		//push_work_queue(command, command_source, 0);
	}

	else if(pkt->type == MESSAGE_TYPE_QUERY)
	{
		
		
		char inquiry;
		uint8_t inquiry_source;
		char* char_ptr = (char*) pkt->data;
		inquiry = *char_ptr;
		inquiry_source = pkt->rx_node;
		
		uint8_t reply_send_mask;

		int reply_num;

		fprintf_P(&usart_stream, PSTR("(%i)inquiry: %c\r\n"), inquiry_source, inquiry);
		//fprintf_P(&usart_stream, PSTR("inq source: "));print_rx_node_by_name(inquiry_source);
		//_delay_ms(1);	// delay for printf

		switch(inquiry)
		{
			case 'I':	// 'I' for id
				fprintf_P(&usart_stream, PSTR("will send: %04x\n\r"), get_swarm_id());

				reply_num = get_swarm_id();
				reply_send_mask = 0x01 << inquiry_source;

				pack_and_send_directional_message_generic(
											(uint8_t*)&reply_num,
											sizeof(reply_num),
											MESSAGE_TYPE_REPLY,
											reply_send_mask);
				break;

			case 'C':	// 'C' for coordinates
				fprintf_P(&usart_stream, PSTR("will send coords\n\r"));
				assign_neighbor_center_coord(inquiry_source);
				break;
				
			case 'S':	// 'S' for snake programs
				fprintf_P(&usart_stream, PSTR("snake query\n\r"));
			
				//efficiency increase: in new implementation of call-response we will already know who is the body, so don't check him
				reply_num = get_body_status();
				reply_send_mask = 0x01 << inquiry_source;

				pack_and_send_directional_message_generic(
				(uint8_t*)&reply_num,
				sizeof(reply_num),
				MESSAGE_TYPE_REPLY,
				reply_send_mask);
				break;
				
			default:
				fprintf_P(&usart_stream, PSTR("UNKNOWN\n\r"));
				break;
		}
	}
	
	else if(pkt->type == MESSAGE_TYPE_REPLY)
	{
		// NOTE: right now, all replies are assumed to be IDs
		
		int reply;
		uint8_t reply_source;
		int* int_ptr = (int*) pkt->data;
		reply = *int_ptr;
		reply_source = pkt->rx_node;
		
		fprintf_P(&usart_stream, PSTR("(%i)reply: %04X\r\n"), reply_source, reply);	// this reply contains a swarm_id from the neighbor
		_delay_ms(1);
		//fprintf_P(&usart_stream, PSTR("re src: "));print_rx_node_by_name(reply_source);
		//fprintf_P(&usart_stream, PSTR("\r\n"));
		//_delay_ms(2);	// for print to catch up

		if (query_context[reply_source] == 'S')
		{
			reply = 1 - reply; //if is_body == 1, that means we want it to be a legal move so fill in that location as a 1
			snake_update_neighbors(reply_source, reply);
			// -> legal_moves[source] = reply
			// update legal moves with responder and what status they are (is_body = 0 is a legal move)
		}
	}

	else if(pkt->type == MESSAGE_TYPE_TOUCH_EVENT)
	{
		fprintf_P(&usart_stream, PSTR("TOUCH packet type received\n\r"), pkt->type);
		
		
		//char staus_char = pkt->data;
		char staus_char = *(pkt->data);
		
		fprintf_P(&usart_stream, PSTR("touch msg: '%c'\r\n"), staus_char);
		
		switch(staus_char)
		{
			case 'N':

			break;
		}
	}

	else if(pkt->type == MESSAGE_TYPE_COOR_ASSIGN)
	{
		//printf_P(PSTR("%u \t"),pkt->rx_node);
		//process_coor_packet((coordinate_assignment_struct*) pkt->data,MESSAGE_TYPE_COOR_ASSIGN,pkt->source_id);
		process_coor_packet((coordinate_assignment_struct*) pkt->data,MESSAGE_TYPE_COOR_ASSIGN);
		
		_delay_ms(1);
		printf_P(PSTR("c-MAIL node %u\n\r"), pkt->rx_node);
	}

	else if(pkt->type == MESSAGE_TYPE_TIME_SYNCH)
	{
		uint16_t sent_time;

		uint8_t message_source;
		
		//uint16_t curr_time = milliseconds;
		
		uint16_t* time_ptr = (uint16_t*) pkt->data;
		//command_received = true;
		sent_time = *time_ptr;
		message_source = pkt->rx_node;


		update_time_accurate(sent_time, message_source);

		/*
		if(message_source | time_source)
		{
			fprintf_P(&usart_stream, PSTR("TIME packet received (OK)\n\r"));
			fprintf_P(&usart_stream, PSTR("us: %u them: %u\n\r"), curr_time, sent_time);
		}
		*/

	}

	else
	{
		// These BOGEY messages have occasionally shown up randomly while working with the project,
		// I believe they may be 'behind the scenes' messages handled by XGRID (perhaps pings to aid
		// with viral programming?), I am printing them with obnoxious print statements so that they
		// are obvious when detected, try to determine their existance/purpose
		// alternatively, they could just be static electricity? IDK
		if(pkt->type == MESSAGE_TYPE_BOGEY1)
			fprintf_P(&usart_stream, PSTR("BOGEY1 INBOUND dir %u [%02x]\n\r"), pkt->rx_node, pkt->source_id);
		if(pkt->type == MESSAGE_TYPE_BOGEY2)
			fprintf_P(&usart_stream, PSTR("BOGEY2 INBOUND dir %u [%02x]\n\r"), pkt->rx_node, pkt->source_id);

		fprintf_P(&usart_stream, PSTR("UNKNOWN pkt type ERR: %i\n\r"), pkt->type);
	}
}







// OLD CODE ////////////////////////////////////////////////////////////////////////

/*
void swarm_color_ping_send()
{
	//NEW
	color_msg_data.red = my_red;
	color_msg_data.green = my_green;
	color_msg_data.blue = my_blue;
	
	Xgrid::Packet pkt;

	pkt.type = MESSAGE_TYPE_PING;
	pkt.flags = 0;
	pkt.radius = 1;
	pkt.data = (uint8_t *)&color_msg_data;
	pkt.data_len = sizeof(color_msg_data);
	
	xgrid.send_packet(&pkt);
}*/