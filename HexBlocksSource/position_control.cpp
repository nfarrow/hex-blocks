

#include "position_control.h"



extern Xgrid xgrid;

//extern uint8_t swarm_mode; // in mode_control.cpp

bool is_top = false;
bool is_bottom = false;
bool is_side1_top = false;
bool is_side1_bottom = false;
bool is_side2_top = false;
bool is_side2_bottom = false;

bool provisional_is_top = true;
bool provisional_is_bottom = true;
bool provisional_is_side1_top = true;
bool provisional_is_side1_bottom = true;
bool provisional_is_side2_top = true;
bool provisional_is_side2_bottom = true;




#define NUM_NEIGHBORS 6	// only defined here



void print_provisional_postion(void)
{
	printf_P(PSTR("provisional position\n\r"));
	_delay_ms(1);
	printf_P(PSTR("bottom: %u\n\r"), provisional_is_bottom);
	_delay_ms(1);
	printf_P(PSTR("left bot: %u\n\r"), provisional_is_side1_bottom);
	_delay_ms(1);
	printf_P(PSTR("left top: %u\n\r"), provisional_is_side2_top);
	_delay_ms(1);
	printf_P(PSTR("top: %u\n\r"), provisional_is_top);
	_delay_ms(1);
	printf_P(PSTR("right top: %u\n\r"), provisional_is_side1_top);
	_delay_ms(1);
	printf_P(PSTR("right bot: %u\n\r"), provisional_is_side2_bottom);
}



void print_known_position(void)
{
	printf_P(PSTR("known position\n\r"));
	_delay_ms(1);
	printf_P(PSTR("bottom: %u\n\r"), is_bottom);
	_delay_ms(1);
	printf_P(PSTR("left bot: %u\n\r"), is_side1_bottom);
	_delay_ms(1);
	printf_P(PSTR("left top: %u\n\r"), is_side2_top);
	_delay_ms(1);
	printf_P(PSTR("top: %u\n\r"), is_top);
	_delay_ms(1);
	printf_P(PSTR("right top: %u\n\r"), is_side1_top);
	_delay_ms(1);
	printf_P(PSTR("right bot: %u\n\r"), is_side2_bottom);
}


uint8_t update_neighbors_part1(uint8_t source_node)
{
	if((source_node >=0)&&(source_node < NUM_NEIGHBORS))
	{
		if((source_node == 0)&&provisional_is_bottom)
		provisional_is_bottom = false;

		if((source_node == 1)&&provisional_is_side1_bottom)
		provisional_is_side1_bottom = false;

		if((source_node == 2)&&provisional_is_side2_top)
		provisional_is_side2_top = false;

		if((source_node == 3)&&provisional_is_top)
		provisional_is_top = false;

		if((source_node == 4)&&provisional_is_side1_top)
		provisional_is_side1_top = false;

		if((source_node == 5)&&provisional_is_side2_bottom)
		provisional_is_side2_bottom = false;
	}
	
	else
	{
		printf_P(PSTR("UNKNOWN packet source: %i\n\r"), source_node);
		return 1;	// error
	}
	
	return 0;
}

uint8_t is_top_block()
{
	return is_top;
}


void update_neighbors_part2()
{
	// TODO: refactor this so that it no longer uses terms like is_top, but rather has_top_neighbor, possibly
	
	is_top = provisional_is_top;
	is_bottom = provisional_is_bottom;
	is_side1_top = provisional_is_side1_top;
	is_side1_bottom = provisional_is_side1_bottom;
	is_side2_top = provisional_is_side2_top;
	is_side2_bottom = provisional_is_side2_bottom;


	provisional_is_bottom = true;	// assume you are top/bottom until proven otherwise
	provisional_is_top = true;
	provisional_is_side1_top = true;
	provisional_is_side1_bottom = true;
	provisional_is_side2_top = true;
	provisional_is_side2_bottom = true;
}


void color_by_position(void)
{
	uint8_t mode = get_swarm_mode();
	
	if((mode == SWARM_MODE_UPDOWN)&&((is_top)&&(!is_bottom)))
		set_primary_color(0,255,0);
	
	if((mode == SWARM_MODE_UPDOWN)&&((!is_top)&&(is_bottom)))
		set_primary_color(255,0,0);
	
	if((mode == SWARM_MODE_UPDOWN)&&((is_top)&&(is_bottom)))
		set_primary_color(255,255,0);

	if((mode == SWARM_MODE_UPDOWN)&&((!is_top)&&(!is_bottom)))
		set_primary_color(0,0,255);

	if((mode == SWARM_MODE_SIDE1)&&((is_side1_top)&&(!is_side1_bottom)))
		set_primary_color(0,255,0);

	if((mode == SWARM_MODE_SIDE1)&&((!is_side1_top)&&(is_side1_bottom)))
		set_primary_color(255,0,0);

	if((mode == SWARM_MODE_SIDE1)&&((is_side1_top)&&(is_side1_bottom)))
		set_primary_color(255,255,0);

	if((mode == SWARM_MODE_SIDE1)&&((!is_side1_top)&&(!is_side1_bottom)))
		set_primary_color(0,0,255);

	if((mode == SWARM_MODE_SIDE2)&&((is_side2_top)&&(!is_side2_bottom)))
		set_primary_color(0,255,0);

	if((mode == SWARM_MODE_SIDE2)&&((!is_side2_top)&&(is_side2_bottom)))
		set_primary_color(255,0,0);

	if((mode == SWARM_MODE_SIDE2)&&((is_side2_top)&&(is_side2_bottom)))
		set_primary_color(255,255,0);

	if((mode == SWARM_MODE_SIDE2)&&((!is_side2_top)&&(!is_side2_bottom)))
		set_primary_color(0,0,255);

}

void print_rx_node_by_name(uint8_t node_num)
{
	switch(node_num)
	{
		case RX_NODE_BOTTOM:		printf_P(PSTR("[0 BOT]\n\r"));		break;
		case RX_NODE_RIGHT_BOT:		printf_P(PSTR("[1 RT BOT]\n\r"));		break;
		case RX_NODE_RIGHT_TOP:		printf_P(PSTR("[2 RT TOP]\n\r"));		break;
		case RX_NODE_TOP:			printf_P(PSTR("[3 TOP]\n\r"));			break;
		case RX_NODE_LEFT_TOP:		printf_P(PSTR("[4 LT TOP]\n\r"));	break;
		case RX_NODE_LEFT_BOT:		printf_P(PSTR("[5 LT BOT]\n\r"));	break;
		default:					printf_P(PSTR("UNKNOWN\n\r"));			break;
	}
}

uint8_t has_neighbor(uint8_t direction_num)
{
	uint8_t answer = 0;

	switch(direction_num)
	{
		case 0:
			answer = !is_bottom;
			break;
		case 1:
			answer = !is_side1_bottom;
			break;
		case 2:
			answer = !is_side2_top;
			break;
		case 3:
			answer = !is_top;
			break;
		case 4:
			answer = !is_side1_top;
			break;
		case 5:
			answer = !is_side2_bottom;
			break;
	}

	return answer;
}