#include "work_stack.h"

/* #DEFINES FOR #ifdef ::
//#define QUE_DEBUG_PRINT
*/

#define MAX_WORK_HEIGHT 16

int8_t work_stack_height = 0;
int8_t max_work_height_seen = 0;	// debugging variable

extern char   __BUILD_NUMBER;	// (original source unknown)

extern Xgrid xgrid; // original in main

// private variables
struct work_struct *work_que_head_ptr = NULL;

uint8_t que_sorted;




void command_decoder(uint8_t command_symbol, uint8_t command_source)
{
	// ultimately, we want to use a look up table, to find the function associated with the symbol, then either
	// 1) return the address of that function			(does not do this)
	// 2) directly push that function onto the stack	(does this some of the time)
	// 3) call the function directly					(does this most of the time)


	/* stole these for menu mode, do we need these? */
	//if(command_symbol == '1')
	//	printf_P(PSTR("GOTO side-1-mode\r\n"));
	//else if(command_symbol == '2')
	//	printf_P(PSTR("GOTO side-2-mode\r\n"));
	//else if(command_symbol == '3')
	//	printf_P(PSTR("GOTO side-3-mode\r\n"));

	if(command_symbol == 'a')
		push_function_none(accel_status, '?', command_source, 0);	// this a test of push functions, no need to actually push this

	else if(command_symbol == 'b')
		lcd_set_backlight(0,0,1);	// no prototype for pushing this yet
				
	else if(command_symbol == 'B')
	{
		if(command_source == WORK_SOURCE_KEYBOARD)
			pack_and_send_message("B", MESSAGE_TYPE_COMMAND, get_message_distance());  // TODO fix WARNING: deprecated conversion from string constant to 'char*' [-Wwrite-strings]

		push_function_uint8(change_swarm_mode, SWARM_MODE_BLUE, '?', command_source, 0);	// this a test of push functions, no need to actually push this
	}
	
	//Don't use c or C: This is reserved as a context symbol for commands in communication handler::rx_pkt()
	//Lets function know we are sending a function pointer
	//else if (command_symbol == 'c')
	//else if (command_symbol == 'C')

	else if(command_symbol == 'd')
		increment_message_distance();
	
	else if(command_symbol == 'D')
	{
		printf_P(PSTR("GOTO color-drops-mode\r\n"));
		if(command_source == WORK_SOURCE_KEYBOARD)
			pack_and_send_message("D", MESSAGE_TYPE_COMMAND, get_message_distance());
	
		change_swarm_mode(SWARM_MODE_COLOR_DROPS);	
	}		
	
	else if(command_symbol == 'f')
	{
		
		camTakePicture();
		GBC_print_image();
	}
	
	else if(command_symbol == 'F')
	{
		//GBC_fake_image();
		
		save_pixel(80,85,100);
		_delay_ms(5);
		save_pixel(81,85,120);
		_delay_ms(5);
		save_pixel(82,85,100);
		_delay_ms(5);
		save_pixel(83,85,140);
		_delay_ms(5);
		
		uint8_t pixels[4];
		pixels[0] = load_pixel(80, 85);
		_delay_ms(5);
		pixels[1] = load_pixel(81, 85);
		_delay_ms(5);
		pixels[2] = load_pixel(82, 85);
		_delay_ms(5);
		pixels[3] = load_pixel(83, 85);
		_delay_ms(5);
		printf_P(PSTR("LOADED %u %u %u %u\r\n"),pixels[0],pixels[1],pixels[2],pixels[3]);
	}

	else if(command_symbol == 'g')
	{
		GBC_fake_image();
		GBC_print_image();
	}

	
	else if(command_symbol == 'G')
	{
		
		if(command_source == WORK_SOURCE_KEYBOARD)
			pack_and_send_message("G", MESSAGE_TYPE_COMMAND, get_message_distance());  // TODO fix WARNING: deprecated conversion from string constant to 'char*' [-Wwrite-strings]
		
		push_function_uint8(change_swarm_mode, SWARM_MODE_GREEN, '?', command_source, 0);
	}	

	else if(command_symbol == 'i')
		check_all_of_I2C();

	else if(command_symbol == 'k')
	{
		if(command_source == WORK_SOURCE_KEYBOARD)
		{
			printf_P(PSTR("kill everyone\n\r"));
			pack_and_send_message("k", MESSAGE_TYPE_COMMAND, get_message_distance());
		}
		
		push_function_none(xboot_reset, '?', command_source, 2000);
	}

	else if(command_symbol == 'm')
		print_current_mode();

	else if(command_symbol == 'M')
	{
		printf_P(PSTR("reserved for music module\r\n"));
		
		/* music player functions commented until able to test them */
		
		//printf_P(PSTR("MUSIC RESET\n\r"));
		
		//MP3_init();
		
		//_delay_ms(2000);
		
		MP3_Command(0);
		printf_P(PSTR("song: %u\n\r"),0);
		_delay_ms(5000);
		
		MP3_Command(1);
		printf_P(PSTR("song: %u\n\r"),1);
		_delay_ms(5000);
		
		MP3_Command(2);
		printf_P(PSTR("song: %u\n\r"),2);
		_delay_ms(5000);
	}
	
	else if(command_symbol == 'n')		
		inquire_all_neighbors();

	else if(command_symbol == 'o')
		print_coordinates(); // if the coords are unknown, this will also check with neighbors
		
	else if(command_symbol == 'p')
	{
		print_provisional_postion();
		print_known_position();
	}
	else if (command_symbol == 'r')
	{
		if(command_source == WORK_SOURCE_KEYBOARD)
			pack_and_send_message("r", MESSAGE_TYPE_COMMAND, get_message_distance());
		push_function_uint8(change_swarm_mode, SWARM_MODE_YELLOW, '?', command_source, 0);
	}

	else if (command_symbol == 'R')
	{
		if(command_source == WORK_SOURCE_KEYBOARD)
			pack_and_send_message("R", MESSAGE_TYPE_COMMAND, get_message_distance());
		push_function_uint8(change_swarm_mode, SWARM_MODE_RED, '?', command_source, 0);
	}
	


	else if(command_symbol == 'T')
	{
		change_swarm_mode(SWARM_MODE_TRANSPARENT);
		
		// TODO?: move the next 4 lines to change swarm mode?
		if(is_opaque())
			go_transparent();
		else
			go_opaque();

		if(command_source == WORK_SOURCE_KEYBOARD)
			pack_and_send_message("T", MESSAGE_TYPE_COMMAND, get_message_distance());
	}
					
	else if(command_symbol == 'v')
		printf_P(PSTR("avr-xgrid build %ld\r\n"), (unsigned long) &__BUILD_NUMBER);

	else if(command_symbol == 'y')
	{
		// go to a menu system
		pack_and_send_message("m", MESSAGE_TYPE_MENU_SYSTEM, 1/*message_distance*/);
		// do it yourself, others pull this command from the message que
		go_to_menu_system(6);
	}
	
	else if(command_symbol == 'Y')
	{	
		// quit the menu system (THIS SHOULD REALLY ONLY BE CALLED FROM THE MENU CENTER BLOCK)
		pack_and_send_message("q", MESSAGE_TYPE_MENU_SYSTEM, 1/*message_distance*/);
		// do it yourself, others pull this command from the message que
		quit_menu_system();
	}
	
	//DELETE ME ASAP! implementation for activating menu by touch panels got wiped with new work stack framework;
	//using this to debug snake for now so I don't have to drop everything and rewrite that
	else if (command_symbol == 's')
	{
		pack_and_send_message("B", MESSAGE_TYPE_COMMAND, get_message_distance());
		push_function_uint8(change_swarm_mode, SWARM_MODE_BLUE, '?', command_source, 0);	
		
		_delay_ms(10);
		push_function_none(snake_head, '?', WORK_SOURCE_SELF, 0);
	}	
	else if(command_symbol == 'Z')
	{
		master_coor_assign();
		assign_coords_all_neighbors();
	}
	
	else if ( command_symbol == 'Q')
	{
		//quit menu system: these needs to be here because we need to be sure ALL blocks are off the menu 
		push_function_none(quit_menu_system, '?', WORK_SOURCE_SELF, 0);
		quit_menu_system();
		quit_second_color_mode();
		printf_P(PSTR("Quitting Menu system"));
		_delay_ms(2);
	}
	
	else if (command_symbol == 'P')
	{
		if(command_source == WORK_SOURCE_KEYBOARD)
			pack_and_send_message("P", MESSAGE_TYPE_COMMAND, get_message_distance());
		push_function_uint8(change_swarm_mode, SWARM_MODE_PURPLE, '?', command_source, 0);
	}


	else if(command_symbol == '0')
	{
		
		/* this is our normal tap input*/
		if(command_source == WORK_SOURCE_TOUCH_TAP)
		{
			//printf_P(PSTR("dir node = %d\r\n"), get_dir_node()); debug statement
			
			//check if we are in menu mode to activate a special mode, 
			//otherwise do nothing
			
			
			if (is_in_menu_mode() && get_dir_node() == 0)
			{
				printf_P(PSTR("activated a menu option: random walk\n\r"));
				pack_and_send_message("Q", MESSAGE_TYPE_COMMAND, get_message_distance()); //quit menu system
				pack_and_send_message("B", MESSAGE_TYPE_COMMAND, get_message_distance()); //swarm mode blue for background
				_delay_ms(10);
				
				//fire up snake
				push_function_none(snake_head, '?', WORK_SOURCE_SELF, 0);
			}
			
		}
		
		
		/*  
			Plate 0 gets a special hold trigger to activate menu mode
			This will work on any plate; after this occurs any of its 6 
			neighbors can be tapped to activate a unique menu option
		*/
		if(command_source == WORK_SOURCE_TOUCH_HOLD)
		{
			coordinates_f electrode_pos = get_electrode_coordf(0);
			printf_P(PSTR("X: %05.3f\tY: %05.3f\r\n"), electrode_pos.x, electrode_pos.y);
			
			if (is_in_menu_mode())
			{
				printf_P(PSTR("Quitting menu mode from touch input command\n\r"));
				
				pack_and_send_message("Q", MESSAGE_TYPE_COMMAND, get_message_distance()); //quit menu system
				
				change_swarm_mode(SWARM_MODE_BLUE); //redundanT?
				pack_and_send_message("B", MESSAGE_TYPE_COMMAND, get_message_distance());
			}
			else{
				printf_P(PSTR("Entering menu mode from touch input command\n\r"));
				pack_and_send_message("m", MESSAGE_TYPE_MENU_SYSTEM, 1/*get_message_distance()*/);
				go_to_menu_system(6);
				go_to_second_color_mode(6);
				
				//we set menu coords because all adjacent blocks go to menu mode; however we only want to be 
				//able to activate menu options from the touch panels directly adjacent to the menu block
				//for example: when in menu mode, touch plate 0 sends a message down and checks if that is the
				// menu block.  If yes, then we activate our menu option
				
			}
			
			
		}
	}
	
	else if(command_symbol == '1')
	{
		
		if(command_source == WORK_SOURCE_TOUCH_TAP)
		{
			//printf_P(PSTR("dir node = %d\r\n"), get_dir_node());
			coordinates_f electrode_pos = get_electrode_coordf(1);
			printf_P(PSTR("X: %05.3f\tY: %05.3f\r\n"), electrode_pos.x, electrode_pos.y);
			
			
			if (is_in_menu_mode() && get_dir_node() == 1)
			{
				printf_P(PSTR("activated a menu option: Swarm mode red\n\r"));
				pack_and_send_message("Q", MESSAGE_TYPE_COMMAND, get_message_distance());
				change_swarm_mode(SWARM_MODE_RED);
				pack_and_send_message("R", MESSAGE_TYPE_COMMAND, get_message_distance());
			}
						
			//change_swarm_mode(SWARM_MODE_BLUE);
		    //pack_and_send_message("B", MESSAGE_TYPE_COMMAND, get_message_distance());	
			
			// quit the menu system TODO re-implement this section
			//pack_and_send_message("q", MESSAGE_TYPE_MENU_SYSTEM, 1/*message_distance*/);
			// do it yourself, others pull this command from the message que
			//quit_menu_system();		
		}
		
		else	// then is from neighbor or keyboard
		{
			printf_P(PSTR("goto 1 MODE\n\r"));
			change_swarm_mode(SWARM_MODE_SIDE1);
						
			if(command_source == WORK_SOURCE_KEYBOARD)
			{
				pack_and_send_message("1", MESSAGE_TYPE_COMMAND, get_message_distance());
			}
		}
	}

	else if(command_symbol == '2')
	{
		
		if(command_source == WORK_SOURCE_TOUCH_TAP)
		{
			//printf_P(PSTR("dir node = %d\r\n"), get_dir_node()); debug statement
			coordinates_f electrode_pos = get_electrode_coordf(2);
			printf_P(PSTR("X: %05.3f\tY: %05.3f\r\n"), electrode_pos.x, electrode_pos.y);
			
			
			if (is_in_menu_mode() && get_dir_node() == 2)
			{
				printf_P(PSTR("activated a menu option: Swarm mode green\n\r"));
				pack_and_send_message("Q", MESSAGE_TYPE_COMMAND, get_message_distance());
				change_swarm_mode(SWARM_MODE_GREEN);
				pack_and_send_message("G", MESSAGE_TYPE_COMMAND, get_message_distance());
			}

						
			//change_swarm_mode(SWARM_MODE_GREEN);
			//pack_and_send_message("G", MESSAGE_TYPE_COMMAND, get_message_distance());	
		}

		else	// then is from neighbor or keyboard
		{
			printf_P(PSTR("goto 2 MODE\n\r"));
			change_swarm_mode(SWARM_MODE_SIDE2);
			
			if(command_source == WORK_SOURCE_KEYBOARD)
			{
				pack_and_send_message("2", MESSAGE_TYPE_COMMAND, get_message_distance());
			}
		}
	}

	else if(command_symbol == '3')
	{
		
		if(command_source == WORK_SOURCE_TOUCH_TAP)
		{
			//printf_P(PSTR("dir node = %d\r\n"), get_dir_node()); debug statement
			coordinates_f electrode_pos = get_electrode_coordf(3);
			printf_P(PSTR("X: %05.3f\tY: %05.3f\r\n"), electrode_pos.x, electrode_pos.y);
			
			if (is_in_menu_mode() && get_dir_node() == 3)
			{
				printf_P(PSTR("activated a menu option: Swarm mode yellow\n\r"));
				pack_and_send_message("Q", MESSAGE_TYPE_COMMAND, get_message_distance());
				change_swarm_mode(SWARM_MODE_YELLOW);
				pack_and_send_message("r", MESSAGE_TYPE_COMMAND, get_message_distance());
			}

			
				
		}
		
		else	// then is from neighbor or keyboard
		{
			printf_P(PSTR("goto 3 MODE\n\r"));
			change_swarm_mode(SWARM_MODE_UPDOWN);
			
			if(command_source == WORK_SOURCE_KEYBOARD)
			{
				pack_and_send_message("3", MESSAGE_TYPE_COMMAND, get_message_distance());
			}
		}	
	}

	else if(command_symbol == '4')
	{
		
		if(command_source == WORK_SOURCE_TOUCH_TAP)
		{
			//printf_P(PSTR("dir node = %d\r\n"), get_dir_node()); debug statement
			coordinates_f electrode_pos = get_electrode_coordf(4);
			printf_P(PSTR("X: %05.3f\tY: %05.3f\r\n"), electrode_pos.x, electrode_pos.y);
			//go_opaque();
						
			
			if (is_in_menu_mode() && get_dir_node() == 4)
			{
				printf_P(PSTR("activated a menu option: Swarm mode blue\n\r"));
				pack_and_send_message("Q", MESSAGE_TYPE_COMMAND, get_message_distance());
				change_swarm_mode(SWARM_MODE_BLUE);
				pack_and_send_message("B", MESSAGE_TYPE_COMMAND, get_message_distance());
			}


			/* old command before changing to menu type system */
			//change_swarm_mode(SWARM_MODE_PURPLE);
			//pack_and_send_message("P", MESSAGE_TYPE_COMMAND, message_distance);
		}
	}

	else if(command_symbol == '5')
	{
		
		if(command_source == WORK_SOURCE_TOUCH_TAP)
		{
			//printf_P(PSTR("dir node = %d\r\n"), get_dir_node()); debug statement
			coordinates_f electrode_pos = get_electrode_coordf(5);
			printf_P(PSTR("X: %05.3f\tY: %05.3f\r\n"), electrode_pos.x, electrode_pos.y);
			
			if (is_in_menu_mode() && get_dir_node() == 5)
			{
				printf_P(PSTR("activated a menu option: Swarm mode purple\n\r"));
				pack_and_send_message("Q", MESSAGE_TYPE_COMMAND, get_message_distance());
				change_swarm_mode(SWARM_MODE_PURPLE);
				pack_and_send_message("P", MESSAGE_TYPE_COMMAND, get_message_distance());
			}
	
			//go_transparent();
			
			//change_swarm_mode(SWARM_MODE_YELLOW);
			//pack_and_send_message("Y", MESSAGE_TYPE_COMMAND, get_message_distance());
		}
	}



	
	else
	{
		printf_P(PSTR("unknown command symbol: %c\n\r"), command_symbol);
	}
	
}

uint8_t push_work_queue(work_struct work)
{
	if((work_stack_height < MAX_WORK_HEIGHT)&&(work_stack_height >= 0))
	{	// queue has room available
		
		work_struct *work_que_new_ptr;

		work_que_new_ptr = (work_struct*)malloc(sizeof(work_struct));
		
		work_que_new_ptr->detail = work.detail;
		work_que_new_ptr->origin = work.origin;
		work_que_new_ptr->execute_time_ms = work.execute_time_ms;
		work_que_new_ptr->execute_time_min = work.execute_time_min;	
		// new fields:
		work_que_new_ptr->f_address = work.f_address;
		work_que_new_ptr->f_type = work.f_type;
		work_que_new_ptr->uint8_arg = work.uint8_arg;
		
		// insert the new work at the head (bottom)
		work_que_new_ptr->next = work_que_head_ptr;
		work_que_head_ptr = work_que_new_ptr;
		
		#ifdef QUE_DEBUG_PRINT
			printf_P(PSTR("QUE%[i]:%c %u:%05u\r\n"),work_stack_height,new_work,(uint8_t)future_min,(uint16_t)future_ms);
			_delay_ms(1);
		#endif
	
		work_stack_height++;
		// debug variable, update if necessary
		if(work_stack_height > max_work_height_seen)
			max_work_height_seen = work_stack_height;
		
		if(work_stack_height > 1)
			// TODO: consider removing the sort step if pushed item had 0 delay, and should be executed immediately
			que_sorted = 0; // we pushed to the bottom, queue is now possibly out of order
		else
			que_sorted = 1; // only one thing in the queue to do (so its sorted)
	}

	else
	{
		printf_P(PSTR("WORK QUE FULL [%i]\n\r"),work_stack_height);
		return 0;	// fail
	}
	
	return 1;	// success
}

// this function simply pops the first item off of the list
struct work_struct pop_work_queue(void)
{
	struct work_struct current_work;
	
	work_struct *work_que_next_ptr;

	if(work_stack_height >= 1)	// this should ALWAYS be true before reaching this point
	{
		work_que_next_ptr = work_que_head_ptr->next;
		
		current_work.detail = work_que_head_ptr->detail;
		current_work.origin = work_que_head_ptr->origin;
		current_work.next = NULL; // probably unnecessary
		// new fields:
		current_work.f_address = work_que_head_ptr->f_address;
		current_work.f_type = work_que_head_ptr->f_type;
		current_work.uint8_arg = work_que_head_ptr ->uint8_arg;
		#ifdef QUE_DEBUG_PRINT
			printf_P(PSTR("PULL[%i]: %c %u\n\r"),work_stack_height,current_work.detail, get_milliseconds());_delay_ms(1);
		#endif

		work_que_head_ptr->next = NULL; // clears memory of previous info, preventing garbage data from appearing significant
		
		free(work_que_head_ptr);
		work_que_head_ptr = work_que_next_ptr;
		
		work_stack_height--;
	}

	else
	{
		printf_P(PSTR("FAIL EMPTY QUEUE!!\n\r"),work_que_head_ptr->execute_time_ms - get_milliseconds());
		// error, you caused a crash
		// you were about to return a garbage struct because there is really no work to do
		// currently, this is solved by not calling this function if work_stack_empty() returns 0
		halt_program(0,255,0);
		
	}

	return current_work;	// this with cause a bug for sure!!
}


// this function rolls the que over until it finds an item that is past due, then leaves that item in the front
// this function should be re-written so that it sorts the que (bubble sort maybe?) and leaves is sorted, so that
// in future calls it only needs to check the front item (and bubble if something new was added (to the font))
uint8_t work_queue_has_work(void)
{
	if(work_que_head_ptr == NULL)
		return 0;
	
	if(!que_sorted)
	{	// bubble sort with linked lists (bubble sort is efficient for us, since we only insert at the bottom)
		sort_work_que(0);
	}

	// now that que is sorted, can just check if first item is due
	if(time_in_order(get_minutes(),get_milliseconds(),work_que_head_ptr->execute_time_min,work_que_head_ptr->execute_time_ms,0))
		return 0;	// our first item is not yet due
	else
		return 1;	// our first item is due
}

uint8_t sort_work_que(uint8_t verify_sort)
{
	#ifdef QUE_DEBUG_PRINT
		printf_P(PSTR("sort "));
	#endif
	
	uint8_t swapped;
	work_struct *work_que_cursor1;
	work_struct *work_que_cursor2;
	
	work_struct *work_que_cursor3;		// used only after swap_nodes
	
	uint8_t in_order = 0;
	
	if(verify_sort)
	{
		in_order = 1;
		
		work_que_cursor1 = work_que_head_ptr;
		while(work_que_cursor1->next != NULL)
		{
			work_que_cursor2 = work_que_cursor1->next;
			if(!time_in_order(work_que_cursor1->execute_time_min,work_que_cursor1->execute_time_ms,work_que_cursor2->execute_time_min,work_que_cursor2->execute_time_ms,0))
				in_order = 0;
			
			work_que_cursor1 = work_que_cursor2;
		}
	}
	
	do{
		#ifdef QUE_DEBUG_PRINT
			printf_P(PSTR("-"));
		#endif
		
		work_que_cursor1 = work_que_head_ptr;
		swapped = 0;		
		
		while(work_que_cursor1->next != NULL)
		{
			#ifdef QUE_DEBUG_PRINT
				printf_P(PSTR("+"));
			#endif
			
			work_que_cursor2 = work_que_cursor1->next;
			
			if(!time_in_order(work_que_cursor1->execute_time_min, work_que_cursor1->execute_time_ms, work_que_cursor2->execute_time_min, work_que_cursor2->execute_time_ms, 0))
			{	
				swap_nodes(work_que_cursor1,work_que_cursor2);
				
				swapped = 1;

				work_que_cursor3 = work_que_cursor1;
				work_que_cursor1 = work_que_cursor2;
				work_que_cursor2 = work_que_cursor3;
				work_que_cursor3 = NULL;
			}
			
			work_que_cursor1 = work_que_cursor2;
		}
			
	}while(swapped);
	
	#ifdef QUE_DEBUG_PRINT
		printf_P(PSTR(":\n\r"));
	#endif
		
	que_sorted = 1; // this is 'global' within this library
	
	return in_order;
}

uint8_t push_function_none(void (*function)(void), char new_work, uint8_t work_source, uint16_t delay_ms)
{

	work_struct work;

	// TODO: write a time struct, and function that takes delay_ms and returns these parameters in a time struct
	if(delay_ms > 60000)
	{
		// error!
		printf_P(PSTR("CANT PLAN > 60 sec OUT!!"));
		halt_program(0,255,0);
	}
	
	uint32_t future_ms = (uint32_t)get_milliseconds() + (uint32_t)delay_ms;
	uint8_t future_min = get_minutes();

	// check if the event occurs after the seconds roll over
	if(future_ms > 60000)
	{
		future_ms -= 60000;
		future_min++;
	}
	// TODO (above) /////////////////////////////////////////////////////////////////////////////////////////////

	work.detail = new_work;
	work.execute_time_min = future_min;
	work.execute_time_ms = future_ms;
	work.origin = work_source;
	work.f_address = (uint16_t)function;
	work.f_type = F_TYPE_NO_ARGS;
	work.uint8_arg = 0;


	//printf_P(PSTR("Pushing function address %x to work stack\r\n"),work.f_address);
	//_delay_ms(20);
	
	return push_work_queue(work);
}

uint8_t push_function_uint8(void (*function)(uint8_t), uint8_t arg, char new_work, uint8_t work_source, uint16_t delay_ms)
{

	work_struct work;

	// TODO: write a time struct, and function that takes delay_ms and returns these parameters in a time struct
	if(delay_ms > 60000)
	{
		// error!
		printf_P(PSTR("CANT PLAN > 60 sec OUT!!"));
		halt_program(0,255,0);
	}
	
	uint32_t future_ms = (uint32_t)get_milliseconds() + (uint32_t)delay_ms;
	uint8_t future_min = get_minutes();

	// check if the event occurs after the seconds roll over
	if(future_ms > 60000)
	{
		future_ms -= 60000;
		future_min++;
		//printf_P(PSTR("do next min @%u"), delay_ms);
	}
	// TODO (above) /////////////////////////////////// MERGE DUPLICATE CODE ///////////////////////////////

	work.detail = new_work;
	work.execute_time_min = future_min;
	work.execute_time_ms = future_ms;
	work.origin = work_source;
	
	work.f_address = (uint16_t)function;
	work.f_type = F_TYPE_1_UINT8;
	work.uint8_arg = arg;

	return push_work_queue(work);
}
 
// work_detail: (a char representing some task)
// work_source: (node that told of work (0-5), or keyboard(6), whichever)
//void execute_task(char work_detail, uint8_t work_source)
void execute_task(work_struct work)
{
	// TODO: this variable scheduled for removal
	// currently, if work_detail is not '?', then see why not
	char work_detail = work.detail;
			
	// this may be useful information, but currently not used?
	// TODO: check in command_decoder to see if it is useful there?
	// (e.g. node that told of work (0-5), or keyboard(6), whichever, for menu functions, etc.)
	uint8_t work_source = work.origin;	
	
	if(work.f_type == 0)
	{
		void (*function)(void);
		function = (void (*)())work.f_address;

		function();
	}

	else if(work.f_type == 1)
	{
		void (*function)(uint8_t);
		function = (void (*)(uint8_t))work.f_address;

		function(work.uint8_arg);
	}
	
	else
	{
		printf_P(PSTR("unknown function type: %u\n\r"), work.f_type);
		halt_program(0,0,0);
	}
	
	if(work_detail != '?')
	{
		// TODO: delete this when it has been identified that the 'detail' field is no longer necessary
		_delay_ms(3); // brief delay to wrap up current print
		printf_P(PSTR("this work has detail: %c\n\r"), work_detail);
	}
}

void print_work_que()
{
	printf_P(PSTR("que height: %i [%i]\n\r"),work_stack_height, max_work_height_seen);
	
	if(work_stack_height >= 1)
	{
		struct work_struct *work_que_print_ptr = NULL;
		
		work_que_print_ptr = work_que_head_ptr;
		
		uint8_t job_num = 1;
		
		while(work_que_print_ptr != NULL)
		{
			//printf_P(PSTR(" job%i:%c %u:%02u.%03u\n\r"),job_num, work_que_print_ptr->detail,
			//	work_que_print_ptr->execute_time_min,
			//	work_que_print_ptr->execute_time_ms/1000,
			//	work_que_print_ptr->execute_time_ms - work_que_print_ptr->execute_time_ms/1000);
			uint16_t int_part, frac_part;
			int_part = work_que_print_ptr->execute_time_ms/1000;
			frac_part = work_que_print_ptr->execute_time_ms - (int_part*1000);
			
			printf_P(PSTR(" job%i:%c "), job_num, work_que_print_ptr->detail);
			_delay_ms(1);
			printf_P(PSTR("%u:%02u.%03u\n\r"), work_que_print_ptr->execute_time_min, int_part, frac_part);
			_delay_ms(1);
			
			work_que_print_ptr = work_que_print_ptr->next;
			
			job_num++;
		}
	}
}

// return:
// 1 if time1 < time2
// 2 if time1 = time2
// 0 otherwise
uint8_t time_in_order(uint8_t min1, uint16_t ms1, uint8_t min2, uint16_t ms2, uint8_t print_it)
{
	if(min2 > min1)
	{
		if(print_it)
			printf_P(PSTR("%u:%02u < %u:%02u\n\r"), min1, ms1/1000, min2, ms2/1000);
		return 1;
	}
	if(min2 == min1)
	{
		if(ms2 > ms1)
		{
			if(print_it)
				printf_P(PSTR("%u:%02u < %u:%02u\n\r"), min1, ms1/1000, min2, ms2/1000);
			return 1;
		}
		if(ms2 == ms1)
		{
			if(print_it)
				printf_P(PSTR("%u:%02u == %u:%02u\n\r"), min1, ms1/1000, min2, ms2/1000);
			return 2;
		}
	}

	if(print_it)
		printf_P(PSTR("%u:%02u > %u:%02u !\n\r"), min1, ms1/1000, min2, ms2/1000);

	return 0;	// false, time is not in order
}

void swap_nodes(work_struct *cursor1, work_struct *cursor2)
{
	struct work_struct *work_que_temp_ptr;

	//printf_P(PSTR("swap %c <-> %c\n\r"), cursor1->detail, cursor2->detail);
	
	// verify that cursor1->next == cursor2 (verify they are in the order we think they are)
	if(cursor1->next != cursor2)
	{
		printf_P(PSTR("ERROR: CURSORS INVALID !\n\r"));
		halt_program(0,0,0);
		return;
	}
	
	work_que_temp_ptr = work_que_head_ptr;

	if(work_que_temp_ptr == cursor1)
	{
		// then cursor1 is on the bottom, nothing else points to it except the head_ptr
		cursor1->next = cursor2->next;
		cursor2->next = cursor1;
		work_que_head_ptr = cursor2;
	}
	else
	{
		// scroll along the list until you find the first cursor
		while(work_que_temp_ptr->next != cursor1)
		{	
			work_que_temp_ptr = work_que_temp_ptr->next;
		
			if(work_que_temp_ptr->next == NULL)
			{
				printf_P(PSTR("ERROR CURSOR 1 NOT FOUND !"));
				halt_program(0,0,0);
				return;
			}
		}
	
		cursor1->next = cursor2->next;
		work_que_temp_ptr->next = cursor2;
		cursor2->next = cursor1;
	}

	//printf_P(PSTR("verify %c <-> %c\n\r"), cursor1->detail, cursor2->detail);
}

