

#ifndef INCFILE_WORK_STACK_H_
#define INCFILE_WORK_STACK_H_


#include <util/delay.h>
#include <stdio.h>			// used for printf_P
#include <avr/pgmspace.h>	// used for PSTR
#include <stdlib.h>			// for itoa

// includes from main
#include "..\HexBlocksMain\xgrid_maintenance.h"

// includes form drivers
#include "..\HexBlocksDrivers\accelerometer.h"
#include "..\HexBlocksDrivers\HexBlockI2C.h"
#include "..\HexBlocksDrivers\high_voltage.h"
#include "..\HexBlocksDrivers\lcd.h"
#include "..\HexBlocksDrivers\MP3_player.h"
#include "..\HexBlocksDrivers\HB_GBC.h"

// includes from source
#include "mode_control.h"
#include "appearance_control.h"
#include "snake_program.h"
#include "communication_handler.h"
#include "position_control.h"
#include "time_manager.h"
#include "touch_handler.h"
#include "coord.h"

// allowed values for work_source (things that are allowed to add functions to the priority queue)
#define WORK_SOURCE_NODE_0			0
#define WORK_SOURCE_NODE_1			1
#define WORK_SOURCE_NODE_2			2
#define WORK_SOURCE_NODE_3			3
#define WORK_SOURCE_NODE_4			4
#define WORK_SOURCE_NODE_5			5
#define WORK_SOURCE_KEYBOARD		6
#define WORK_SOURCE_SELF			9
#define WORK_SOURCE_TOUCH_TAP		10
#define WORK_SOURCE_TOUCH_HOLD		11
#define WORK_SOURCE_TOUCH_RELEASE	12
#define WORK_SOURCE_GESTURE			20

// function types that can be pushed to work stack
#define F_TYPE_NO_ARGS			0
#define F_TYPE_1_UINT8			1

struct work_struct
{
	char detail;
	uint8_t origin;
	uint16_t execute_time_ms;
	uint8_t execute_time_min;

	// variables to describe the function that is being stored
	uint8_t f_type;			// enumerated function type (what parameters is takes)
	uint16_t f_address;		// 16-bit address in memory

	// variables kept for every function on the stack, regardless if they are needed
	uint8_t uint8_arg;
	
	struct work_struct *next;
};

/*
	f_structs are created in communication_handler.cpp::pack_and_push_function_none and _uint8_t
	they contain all the details we need about the function being sent
	pointer to struct being created is placed in data* field of xgrid packets
*/
struct f_struct {
	uint8_t detail;
	uint16_t f_address;
	uint8_t arg1;
	
} ;

/* USER ACESSABLE FUNCTIONS */

// for push_function_xxxx:
// xxxx is the type of function you are pushing (defined only by what parameters it takes)
// arg1, arg2, ... are the arguments to that function
// new_work is a 'char' representing/describing the work (this parameter is scheduled to be deleted ASAP)
// work_source uses #defined uints to specify who is adding the work to the priority queue
// delay_ms is the time delay between when function is pushed and when it will be executed
// returns 1 if new_work was added to queue,
// returns 0 if priority queue is full (new_work NOT added)

uint8_t push_function_none(void (*function)(void), char new_work, uint8_t work_source, uint16_t delay_ms);

uint8_t push_function_uint8(void (*function)(uint8_t), uint8_t arg, char new_work, uint8_t work_source, uint16_t delay_ms);

void print_work_que();


/* AMBIGUOUS IF USER SHOULD CALL */

// this is called to 'decode' (interpret) the single-char commands typed by the
// user into executable function calls, some functions are executed immediately,
// the rest of the functions are pushed to the priority queue 
void command_decoder(uint8_t command_symbol, uint8_t command_source);

uint8_t work_queue_has_work(void);


/* PRIVATE FUNCTIONS - DO NOT CALL DIRECTLY */

// returns 1 if new_work was added to queue,
// returns 0 if queue is full (new_work NOT added)
uint8_t push_work_queue(work_struct work);

// returns 0x00 if no work, else returns the char representing the work to do
work_struct pop_work_queue(void);

uint8_t sort_work_que(uint8_t verify_sort);

void execute_task(work_struct work);

uint8_t time_in_order(uint8_t min1, uint16_t ms1, uint8_t min2, uint16_t ms2, uint8_t print_it);

void swap_nodes(work_struct *cursor1, work_struct *cursor2);


#endif //INCFILE_WORK_STACK_H_