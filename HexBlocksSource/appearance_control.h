
#include <avr/io.h>			// used for uint8_t
#include <stdio.h>			// used for printf_P
#include <avr/pgmspace.h>	// used for PSTR
#include <stdlib.h>			// used for rand

#include "..\HexBlocksMain\xgrid.h"

#include "mode_control.h"	// for #defines of modes
#include "..\HexBlocksDrivers\RGB_LED.h"
#include "work_stack.h"

//Nicks Favorite Colors
// MAUVE 35 57 26


void init_appearance(uint8_t swarm_mode);

void set_primary_color(uint8_t red, uint8_t green, uint8_t blue);

void set_secondary_color(uint8_t red, uint8_t green, uint8_t blue);

void print_color_status(void);

void color_update();

void color_update_scheduled();	// calls color_update and reschedules itself 

void enter_random_color_mode(); // this initiates recursive random color mode

void get_with_the_program();	// asks neighbor node what mode it should be in (appropriate on boot up)

void go_to_second_color_mode(uint8_t dir_node_for_now);

void quit_second_color_mode(void);

uint8_t get_dir_node();