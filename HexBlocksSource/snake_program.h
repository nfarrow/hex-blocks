#ifndef INCFILE_SNAKE_PROGRAM_H_
#define INCFILE_SNAKE_PROGRAM_H_


#include <stdio.h>
#include <stdlib.h>
#include <avr/io.h>			// used for uint8_t
#include <math.h>


#include "../HexBlocksMain/xgrid_maintenance.h"
#include "position_control.h"
#include "coord.h"		//for has_neighbors



/*
Author: Andrew Gordon 
Created: 9/19/2014

Program for a snake or pixel to do a random walk (TODO: turn it into a game). A pixel is just a snake of length 1.
This is meant to be an interactive menu option
*/

//prop_delay defines how fast the snake moves
#define PROP_DELAY_ms 750
#define INQUIRY_DELAY_ms 300


//member functions
void move(void);	//move a random direction distance 1
uint8_t check_legal_moves(void);		//check for legal moves, return true or false 
void snake_query_neighbor(void);  //need a definite call-response to check is_body/is_head facts
void set_color(uint8_t newred, uint8_t newgreen, uint8_t newblue); //set color
void snake_head(void);
void snake_body(void);
void snake_tail(void);
void kill_snake(void);
void snake_move(void);

//getters and setters
uint8_t get_snake_length(void);
uint8_t get_body_status(void);
uint8_t get_head_status(void);
void snake_update_neighbors(uint8_t source, uint8_t status);
void set_successor(uint8_t new_successor);
uint8_t get_successor();

#endif