/*
 * IncFile1.h
 *
 * Created: 10/22/2013 12:05:11 PM
 *  Author: plant
 */ 
#ifndef INCFILE1_H_
#define INCFILE1_H_

#include <stdio.h>
#include <stdlib.h>

#include "work_stack.h"
#include "communication_handler.h"
#include "../HexBlocksMain/xgrid_maintenance.h"


// COMMENTS ON FUNCTION:
// Self-assignment of coordinates has been tested to work, but is certainly not robust to failure
// there is no protection against spurious coordinate (re)assignment [e.g. bad packet can trigger reassignment cascade]
// there is also no guarantee against assignment cycles crashing the entire wall (although this is unlikely to occur)
// if the seed block is in the wall, then all coordinates should be assigned within 5 second of boot up, then no further coordinate system packets will be sent
// if a block does not have coordinates assigned, it will attempt to ask its neighbors about once every 5 seconds until it gets a positive response
// any time a block has its coordinates assigned (or reassigned), it will immediately pass this information forward to neighbors [cascade]

// there are really only two user functions to call
// 1) get_electrode_coordf( number )	-> returns struct (x,y) floats
// 2) get_center_coordf()				-> returns struct (x,y) floats
// 3) print_coordinates()				-> should be more for debugging really, no need to print coords under general operation unless a serial cable is attached


// which block will seed the coordinates upon boot-up?
// you may choose to not have a seed by:
//	1) commenting out all of the options
//	2) not placing the seed block in the wall
// if there is no seed, the origin will need to be manually specified
// [this is the known ID list] 

//#define SEED_COORDINATE_ID 0xDA4A	// block 1 (also is camera block)
//#define SEED_COORDINATE_ID 0xB64A	// block 2
//#define SEED_COORDINATE_ID 0x0B4B	// block 3
//#define SEED_COORDINATE_ID ??		// block 4
//#define SEED_COORDINATE_ID ??		// block 5
//#define SEED_COORDINATE_ID 0xAE47	// block 6 (also is music block)
//#define SEED_COORDINATE_ID 0xE645	// block 7
//#define SEED_COORDINATE_ID ??		// block 8
//#define SEED_COORDINATE_ID 0x3E40	// block 9
//#define SEED_COORDINATE_ID 0x1643	// block 10
//#define SEED_COORDINATE_ID 0xCE45	// block 11
//#define SEED_COORDINATE_ID 0xAE44	// block 12
//#define SEED_COORDINATE_ID 0xCE46	// block 13 (also is LCD block)
//#define SEED_COORDINATE_ID ??		// block 14
//#define SEED_COORDINATE_ID ??		// block 15

// COMMENTS ON COORDINATES:
// There is only 1 'origin' block in the structure (if it exists, see above).
// The origin has integer coordinates (0,0), same for float valued coords.
// The UNITS being used for measurement are 'block half units'
// in general: 
//				neighbor		x-coord		y-coord
//				--------		-------		-------
//				self			X			Y		
//				0				X			Y - 2
//				1				X + 2		Y - 1
//				2				X + 2		Y + 1
//				3				X			Y + 2
//				4				X - 2		Y + 1
//				5				X - 2		Y - 1

#define AVG_BLOCK_HALF_WIDTH_SPACE_FILLING 5.5985 // inches (NDF, may be off a little, but not much) 
// avg. width = this is "half-way up the trapezoid"
#define BLOCK_HALF_HEIGHT_SPACE_FILLING 4.691 // inches (NDF, close to exact) 

struct coordinates 
{	// integer valued coordinates (use these preferred)
	int8_t x_coord;
	int8_t y_coord;

	bool assigned;	// if false, x_coord & y_coord are garbage
};

struct coordinates_f
{	// float valued coordinates (in inches)
	float x;
	float y;
};


struct coordinate_assignment_struct{
	uint16_t swarm_id;
	uint8_t rank;
	int x_coor; // Coordinates on grid for center and touch plates
	int y_coor;
	bool assigned;
};


// USER ACESSABLE FUNCTIONS:

coordinates_f get_electrode_coordf(uint8_t elec_num);

coordinates_f get_center_coordf(void);

void print_coordinates(void);

coordinates get_self_coord(void);

// INIT (call as the last init!)

void init_coordinates(uint16_t swarmID);

// USERS ARE NOT TO CALL THE FOLLOWING:

void master_coor_assign();

void assign_neighbor_center_coord(uint8_t node_num);

void assign_coords_all_neighbors();

void process_coor_packet(coordinate_assignment_struct* received_coor, uint8_t message_type);


void check_random_neighbor_for_coords(void);

#endif /* INCFILE1_H_ */