#ifndef MODE_CONTROL_H
#define MODE_CONTROL_H


#include <avr/io.h>			// used for uint8_t
#include <stdio.h>			// used for printf_P
#include <avr/pgmspace.h>	// used for PSTR

#include <util/delay.h>

#include "..\HexBlocksMain\xgrid.h"
#include "communication_handler.h"
#include "appearance_control.h"


#define SWARM_MODE_RANDOM		0
#define SWARM_MODE_WHITE		1
#define SWARM_MODE_UPDOWN		2
#define SWARM_MODE_SIDE1		3
#define SWARM_MODE_SIDE2		4
#define SWARM_MODE_COLOR_DROPS	5
#define SWARM_MODE_RED			6
#define SWARM_MODE_TRANSPARENT	7
#define SWARM_MODE_GREEN		8
#define SWARM_MODE_BLUE			9
#define SWARM_MODE_YELLOW		10
#define SWARM_MODE_PURPLE		11
#define SWARM_MODE_ORANGE		12		

#define SWARM_MODE_DISPLAY_VERSION_NUMBER	99	// VERSION_NUMBER synonym BUILD_NUMBER

void change_swarm_mode_pointable(void* arg);

//void change_swarm_mode(uint8_t new_mode, uint8_t forward_mode_info);
void change_swarm_mode(uint8_t new_mode);

uint8_t get_swarm_mode(void);

void print_current_mode(void);

// this takes you to the user interface
void go_to_menu_system(uint8_t lead_node_dir);

void quit_menu_system();

uint8_t is_in_menu_mode();

#endif