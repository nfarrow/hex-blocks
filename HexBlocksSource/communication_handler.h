
#include <avr/io.h>			// used for uint8_t
#include <stdio.h>			// used for printf_P
#include <avr/pgmspace.h>	// used for PSTR
#include <util/delay.h>		// used for _delay_ms


#include "coord.h" // for assign_neighbor_center_coord
//#include "snake_program.h"

#define NUM_NEIGHBORS 6


#include "..\HexBlocksMain\xgrid.h"
#include "..\HexBlocksMain\xgrid_maintenance.h"

#define MESSAGE_TYPE_PING			0
#define MESSAGE_TYPE_COMMAND		1
#define MESSAGE_TYPE_COLOR			2
#define MESSAGE_TYPE_QUERY			3
#define MESSAGE_TYPE_REPLY			4
#define MESSAGE_TYPE_COOR_ASSIGN	5
#define MESSAGE_TYPE_MENU_SYSTEM	10
#define MESSAGE_TYPE_TOUCH_EVENT	20
#define MESSAGE_TYPE_TIME_SYNCH		55
#define MESSAGE_TYPE_BOGEY1			68
#define MESSAGE_TYPE_BOGEY2			69
// warning, message types 68,69 seem important, possibly for reprogramming, dont use

#define NODE_0_bm	0b00000001
#define NODE_1_bm	0b00000010
#define NODE_2_bm	0b00000100
#define NODE_3_bm	0b00001000
#define NODE_4_bm	0b00010000
#define NODE_5_bm	0b00100000
#define NODE_ALL_bm 0b00111111


//previous definition of f_struct, nooo.....include tree stuff@!
//what do?  Probably keep all pack_and_push function params the same, and declare a function struct in the function
//init the values of the struct with the appropriate args, address, etc.  message_len = size(f_struct) and data = &f_struct 
//this way xgrid knows how many bytes to read in and starts at beginning of f_struct



// this is the general functional form of the node bitmasks above
uint8_t node_bitmask(uint8_t dir);

void init_message_tally();

uint8_t tally_message(uint8_t source_node);

void inquire_all_neighbors(void);

void print_message_count(void);

void pack_and_send_message(char* message_string, uint8_t msg_type, uint8_t msg_radius);

void pack_and_send_directional_message(char* message_string, uint8_t msg_type, uint8_t send_mask);

void pack_and_send_directional_message_generic(uint8_t* message_data, uint8_t message_len, uint8_t msg_type, uint8_t send_mask);

void pack_and_push_function_none(void (*function_ptr)(void), uint8_t send_mask );

void pack_and_push_function_uint8(void (*function_ptr)(uint8_t), uint8_t arg, uint8_t send_mask );

uint8_t string_length(char* message_string);

void swarm_id_ping_send(void);

void increment_message_distance(void);

uint8_t get_message_distance(void);