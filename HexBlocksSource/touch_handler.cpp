

#include "touch_handler.h"


#define MESSAGE_TYPE_TOUCH_EVENT 20

#define HELD_KEY_DECISION_TIME_ms	500


extern uint16_t milliseconds;	// in main

// (NDF 11/24/13, verified, these are consistent with node packet directions)
#define ELECTRODE_0_bm	0b00100000
#define ELECTRODE_1_bm	0b00010000
#define ELECTRODE_2_bm	0b00001000
#define ELECTRODE_3_bm	0b00000100
#define ELECTRODE_4_bm	0b00000010
#define ELECTRODE_5_bm	0b00000001


#define TOUCH_TAP_bm				0b00000000
#define TOUCH_HOLD_bm				0b01000000
#define TOUCH_HOLD_RELEASE_bm		0b10000000
#define TOUCH_EVENT_bm				0b11000000
#define TOUCH_ELECTRODES_bm			0b00111111

Xgrid::Packet touch_pkt;

extern Xgrid xgrid;

static uint16_t last_action_time_ms[6];	// in reality, this is only touch-down time
static uint8_t keys_held = 0;
static uint8_t keys_reported_held = 0;
static uint8_t last_touch_status = 0;
static uint8_t touch_backlog = 0;

uint8_t touch_update()
{
	uint8_t touch_status;
	uint8_t process_result = 0;
	uint8_t touch_diff;
	uint8_t first_touch_processed = 0; // boolean

	if(verify_touch_existance())
	{
		if(touch_backlog) // if there is a backlog, handle this INSTEAD of getting a new measurement
		{			
			for(uint8_t e = 0; e < 6; e++)
			{
				if((touch_backlog>>(5-e)) == 1)
				{
					process_result = process_touch_single_electrode(last_touch_status, e);

					// remove this electrode from the backlog list
					touch_backlog &= ~electrode_bm_from_num(e);

					e = 6; // force break out of for-loop
				}
			}
		}
		
		else
		{
			touch_status = touch_read(0x00);	// Read touch status

			uint8_t touch_toggle;
			
			if((touch_diff = touch_status ^ last_touch_status)) // assignment equals, XOR
			{
				touch_toggle = 0;
				
				// electrode 0 --> LED 11
				// electrode 1 --> LED 10
				// electrode 2 --> LED 9
				// electrode 3 --> LED 8
				// electrode 4 --> LED 7
				// electrode 5 --> LED 6

				if(touch_diff & 0b00000001)
				touch_toggle |= 0b10000000;
				
				if(touch_diff & 0b00000010)
				touch_toggle |= 0b01000000;

				if(touch_diff & 0b00000100)
				touch_toggle |= 0b00100000;

				if(touch_diff & 0b00001000)
				touch_toggle |= 0b00010000;

				if(touch_diff & 0b00010000)
				touch_toggle |= 0b00001000;

				if(touch_diff & 0b00100000)
				touch_toggle |= 0b00000100;
				
				touch_write(GPIO_TOGGLE, touch_toggle);
			
				// a pre-check to see if more than one electrode status has changed at the same time...
				if(	  (touch_diff != 0b00000001)
				&&(touch_diff != 0b00000010)
				&&(touch_diff != 0b00000100)
				&&(touch_diff != 0b00001000)
				&&(touch_diff != 0b00010000)
				&&(touch_diff != 0b00100000)
				&&(touch_diff != 0b00000000))
				{
					printf_P(PSTR("SIMULTANEOUS TOUCH\r\n"));
					//double_touch_status = DOUBLE_TOUCH_IDENTIFIED;
				}

				for(uint8_t e = 0; e < 6; e++)
				{
					if(((touch_diff>>(5-e))&1) == 1) // bitwise &		[WARNING]
					{
						//printf_P(PSTR("TF %u\r\n"),e);
						//if(double_touch_status == DOUBLE_TOUCH_IDENTIFIED)
						if(!first_touch_processed)
						{
							process_result = process_touch_single_electrode(touch_status, e);
							//double_touch_status == DOUBLE_TOUCH_PROCESSED;
							first_touch_processed = 1;
						}
						else
						{	
							touch_backlog |= electrode_bm_from_num(e);
							//printf_P(PSTR("addback %u\r\n"),e);
						}
					}
				}
			}

			else // no touch_diff
			{
				process_result = check_for_held_keys();
			}

			// only update last_touch_status if a measurement actually happened
			last_touch_status = touch_status;	// this writes to a global-private variable
		}
	}
	
	else
	{
		process_result = 0;	// sorta-error, touch does not exist
	}
	
	return process_result;
}



void handle_touch(uint8_t touch_info)
{
// ************** ANYTHING YOU DO WITH TOUCH INPUT SHOULD GO IN HERE, PUSHING WORK IS ENCOURAGED ****************//

	if(touch_info & TOUCH_ELECTRODES_bm)	// check if there is non-zero input
	{
		uint8_t electrode_num = electrode_num_from_bm(touch_info & TOUCH_ELECTRODES_bm);
		
		/*
		coordinates_f electrode_pos = get_electrode_coordf(electrode_num);
		printf_P(PSTR("X: %05.3f\tY: %05.3f\r\n"), electrode_pos.x, electrode_pos.y);
		*/

		if((touch_info & TOUCH_EVENT_bm) == TOUCH_TAP_bm)
		{
			printf_P(PSTR("key tapped: %u\r\n"), electrode_num);

			command_decoder('0'+electrode_num, WORK_SOURCE_TOUCH_TAP);
			// push...
		}

		else if((touch_info & TOUCH_EVENT_bm) == TOUCH_HOLD_bm)
		{
			printf_P(PSTR("key held: %u\r\n"), electrode_num);
			
			command_decoder('0'+electrode_num, WORK_SOURCE_TOUCH_HOLD);
			// push...
		}
		
		else if((touch_info & TOUCH_EVENT_bm) == TOUCH_HOLD_RELEASE_bm)
		{
			printf_P(PSTR("key release: %u\r\n"), electrode_num);

			command_decoder('0'+electrode_num, WORK_SOURCE_TOUCH_RELEASE);
			// push...
		}
	}

// ************** ANYTHING YOU DO WITH TOUCH INPUT SHOULD GO ABOVE HERE, PUSHING WORK IS ENCOURAGED ****************//
}

uint8_t process_touch_single_electrode(uint8_t touch_info, uint8_t electrode_num)
{
	// INPUTS:
	// touch_info = 0x00xxxxxx (x = 0/1, 1 = currently activated electrodes)
	// electrode_num = [0,5]

	uint16_t touch_duration;

	uint8_t electrode_bm = electrode_bm_from_num(electrode_num);

	uint8_t touch_return = 0;	// changes value if a key is released OR a key is determined to be held

	if(touch_info & electrode_bm)	// this is a new 'key-press' event
	{
		last_action_time_ms[electrode_num] = milliseconds; // record the time the touching began

		keys_held |= electrode_bm;
	}
		
	else // this is a new 'key-release' event
	{	// if you are here, then there is for sure an event to report (tap/hold release)
		touch_duration = get_touch_duration(electrode_num);

		keys_held &= ~electrode_bm;
			
		if(touch_duration <= HELD_KEY_DECISION_TIME_ms)
		{
			touch_return = electrode_bm | TOUCH_TAP_bm;
			//GOOD DEBUG PRINT:printf_P(PSTR("tap: %u (%u)\r\n"), electrode_num, touch_duration);
		}
		else
		{
			//keys_reported_held &= ~touch_difference;
			keys_reported_held &= ~electrode_bm;
			touch_return = electrode_bm | TOUCH_HOLD_RELEASE_bm;
			//GOOD DEBUG PRINT:printf_P(PSTR("finally free!: %u (%u)\r\n"), electrode_num, touch_duration);
		}
	}

	return touch_return;
}


uint8_t check_for_held_keys()
{
	uint8_t held_return = 0;
	
	if(keys_held != keys_reported_held) // are there any keys being held that we should report on?
	{
		// check if any of these keys have been held for > 500 ms (decision time)
		// and if so, report them as being held

		uint8_t temp_bm;

		for(uint8_t e_num = 0; e_num < 6; e_num++)
		{
			temp_bm = electrode_bm_from_num(e_num);
			if(keys_held & ~keys_reported_held & temp_bm)	// bitwise &'s
			{
				if(get_touch_duration(e_num) > HELD_KEY_DECISION_TIME_ms)
				{
					held_return = temp_bm | TOUCH_HOLD_bm;
					keys_reported_held |= temp_bm;
					//GOOD DEBUG PRINT:printf_P(PSTR("held yo: %u (%u)\r\n"), e_num, get_touch_duration(e_num));
					e_num = 6; // force break out of for-loop so only one key's data is returned at a time (breaks a tie)
				}
			}
		}
	}

	return held_return;
}


uint8_t electrode_num_from_bm(uint8_t electrode_bm)
{
	switch(electrode_bm)
	{
		case ELECTRODE_0_bm:
			return 0;
			break;
		case ELECTRODE_1_bm:
			return 1;
			break;
		case ELECTRODE_2_bm:
			return 2;
			break;
		case ELECTRODE_3_bm:
			return 3;
			break;
		case ELECTRODE_4_bm:
			return 4;
			break;
		case ELECTRODE_5_bm:
			return 5;
			break;
		default:
			printf_P(PSTR("ERROR 298\r\n"));
	}
	return 9;
}	

uint8_t electrode_bm_from_num(uint8_t electrode_num)
{
	if(electrode_num < 6)
	{
		return 1 << (5-electrode_num);
	}

	else
	{
		printf_P(PSTR("ERROR 299 (%u)\r\n"), electrode_num);
		
		return 0;	// error
	}
}

uint16_t get_touch_duration(uint8_t electrode_num)
{
	uint16_t duration;

	if(milliseconds < last_action_time_ms[electrode_num])	
	{
		// then the clock wrapped around during the touch event
		// this two step arithmetic correctly accounts for wrap-around time, without overflowing itself
		duration = 60000 - last_action_time_ms[electrode_num];		
		duration += milliseconds;
		// TODO: no hard-coded numbers! (60000 is clock wrap-around constant = 60 seconds)
	}

	else
	{
		duration = milliseconds - last_action_time_ms[electrode_num];
	}

	return duration;
}


