// coord.cpp

// when you press 'z' through the serial port, the block becomes the master and disperses the coordinates

// TODO: okay, I did something stupid... I put the seek out coordinates function inside the print_coords() routine
// so that in order to begin the seek operations, you must first attempt to print your coords,
// and only when you realize you dont have coords, then you seek them out,
// finally, I also made the print_coords function recursive by pushing the command 'o' onto the stack,
// this will call the print_coords() again later incase the neigbor you checked with didn't know
// ALL OF THE ABOVE IS A BAD WAY TO DO THINGS, FIX THIS AND THEN DESCRIBE BETTER WHAT IS GOING ON



#include "coord.h"

uint8_t told_neighbors = 0; 

coordinates my_coord;

// this needs to be called AFTER the work stack init and AFTER ID assignment
void init_coordinates(uint16_t swarmID)
{
	#ifdef SEED_COORDINATE_ID
		if(swarmID == SEED_COORDINATE_ID)
		{ // winner! I'm the origin.
			//master_coor_assign();
			push_work_queue('Z', WORK_SOURCE_SELF, 3000);	// TODO: magic numbers erase
		}

		else
		{
			push_work_queue('o', WORK_SOURCE_SELF, rand()%6000); // currently this is set to call print_coordinates()
		}

	#endif

	/*	trying something new: if no seed is defined, then blocks do not actively seek out coords
	#else
		push_work_queue('o', WORK_SOURCE_SELF, rand()%6000); // currently this is set to call print_coordinates()
	#endif
	*/
}

void master_coor_assign()
{	//coordinates for the master block only
	printf_P(PSTR("m_c_a()\n\r"));
	
	my_coord.x_coord = 0;
	my_coord.y_coord = 0;
	my_coord.assigned = true;
}

coordinates_f get_electrode_coordf(uint8_t elec_num)
{
	coordinates_f	ret_coords;		// coordinates we will return
	
	if(!my_coord.assigned)
	{
		printf_P(PSTR("ERROR: center unknown\r\n"));
		ret_coords.x = 0;
		ret_coords.y = 0;
	}

	else
	{
		switch(elec_num)
		{
			case 0:
				//ret_coords.x = my_coord.x_coord;
				//ret_coords.y = my_coord.y_coord - 0.3;
				ret_coords.x = (my_coord.x_coord) * AVG_BLOCK_HALF_WIDTH_SPACE_FILLING;
				ret_coords.y = (my_coord.y_coord - 1.0) * BLOCK_HALF_HEIGHT_SPACE_FILLING;
				break;
			case 1:
				//ret_coords.x = my_coord.x_coord + 0.4;
				//ret_coords.y = my_coord.y_coord - 0.2;
				ret_coords.x = (my_coord.x_coord + 1.0) * AVG_BLOCK_HALF_WIDTH_SPACE_FILLING;
				ret_coords.y = (my_coord.y_coord - 0.5) * BLOCK_HALF_HEIGHT_SPACE_FILLING;
				break;
			case 2:
				//ret_coords.x = my_coord.x_coord + 0.4;
				//ret_coords.y = my_coord.y_coord + 0.2;
				ret_coords.x = (my_coord.x_coord + 1.0) * AVG_BLOCK_HALF_WIDTH_SPACE_FILLING;
				ret_coords.y = (my_coord.y_coord + 0.5) * BLOCK_HALF_HEIGHT_SPACE_FILLING;
				break;
			case 3:
				//ret_coords.x = my_coord.x_coord;
				//ret_coords.y = my_coord.y_coord + 0.3;
				ret_coords.x = (my_coord.x_coord) * AVG_BLOCK_HALF_WIDTH_SPACE_FILLING;
				ret_coords.y = (my_coord.y_coord + 1.0) * BLOCK_HALF_HEIGHT_SPACE_FILLING;
				break;
			case 4:
				//ret_coords.x = my_coord.x_coord - 0.4;
				//ret_coords.y = my_coord.y_coord + 0.2;
				ret_coords.x = (my_coord.x_coord - 1.0) * AVG_BLOCK_HALF_WIDTH_SPACE_FILLING;
				ret_coords.y = (my_coord.y_coord + 0.5) * BLOCK_HALF_HEIGHT_SPACE_FILLING;
				break;
			case 5:
				//ret_coords.x = my_coord.x_coord - 0.4;
				//ret_coords.y = my_coord.y_coord - 0.2;
				ret_coords.x = (my_coord.x_coord - 1.0) * AVG_BLOCK_HALF_WIDTH_SPACE_FILLING;
				ret_coords.y = (my_coord.y_coord - 0.5) * BLOCK_HALF_HEIGHT_SPACE_FILLING;
				break;
			default:
				printf_P(PSTR("ERROR: ECU%u\r\n"), elec_num);
				ret_coords.x = 0;
				ret_coords.y = 0;
				break;
		}
	}
	
	return ret_coords;
}

coordinates_f get_center_coordf(void)
{
	coordinates_f	ret_coords;		// coordinates we will return
	
	if(!my_coord.assigned)
	{
		printf_P(PSTR("ERROR: center unknown\r\n"));
		ret_coords.x = 0;
		ret_coords.y = 0;
	}

	else
	{
		ret_coords.x = my_coord.x_coord * AVG_BLOCK_HALF_WIDTH_SPACE_FILLING;
		ret_coords.y = my_coord.y_coord * BLOCK_HALF_HEIGHT_SPACE_FILLING;	
	}
	
	return ret_coords;
}

coordinates get_self_coord(void)
{
	if(!my_coord.assigned)
		printf_P(PSTR("ERROR: coords unknown\r\n"));
	
	return my_coord;
}

void assign_neighbor_center_coord(uint8_t node_num)
{
	//printf_P(PSTR("assign_neighbor_center_coord()\n\r"));
	printf_P(PSTR("ANCC[%u]\n\r"), node_num);
	
	coordinate_assignment_struct send_coord;

	uint8_t send_bitmask = NODE_ALL_bm;		// all six directions (or should we init to zero?)

	// NOTE: currently, the UNITS being used for measurement are 'block half units'
	// so a block directly underneath is -2 'block half units' different in the Y value
	// a block to the right and down one is:
	//			X -> X + 2
	//			Y -> Y - 1

	if(my_coord.assigned == true)
	{
		send_coord.swarm_id = get_swarm_id();
		switch(node_num)
		{
			case 0:
				send_coord.x_coor = my_coord.x_coord;
				send_coord.y_coor = my_coord.y_coord - 2;
				send_coord.assigned = true;
				send_bitmask = NODE_0_bm;
				break;
		
			case 1:
		
				send_coord.x_coor = my_coord.x_coord + 2;
				send_coord.y_coor = my_coord.y_coord - 1;
				send_coord.assigned = true;
				send_bitmask = NODE_1_bm;
				break;
		
			case 2:
		
				send_coord.x_coor = my_coord.x_coord + 2;
				send_coord.y_coor = my_coord.y_coord + 1;
				send_coord.assigned = true;
				send_bitmask = NODE_2_bm;
				break;

			case 3:
		
				send_coord.x_coor = my_coord.x_coord;
				send_coord.y_coor = my_coord.y_coord + 2;
				send_coord.assigned = true;
				send_bitmask = NODE_3_bm;
				break;
		
			case 4:
		
				send_coord.x_coor = my_coord.x_coord - 2;
				send_coord.y_coor = my_coord.y_coord + 1;
				send_coord.assigned = true;
				send_bitmask = NODE_4_bm;
				break;
		
			case 5:
		
				send_coord.x_coor = my_coord.x_coord - 2;
				send_coord.y_coor = my_coord.y_coord - 1;
				send_coord.assigned = true;
				send_bitmask = NODE_5_bm;
				break;
		}
		
		pack_and_send_directional_message_generic(
									(uint8_t*)&send_coord,
									sizeof(coordinate_assignment_struct),
									MESSAGE_TYPE_COOR_ASSIGN,
									//(uint16_t)send_bitmask);
									send_bitmask);
	}

	else
	{
		printf_P(PSTR("unable to assign\n\r"));
	}
		
}

void assign_coords_all_neighbors(void)
{
	//printf_P(PSTR("assign_coords_all_neighbors()\n\r"));
	
	//if((my_coord.assigned == true)&&(!told_neighbors))
	if(my_coord.assigned == true)
	{		
		printf_P(PSTR("candy!\n\r"));
		
		assign_neighbor_center_coord(0);
		assign_neighbor_center_coord(1);
		assign_neighbor_center_coord(2);
		assign_neighbor_center_coord(3);
		assign_neighbor_center_coord(4);
		assign_neighbor_center_coord(5);
	}

	told_neighbors = true;
	
}

void process_coor_packet(coordinate_assignment_struct* received_coor,uint8_t message_type)
{
	//printf_P(PSTR("process_coor_packet()\n\r"));
	//printf_P(PSTR("coordMAIL: %02X\n\r"), received_coor->swarm_id);
	printf_P(PSTR("c-MAIL fm: %02X\n\r"), received_coor->swarm_id);
	_delay_ms(1);
	
	if(my_coord.assigned == true)
	{
		if((my_coord.x_coord == received_coor->x_coor)&&(my_coord.y_coord == received_coor->y_coor))
		{
			printf_P(PSTR("SAME LAME\n\r"));
			
		}

		else
		{
			printf_P(PSTR("REASSIGNING\n\r"));
			print_coordinates();
			my_coord.x_coord = received_coor->x_coor;
			my_coord.y_coord = received_coor->y_coor;
		
			assign_coords_all_neighbors();
		}	
	}

	else // not yet assigned
	{
		my_coord.x_coord = received_coor->x_coor;
		my_coord.y_coord = received_coor->y_coor;
		my_coord.assigned = true;
		
		assign_coords_all_neighbors();
	}
		
	print_coordinates();
}


void print_coordinates(void)
{
	printf_P(PSTR("my coords:"));

	if(my_coord.assigned)
	{
		printf_P(PSTR("\tx %i, y %i\r\n"), my_coord.x_coord, my_coord.y_coord);
	}
	
	else
	{
		printf_P(PSTR("\tunassigned\r\n"));

		// now would be a good time to phone a friend:

		check_random_neighbor_for_coords();
		//pack_and_send_message("C", MESSAGE_TYPE_QUERY, 1); // [WARNING]: deprecated conversion from string constant to 'char*' [-Wwrite-strings]


		/* REMOVED 10/31/14, as seeing it likely unnecessary to ask multiple times
		push_work_queue('o', WORK_SOURCE_SELF, rand()%10000);	// will check again shortly, this pushes a new call to print_coordinates() onto the stack
																// to run this all over again, to keep seeking.
																// This was the first real use of pushing things on the stack to do later,
																// which is why this code is probably convoluted.  this all needs to be fixed (TODO)
																*/
	}	
}

void check_random_neighbor_for_coords(void)
{
	uint8_t rand_dir;

	if(has_neighbor(0)||has_neighbor(1)||has_neighbor(2)||has_neighbor(3)||has_neighbor(4)||has_neighbor(5))
	{
		do 
		{
			rand_dir = rand()%NUM_NEIGHBORS;
		} while (!has_neighbor(rand_dir));
	
		pack_and_send_directional_message("C", MESSAGE_TYPE_QUERY, node_bitmask(rand_dir));
		
		printf_P(PSTR("checking with %i\r\n"), rand_dir);
	}
	
	else
		printf_P(PSTR("no one to check with\r\n"));
}



