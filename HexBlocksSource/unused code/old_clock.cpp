// clock.c

#include "clock.h"



//#ifdef __cplusplus
//extern Usart usart;	// Anshul test code
//#endif

// NOTE: incompatible data types, '1' is not an int?
int long_month[7] = {'1','3','5','7','8','10','12'};	//months with 31 days
int short_month[4] = {'4','6','9','11'};				//months with 30 days

int y = 0;

int date_time[6] = {1,1,2013,0,0,0}; // default time

CLOCK clock;


void RTC_Init(void)
{
	/* Initialization of entire clock cascade is shown here for detail,
	but is executed elsewhere as part of general boot up sequence.
	Do not un-comment this block, only keep the TCC1 part!

	// Set system clock to 32 MHz
	
	 CCP = CCP_IOREG_gc;
	OSC.CTRL = OSC_RC32MEN_bm;
	while(!(OSC.STATUS & OSC_RC32MRDY_bm));
	CCP = CCP_IOREG_gc;
	CLK.CTRL = 0x01;
	
	// Set internal 32kHz oscillator as clock source for RTC.
	
	CLK.RTCCTRL = CLK_RTCSRC_RCOSC_gc | CLK_RTCEN_bm;
	while (RTC.STATUS & RTC_SYNCBUSY_bm);	// wait for SYNCBUSY to clear
		       	
	// Configure RTC period to 1 ms

	RTC.PER = RTC_CYCLES_MS - 1;
	RTC.CNT = 0;
	RTC.COMP = 0;
	RTC.CTRL = ( RTC.CTRL & ~RTC_PRESCALER_gm ) | RTC_PRESCALER_DIV1_gc;


	// Enable overflow interrupt.
	
	RTC.INTCTRL = ( RTC.INTCTRL &
	~( RTC_COMPINTLVL_gm | RTC_OVFINTLVL_gm ) ) |
	RTC_OVFINTLVL_LO_gc |
	RTC_COMPINTLVL_OFF_gc;

	*/
	 
	 // TCC1 (Timer-Counter C1)	(there is also TCC0 elsewhere, these must be compatible)
	 TCC1.CTRLA = TC_CLKSEL_DIV256_gc;
	 TCC1.CTRLB = 0;
	 TCC1.CTRLC = 0;
	 TCC1.CTRLD = 0;
	 TCC1.CTRLE = 0;
	 TCC1.INTCTRLA = TC_OVFINTLVL_LO_gc;
	 TCC1.INTCTRLB = 0;
	 TCC1.CNT = 0;
	 TCC1.PER = 125;
}
	
void PLL_init(void)
{	// phase lock loop (increases stability of CPU clock)
	OSC.PLLCTRL = OSC_PLLSRC_RC32M_gc | 0x06;
	OSC.CTRL |= OSC_PLLEN_bm;
	while (!(OSC.STATUS & OSC_PLLRDY_bm));
	CCP = CCP_IOREG_gc;
	CLK.CTRL = CLK_SCLKSEL_PLL_gc;
}



void new_year()
{	// if the date is 12/32, then it is a new year
	if(clock.day > 31 && clock.month ==  12)
	{
		clock.day = 1;
		clock.month = 1;
		clock.year++;
	}
}

void leap_year()
{	
	// when is this function called?
	// it is a leap year??, check in February 
	
	uint8_t is_leap_year;
	
	if(clock.year % 400 == 0)
		is_leap_year = 1;
	else if(clock.year % 100 == 0)
		is_leap_year = 0;
	else if (clock.year % 4 == 0)
		is_leap_year = 1;
	else
		is_leap_year = 0;

	if(is_leap_year)
	{
		if(clock.day == 29)
		{
			clock.day = 1;
			clock.month = clock.month + 1;
		}
	}
	else
	{
		if(clock.day == 28)
		{
			clock.day = 1;
			clock.month = clock.month + 1;
		}
	}
}

void init_date_and_time(int month,int day,int year,int hr,int min,int sec, int ms, int gap )
{ // initial parameters
	clock.millisecond = ms;
	clock.offset = gap;
	clock.second = sec;         //0-59
	clock.minute = min;         //0-59
	clock.hour = hr;           //0-23
	clock.day = day;           //1-31
	clock.month = month;          //0-12
	clock.year = year;  //2011-??
	//clock.assign = true;
	clock.assign = 1;			
}






int RTC_getSeconds(void)
{
	return clock.second;
}
int RTC_getMs(void)
{
	return clock.millisecond;
}

int RTC_getMinutes(void)
{
	return clock.minute;
}

int RTC_getHours(void)
{
	return clock.hour;
}

int RTC_getDays(void)
{
	return clock.day;
}

int RTC_getYears(void)
{
	return clock.year;
}
int RTC_getMonth(void)
{
	return clock.month;
}





int change_up(int time)
{	//increment time up 
	time++;
	return time;
}

int change_down(int time)
{	// decrease time
	time--;
	return time;
}

int set_date(int date)
{	//up down ui to change number
	int touchstatus; 
	
	touchstatus = touch_update();	//TODO: do not rely on touch_handler
	if(touchstatus == 2)
	{	// up 
		date = change_up(date);
	}
	if(touchstatus == 32)
	{
		if(date > 0)
		{	// down
			date = change_down(date);	
		}			
	}		
	char time[5];			//print time onto the lcd screen (TODO: this needs to be handled by user-code)
	itoa(date, time, 10);
	//lcd_puts(&time);
	lcd_put_chars(time);
	return date;
}

// this requires both an LCD and working touch panels:
int set_time(int input, int i)
{	// scroll across parameters by touching two top sides
	_delay_ms(10000);
	uint8_t touchstat = touch_update();		//TODO: do not rely on touch_handler
	lcd_clear_screen();
	while( touchstat != 12)
	{
		if(i ==0)
			lcd_put_chars("Month:");
		else if(i ==1)
			lcd_put_chars("Day:");
		else if(i ==2)
			lcd_put_chars("Year:");
		else if(i ==3)
			lcd_put_chars("Hour:");
		else if(i ==4)
			lcd_put_chars("Minute:");
		else if(i ==5)
			lcd_put_chars("Second:");

		input = set_date(input);
		_delay_ms(10000);
		touchstat = touch_update();
		lcd_clear_screen();
		
		return input;
	}
	
	return date_time[i];
}


void reset_clock()
{	//reset the clock with the block
	lcd_clear_screen();
	lcd_put_chars("Clock Reset");
	_delay_ms(20000);			// WARNING!!! (TODO) this blocking delay is 20 seconds long!!!
	
	while (y < 6)
	{
		date_time[y] = set_time(date_time[y],y);
		y++;
	}

	lcd_clear_screen();
	init_date_and_time(date_time[5],date_time[4],date_time[3],date_time[2],date_time[0],date_time[1],0,0);
	_delay_ms(30000);			// WARNING!!! (TODO) this blocking delay is 30 seconds long!!!
}


ISR(TCC1_OVF_vect) // actual clock, increment time and date
{
	clock.millisecond = clock.millisecond + 1;
	if(clock.millisecond % clock.offset == 0)
	{
		if(clock.offset < 0)
		{
			clock.millisecond =  clock.millisecond - 1;
		}
		else
		{
			clock.millisecond = clock.millisecond + 1;
		}
	}
	else if(clock.millisecond == 1000)
	{
		clock.millisecond = 0;
		clock.second = clock.second + 1;
		
		if(clock.second == 60)
		{
			clock.second = 0;
			clock.minute = clock.minute + 1;
			
			if(clock.minute == 60)
			{
				clock.minute = 0;
				clock.hour = clock.hour + 1;
				
				if(clock.hour == 24)
				{
					clock.hour= 0;
					clock.day = clock.day + 1;
					
					for(int z = 0; z < 7; z++)
					{
						if(clock.month == long_month[z] && clock.day == 31)
						{
							clock.month = clock.month + 1;
							clock.day = 1;
							new_year();
						}
					}
					
					for(int a = 0; a < 7; a++)
					{
						if(clock.month == short_month[a] && clock.day == 31)
						{
							clock.month = clock.month + 1;
							clock.day = 1;
						}
					}
					
					if(clock.month == 2)
					{
						leap_year();
					}
				}
			}
		}
	}
}

/* Unused Interrupt Vectors
 
ISR(RTC_COMP_vect)
{
}

ISR(RTC_OVF_vect)
{
}

*/