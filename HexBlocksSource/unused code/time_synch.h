
#include <avr/io.h>			// used for uint8_t
#include <stdio.h>			// used for printf_P
#include <avr/pgmspace.h>	// used for PSTR
#include <util/atomic.h>	



#include <util/delay.h>

#include "..\HexBlocksMain\xgrid.h"
#include "appearance_control.h"			// for set_primary_color()
//#include "mode_control.h"				// for swarm_mode #definitions




// THESE BELONG IN COMMUNICATION HANDLER
//#define NODE_0_bm	0b00000001
//#define NODE_1_bm	0b00000010
//#define NODE_2_bm	0b00000100
//#define NODE_3_bm	0b00001000
//#define NODE_4_bm	0b00010000
//#define NODE_5_bm	0b00100000







void RTC_init_new(void);

uint16_t time_to_send_message(uint8_t message_num_bytes);

void update_time_accurate(uint16_t sent_time, uint8_t message_source);

uint8_t is_clock_synchronized_to_neighbor(void);

void stay_synchronized_check_and_update(void);