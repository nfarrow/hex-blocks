// clock.h

// WARNING!!! This code is INCOMPATIBLE with fan-blocks !!!

// WARNING: Majority of this code COPY-PASTED from internet, was originally coded to use RTC 
//			of XMEGA, then, subsequently ported to use TCC1.  TCC1 is reserved for motor 2A, 
//			motor 2B or fan 2.  TODO, put this code back on RTC

// WARNING: Currently Xgrid is using TCC0, this must be compatible with TCC1

// NOTE: NONE OF THIS CODE IS CURRENTLY USED!! (hence the name old_clock.h and not clock.h)
// This code was began by a prior student to implement a clock on the blocks.
// A clock will be implemented eventually, so this code remains as a stub. It is NOT compiled.

// WARNING: This code contains library links directly to driver code that it should not have access to.
// This includes function access to lcd.h and touch_handler.h.

#ifndef RTC_CLOCK_H
#define RTC_CLOCK_H

#include <avr/io.h>
#include <avr/interrupt.h>		// required to use ISR(vector)
#include <stdlib.h>

#include "lcd.h"				// TODO: move entangled functions to user-code


#ifdef __cplusplus
#include "..\HexBlocksMain\xgrid_usart.h"
#include "touch_handler.h"				// TODO: move entangled functions to user-code
extern "C"
{
#endif
	
//TODO: eliminate COPY-PASTED macros that aren't being used
#define RTC_Busy()               ( RTC.STATUS & RTC_SYNCBUSY_bm )
#define RTC_GetOverflowFlag()    ( RTC.INTFLAGS & RTC_OVFIF_bm )
#define RTC_GetCompareFlag()     ( RTC.INTFLAGS & RTC_COMPIF_bm )
#define RTC_SetCount( _rtccnt )  ( RTC.CNT = (_rtccnt) )
#define RTC_GetCount()           ( RTC.CNT )
#define RTC_SetPeriod( _rtcper ) ( RTC.PER = (_rtcper) )
#define RTC_GetPeriod()          ( RTC.PER )
#define RTC_SetCompareValue( _cmpVal ) ( RTC.COMP = (_cmpVal) )

#define RTC_GetCompareValue()    ( RTC.COMP )

#define RTC_CYCLES_1S     1024
#define RTC_CYCLES_MS     1

//Clock structure
typedef struct
{
	// TODO: these need to be smaller data types
	
	int millisecond;
	int second;         //0-59
	int minute;         //0-59
	int hour;           //0-23
	int day;			//1-31
	int month;          //0-12
	int year;			//2011-??
	int offset;
	
	uint8_t assign;		//new date
} CLOCK;


//static CLOCK clock;

	



	
	
void RTC_Init(void); //Initialize TCC for clock. Prescaler set for millisecond precision
void PLL_init(void); // Initialize PLL for clock stability

// TODO: these need to be return smaller (correct) data types
int RTC_getMinutes(void); // return current minute
int RTC_getSeconds(void); // return current second
int RTC_getHours(void);// return current hour
int RTC_getDays(void);// return current day
int RTC_getYears(void);// return current year
int RTC_getMonth(void);// return current month
int RTC_getMs(void);// return current millisecond
	
// initialize clock with current date and time
void init_date_and_time(int month,int day,int year,int hr,int min,int sec, int ms, int gap );  

//check whether the year is a leap year, also increments date
void leap_year(void); 

//Increment the year when the month is 12 and day 31 ends
void new_year(void);

// for clock setting, increment parameter up
int change_up(int time);

// for clock setting, increment parameter down 
int change_down(int time);

 // UP DOWN user interface for increasing or decreasing parameter
int set_date(int date);

// UI to change parameter ie. month to day
int set_time(int input, int i);

// UI to reset clock 
void reset_clock(void);

#ifdef __cplusplus
}
#endif

#endif


