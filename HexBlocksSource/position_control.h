
#include <avr/io.h>			// used for uint8_t
#include <stdio.h>			// used for printf_P
#include <avr/pgmspace.h>	// used for PSTR

#include <util/delay.h>

#include "..\HexBlocksMain\xgrid.h"
#include "appearance_control.h"			// for set_primary_color()
#include "mode_control.h"				// for swarm_mode #definitions

// node numbers, used in serial communication: 
// these are numbered counterclockwise as you are looking at the front of the block
#define RX_NODE_BOTTOM		0
#define RX_NODE_RIGHT_BOT	1
#define RX_NODE_RIGHT_TOP	2
#define RX_NODE_TOP			3
#define RX_NODE_LEFT_TOP	4
#define RX_NODE_LEFT_BOT	5
#define RX_KEYBOARD			6


#define SWARM_POSITION_UNKNOWN			0
#define SWARM_POSITION_TOP				1	// e.g. has a neighbor below, but no neighbor above
#define SWARM_POSITION_BOTTOM			2	// e.g. has a neighbor above, but no neighbor below
#define SWARM_POSITION_MIDDLE			3	// e.g. has neighbors both above and below
#define SWARM_POSITION_TOP_AND_BOTTOM	4	// e.g. has no neighbors above or below


void print_provisional_postion(void);

void print_known_position(void);

uint8_t is_top_block(void);


// the update_neighbors pair of functions is responsible for checking for the existence of neighbors and updating the block's own
// knowledge about the existence of neighbors. This periodic checking for the existence of neighbors is required if the blocks are
// going to be added or removed while power is supplied to the wall.
uint8_t update_neighbors_part1(uint8_t source_node); // currently called in rx_pkt()

void update_neighbors_part2(); // currently called in TCC0 interrupt (TODO: move elsewhere)

void color_by_position(void);

void print_rx_node_by_name(uint8_t node_num);


// check if a neighbor exists in a specified direction (0-5)
// returns 0 if NO neighbor, returns non-zero otherwise
uint8_t has_neighbor(uint8_t direction_num);