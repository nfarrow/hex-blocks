// time_manager.h
// this file evolved out of time_synch.cpp/h
// use this library instead of the old time_synch


#include <avr/io.h>			// used for uint8_t
#include <stdio.h>			// used for printf_P
#include <avr/pgmspace.h>	// used for PSTR
#include <util/atomic.h>	
#include <util/delay.h>

#include "..\HexBlocksMain\xgrid.h"
#include "appearance_control.h"			// for set_primary_color()
//#include "mode_control.h"				// for swarm_mode #definitions

void deactivate_print_time();

void activate_print_time();



uint16_t get_milliseconds();

uint8_t get_minutes();

void increment_milliseconds();

void increment_minutes();

void print_time();

void RTC_init_new(void);

uint16_t time_to_send_message(uint8_t message_num_bytes);

void update_time_accurate(uint16_t sent_time, uint8_t message_source);

uint8_t is_clock_synchronized_to_neighbor(void);

void stay_synchronized_check_and_update(void);