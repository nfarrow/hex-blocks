#include "appearance_control.h"

extern Xgrid xgrid; // original in main

// primary colors are what the block is displaying most of the time
uint8_t primary_red;
uint8_t primary_green;
uint8_t primary_blue;

// secondary colors are what the block displays under temporary conditions (such as being a menu, or blinking to indicate something)
uint8_t secondary_red;
uint8_t secondary_green;
uint8_t secondary_blue;

// target colors should be used for such things as gradual fade of colors (fade from one color to another)
// or for when blocks try to average their colors with their neighbors (each block has a distinct target color,
// but the actual color displayed will be an average of theirs and their neighbors target colors)
uint8_t target_red;
uint8_t target_green;
uint8_t target_blue;


int scheduled_color_updates = 0; // for debug, counts # of times color update successfully re-scheduled itself

// boolean
uint8_t display_secondary_color = 0;

uint8_t dir_node;

void init_appearance(uint8_t swarm_mode)
{
	//primary_red = 0;
	//primary_green = 0;
	//primary_blue = 0;

	if(swarm_mode == SWARM_MODE_RED)//{primary_red = 255;}
		set_primary_color(255,0,0);

	else if(swarm_mode == SWARM_MODE_WHITE)//{primary_red = 255;primary_green = 255;primary_blue = 255;}
		set_primary_color(255,255,255);

	else if(swarm_mode == SWARM_MODE_DISPLAY_VERSION_NUMBER)
	{
		// reseed random numbers with compile version number
		srand((uint16_t)&__BUILD_NUMBER);	

		primary_red = (uint8_t)(rand()%255);
		primary_green = (uint8_t)(rand()%255);
		primary_blue = (uint8_t)(rand()%255);

		set_primary_color(primary_red, primary_green, primary_blue);
		// reseed random numbers with swarm_id (this is the original seed)
		srand(get_swarm_id());	// swarm_id borrowed from main.cpp
	}

	else
		set_primary_color(0,0,0);
}

void set_primary_color(uint8_t red, uint8_t green, uint8_t blue)
{
	primary_red = red;
	primary_green = green;
	primary_blue = blue;
	
	set_red_led(primary_red);
	set_green_led(primary_green);
	set_blue_led(primary_blue);

	printf_P(PSTR("NPC[%i,%i,%i]\r\n"),red, green, blue);	// NPC = new primary color
}

void set_secondary_color(uint8_t red, uint8_t green, uint8_t blue)
{
	secondary_red = red;
	secondary_green = green;
	secondary_blue = blue;
	
	if(display_secondary_color)
	{
		set_red_led(secondary_red);
		set_green_led(secondary_green);
		set_blue_led(secondary_blue);
	}

	printf_P(PSTR("NSC[%i,%i,%i]\r\n"),red, green, blue);	// NSC = new secondary color
}


void print_color_status(void)
{
	printf_P(PSTR("my_color: [%u, %u, %u]\n\r"), primary_red, primary_green, primary_blue);
	printf_P(PSTR("internal: [%u, %u, %u]\n\r"), get_red_led(), get_green_led(), get_blue_led());
}

void color_update()
{
	
	
	if(primary_red != target_red)
	{
		if(primary_red < target_red)
			primary_red++;
		else
			primary_red--;
			
		set_red_led(primary_red);
	}
	
	else if(primary_green != target_green)
	{
		if(primary_green < target_green)
			primary_green++;
		else
			primary_green--;
		
		set_green_led(primary_green);
	}
	
	else if(primary_blue != target_blue)
	{
		if(primary_blue < target_blue)
			primary_blue++;
		else
			primary_blue--;
		
		set_blue_led(primary_blue);
	}
	
	else
	{	// time to pick a new color!
		int rand1 = rand(); // rand 1 chooses up/down
		int rand2 = rand();	// rand 2 chooses the color
		
		#define RAND_COLOR_DIFF 100
		
		uint8_t color_diff = RAND_COLOR_DIFF;
		if(rand2%3 == 0)	// BLUE
		{
			if((rand1%2==0)&&(primary_blue > color_diff)) // down and dont go negative
				target_blue-=color_diff;
			else if(primary_blue < 255-color_diff) // else, can we go up?
				target_blue+=color_diff;
		}
		
		else if(rand2%3 == 1) // GREEN
		{
			if((rand1%2==0)&&(primary_green > color_diff))
				target_green-=color_diff;
			else if(primary_green < 255-color_diff)
				target_green+=color_diff;
		}
		
		else if(rand2%3 == 2) // RED
		{
			if((rand1%2==0)&&(primary_red > color_diff))
				target_red-=color_diff;
			else if(primary_red < 255-color_diff)
				target_red+=color_diff;
		}	
	}
}

void color_update_scheduled()
{
	scheduled_color_updates++;
	
	color_update();
	
	if(get_swarm_mode() == SWARM_MODE_RANDOM)
		// this is the recursive part of the random colors, it schedules a color change here
		//push_work_queue('$', WORK_SOURCE_SELF, 20);
		push_function_none(color_update_scheduled, '$', WORK_SOURCE_SELF, 20);
}

void enter_random_color_mode()
{
	// reset the color randomizers
	target_red = 0;
	target_green = 0;
	target_blue = 0;	
	
	color_update_scheduled(); // get the cycle going
	
	// ? do we need to set the swarm_mode flag to SWARM_MODE_RANDOM ?
}

void get_with_the_program()
{
	// TODO implement this
	// THE IDEA: when called you check with your neighbors to see what they are doing, and then you behave the same
	// this is necessary so that blocks behave correctly when added to an already established structure
	// or when code is propagating through a structure that is doing a group task
}

void go_to_second_color_mode(uint8_t dir_node_for_now)
{
	display_secondary_color = 1;
	dir_node = dir_node_for_now;
	
	switch(dir_node_for_now)
	{
		case 0:
			set_secondary_color(255,0,50);
			break;
		case 1:
			set_secondary_color(0,255,50);
			break;
		case 2:
			set_secondary_color(50,0,255);
			break;
		case 3:
			set_secondary_color(50,255,0);
			break;
		case 4:
			set_secondary_color(255,50,0);
			break;
		case 5:
			set_secondary_color(0,50,255);
			break;
		case 6:	// me, im in the center!
			set_secondary_color(10,10,10);
			break;
	}
}

void quit_second_color_mode(void)
{
	display_secondary_color = 0;
	
	set_red_led(primary_red);
	set_green_led(primary_green);
	set_blue_led(primary_blue);
}

uint8_t get_dir_node()
{
	return dir_node;
}




/* THE CODE BELOW WAS USED FOR THE "COLOR-DROPS MODE"
this code was taken from the TCC0 interrupt in MAK
(TODO) re-implement this functionality here using some method that makes sense

	if (milliseconds % 250 == 0)
	{
		if((swarm_mode == SWARM_MODE_COLOR_DROPS)&&is_top_block()&&(turns_since_dropped > 10))
		{
					
			if((my_red == 0)&&(my_green == 0)&&(my_blue == 0))
			{
				if(rand()%18 == 0)
					my_blue = 255;
				if(rand()%18 == 0)
					my_green = 255;
				if(rand()%18 == 0)
					my_red = 255;
						
				else
				{
					fprintf_P(&usart_stream, PSTR("send color down: %i,%i,%i\n\r"), my_red, my_green, my_blue);
					color_msg_data.red = my_red;
					color_msg_data.green = my_green;
					color_msg_data.blue = my_blue;
						
					Xgrid::Packet pkt;
					pkt.type = MESSAGE_TYPE_COLOR;
					pkt.flags = 0;
					pkt.radius = 1;
					pkt.data = (uint8_t *)&color_msg_data;
					pkt.data_len = sizeof(color_msg_data);
						
					xgrid.send_packet(&pkt, 0b00000001);
				}
				
			}
				
			else
			{
				fprintf_P(&usart_stream, PSTR("send color down: %i,%i,%i\n\r"), my_red, my_green, my_blue);
				color_msg_data.red = my_red;
				color_msg_data.green = my_green;
				color_msg_data.blue = my_blue;
				
				Xgrid::Packet pkt;
				pkt.type = MESSAGE_TYPE_COLOR;
				pkt.flags = 0;
				pkt.radius = 1;
				pkt.data = (uint8_t *)&color_msg_data;
				pkt.data_len = sizeof(color_msg_data);
				
				xgrid.send_packet(&pkt, 0b00000001);

				if(my_red > 99)
					my_red-=100;
				else
					my_red = 0;
						
				if(my_green > 99)
					my_green-=100;
				else
					my_green = 0;

				if(my_blue > 99)
					my_blue-=100;
				else
					my_blue = 0;
			}

			set_rgb(my_red, my_green, my_blue);			
		}			
	}




*/