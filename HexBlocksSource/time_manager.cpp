// time_manager.cpp
// this file evolved out of time_synch.cpp/h
// use this library instead of the old time_synch

#include "time_manager.h"

extern Xgrid xgrid;

uint16_t milliseconds = 0;
uint8_t minutes = 0;


uint8_t synchronization_began = 0;

uint16_t ms_since_last_correction = 0;

uint16_t time_correction_period = 0;
int8_t time_correction_direction = 0;

uint16_t rtc_seconds = 0;

uint8_t synch_status = 0;	// (0 = not began, 1 = first attempts, 2 = confirmed synched)


uint8_t time_source = NODE_2_bm | NODE_4_bm;	// temp for testing, this should be dynamic
uint8_t time_sink = NODE_1_bm | NODE_5_bm;		// temp for testing, this should be dynamic!!

uint16_t last_time_synch_time;

uint8_t allow_print_time = 1;	// boolean

void deactivate_print_time()
{
	allow_print_time = 0;
}

void activate_print_time()
{
	allow_print_time = 1;
}


uint16_t get_milliseconds()
{
	return milliseconds;
}

uint8_t get_minutes()
{
	return minutes;
}

void increment_milliseconds()
{
	milliseconds++;

	if(milliseconds == 60000)
	{
		milliseconds = 0;
		increment_minutes();
	}

	if(milliseconds%1000 == 0)
		print_time();
}

void increment_minutes()
{
	minutes++;
	// rollover not implemented yet (no hours)
}

void print_time()
{
	uint8_t seconds = milliseconds/1000;
	
	if(allow_print_time)
		printf_P(PSTR("%u:%02u\r\n"),minutes,seconds);
}

void RTC_init_new()
{
	CLK.RTCCTRL = CLK_RTCSRC_RCOSC_gc | CLK_RTCEN_bm;	// ref: doc8331.pdf, pg 92

	//CLK_RTCSRC_RCOSC_gc = (0x02<<1),  /* 1.024 kHz from internal 32.768 kHz RC oscillator */
	//#define CLK_RTCEN_bm  0x01  /* Clock Source Enable bit mask. */

	RTC.INTCTRL = RTC_OVFINTLVL_MED_gc;              // med level overflow interrupt to increment the epoch counter
	while (RTC.STATUS & RTC_SYNCBUSY_bm);
	RTC.PER = 0xFFFF;
	while (RTC.STATUS & RTC_SYNCBUSY_bm);
	RTC.CTRL = RTC_PRESCALER_DIV1_gc;
	while (RTC.STATUS & RTC_SYNCBUSY_bm);
	RTC.CNT = 0;
}


uint16_t time_to_send_message(uint8_t message_num_bytes)
{
	// TODO: this was measured experimentally (by who?, with what block(s))
	// TODO: no magic numbers
	return ((0.0526 * message_num_bytes) + 1.55);	// milliseconds
}

// this shouldn't be called anywhere currently (TODO)
void update_time_accurate(uint16_t sent_time, uint8_t message_source)
{
	// 1) record the time this message arrived according to our local clock
	// 2) verify that this message is from our time source
	// 3) if this is our FIRST received time-message
	//		-set our clock to match the message (accounting for transmit delay)
	//		-ignore what was recorded in (1)
	// 4) if this is not our FIRST received time-message
	//		-check how well our clock matches the message (accounting for transmit delay)
	//		-compute the error
	//		-adjust our clock rate accordingly
	//		-set our clock to match the message (accounting for transmit delay)
	// REPEAT as needed, the error should drop quickly with only few iterations

	uint16_t curr_time = milliseconds;

	uint16_t corrected_time;

	int16_t time_since_last_synch_event_us;
	int16_t time_since_last_synch_event_them;

	int16_t time_error;

	//float clock_drift_per_ms;

	int16_t clock_error_period;			// how many ms elapse before clock gets ahead by 1 ms 

	//if(message_source & time_source)	// want this!
	if((message_source == 2)||(message_source == 4))	// but have this!
	{
		// this time message IS from our source
		corrected_time = sent_time + time_to_send_message(2);	// TODO: 2 = sizeof(uint16_t), later convert to generic time datatype

		if(synchronization_began)
		{
			time_error = curr_time - corrected_time;

			// verify ( last_time_synch_time < curr_time )
			time_since_last_synch_event_us = curr_time - last_time_synch_time;

			time_since_last_synch_event_them = corrected_time - last_time_synch_time;

			//clock_drift_per_ms = time_error/time_since_last_synch_event_us;

			clock_error_period = time_since_last_synch_event_us/time_error;

			milliseconds = corrected_time; // this might trip the j-catch timeout

			//printf_P(PSTR("TIME packet received (OK)\n\r"));
			//_delay_ms(1);
			//printf_P(PSTR("us: %u them: %u\n\r"), curr_time, corrected_time);
			//_delay_ms(1);
			printf_P(PSTR("TSLSEu: %i\n\r"), time_since_last_synch_event_us);
			_delay_ms(1);
			printf_P(PSTR("TSLSEt: %i\n\r"), time_since_last_synch_event_them);
			_delay_ms(1);
			printf_P(PSTR("error: %i\n\r"), time_error);
			_delay_ms(1);
			printf_P(PSTR("period err: %i\n\r"), clock_error_period); // phase shift??

			
			if(synch_status != 1)
			{
				if(clock_error_period < 0)
				{
					time_correction_period = -clock_error_period;
					time_correction_direction = -1;
				}

				else
				{
					time_correction_period = clock_error_period;
					time_correction_direction = 1;
				}

				synch_status = 1;

				printf_P(PSTR("*NSYNC\n\r"));
			}

			else
			{
				printf_P(PSTR("old news\n\r"));
			}
		}

		else
		{
			synchronization_began = 1;	// 

			milliseconds = corrected_time; // this might trip the j-catch timeout

			printf_P(PSTR("TIME packet received (1st)\n\r"));
			printf_P(PSTR("us: %u them: %u\n\r"), curr_time, corrected_time);
		}

		last_time_synch_time = corrected_time;
	}

	else
	{
		// this time message is NOT from our source
		printf_P(PSTR("TIME packet received (NOT OK)\n\r"));
		printf_P(PSTR("source: %x allowed: %x\n\r"), message_source, time_source);
		printf_P(PSTR("source: %i\n\r"), message_source);
	}
}

uint8_t is_clock_synchronized_to_neighbor()
{
	if(synch_status > 0)
		return 1;
	else
		return 0;	
}


void stay_synchronized_check_and_update()
{
	ms_since_last_correction++;

	if(ms_since_last_correction > time_correction_period)
	{
		milliseconds += time_correction_direction;
		ms_since_last_correction = 0;
	}
}


ISR( RTC_OVF_vect )
{
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) // Disable interrupts
	{
		rtc_seconds++; // don't know why this is seconds, as this called once a minute
		// TODO: find out what settings are, such that this is called once a minute
	}

	if(allow_print_time)
	{
		// this goes off once a minute
		printf_P(PSTR("tock %i\n\r"),rtc_seconds);
	}
}

