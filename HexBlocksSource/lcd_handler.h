
#include <avr/io.h>			// used for uint8_t
#include <stdio.h>			// used for printf_P
#include <avr/pgmspace.h>	// used for PSTR
#include <stdlib.h>			// provides itoa

#include "..\HexBlocksMain\xgrid.h"
#include "..\HexBlocksDrivers\lcd.h"
#include "time_manager.h"

void handle_lcd(void);