﻿#include "snake_program.h"

#include <stdio.h> 
#include <stdlib.h>

#define SNAKE_DEFAULT_RED 255
#define SNAKE_DEFAULT_GREEN 255
#define SNAKE_DEFAULT_BLUE 0

#define SNAKE_LENGTH 3

#define NOMOVE_ERROR 255
#define NO_SUCCESSOR 254

#define SNAKE_NEIGHBOR_UNKNOWN 0
#define SNAKE_NEIGHBOR_LEGAL_MOVE 1
#define SNAKE_NEIGHBOR_BUSY 2

/* RGB values for what color the snake will be */
uint8_t snake_red = SNAKE_DEFAULT_RED;
uint8_t snake_green = SNAKE_DEFAULT_GREEN;
uint8_t snake_blue = SNAKE_DEFAULT_BLUE;




uint8_t is_body;
uint8_t is_head;

uint8_t successor = NO_SUCCESSOR;
uint8_t curr_length;

uint8_t snake_neighbor_status[6] = {0,0,0,0,0,0}; // 0 = SNAKE_NEIGHBOR_UNKNOWN


/* use our array snake_neighbor_status, to be updated each time the head wants to move.


Returns who is a body, who is a block, and who isn't there
   also use "checked[]" to see if our random move has checked that block for efficiency.  If this sums to 6 at the end of the loop 
   and we still don't have a legal move, that means there are no legal moves.
   called in move()
 */
uint8_t check_legal_moves()
{
	uint8_t move_not_found = true; 
	uint8_t randneighbor;
	uint8_t checked[6] = {0,0,0,0,0,0};
	uint8_t checkCount;
	uint8_t i;
	
	while (move_not_found)
	{
		checkCount = 0; 
		randneighbor = rand() % 6;

		
		if (snake_neighbor_status[randneighbor] == SNAKE_NEIGHBOR_LEGAL_MOVE)
		{
			move_not_found = false;
			printf_P(PSTR("Move found! Moving to neighbor %d\r\n"), randneighbor);
			_delay_ms(3);
			return randneighbor;
		}
		else if (snake_neighbor_status[randneighbor] == 0 && checked[randneighbor] == 0)
		{
			checked[randneighbor] = 1; // 1 = true
		}
		for (i=0;i <6; i++)
		{
			checkCount += checked[i]; 
		}
		if (checkCount == 6){break;}
		
	}
	printf(PSTR("Error code %d: No legal moves for snake\n"), NOMOVE_ERROR);
	return NOMOVE_ERROR;
}


/* used in HEXBLOCKS main in queries: 'S'.
called in move(). 
Send a snake query to find out who is a nearby snake and determine legal move.
This is turn creates a response that calls snake_update_neighbor to determine legal moves */
void snake_query_neighbor()
{	
	for(uint8_t i = 0; i < 6; i++)
		snake_neighbor_status[i] = SNAKE_NEIGHBOR_UNKNOWN;
	
	pack_and_send_message("S",MESSAGE_TYPE_QUERY,1); 
}





/*
	Determines where the snake goes
	snake move does:
		i) tell head where to move next
		ii) If we have a successor (every move except for the first) tell it to decrement length by 1

*/
void snake_move(void)
{
	curr_length--;
	
	printf_P(PSTR("in MOVE with (%u) \r\n"), curr_length);
	_delay_ms(3);

	
	uint8_t nextmove;
	nextmove = check_legal_moves();
		
	// tell next block to become snake head
	// don't do anything until we find a legal move		
	if (nextmove != NOMOVE_ERROR)
	{
		
			//if we have a successor, tell him where to go
			if (successor != NO_SUCCESSOR) 
			{
				printf_P(PSTR("sending to successor block: %u\r\n"), successor);
				pack_and_push_function_none(snake_body, node_bitmask(successor));
			}				
			
			
			
			
			_delay_ms(10); //give time for successor to be communicated to body blocks (Necessary? Not sure)
			pack_and_push_function_none(snake_head,  node_bitmask(nextmove));

	
			is_head = false;
			is_body = true;
			
			set_primary_color(snake_red, snake_green, snake_blue);
			return;
			
	}
	else if (nextmove == NOMOVE_ERROR)
	{
		printf_P(PSTR("Error: no legal moves\r\n"));
			
		kill_snake();
		return;

	}
	
	else
	{
		printf_P(PSTR("crazy next_move %u\r\n"), nextmove);
		// you shouldn't be able to get here
		halt_program(0, 255, 0);
		return;
	}
}



/* set snake color; not sure where to use this yet.  Otherwise we are DEFAULTRED, etc */
void set_color(uint8_t newred, uint8_t newgreen, uint8_t newblue)
{
	snake_red = newred;
	snake_green = newgreen;
	snake_blue = newblue;
}

/*  This is called when we invoke snake from menu option, or every time the head picks a legal move for body to follow.
	If from menu, assume no successor (haven't moved anywhere yet).  
	In both cases, set curr_length to global SNAKELENGTH, so we know what to send to subsequent portions of the body (or if snake died
	here earlier we need to avoid int underflow).
	calls move, which determines next legal move for head (if  the move exists), calls body on successor (if he exists), or kills snake if nothing legal
*/
void snake_head(void)
{
	//init
	curr_length = SNAKE_LENGTH;
	is_head = true;
	set_primary_color(snake_red,snake_green,snake_blue);
	
	//populate legal moves
	snake_query_neighbor();
	
	push_function_none(snake_move, '?', WORK_SOURCE_SELF, PROP_DELAY_ms);
	
}

/* 
   governs body blocks of snake
   called in move each time decrementing length by 1 until we reach 0, then call cleanup function
   Error catch included for int underflow on uint8_t
*/
void snake_body()
{
	printf_P(PSTR("in BODY(%u):"), curr_length);
	//set global for this block so next block knows what is going on
	curr_length--;
	
	//error catch for int underflow
	if (curr_length == 255)
	{
		// you shouldn't be able to get here
		printf(PSTR("Error: Integer underflow on uint8_t when decrementing snake length\n"));
		//set_primary_color(0,0,255);
		//halt_program(0,255,255);
		snake_tail();
	}	
	
	//if len == 0, we are at the tail; cleanup
	if (curr_length == 0)
	{
		snake_tail();
	}
	// else send to your successor
	else
	{
		//printf_P(PSTR("in BODY(): sending to succesor block: %d\r\n"), successor);
		printf_P(PSTR("sending to successor block: %u\r\n"), successor);
		pack_and_push_function_none(snake_body, node_bitmask(successor));
	}
	
}

/* 
	Cleanup function
	reset color of tail from snake to blue to match background
	make sure everyone knows they are dead (is_body=false) so we don't interfere with next snake
	reset len to default length so we don't underflow next time around
*/
void snake_tail(void)
{
	printf_P(PSTR("cleaning up...\r\n"));
	
	successor = NO_SUCCESSOR;
	is_body = false;
	curr_length = SNAKE_LENGTH;
	set_primary_color(0,0,255);

}

/* used in HEXBLOCKSmain; we receive information from the blocks around us after a 'S' query and define legal moves by 
   both who is the body and of course if there is no block there */
void snake_update_neighbors(uint8_t source, uint8_t status)
{
	printf_P(PSTR("updating neighbor[%d] with status %d\r\n"), source, status);
	_delay_ms(3);
	snake_neighbor_status[source] = status;
}

void set_successor(uint8_t new_successor)
{
	successor = new_successor;
}


/* Useful in determining where we are in the body.
Called in work stack ('t') to determine if we are the tail, or
  to call snake_body with length - 1*/
uint8_t get_snake_length()
{
	return curr_length;
}


/* Used to determine legal moves; if someone next to us is body, we can't go there */
uint8_t get_body_status()
{
	return is_body;
}

/* Determines who is the head to send information back to the body. */
uint8_t get_head_status()
{
	return is_head;
}

void kill_snake()
{
	if(successor != NO_SUCCESSOR)
		pack_and_push_function_none(kill_snake, node_bitmask(successor));
	//cleanup
	snake_tail();
}


uint8_t get_successor()	// only used for debug print statements
{
	return successor;
}

