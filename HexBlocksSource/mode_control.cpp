

#include "mode_control.h"



extern Xgrid xgrid;


extern uint8_t message_distance; // currently in main

uint8_t swarm_mode;
uint8_t menu_mode = 0;	// TODO: is this 'concept' of a menu flag implemented (in duplicate) elsewhere?


void change_swarm_mode_pointable(void* arg)
{
	//uint8_t mode = (uint8_t)arg;		// does not compile, but this is how droplet code is written??
	
	//uint8_t mode = *((uint8_t*)arg);	// compiles, DOES NOT WORK
	
	//uint8_t *mode = (uint8_t *)arg;		// does not compile
	
	uint8_t mode = *((uint8_t*)(&arg));	// works
	
	change_swarm_mode(mode);
}


//void change_swarm_mode(uint8_t new_mode, uint8_t forward_mode_info)
void change_swarm_mode(uint8_t new_mode)
{
	swarm_mode = new_mode;

	/* IGNORE THIS FOR NOW
	if(forward_mode_info)
	{
		pack_and_send_message("D", MESSAGE_TYPE_COMMAND, message_distance);	// original (want to keep)
		//pack_and_send_message("D", 1, 0);
	
		//swarm_mode = SWARM_MODE_COLOR_DROPS;
		printf_P(PSTR("send cmd COLOR DROPS MODE\n\r"));
	}
	*/
	
	switch(new_mode)
	{
		case SWARM_MODE_BLUE:
			set_primary_color(0, 0, 255);
			break;
		
		case SWARM_MODE_ORANGE:
			set_primary_color(255, 255, 55);
			break;

		case SWARM_MODE_GREEN:
			set_primary_color(0, 255, 0);
			break;
	
		case SWARM_MODE_PURPLE:
			set_primary_color(255, 0, 255);
			break;
		
		case SWARM_MODE_YELLOW:
			set_primary_color(255, 255, 0);
			break;
			
		case SWARM_MODE_WHITE:
			set_primary_color(255, 255, 255);
			break;
			
		case SWARM_MODE_RED:
			set_primary_color(255, 0, 0);
			break;
			
		case SWARM_MODE_RANDOM:
			color_update_scheduled();	// this begins the cycle?
			break;
			
		default:
			printf_P(PSTR("\tWARNING unhandled new mode: %i\r\n"), swarm_mode);
			_delay_ms(1);
			break;
	}
}

uint8_t get_swarm_mode()
{
	return swarm_mode;
}

void print_current_mode(void)
{
	//fprintf_P(&usart_stream, PSTR("SWARM MODE: %i\r\n"), swarm_mode);
	printf_P(PSTR("SWARM MODE: %i\r\n"), swarm_mode);
	/*	
	fprintf_P(&usart_stream, PSTR("0=RAND\r\n"));
	fprintf_P(&usart_stream, PSTR("1=WHITE\r\n"));
	_delay_ms(1);
	fprintf_P(&usart_stream, PSTR("2=UPDOWN\r\n"));
	fprintf_P(&usart_stream, PSTR("3=SIDE1\r\n"));
	fprintf_P(&usart_stream, PSTR("4=SIDE2\r\n"));
	fprintf_P(&usart_stream, PSTR("5=COL DROP\r\n"));	
	*/
}

void go_to_menu_system(uint8_t lead_node_dir)
{
	// lead_node_dir is the node direction of the leader [0-5] (if 6, you are the leader!)
	
	menu_mode = 1;
	go_to_second_color_mode(lead_node_dir);
}

void quit_menu_system()
{
	menu_mode = 0;
	quit_second_color_mode();
}

uint8_t is_in_menu_mode()
{
	return menu_mode;
}