#include "serial_handler.h"


extern int scheduled_color_updates; // maintained in appearance_control.cpp
									// for debug, counts # of times color update successfully re-scheduled itself


void serial_char_process(uint8_t input_char)
{
	if (input_char == 0x1b)
		xboot_reset();
	
	else if(input_char == 't')	// test to add multiple items (work stack stress test)
	{	
		//push_work_queue('c', RX_KEYBOARD, 10006);
		push_function_none(print_message_count, 'c', RX_KEYBOARD, 10006);
		push_function_none(print_proximity_values, 'c', RX_KEYBOARD, 10006);
		push_function_none(print_color_status, 'c', RX_KEYBOARD, 10006);

		//push_work_queue('I', RX_KEYBOARD, 906);
		push_function_none(check_all_of_I2C,'I', RX_KEYBOARD, 906);

		//push_work_queue('x', RX_KEYBOARD, 5666);
		push_function_none(lcd_getxy, 'x', RX_KEYBOARD, 5666);
		push_function_none(lcd_print_contents, 'x', RX_KEYBOARD, 5666);

		//push_work_queue('i', RX_KEYBOARD, 888);
		push_function_none(print_swarm_id,'I', RX_KEYBOARD, 888);		
	}
	
	else if(input_char == 'q')	// don't push this on the que
	{
		print_work_que();
		printf_P(PSTR("numup %i\r\n"), scheduled_color_updates);
	}
	
	else
	{
		command_decoder(input_char, WORK_SOURCE_KEYBOARD);
		//push_work_queue(input_char, RX_KEYBOARD, 0);

	}

}