#include <avr/io.h>			// used for uint8_t
#include <stdio.h>			// used for printf_P
#include <avr/pgmspace.h>	// used for PSTR
#include "work_stack.h"

void serial_char_process(uint8_t input_char);