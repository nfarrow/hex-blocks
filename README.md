# Notes on general program flow
The entire project is contained here. 
All the files that are in the project are compiled for every block that runs this code. 
Therefore, all blocks share the same code. 
Identical code on each block (despite there being different hardware on some blocks) is necessary to ensure that viral programming works as intended.
The blocks should be able to detect the presence/absence of hardware components (when possible) to avoid calling code for components that do not exist.
 
# The three layers of abstraction: Main, Drivers, and Source. 

The main file contains the kernel of the operating system: HEXBLOCKSmain.c.
The majority of files in the main file are the communication subsystem known a xgrid.
Xgrid controls all of the serial communication (USARTS) for both block-to-block data packets and block-to-computer ASCII character stream.
The code in main() is called after the bootloader (xboot) hands over control to the application section of the memory (instruction register 0x00000 to 0x10000).
Main() first calls all of the initialization routines which enable use of all of the hardware components.
After initialization is done once, the code enters and infinite loop of checking inputs, processing data, and executing outputs (generally in that order).
This loop is required to complete within a certain amount of time or else fail and halt the program. 

Drivers form the lowest abstraction layer for the hardware peripherals,
Drivers are used to encapsulate direct access to pin registers, I/O ports, etc.. 
Each component on the PCB will have its own dedicated driver library. 
For example, the code which toggles the LED colors is in the drivers file (RGB_LED.h/c).

The "source libraries" form the user code layer of abstraction.
These files form the basis of the majority of the functionality of the blocks, and also include the work stack, currently responsible for maintaining the task-stack.
Each task (or job) that the block can do is associated with a character (e.g. 'c').
Other more complex block behaviors are also coded into the source files.
For example, the code which keeps track of what color the block should display is in the source file (appearance_contol.h/cpp).


# Conventions used in this project:
- Define new data types at the TOP of your header file
- Keep ISR routines at the BOTTOM of the source file
- Use #defines to use numerical constants (no magic numbers)
- Never use numerical constants in the code without comment description
- Verify that any new code propagates virally before pushing to the repository

# Data Types:
- Use the smallest data type that you need, RAM is limited to 8 kB!
- uint8_t = 1 byte
- int8_t = 1 byte
- int16_t = 2 bytes
- int = 4 bytes
- (do NOT use an int when all you need is an uint8_t)

# Global Variables:
- Global variables are used as private variables within source files
- If global variables are used, limit their scope to only the source file they are defined in (keep them private)
- Define 'private' global variables TOP of your source file
- To access global variables across source files, pass them as a return value from a public function (do not use extern keyword)

# Init Routines:
- Init routines are generally only called when hardware pins or functions need to be allocated
for the program
- There is a master init() in HEXBLOCKSmain, which calls all other inits.
- Define program inits as foo_init().  Only define an init if you need one, most programs will not need an init routine.
- Some inits may take block ID as input which will only initialize the function on the block with matching block ID.